package com.asianmedia.easterneye.adapter.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0014\u0010\u0013\u001a\u00020\f2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter$ViewHolder;", "()V", "context", "Landroid/content/Context;", "list", "", "Lcom/asiantimes/subscriptionmodel/SubscriptionDescription;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class DescriptionAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter.ViewHolder> {
    private android.content.Context context;
    private java.util.List<com.asiantimes.subscriptionmodel.SubscriptionDescription> list;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.asiantimes.subscriptionmodel.SubscriptionDescription> list) {
    }
    
    public DescriptionAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "textViewDescription", "Landroid/widget/TextView;", "getTextViewDescription", "()Landroid/widget/TextView;", "viewVerticalBottom", "getViewVerticalBottom", "()Landroid/view/View;", "viewVerticalTop", "getViewVerticalTop", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView textViewDescription = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View viewVerticalTop = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View viewVerticalBottom = null;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTextViewDescription() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getViewVerticalTop() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getViewVerticalBottom() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}