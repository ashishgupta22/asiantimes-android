package com.asianmedia.easterneye.adapter.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0017B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\rH\u0017J\u0018\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016J\"\u0010\u0016\u001a\u00020\u000f2\u001a\u0010\b\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\n0\tj\n\u0012\u0006\u0012\u0004\u0018\u00010\n`\u000bR\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\b\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\n0\tj\n\u0012\u0006\u0012\u0004\u0018\u00010\n`\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter$ViewHolder;", "onClickPositionListener", "Lcom/asiantimes/interfaces/OnClickPositionListener;", "(Lcom/asiantimes/interfaces/OnClickPositionListener;)V", "context", "Landroid/content/Context;", "subscriptionPlanData", "Ljava/util/ArrayList;", "Lcom/asiantimes/subscriptionmodel/SubscriptionPlanData;", "Lkotlin/collections/ArrayList;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class SubscriptionListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter.ViewHolder> {
    private android.content.Context context;
    private java.util.ArrayList<com.asiantimes.subscriptionmodel.SubscriptionPlanData> subscriptionPlanData;
    private final com.asiantimes.interfaces.OnClickPositionListener onClickPositionListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.subscriptionmodel.SubscriptionPlanData> subscriptionPlanData) {
    }
    
    public SubscriptionListAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickPositionListener onClickPositionListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "textViewSubscriptionPlan", "Landroid/widget/TextView;", "getTextViewSubscriptionPlan", "()Landroid/widget/TextView;", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView textViewSubscriptionPlan = null;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTextViewSubscriptionPlan() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}