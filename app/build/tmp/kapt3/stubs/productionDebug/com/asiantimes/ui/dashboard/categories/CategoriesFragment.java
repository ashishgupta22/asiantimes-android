package com.asiantimes.ui.dashboard.categories;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002J\u0012\u0010\u0017\u001a\u00020\u00142\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J&\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u001a\u0010!\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u0010\"\u001a\u00020\u0014H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006#"}, d2 = {"Lcom/asiantimes/ui/dashboard/categories/CategoriesFragment;", "Lcom/asiantimes/base/BaseFragment;", "Landroid/view/View$OnClickListener;", "()V", "bindingView", "Lcom/asiantimes/databinding/FragmentCategoriesBinding;", "catList", "", "Lcom/asiantimes/model/GetAllCategoriesModel$Post;", "categoriesAdapter", "Lcom/asiantimes/ui/dashboard/categories/CategoryAdapterNew;", "filteredList", "", "viewModel", "Lcom/asiantimes/ui/splash/SplashViewModel;", "getViewModel", "()Lcom/asiantimes/ui/splash/SplashViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getAllCategoriesAPI", "", "getAllCategoriesObserver", "getCateList", "onClick", "view", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "setAdapter", "app_productionDebug"})
public final class CategoriesFragment extends com.asiantimes.base.BaseFragment implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.FragmentCategoriesBinding bindingView;
    private com.asiantimes.ui.dashboard.categories.CategoryAdapterNew categoriesAdapter;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post> catList;
    private final java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post> filteredList = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.splash.SplashViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void setAdapter() {
    }
    
    private final void getAllCategoriesAPI() {
    }
    
    private final void getAllCategoriesObserver() {
    }
    
    private final java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post> getCateList() {
        return null;
    }
    
    public CategoriesFragment() {
        super();
    }
}