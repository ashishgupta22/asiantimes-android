package com.asiantimes.ui.dashboard.video;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0014\u0010\u0013\u001a\u00020\f2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/TrendingVideoAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/dashboard/video/TrendingVideoAdapter$ViewHolder;", "OnClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "(Lcom/asiantimes/interfaces/OnClickPositionViewListener;)V", "trendingVideoList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetVideoListModel$TrandListBean;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class TrendingVideoAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.dashboard.video.TrendingVideoAdapter.ViewHolder> {
    private java.util.ArrayList<com.asiantimes.model.GetVideoListModel.TrandListBean> trendingVideoList;
    private final com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.ui.dashboard.video.TrendingVideoAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.dashboard.video.TrendingVideoAdapter.ViewHolder holder, int position) {
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetVideoListModel.TrandListBean> trendingVideoList) {
    }
    
    public TrendingVideoAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/TrendingVideoAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterTrendingVideoBinding;", "(Lcom/asiantimes/databinding/AdapterTrendingVideoBinding;)V", "bind", "", "OnClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "cateList", "Lcom/asiantimes/model/GetVideoListModel$TrandListBean;", "parentPosition", "", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.asiantimes.databinding.AdapterTrendingVideoBinding bindingView = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener, @org.jetbrains.annotations.NotNull()
        com.asiantimes.model.GetVideoListModel.TrandListBean cateList, int parentPosition) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterTrendingVideoBinding bindingView) {
            super(null);
        }
    }
}