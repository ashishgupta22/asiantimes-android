package com.asiantimes.ui.cart;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b!\u0018\u0000 \u00b6\u00012\u00020\u0001:\u0002\u00b6\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0085\u0001\u001a\u00030\u0086\u0001H\u0002J\n\u0010\u0087\u0001\u001a\u00030\u0088\u0001H\u0002J0\u0010\u0089\u0001\u001a\u00020V2\b\u0010f\u001a\u0004\u0018\u00010\u00042\t\u0010\u008a\u0001\u001a\u0004\u0018\u00010\u00042\b\u0010i\u001a\u0004\u0018\u00010\u00042\b\u0010\u008b\u0001\u001a\u00030\u008c\u0001J\t\u0010\u008d\u0001\u001a\u00020VH\u0002J\'\u0010\u008e\u0001\u001a\u00020V2\u0007\u0010\u008f\u0001\u001a\u00020\t2\u0007\u0010\u0090\u0001\u001a\u00020\t2\n\u0010\u0091\u0001\u001a\u0005\u0018\u00010\u0092\u0001H\u0016J\t\u0010\u0093\u0001\u001a\u00020VH\u0016J\u0015\u0010\u0094\u0001\u001a\u00020V2\n\u0010\u0095\u0001\u001a\u0005\u0018\u00010\u0096\u0001H\u0015J\t\u0010\u0097\u0001\u001a\u00020VH\u0014J\u0097\u0002\u0010\u0098\u0001\u001a\u00020V2\t\u0010\u0099\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009a\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009b\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009d\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009e\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u009f\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a0\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a1\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a2\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a3\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a4\u0001\u001a\u0004\u0018\u00010\u00042\b\u0010f\u001a\u0004\u0018\u00010\u00042\u0007\u0010\u00a5\u0001\u001a\u00020\u00042\t\u0010\u00a6\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a7\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a8\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00a9\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00aa\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00ab\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00ac\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00ad\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00ae\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00af\u0001\u001a\u0004\u0018\u00010\u00042\t\u0010\u00b0\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u00b1\u0001\u001a\u00020VH\u0002J\t\u0010\u00b2\u0001\u001a\u00020VH\u0002J0\u0010\u00b3\u0001\u001a\u00020V2\b\u0010f\u001a\u0004\u0018\u00010\u00042\u0007\u0010\u00b4\u0001\u001a\u00020\u00042\u0007\u0010\u0099\u0001\u001a\u00020\u00042\t\u0010\u00b5\u0001\u001a\u0004\u0018\u00010\u0004H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010$\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\'\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010*\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010,\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010-\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010/\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00101\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00103\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00104\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00105\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00107\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u00109\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u0010\u0010>\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010?\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010@\u001a\u0004\u0018\u00010AX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010B\u001a\u0004\u0018\u00010AX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010D\"\u0004\bE\u0010FR\u001c\u0010G\u001a\u0004\u0018\u00010AX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010D\"\u0004\bI\u0010FR\u0010\u0010J\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010K\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010L\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010M\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010N\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bO\u0010;\"\u0004\bP\u0010=R\u0010\u0010Q\u001a\u0004\u0018\u00010RX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010S\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010T\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010U\u001a\u00020V8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\bW\u0010XR\u001c\u0010Y\u001a\u0004\u0018\u00010ZX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010\\\"\u0004\b]\u0010^R\u0010\u0010_\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010`\u001a\u0004\u0018\u00010aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bb\u0010c\"\u0004\bd\u0010eR\u001c\u0010f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010;\"\u0004\bh\u0010=R\u001c\u0010i\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010;\"\u0004\bk\u0010=R\u001c\u0010l\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bm\u0010;\"\u0004\bn\u0010=R\u001c\u0010o\u001a\u0004\u0018\u00010pX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bq\u0010r\"\u0004\bs\u0010tR\u001a\u0010u\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010\u000b\"\u0004\bw\u0010xR\u001c\u0010y\u001a\u0004\u0018\u00010zX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b{\u0010|\"\u0004\b}\u0010~R \u0010\u007f\u001a\u00030\u0080\u00018FX\u0086\u0084\u0002\u00a2\u0006\u0010\n\u0006\b\u0083\u0001\u0010\u0084\u0001\u001a\u0006\b\u0081\u0001\u0010\u0082\u0001\u00a8\u0006\u00b7\u0001"}, d2 = {"Lcom/asiantimes/ui/cart/PaymentActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "address_first_deliver_payment", "", "address_first_payment", "address_second_deliver_payment", "address_second_payment", "buildVersionCode", "", "getBuildVersionCode", "()I", "buttonStripePay", "Lcom/asiantimes/base/RippleButton;", "button_google_pay", "cardInputWidget", "Lcom/stripe/android/view/CardMultilineWidget;", "checkboxDeliverPayment", "Landroid/widget/CheckBox;", "city_deliver_payment", "city_payment", "company_name_deliver_payment", "company_name_payment", "country_deliver_payment", "country_payment", "dbHelper", "Lcom/asiantimes/database/AppDatabase;", "getDbHelper", "()Lcom/asiantimes/database/AppDatabase;", "setDbHelper", "(Lcom/asiantimes/database/AppDatabase;)V", "editAddressFirstDeliverPayment", "Landroid/widget/EditText;", "editAddressFirstPayment", "editAddressSecondDeliverPayment", "editAddressSecondPayment", "editAmountPayment", "editCityDeliverPayment", "editCityPayment", "editCompanyNameDeliverPayment", "editCompanyNamePayment", "editCountryDeliverPayment", "editCountryPayment", "editEmailDeliverPayment", "editEmailPayment", "editLastNameDeliverPayment", "editLastNamePayment", "editMobileDeliverPayment", "editMobilePayment", "editNameDeliverPayment", "editNamePayment", "editNoteOrderPayment", "editPostalCodeDeliverPayment", "editPostalCodePayment", "editStatePayment", "email_deliver_payment", "email_payment", "formShow", "getFormShow", "()Ljava/lang/String;", "setFormShow", "(Ljava/lang/String;)V", "lastNameDeliverPayment", "lastNamePayment", "linearDeliverAddress", "Landroid/widget/LinearLayout;", "linearHeaderProgress", "getLinearHeaderProgress", "()Landroid/widget/LinearLayout;", "setLinearHeaderProgress", "(Landroid/widget/LinearLayout;)V", "linearSubscribeForm", "getLinearSubscribeForm", "setLinearSubscribeForm", "mobile_deliver_payment", "mobile_payment", "name_deliver_payment", "name_payment", "packageType", "getPackageType", "setPackageType", "paymentsClient", "Lcom/google/android/gms/wallet/PaymentsClient;", "postal_code_deliver_payment", "postal_code_payment", "premiumPostArray", "", "getPremiumPostArray", "()Lkotlin/Unit;", "rl_actionbar", "Landroid/widget/RelativeLayout;", "getRl_actionbar", "()Landroid/widget/RelativeLayout;", "setRl_actionbar", "(Landroid/widget/RelativeLayout;)V", "state_payment", "stripe", "Lcom/stripe/android/Stripe;", "getStripe", "()Lcom/stripe/android/Stripe;", "setStripe", "(Lcom/stripe/android/Stripe;)V", "subscriptionId", "getSubscriptionId", "setSubscriptionId", "subscriptionName", "getSubscriptionName", "setSubscriptionName", "subscriptionType", "getSubscriptionType", "setSubscriptionType", "text_card_payment", "Lcom/asiantimes/widget/TextViewRegular;", "getText_card_payment", "()Lcom/asiantimes/widget/TextViewRegular;", "setText_card_payment", "(Lcom/asiantimes/widget/TextViewRegular;)V", "totalAmount", "getTotalAmount", "setTotalAmount", "(I)V", "userData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserData", "()Lcom/asiantimes/model/CommonModel$UserDetail;", "setUserData", "(Lcom/asiantimes/model/CommonModel$UserDetail;)V", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionHomeViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionHomeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "createPaymentDataRequest", "Lcom/google/android/gms/wallet/PaymentDataRequest;", "createTokenizationParameters", "Lcom/google/android/gms/wallet/PaymentMethodTokenizationParameters;", "getShippingPrice", "subscription_plan", "isUk", "", "initView", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "payNowPaymentAPI", "tokenId", "currency", "name", "lastName", "companyName", "email", "mobile", "addressLineOne", "addressLineTow", "city", "country", "postalCode", "isCheckboxChecked", "nameDeliver", "lastNameDeliver", "companyNameDeliver", "emailDeliver", "mobileDeliver", "addressLineOneDeliver", "addressLineTowDeliver", "cityDeliver", "countryDeliver", "postalCodeDeliver", "orderNoteDeliver", "redirectToHome", "redirectToMainActivity", "setPremiumSubscription", "subscriptionPrice", "subscriptionTitle", "Companion", "app_productionDebug"})
public final class PaymentActivity extends com.asiantimes.base.BaseActivity {
    @org.jetbrains.annotations.Nullable()
    private android.widget.RelativeLayout rl_actionbar;
    @org.jetbrains.annotations.Nullable()
    private com.stripe.android.Stripe stripe;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout linearHeaderProgress;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout linearSubscribeForm;
    private java.lang.String name_payment;
    private java.lang.String email_payment;
    private java.lang.String mobile_payment;
    private java.lang.String address_first_payment;
    private java.lang.String address_second_payment;
    private java.lang.String city_payment;
    private java.lang.String state_payment;
    private java.lang.String country_payment;
    private java.lang.String postal_code_payment;
    private java.lang.String company_name_payment;
    private java.lang.String lastNamePayment;
    private java.lang.String name_deliver_payment;
    private java.lang.String address_first_deliver_payment;
    private java.lang.String address_second_deliver_payment;
    private java.lang.String city_deliver_payment;
    private java.lang.String country_deliver_payment;
    private java.lang.String postal_code_deliver_payment;
    private java.lang.String email_deliver_payment;
    private java.lang.String mobile_deliver_payment;
    private java.lang.String company_name_deliver_payment;
    private java.lang.String lastNameDeliverPayment;
    private int totalAmount = 0;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.database.AppDatabase dbHelper;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String subscriptionId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String subscriptionType;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String subscriptionName;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String formShow;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String packageType;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.widget.TextViewRegular text_card_payment;
    private android.widget.EditText editNamePayment;
    private android.widget.EditText editLastNamePayment;
    private android.widget.EditText editEmailPayment;
    private android.widget.EditText editMobilePayment;
    private android.widget.EditText editCompanyNamePayment;
    private android.widget.EditText editAddressFirstPayment;
    private android.widget.EditText editAddressSecondPayment;
    private android.widget.EditText editCityPayment;
    private android.widget.EditText editStatePayment;
    private android.widget.EditText editCountryPayment;
    private android.widget.EditText editPostalCodePayment;
    private android.widget.CheckBox checkboxDeliverPayment;
    private android.widget.LinearLayout linearDeliverAddress;
    private android.widget.EditText editNameDeliverPayment;
    private android.widget.EditText editLastNameDeliverPayment;
    private android.widget.EditText editEmailDeliverPayment;
    private android.widget.EditText editMobileDeliverPayment;
    private android.widget.EditText editCompanyNameDeliverPayment;
    private android.widget.EditText editAddressFirstDeliverPayment;
    private android.widget.EditText editAddressSecondDeliverPayment;
    private android.widget.EditText editCityDeliverPayment;
    private android.widget.EditText editCountryDeliverPayment;
    private android.widget.EditText editPostalCodeDeliverPayment;
    private android.widget.EditText editNoteOrderPayment;
    private android.widget.EditText editAmountPayment;
    private com.stripe.android.view.CardMultilineWidget cardInputWidget;
    private com.asiantimes.base.RippleButton buttonStripePay;
    private com.asiantimes.base.RippleButton button_google_pay;
    private com.google.android.gms.wallet.PaymentsClient paymentsClient;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.model.CommonModel.UserDetail userData;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy viewModel$delegate = null;
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 1023;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.ui.cart.PaymentActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.RelativeLayout getRl_actionbar() {
        return null;
    }
    
    public final void setRl_actionbar(@org.jetbrains.annotations.Nullable()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.stripe.android.Stripe getStripe() {
        return null;
    }
    
    public final void setStripe(@org.jetbrains.annotations.Nullable()
    com.stripe.android.Stripe p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLinearHeaderProgress() {
        return null;
    }
    
    public final void setLinearHeaderProgress(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLinearSubscribeForm() {
        return null;
    }
    
    public final void setLinearSubscribeForm(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    public final int getTotalAmount() {
        return 0;
    }
    
    public final void setTotalAmount(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.database.AppDatabase getDbHelper() {
        return null;
    }
    
    public final void setDbHelper(@org.jetbrains.annotations.Nullable()
    com.asiantimes.database.AppDatabase p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubscriptionId() {
        return null;
    }
    
    public final void setSubscriptionId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubscriptionType() {
        return null;
    }
    
    public final void setSubscriptionType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubscriptionName() {
        return null;
    }
    
    public final void setSubscriptionName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFormShow() {
        return null;
    }
    
    public final void setFormShow(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPackageType() {
        return null;
    }
    
    public final void setPackageType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.widget.TextViewRegular getText_card_payment() {
        return null;
    }
    
    public final void setText_card_payment(@org.jetbrains.annotations.Nullable()
    com.asiantimes.widget.TextViewRegular p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.model.CommonModel.UserDetail getUserData() {
        return null;
    }
    
    public final void setUserData(@org.jetbrains.annotations.Nullable()
    com.asiantimes.model.CommonModel.UserDetail p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel getViewModel() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void payNowPaymentAPI(@org.jetbrains.annotations.Nullable()
    java.lang.String tokenId, @org.jetbrains.annotations.Nullable()
    java.lang.String currency, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String lastName, @org.jetbrains.annotations.Nullable()
    java.lang.String companyName, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String mobile, @org.jetbrains.annotations.Nullable()
    java.lang.String addressLineOne, @org.jetbrains.annotations.Nullable()
    java.lang.String addressLineTow, @org.jetbrains.annotations.Nullable()
    java.lang.String city, @org.jetbrains.annotations.Nullable()
    java.lang.String country, @org.jetbrains.annotations.Nullable()
    java.lang.String postalCode, @org.jetbrains.annotations.Nullable()
    java.lang.String subscriptionId, @org.jetbrains.annotations.NotNull()
    java.lang.String isCheckboxChecked, @org.jetbrains.annotations.Nullable()
    java.lang.String nameDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String lastNameDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String companyNameDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String emailDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String mobileDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String addressLineOneDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String addressLineTowDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String cityDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String countryDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String postalCodeDeliver, @org.jetbrains.annotations.Nullable()
    java.lang.String orderNoteDeliver) {
    }
    
    private final kotlin.Unit getPremiumPostArray() {
        return null;
    }
    
    private final void initView() {
    }
    
    private final void setPremiumSubscription(java.lang.String subscriptionId, java.lang.String subscriptionPrice, java.lang.String tokenId, java.lang.String subscriptionTitle) {
    }
    
    private final void redirectToMainActivity() {
    }
    
    public final void getShippingPrice(@org.jetbrains.annotations.Nullable()
    java.lang.String subscriptionId, @org.jetbrains.annotations.Nullable()
    java.lang.String subscription_plan, @org.jetbrains.annotations.Nullable()
    java.lang.String subscriptionName, boolean isUk) {
    }
    
    private final com.google.android.gms.wallet.PaymentMethodTokenizationParameters createTokenizationParameters() {
        return null;
    }
    
    private final com.google.android.gms.wallet.PaymentDataRequest createPaymentDataRequest() {
        return null;
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void redirectToHome() {
    }
    
    private final int getBuildVersionCode() {
        return 0;
    }
    
    public PaymentActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/asiantimes/ui/cart/PaymentActivity$Companion;", "", "()V", "LOAD_PAYMENT_DATA_REQUEST_CODE", "", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}