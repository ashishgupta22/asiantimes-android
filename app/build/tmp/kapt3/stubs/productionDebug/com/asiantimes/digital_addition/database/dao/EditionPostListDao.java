package com.asiantimes.digital_addition.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0012\u0010\b\u001a\u00020\u00032\b\u0010\t\u001a\u0004\u0018\u00010\u0005H\'\u00a8\u0006\n"}, d2 = {"Lcom/asiantimes/digital_addition/database/dao/EditionPostListDao;", "", "delete", "", "getEditionPostById", "Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "editionId", "", "insert", "editionPostListEntity", "app_productionDebug"})
public abstract interface EditionPostListDao {
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.Nullable()
    com.asiantimes.digital_addition.model.EditionPostListModel editionPostListEntity);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from edition_post_list where editionId in (:editionId)")
    public abstract com.asiantimes.digital_addition.model.EditionPostListModel getEditionPostById(int editionId);
    
    @androidx.room.Query(value = "delete from edition_post_list")
    public abstract void delete();
}