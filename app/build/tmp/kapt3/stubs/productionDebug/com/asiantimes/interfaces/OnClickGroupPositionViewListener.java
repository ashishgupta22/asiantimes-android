package com.asiantimes.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J*\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H&\u00a8\u0006\t"}, d2 = {"Lcom/asiantimes/interfaces/OnClickGroupPositionViewListener;", "", "onClickPositionViewListener", "", "object", "group_position", "", "position", "view", "app_productionDebug"})
public abstract interface OnClickGroupPositionViewListener {
    
    public abstract void onClickPositionViewListener(@org.jetbrains.annotations.Nullable()
    java.lang.Object object, int group_position, int position, int view);
}