package com.asiantimes.ui.photo;

import java.lang.System;

/**
 * Created by inte on 01-Mar-18.
 */
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010=\u001a\u00020\u00002\b\u0010>\u001a\u0004\u0018\u00010?2\b\u0010@\u001a\u0004\u0018\u00010?J\u000e\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u00020DJ\u0012\u0010E\u001a\u00020B2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J&\u0010H\u001a\u0004\u0018\u00010D2\u0006\u0010I\u001a\u00020J2\b\u0010K\u001a\u0004\u0018\u00010L2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J\b\u0010M\u001a\u00020BH\u0016J\b\u0010N\u001a\u00020BH\u0016J\b\u0010O\u001a\u00020BH\u0002J\u0010\u0010P\u001a\u00020B2\b\u0010Q\u001a\u0004\u0018\u00010?J\u0010\u0010R\u001a\u00020B2\u0006\u0010S\u001a\u00020TH\u0002J\u0010\u0010U\u001a\u00020B2\b\u0010Q\u001a\u0004\u0018\u00010?J\u0010\u0010V\u001a\u00020B2\u0006\u0010W\u001a\u00020TH\u0002J\b\u0010X\u001a\u00020BH\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001d0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\"\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\u001c\u0010(\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010%\"\u0004\b*\u0010\'R\u001c\u0010+\u001a\u0004\u0018\u00010,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u001c\u00101\u001a\u0004\u0018\u000102X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u000e\u00107\u001a\u000208X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00109\u001a\u0004\u0018\u00010:X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010;\u001a\b\u0012\u0004\u0012\u00020<0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Y"}, d2 = {"Lcom/asiantimes/ui/photo/PhotoSectionFragment;", "Landroidx/fragment/app/Fragment;", "()V", "layoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "getLayoutManager", "()Landroidx/recyclerview/widget/GridLayoutManager;", "setLayoutManager", "(Landroidx/recyclerview/widget/GridLayoutManager;)V", "ll_error_news_message", "Landroid/widget/LinearLayout;", "getLl_error_news_message", "()Landroid/widget/LinearLayout;", "setLl_error_news_message", "(Landroid/widget/LinearLayout;)V", "ll_gif_news_layout", "Landroid/widget/RelativeLayout;", "getLl_gif_news_layout", "()Landroid/widget/RelativeLayout;", "setLl_gif_news_layout", "(Landroid/widget/RelativeLayout;)V", "mLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "getMLayoutManager", "()Landroidx/recyclerview/widget/LinearLayoutManager;", "setMLayoutManager", "(Landroidx/recyclerview/widget/LinearLayoutManager;)V", "mainPostLists", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/MainPostList;", "photoCategoryArrayList", "Lcom/asiantimes/model/PhotoCategory;", "photoListAdapter", "Lcom/asiantimes/ui/photo/PhotoListAdapter;", "rv_photo_header", "Landroidx/recyclerview/widget/RecyclerView;", "getRv_photo_header", "()Landroidx/recyclerview/widget/RecyclerView;", "setRv_photo_header", "(Landroidx/recyclerview/widget/RecyclerView;)V", "rv_photo_section", "getRv_photo_section", "setRv_photo_section", "state", "Landroid/os/Parcelable;", "getState", "()Landroid/os/Parcelable;", "setState", "(Landroid/os/Parcelable;)V", "tv_retry_news", "Landroid/widget/TextView;", "getTv_retry_news", "()Landroid/widget/TextView;", "setTv_retry_news", "(Landroid/widget/TextView;)V", "viewModel", "Lcom/asiantimes/ui/photo/PhotosViewModel;", "viewMorePhotoAdapter", "Lcom/asiantimes/ui/photo/ViewMorePhotoAdapter;", "viewMorePhotoArrayList", "Lcom/asiantimes/model/ViewMorePhoto;", "getInstance", "type", "", "categoryId", "initializationView", "", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onPause", "onResume", "setAdapter", "setPhotoArticles", "categoryID", "setPhotoArticlesData", "responseResult", "Lcom/asiantimes/model/ResponseResult;", "setPhotoArticlesInMenu", "setPhotoMoreData", "responsePacket", "setPhotoVideoError", "app_productionDebug"})
public final class PhotoSectionFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.LinearLayoutManager mLayoutManager;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.GridLayoutManager layoutManager;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rv_photo_section;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rv_photo_header;
    private com.asiantimes.ui.photo.PhotoListAdapter photoListAdapter;
    private com.asiantimes.ui.photo.ViewMorePhotoAdapter viewMorePhotoAdapter;
    private final java.util.ArrayList<com.asiantimes.model.PhotoCategory> photoCategoryArrayList = null;
    private final java.util.ArrayList<com.asiantimes.model.ViewMorePhoto> viewMorePhotoArrayList = null;
    private final java.util.ArrayList<com.asiantimes.model.MainPostList> mainPostLists = null;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout ll_error_news_message;
    @org.jetbrains.annotations.Nullable()
    private android.widget.RelativeLayout ll_gif_news_layout;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_retry_news;
    @org.jetbrains.annotations.Nullable()
    private android.os.Parcelable state;
    private final com.asiantimes.ui.photo.PhotosViewModel viewModel = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.LinearLayoutManager getMLayoutManager() {
        return null;
    }
    
    public final void setMLayoutManager(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.LinearLayoutManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.GridLayoutManager getLayoutManager() {
        return null;
    }
    
    public final void setLayoutManager(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.GridLayoutManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRv_photo_section() {
        return null;
    }
    
    public final void setRv_photo_section(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRv_photo_header() {
        return null;
    }
    
    public final void setRv_photo_header(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLl_error_news_message() {
        return null;
    }
    
    public final void setLl_error_news_message(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.RelativeLayout getLl_gif_news_layout() {
        return null;
    }
    
    public final void setLl_gif_news_layout(@org.jetbrains.annotations.Nullable()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_retry_news() {
        return null;
    }
    
    public final void setTv_retry_news(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.os.Parcelable getState() {
        return null;
    }
    
    public final void setState(@org.jetbrains.annotations.Nullable()
    android.os.Parcelable p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.ui.photo.PhotoSectionFragment getInstance(@org.jetbrains.annotations.Nullable()
    java.lang.String type, @org.jetbrains.annotations.Nullable()
    java.lang.String categoryId) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void initializationView(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    private final void setAdapter() {
    }
    
    public final void setPhotoArticles(@org.jetbrains.annotations.Nullable()
    java.lang.String categoryID) {
    }
    
    public final void setPhotoArticlesInMenu(@org.jetbrains.annotations.Nullable()
    java.lang.String categoryID) {
    }
    
    private final void setPhotoArticlesData(com.asiantimes.model.ResponseResult responseResult) {
    }
    
    private final void setPhotoMoreData(com.asiantimes.model.ResponseResult responsePacket) {
    }
    
    private final void setPhotoVideoError() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    public PhotoSectionFragment() {
        super();
    }
}