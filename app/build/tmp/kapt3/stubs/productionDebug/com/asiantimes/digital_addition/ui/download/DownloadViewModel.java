package com.asiantimes.digital_addition.ui.download;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J$\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "buySingleProduct", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/digital_addition/model/BuySingleProductModel;", "requestBean", "Lcom/asiantimes/model/RequestBean;", "getATDigitalEditionList", "Lcom/asiantimes/digital_addition/model/EditionListModel;", "getATDigitalEditionPostList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "getSearchResult", "Lcom/asiantimes/digital_addition/model/SearchModel;", "submitFeedback", "Lcom/asiantimes/model/CommonModel;", "yourReview", "Lokhttp3/RequestBody;", "yourEmail", "body", "Lokhttp3/MultipartBody$Part;", "app_productionDebug"})
public final class DownloadViewModel extends androidx.lifecycle.ViewModel {
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionPostListModel> getATDigitalEditionPostList(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.BuySingleProductModel> buySingleProduct(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionListModel> getATDigitalEditionList(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.SearchModel> getSearchResult(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> submitFeedback(@org.jetbrains.annotations.NotNull()
    okhttp3.RequestBody yourReview, @org.jetbrains.annotations.NotNull()
    okhttp3.RequestBody yourEmail, @org.jetbrains.annotations.NotNull()
    okhttp3.MultipartBody.Part body) {
        return null;
    }
    
    public DownloadViewModel() {
        super();
    }
}