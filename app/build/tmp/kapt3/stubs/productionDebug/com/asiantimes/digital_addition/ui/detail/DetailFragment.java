package com.asiantimes.digital_addition.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0017\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0007H\u0002J\b\u0010\u001e\u001a\u00020\u001cH\u0002J\u0012\u0010\u001f\u001a\u00020\u001c2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J&\u0010\"\u001a\u0004\u0018\u00010!2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u0010\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0016J\b\u0010-\u001a\u00020\u001cH\u0002J\u0010\u0010.\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u0017\u0010\u0018\u00a8\u0006/"}, d2 = {"Lcom/asiantimes/digital_addition/ui/detail/DetailFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "Landroidx/appcompat/widget/PopupMenu$OnMenuItemClickListener;", "postCatNewsList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "editionId", "", "(Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;Ljava/lang/String;)V", "bindingView", "Lcom/asiantimes/databinding/FragmentDetailBinding;", "detailAdapter", "Lcom/asiantimes/digital_addition/ui/detail/DetailAdapter;", "localDB", "Lcom/asiantimes/digital_addition/database/AppDatabase;", "mImageViewBack", "Landroid/widget/ImageView;", "mImageViewMenu", "mImageViewShare", "popup", "Landroidx/appcompat/widget/PopupMenu;", "viewModel", "Lcom/asiantimes/digital_addition/ui/detail/DetailViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/detail/DetailViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "addBookmarkAPI", "", "isRemoved", "getATDigitalEditionPostListAPI", "onClick", "view", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onMenuItemClick", "", "item", "Landroid/view/MenuItem;", "setData", "showCustomPopUpMenu", "app_productionDebug"})
public final class DetailFragment extends androidx.fragment.app.Fragment implements android.view.View.OnClickListener, androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener {
    private com.asiantimes.databinding.FragmentDetailBinding bindingView;
    private com.asiantimes.digital_addition.ui.detail.DetailAdapter detailAdapter;
    private android.widget.ImageView mImageViewBack;
    private android.widget.ImageView mImageViewMenu;
    private android.widget.ImageView mImageViewShare;
    private androidx.appcompat.widget.PopupMenu popup;
    private com.asiantimes.digital_addition.database.AppDatabase localDB;
    private final kotlin.Lazy viewModel$delegate = null;
    private final com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList = null;
    private final java.lang.String editionId = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.digital_addition.ui.detail.DetailViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void showCustomPopUpMenu(android.view.View view) {
    }
    
    @java.lang.Override()
    public boolean onMenuItemClick(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void setData() {
    }
    
    private final void addBookmarkAPI(java.lang.String isRemoved) {
    }
    
    private final void getATDigitalEditionPostListAPI() {
    }
    
    public DetailFragment(@org.jetbrains.annotations.NotNull()
    com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList, @org.jetbrains.annotations.Nullable()
    java.lang.String editionId) {
        super();
    }
}