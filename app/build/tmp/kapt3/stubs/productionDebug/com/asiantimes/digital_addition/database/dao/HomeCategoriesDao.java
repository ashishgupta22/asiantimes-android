package com.asiantimes.digital_addition.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bg\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/asiantimes/digital_addition/database/dao/HomeCategoriesDao;", "", "app_productionDebug"})
public abstract interface HomeCategoriesDao {
}