package com.asiantimes.digital_addition.ui.setting;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0012\u0010\u001c\u001a\u00020\u00192\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0018\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0007H\u0002J\u0010\u0010$\u001a\u00020\u00192\u0006\u0010%\u001a\u00020\u0007H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u00020\u00138FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006&"}, d2 = {"Lcom/asiantimes/digital_addition/ui/setting/DigitalFeedbackActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "body", "Lokhttp3/MultipartBody$Part;", "imageString", "", "imageUri", "Landroid/net/Uri;", "requestFile", "Lokhttp3/RequestBody;", "userData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserData", "()Lcom/asiantimes/model/CommonModel$UserDetail;", "setUserData", "(Lcom/asiantimes/model/CommonModel$UserDetail;)V", "viewModel", "Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onClick", "", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "persistImage", "Ljava/io/File;", "bitmap", "Landroid/graphics/Bitmap;", "name", "submitFeedback", "comment", "app_productionDebug"})
public final class DigitalFeedbackActivity extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    private android.net.Uri imageUri;
    private java.lang.String imageString = "";
    private okhttp3.MultipartBody.Part body;
    private okhttp3.RequestBody requestFile;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.model.CommonModel.UserDetail userData;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.model.CommonModel.UserDetail getUserData() {
        return null;
    }
    
    public final void setUserData(@org.jetbrains.annotations.Nullable()
    com.asiantimes.model.CommonModel.UserDetail p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.digital_addition.ui.download.DownloadViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void submitFeedback(java.lang.String comment) {
    }
    
    private final java.io.File persistImage(android.graphics.Bitmap bitmap, java.lang.String name) {
        return null;
    }
    
    public DigitalFeedbackActivity() {
        super();
    }
}