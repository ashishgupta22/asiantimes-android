package com.asiantimes.ui.sign_up;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0012\u0010\u0016\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\b\u0010\u0019\u001a\u00020\u0013H\u0002J\b\u0010\u001a\u001a\u00020\u0013H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001b"}, d2 = {"Lcom/asiantimes/ui/sign_up/SignUpActivityActivity;", "Lcom/asiantimes/base/BaseActivity;", "Landroid/view/View$OnClickListener;", "()V", "bindingView", "Lcom/asiantimes/databinding/ActivitySignUpActivityBinding;", "confirmPassword", "", "email", "name", "number", "password", "viewModel", "Lcom/asiantimes/ui/sign_up/SignUpViewModel;", "getViewModel", "()Lcom/asiantimes/ui/sign_up/SignUpViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onClick", "", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "registerAPI", "registerObserver", "app_productionDebug"})
public final class SignUpActivityActivity extends com.asiantimes.base.BaseActivity implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.ActivitySignUpActivityBinding bindingView;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.lang.String name;
    private java.lang.String email;
    private java.lang.String number;
    private java.lang.String password;
    private java.lang.String confirmPassword;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.sign_up.SignUpViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void registerAPI() {
    }
    
    private final void registerObserver() {
    }
    
    public SignUpActivityActivity() {
        super();
    }
}