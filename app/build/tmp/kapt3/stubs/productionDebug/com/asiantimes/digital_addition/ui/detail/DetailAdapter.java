package com.asiantimes.digital_addition.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\bH\u0016J\u0018\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\bH\u0016J\u0016\u0010\u0011\u001a\u00020\n2\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005R\u0016\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/asiantimes/digital_addition/ui/detail/DetailAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/digital_addition/ui/detail/DetailAdapter$ViewHolder;", "()V", "dataList", "", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "postList", "ViewHolder", "app_productionDebug"})
public final class DetailAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.digital_addition.ui.detail.DetailAdapter.ViewHolder> {
    private java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> dataList;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.digital_addition.ui.detail.DetailAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.digital_addition.ui.detail.DetailAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> postList) {
    }
    
    public DetailAdapter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010\u00a8\u0006\u0018"}, d2 = {"Lcom/asiantimes/digital_addition/ui/detail/DetailAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterEditionDetailBinding;", "(Lcom/asiantimes/databinding/AdapterEditionDetailBinding;)V", "imageViewDetail", "Landroid/widget/ImageView;", "getImageViewDetail", "()Landroid/widget/ImageView;", "layoutHeader", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getLayoutHeader", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "textViewDetail", "Landroidx/appcompat/widget/AppCompatTextView;", "getTextViewDetail", "()Landroidx/appcompat/widget/AppCompatTextView;", "textViewTitle", "getTextViewTitle", "bindDetailText", "", "productList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "bindHeader", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final androidx.appcompat.widget.AppCompatTextView textViewTitle = null;
        @org.jetbrains.annotations.NotNull()
        private final androidx.constraintlayout.widget.ConstraintLayout layoutHeader = null;
        @org.jetbrains.annotations.NotNull()
        private final androidx.appcompat.widget.AppCompatTextView textViewDetail = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView imageViewDetail = null;
        private final com.asiantimes.databinding.AdapterEditionDetailBinding bindingView = null;
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.widget.AppCompatTextView getTextViewTitle() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.constraintlayout.widget.ConstraintLayout getLayoutHeader() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.widget.AppCompatTextView getTextViewDetail() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getImageViewDetail() {
            return null;
        }
        
        public final void bindHeader(@org.jetbrains.annotations.NotNull()
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList productList) {
        }
        
        public final void bindDetailText(@org.jetbrains.annotations.NotNull()
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList productList) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterEditionDetailBinding bindingView) {
            super(null);
        }
    }
}