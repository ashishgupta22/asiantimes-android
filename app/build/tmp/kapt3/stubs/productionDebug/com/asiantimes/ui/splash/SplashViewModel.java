package com.asiantimes.ui.splash;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\t\u00a8\u0006\u0011"}, d2 = {"Lcom/asiantimes/ui/splash/SplashViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "liveDataAllCategories", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/GetAllCategoriesModel;", "getLiveDataAllCategories", "()Landroidx/lifecycle/MutableLiveData;", "setLiveDataAllCategories", "(Landroidx/lifecycle/MutableLiveData;)V", "liveDataUpdateDbVersion", "Lcom/asiantimes/model/CommonModel;", "getLiveDataUpdateDbVersion", "setLiveDataUpdateDbVersion", "getAllCategories", "", "updateDbVersion", "app_productionDebug"})
public final class SplashViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> liveDataUpdateDbVersion;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetAllCategoriesModel> liveDataAllCategories;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> getLiveDataUpdateDbVersion() {
        return null;
    }
    
    public final void setLiveDataUpdateDbVersion(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetAllCategoriesModel> getLiveDataAllCategories() {
        return null;
    }
    
    public final void setLiveDataAllCategories(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetAllCategoriesModel> p0) {
    }
    
    public final void updateDbVersion() {
    }
    
    public final void getAllCategories() {
    }
    
    public SplashViewModel() {
        super();
    }
}