package com.asiantimes.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 R2\u00020\u00012\u00020\u0002:\u0001RB\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u00101\u001a\u000202H\u0004J\b\u00103\u001a\u000204H\u0002J\b\u00105\u001a\u000202H\u0002J\u0006\u00106\u001a\u000202J\"\u00107\u001a\u0002022\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u0002092\b\u0010;\u001a\u0004\u0018\u00010<H\u0014J\b\u0010=\u001a\u000202H\u0016J\u0010\u0010>\u001a\u0002022\u0006\u0010?\u001a\u00020@H\u0016J\u0012\u0010A\u001a\u0002022\b\u0010B\u001a\u0004\u0018\u00010CH\u0014J\b\u0010D\u001a\u000202H\u0014J+\u0010E\u001a\u0002022\u0006\u00108\u001a\u0002092\f\u0010F\u001a\b\u0012\u0004\u0012\u00020\u00070G2\u0006\u0010H\u001a\u00020IH\u0016\u00a2\u0006\u0002\u0010JJ\b\u0010K\u001a\u000202H\u0014J\u000e\u0010L\u001a\u0002022\u0006\u0010M\u001a\u000204J\u0018\u0010N\u001a\u0002022\u0006\u0010O\u001a\u00020\u00072\u0006\u0010P\u001a\u00020QH\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0010\u0010\f\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\t\"\u0004\b\u000f\u0010\u000bR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\t\"\u0004\b\u001a\u0010\u000bR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\t\"\u0004\b\u001f\u0010\u000bR\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\"0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010#\u001a\u0004\u0018\u00010$X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u001a\u0010)\u001a\u00020*X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u0010\u0010/\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006S"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionHomePageActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "action_bar_title", "Lcom/asiantimes/widget/TextViewRegular;", "address", "", "getAddress", "()Ljava/lang/String;", "setAddress", "(Ljava/lang/String;)V", "callFrom", "country", "getCountry", "setCountry", "hidesubscriptionpage", "Landroid/widget/RelativeLayout;", "getHidesubscriptionpage", "()Landroid/widget/RelativeLayout;", "setHidesubscriptionpage", "(Landroid/widget/RelativeLayout;)V", "iv_menuBack", "Landroid/widget/ImageView;", "lattitude", "getLattitude", "setLattitude", "location", "Lim/delight/android/location/SimpleLocation;", "longitude", "getLongitude", "setLongitude", "planListSerializes", "", "Lcom/asiantimes/model/PlanListSerialize;", "progress_bar", "Landroid/widget/ProgressBar;", "getProgress_bar", "()Landroid/widget/ProgressBar;", "setProgress_bar", "(Landroid/widget/ProgressBar;)V", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionHomeViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionHomeViewModel;", "setViewModel", "(Lcom/asiantimes/subscription/viewmodel/SubscriptionHomeViewModel;)V", "visit_uk_site", "yes_continue", "buildAlertMessageNoGps", "", "checkAndRequestPermissions", "", "getLocation", "initializationView", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onPause", "onRequestPermissionsResult", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "setSubscriptionList", "isuk", "showDialogOK", "message", "okListener", "Landroid/content/DialogInterface$OnClickListener;", "Companion", "app_productionDebug"})
public final class SubscriptionHomePageActivity extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    @org.jetbrains.annotations.Nullable()
    private android.widget.ProgressBar progress_bar;
    private com.asiantimes.widget.TextViewRegular yes_continue;
    private com.asiantimes.widget.TextViewRegular visit_uk_site;
    private com.asiantimes.widget.TextViewRegular action_bar_title;
    private android.widget.ImageView iv_menuBack;
    private final java.util.List<com.asiantimes.model.PlanListSerialize> planListSerializes = null;
    private im.delight.android.location.SimpleLocation location;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String lattitude;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String longitude;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String address;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String country;
    @org.jetbrains.annotations.Nullable()
    private android.widget.RelativeLayout hidesubscriptionpage;
    private java.lang.String callFrom;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel viewModel;
    private static final int PERMISSION_REQUEST_CODE = 1;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.subscription.SubscriptionHomePageActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ProgressBar getProgress_bar() {
        return null;
    }
    
    public final void setProgress_bar(@org.jetbrains.annotations.Nullable()
    android.widget.ProgressBar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLattitude() {
        return null;
    }
    
    public final void setLattitude(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLongitude() {
        return null;
    }
    
    public final void setLongitude(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddress() {
        return null;
    }
    
    public final void setAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCountry() {
        return null;
    }
    
    public final void setCountry(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.RelativeLayout getHidesubscriptionpage() {
        return null;
    }
    
    public final void setHidesubscriptionpage(@org.jetbrains.annotations.Nullable()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initializationView() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void setSubscriptionList(boolean isuk) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    protected final void buildAlertMessageNoGps() {
    }
    
    private final void getLocation() {
    }
    
    private final boolean checkAndRequestPermissions() {
        return false;
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void showDialogOK(java.lang.String message, android.content.DialogInterface.OnClickListener okListener) {
    }
    
    public SubscriptionHomePageActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionHomePageActivity$Companion;", "", "()V", "PERMISSION_REQUEST_CODE", "", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}