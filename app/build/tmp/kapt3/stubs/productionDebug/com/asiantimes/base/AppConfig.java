package com.asiantimes.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"}, d2 = {"Lcom/asiantimes/base/AppConfig;", "Landroid/app/Application;", "()V", "onCreate", "", "Companion", "app_productionDebug"})
public final class AppConfig extends android.app.Application {
    @org.jetbrains.annotations.Nullable()
    private static com.asiantimes.api.ApiInterface msApiInterface;
    private static android.content.Context context;
    private static com.asiantimes.widget.CustomProgressDialog progressDialog;
    public static com.nostra13.universalimageloader.core.ImageLoader msImageLoader;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ACCESS_TOKEN = "51232b15cbd4336194aeed9020ba0b4b";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.base.AppConfig.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public AppConfig() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0015\u001a\u00020\u000eJ\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\u0006\u0010\u0019\u001a\u00020\u001aJ\u000e\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001dJ\"\u0010\u001e\u001a\u00020\u001a2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\u00042\u0006\u0010\"\u001a\u00020#J\u000e\u0010$\u001a\u00020\u001a2\u0006\u0010\u0005\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"}, d2 = {"Lcom/asiantimes/base/AppConfig$Companion;", "", "()V", "ACCESS_TOKEN", "", "context", "Landroid/content/Context;", "msApiInterface", "Lcom/asiantimes/api/ApiInterface;", "getMsApiInterface", "()Lcom/asiantimes/api/ApiInterface;", "setMsApiInterface", "(Lcom/asiantimes/api/ApiInterface;)V", "msImageLoader", "Lcom/nostra13/universalimageloader/core/ImageLoader;", "getMsImageLoader", "()Lcom/nostra13/universalimageloader/core/ImageLoader;", "setMsImageLoader", "(Lcom/nostra13/universalimageloader/core/ImageLoader;)V", "progressDialog", "Lcom/asiantimes/widget/CustomProgressDialog;", "getImageLoader", "getInstance", "getRequestQueue", "Lretrofit2/Retrofit;", "hideProgress", "", "hideSoftKeyboard", "activity", "Landroid/app/Activity;", "loadImage", "imageView", "Landroid/widget/ImageView;", "url", "defaultResId", "", "showProgress", "app_productionDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.asiantimes.api.ApiInterface getMsApiInterface() {
            return null;
        }
        
        public final void setMsApiInterface(@org.jetbrains.annotations.Nullable()
        com.asiantimes.api.ApiInterface p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.nostra13.universalimageloader.core.ImageLoader getMsImageLoader() {
            return null;
        }
        
        public final void setMsImageLoader(@org.jetbrains.annotations.NotNull()
        com.nostra13.universalimageloader.core.ImageLoader p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final android.content.Context getInstance() {
            return null;
        }
        
        /**
         * Image Loader
         */
        @org.jetbrains.annotations.NotNull()
        public final com.nostra13.universalimageloader.core.ImageLoader getImageLoader() {
            return null;
        }
        
        private final retrofit2.Retrofit getRequestQueue() {
            return null;
        }
        
        public final void showProgress(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void hideProgress() {
        }
        
        public final void hideSoftKeyboard(@org.jetbrains.annotations.NotNull()
        android.app.Activity activity) {
        }
        
        public final void loadImage(@org.jetbrains.annotations.Nullable()
        android.widget.ImageView imageView, @org.jetbrains.annotations.Nullable()
        java.lang.String url, int defaultResId) {
        }
        
        private Companion() {
            super();
        }
    }
}