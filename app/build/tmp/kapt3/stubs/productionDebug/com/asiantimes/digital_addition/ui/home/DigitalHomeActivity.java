package com.asiantimes.digital_addition.ui.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010!\u001a\u00020\"H\u0002J\b\u0010#\u001a\u00020\"H\u0002J\b\u0010$\u001a\u00020\"H\u0002J\b\u0010%\u001a\u00020\"H\u0002J\b\u0010&\u001a\u00020\"H\u0002J\u0012\u0010\'\u001a\u00020\"2\b\u0010(\u001a\u0004\u0018\u00010)H\u0016J\u0012\u0010*\u001a\u00020\"2\b\u0010+\u001a\u0004\u0018\u00010,H\u0014J\u0018\u0010-\u001a\u00020\"2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020,H\u0016J\b\u00101\u001a\u00020\"H\u0014J\b\u00102\u001a\u00020\"H\u0002J\b\u00103\u001a\u00020\"H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001e\u00a8\u00064"}, d2 = {"Lcom/asiantimes/digital_addition/ui/home/DigitalHomeActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "Lcom/asiantimes/services/BackgroundApiResultReceiver$Receiver;", "()V", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/ActivityDigitalHomeBinding;", "bindingViewDrawer", "Lcom/asiantimes/databinding/DigitalDrawerBinding;", "digitalHomePagerAdapter", "Lcom/asiantimes/digital_addition/ui/home/DigitalHomePagerAdapter;", "localDB", "Lcom/asiantimes/digital_addition/database/AppDatabase;", "mReceiver", "Lcom/asiantimes/services/BackgroundApiResultReceiver;", "getMReceiver", "()Lcom/asiantimes/services/BackgroundApiResultReceiver;", "setMReceiver", "(Lcom/asiantimes/services/BackgroundApiResultReceiver;)V", "userData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserData", "()Lcom/asiantimes/model/CommonModel$UserDetail;", "setUserData", "(Lcom/asiantimes/model/CommonModel$UserDetail;)V", "viewModel", "Lcom/asiantimes/digital_addition/ui/home/DigitalHomeViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/home/DigitalHomeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "checkSubscriptionPlanAPI", "", "clearPreference", "getEditionCategoryAPI", "getEditionListAPI", "getPremiumPostArray", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onReceiveResult", "resultCode", "", "resultData", "onResume", "setProfile", "viewPagerSetup", "app_productionDebug"})
public final class DigitalHomeActivity extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener, com.asiantimes.services.BackgroundApiResultReceiver.Receiver {
    private com.asiantimes.databinding.ActivityDigitalHomeBinding bindingView;
    private com.asiantimes.databinding.DigitalDrawerBinding bindingViewDrawer;
    private com.asiantimes.digital_addition.database.AppDatabase localDB;
    private com.asiantimes.digital_addition.ui.home.DigitalHomePagerAdapter digitalHomePagerAdapter;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.services.BackgroundApiResultReceiver mReceiver;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.model.CommonModel.UserDetail userData;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.asiantimes.database.AppDatabase appDatabase;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.services.BackgroundApiResultReceiver getMReceiver() {
        return null;
    }
    
    public final void setMReceiver(@org.jetbrains.annotations.Nullable()
    com.asiantimes.services.BackgroundApiResultReceiver p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.model.CommonModel.UserDetail getUserData() {
        return null;
    }
    
    public final void setUserData(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.CommonModel.UserDetail p0) {
    }
    
    private final com.asiantimes.digital_addition.ui.home.DigitalHomeViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onReceiveResult(int resultCode, @org.jetbrains.annotations.NotNull()
    android.os.Bundle resultData) {
    }
    
    private final void viewPagerSetup() {
    }
    
    private final void setProfile() {
    }
    
    private final void clearPreference() {
    }
    
    private final void getEditionCategoryAPI() {
    }
    
    private final void getEditionListAPI() {
    }
    
    private final void getPremiumPostArray() {
    }
    
    private final void checkSubscriptionPlanAPI() {
    }
    
    public DigitalHomeActivity() {
        super();
    }
}