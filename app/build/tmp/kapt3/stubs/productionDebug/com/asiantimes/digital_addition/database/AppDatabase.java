package com.asiantimes.digital_addition.database;

import java.lang.System;

@androidx.room.TypeConverters(value = {com.asiantimes.digital_addition.database.converter.DataConverter.class, com.asiantimes.digital_addition.database.converter.HomeCategoriesConverter.class})
@androidx.room.Database(entities = {com.asiantimes.model.GetAllCategoriesModel.Post.class, com.asiantimes.digital_addition.model.EditionCategoryModel.DigitalEditionCategoryList.class, com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList.class, com.asiantimes.digital_addition.model.EditionPostListModel.class}, version = 2, exportSchema = false)
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\bH&\u00a8\u0006\n"}, d2 = {"Lcom/asiantimes/digital_addition/database/AppDatabase;", "Landroidx/room/RoomDatabase;", "()V", "editionCategoryDao", "Lcom/asiantimes/digital_addition/database/dao/EditionCategoryDao;", "editionListDao", "Lcom/asiantimes/digital_addition/database/dao/EditionListDao;", "editionPostListDao", "Lcom/asiantimes/digital_addition/database/dao/EditionPostListDao;", "Companion", "app_productionDebug"})
public abstract class AppDatabase extends androidx.room.RoomDatabase {
    private static volatile com.asiantimes.digital_addition.database.AppDatabase INSTANCE;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.digital_addition.database.AppDatabase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.asiantimes.digital_addition.database.dao.EditionCategoryDao editionCategoryDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.asiantimes.digital_addition.database.dao.EditionListDao editionListDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.asiantimes.digital_addition.database.dao.EditionPostListDao editionPostListDao();
    
    public AppDatabase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/asiantimes/digital_addition/database/AppDatabase$Companion;", "", "()V", "INSTANCE", "Lcom/asiantimes/digital_addition/database/AppDatabase;", "getDatabase", "context", "Landroid/content/Context;", "app_productionDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.digital_addition.database.AppDatabase getDatabase(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}