package com.asiantimes.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B%\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\u0012\u0010\u001b\u001a\u00020\u00162\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J&\u0010\u001e\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u001a\u0010%\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0018\u0010&\u001a\u00020\u00162\u0006\u0010\'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u001aH\u0002J\u0010\u0010)\u001a\u00020\u00162\u0006\u0010*\u001a\u00020\u0007H\u0002J\u001a\u0010+\u001a\u00020\u00162\b\u0010\u0005\u001a\u0004\u0018\u00010,2\u0006\u0010-\u001a\u00020.H\u0002J\u000e\u0010/\u001a\u00020\u00162\u0006\u00100\u001a\u000201R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012\u00a8\u00062"}, d2 = {"Lcom/asiantimes/ui/detail/NewsDetailFragment;", "Lcom/asiantimes/base/BaseFragment;", "Landroid/view/View$OnClickListener;", "activytType", "", "newsDetailData", "", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "position", "", "(Ljava/lang/String;Ljava/util/List;I)V", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/FragmentNewsDetailBinding;", "viewModel", "Lcom/asiantimes/ui/detail/NewsDetailsViewModel;", "getViewModel", "()Lcom/asiantimes/ui/detail/NewsDetailsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindBookMark", "", "bindDetails", "bindPremium", "checkLoginStatus", "", "onClick", "view", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "saveBookmarkAPI", "postId", "isBookmark", "saveBookmarkObserver", "data", "setDetailData", "Lcom/asiantimes/model/GetTopStoriesModel$PostDetail;", "contentParams", "Landroid/widget/LinearLayout$LayoutParams;", "setFontSetText", "fontValue", "", "app_productionDebug"})
public final class NewsDetailFragment extends com.asiantimes.base.BaseFragment implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.FragmentNewsDetailBinding bindingView;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.asiantimes.database.AppDatabase appDatabase;
    private java.lang.String activytType;
    private final java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> newsDetailData = null;
    private final int position = 0;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.detail.NewsDetailsViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setFontSetText(float fontValue) {
    }
    
    private final void bindDetails() {
    }
    
    private final void setDetailData(com.asiantimes.model.GetTopStoriesModel.PostDetail newsDetailData, android.widget.LinearLayout.LayoutParams contentParams) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void saveBookmarkAPI(java.lang.String postId, boolean isBookmark) {
    }
    
    private final void saveBookmarkObserver(com.asiantimes.model.GetTopStoriesModel.PostList data) {
    }
    
    private final void bindBookMark() {
    }
    
    private final void bindPremium() {
    }
    
    private final boolean checkLoginStatus() {
        return false;
    }
    
    public NewsDetailFragment(@org.jetbrains.annotations.NotNull()
    java.lang.String activytType, @org.jetbrains.annotations.NotNull()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> newsDetailData, int position) {
        super();
    }
}