package com.asiantimes.dialog;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/asiantimes/dialog/DialogOK;", "Landroid/app/Dialog;", "title", "", "context", "Landroid/content/Context;", "onClickListener", "Lcom/asiantimes/interfaces/OnClickListener;", "(Ljava/lang/String;Landroid/content/Context;Lcom/asiantimes/interfaces/OnClickListener;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_productionDebug"})
public final class DialogOK extends android.app.Dialog {
    private final java.lang.String title = null;
    private final com.asiantimes.interfaces.OnClickListener onClickListener = null;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public DialogOK(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickListener onClickListener) {
        super(null);
    }
}