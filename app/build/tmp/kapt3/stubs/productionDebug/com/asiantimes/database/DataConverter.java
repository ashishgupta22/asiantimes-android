package com.asiantimes.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u0007J\u001a\u0010\b\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0006H\u0007J\u001a\u0010\n\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0006H\u0007J\u001a\u0010\f\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0006H\u0007J\u001a\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007J\u001a\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007J\u001a\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007J\u001a\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/asiantimes/database/DataConverter;", "Ljava/io/Serializable;", "()V", "fromOptionValuesList", "", "optionValues", "", "Lcom/asiantimes/model/GetAllCategoriesModel$Post$SubCatArr;", "fromOptionValuesList1", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "fromOptionValuesList2", "Lcom/asiantimes/model/GetTopStoriesModel$CategoryList;", "fromOptionValuesList3", "Lcom/asiantimes/model/GetTopStoriesModel$PostDetail;", "toOptionValuesList", "optionValuesString", "toOptionValuesList1", "toOptionValuesList2", "toOptionValuesList3", "app_productionDebug"})
public final class DataConverter implements java.io.Serializable {
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String fromOptionValuesList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post.SubCatArr> optionValues) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post.SubCatArr> toOptionValuesList(@org.jetbrains.annotations.Nullable()
    java.lang.String optionValuesString) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String fromOptionValuesList1(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> optionValues) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> toOptionValuesList1(@org.jetbrains.annotations.Nullable()
    java.lang.String optionValuesString) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String fromOptionValuesList2(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.CategoryList> optionValues) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModel.CategoryList> toOptionValuesList2(@org.jetbrains.annotations.Nullable()
    java.lang.String optionValuesString) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String fromOptionValuesList3(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostDetail> optionValues) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModel.PostDetail> toOptionValuesList3(@org.jetbrains.annotations.Nullable()
    java.lang.String optionValuesString) {
        return null;
    }
    
    public DataConverter() {
        super();
    }
}