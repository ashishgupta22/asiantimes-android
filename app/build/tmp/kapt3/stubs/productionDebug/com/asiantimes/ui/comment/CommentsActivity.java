package com.asiantimes.ui.comment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010+\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.H\u0014J\b\u0010/\u001a\u00020,H\u0014J\u001a\u00100\u001a\u00020,2\b\u00101\u001a\u0004\u0018\u00010\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017J\b\u00102\u001a\u00020,H\u0002J\u001a\u00103\u001a\u00020,2\b\u00101\u001a\u0004\u0018\u00010\u00172\b\u00104\u001a\u0004\u0018\u00010\u0017J\u0010\u00105\u001a\u00020,2\u0006\u00106\u001a\u000207H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u0010\u0010&\u001a\u0004\u0018\u00010\'X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010\'X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"}, d2 = {"Lcom/asiantimes/ui/comment/CommentsActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "commentAdapter", "Lcom/asiantimes/ui/comment/CommentAdapter;", "commentsArrayList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/InsertCommentsModel;", "edt_reply_comment", "Landroid/widget/EditText;", "gif_loader_comments", "Lcom/asiantimes/widget/GifImageView;", "ll_error_message_comments", "Landroid/widget/LinearLayout;", "ll_gif_comments", "Landroid/widget/RelativeLayout;", "ll_sendMsg", "getLl_sendMsg", "()Landroid/widget/LinearLayout;", "setLl_sendMsg", "(Landroid/widget/LinearLayout;)V", "myCommentsArrayList", "nextPage", "", "rv_replyList", "Landroidx/recyclerview/widget/RecyclerView;", "scrollView", "Landroidx/core/widget/NestedScrollView;", "getScrollView", "()Landroidx/core/widget/NestedScrollView;", "setScrollView", "(Landroidx/core/widget/NestedScrollView;)V", "tabLayout", "Lcom/google/android/material/tabs/TabLayout;", "getTabLayout", "()Lcom/google/android/material/tabs/TabLayout;", "setTabLayout", "(Lcom/google/android/material/tabs/TabLayout;)V", "title", "Landroid/widget/TextView;", "tv_no_data_comments", "viewModel", "Lcom/asiantimes/ui/comment/CommentViewModel;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "setCommentList", "postId", "setCommentListObserve", "setCommentReply", "comment", "setCommentsListData", "responseResult", "Lcom/asiantimes/model/ResponseResult;", "app_productionDebug"})
public final class CommentsActivity extends com.asiantimes.base.BaseActivity {
    private androidx.recyclerview.widget.RecyclerView rv_replyList;
    private java.util.ArrayList<com.asiantimes.model.InsertCommentsModel> commentsArrayList;
    private java.util.ArrayList<com.asiantimes.model.InsertCommentsModel> myCommentsArrayList;
    private com.asiantimes.ui.comment.CommentAdapter commentAdapter;
    private android.widget.TextView tv_no_data_comments;
    private android.widget.RelativeLayout ll_gif_comments;
    private com.asiantimes.widget.GifImageView gif_loader_comments;
    private android.widget.LinearLayout ll_error_message_comments;
    private java.lang.String nextPage = "1";
    private android.widget.EditText edt_reply_comment;
    private android.widget.TextView title;
    @org.jetbrains.annotations.Nullable()
    private androidx.core.widget.NestedScrollView scrollView;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.material.tabs.TabLayout tabLayout;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout ll_sendMsg;
    private final com.asiantimes.ui.comment.CommentViewModel viewModel = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.core.widget.NestedScrollView getScrollView() {
        return null;
    }
    
    public final void setScrollView(@org.jetbrains.annotations.Nullable()
    androidx.core.widget.NestedScrollView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.material.tabs.TabLayout getTabLayout() {
        return null;
    }
    
    public final void setTabLayout(@org.jetbrains.annotations.Nullable()
    com.google.android.material.tabs.TabLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLl_sendMsg() {
        return null;
    }
    
    public final void setLl_sendMsg(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setCommentListObserve() {
    }
    
    public final void setCommentList(@org.jetbrains.annotations.Nullable()
    java.lang.String postId, @org.jetbrains.annotations.Nullable()
    java.lang.String nextPage) {
    }
    
    private final void setCommentsListData(com.asiantimes.model.ResponseResult responseResult) {
    }
    
    public final void setCommentReply(@org.jetbrains.annotations.Nullable()
    java.lang.String postId, @org.jetbrains.annotations.Nullable()
    java.lang.String comment) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public CommentsActivity() {
        super();
    }
}