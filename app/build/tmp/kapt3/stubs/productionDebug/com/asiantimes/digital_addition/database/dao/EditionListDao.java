package com.asiantimes.digital_addition.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\tH\'J\u0018\u0010\n\u001a\u00020\u00032\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\tH\'\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/digital_addition/database/dao/EditionListDao;", "", "delete", "", "getEditionById", "Lcom/asiantimes/digital_addition/model/EditionListModel$DigitalEditionList;", "productId", "", "getEditionList", "", "insert", "editionListEntity", "app_productionDebug"})
public abstract interface EditionListDao {
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList> editionListEntity);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from edition_list_table")
    public abstract java.util.List<com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList> getEditionList();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from edition_list_table where productId in (:productId)")
    public abstract com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList getEditionById(int productId);
    
    @androidx.room.Query(value = "delete from edition_list_table")
    public abstract void delete();
}