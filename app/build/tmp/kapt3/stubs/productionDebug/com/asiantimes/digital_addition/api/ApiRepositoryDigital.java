package com.asiantimes.digital_addition.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0004J\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007J$\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020#\u00a8\u0006$"}, d2 = {"Lcom/asiantimes/digital_addition/api/ApiRepositoryDigital;", "", "()V", "addBookmark", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/CommonModel;", "requestBean", "Lcom/asiantimes/model/RequestBean;", "addTransaction", "Lcom/asiantimes/model/ResponseResult;", "buySingleProduct", "Lcom/asiantimes/digital_addition/model/BuySingleProductModel;", "checkSubscriptionPlan", "Lcom/asiantimes/digital_addition/model/CheckSubscriptionModel;", "getATDigitalEditionBookmarks", "Lcom/asiantimes/digital_addition/model/BookmarksModel;", "getATDigitalEditionList", "Lcom/asiantimes/digital_addition/model/EditionListModel;", "getATDigitalEditionPostList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "getDigitalEditionCategory", "Lcom/asiantimes/digital_addition/model/EditionCategoryModel;", "getPremiumPostRequestNew", "getSearchResult", "Lcom/asiantimes/digital_addition/model/SearchModel;", "getShippingPrice", "getSpecialSubscription", "getSubScriptionPlan", "getSubScriptionPlanDetails", "setSubmitPayment", "submitFeedback", "yourReview", "Lokhttp3/RequestBody;", "yourEmail", "body", "Lokhttp3/MultipartBody$Part;", "app_productionDebug"})
public final class ApiRepositoryDigital {
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.digital_addition.api.ApiRepositoryDigital INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionListModel> getATDigitalEditionList(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.CheckSubscriptionModel> checkSubscriptionPlan(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionCategoryModel> getDigitalEditionCategory() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionPostListModel> getATDigitalEditionPostList(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getSubScriptionPlanDetails(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getSpecialSubscription(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> addTransaction(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getPremiumPostRequestNew(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.SearchModel> getSearchResult(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> submitFeedback(@org.jetbrains.annotations.NotNull()
    okhttp3.RequestBody yourReview, @org.jetbrains.annotations.NotNull()
    okhttp3.RequestBody yourEmail, @org.jetbrains.annotations.NotNull()
    okhttp3.MultipartBody.Part body) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getSubScriptionPlan(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> setSubmitPayment(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getShippingPrice(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.BookmarksModel> getATDigitalEditionBookmarks(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> addBookmark(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.BuySingleProductModel> buySingleProduct(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    private ApiRepositoryDigital() {
        super();
    }
}