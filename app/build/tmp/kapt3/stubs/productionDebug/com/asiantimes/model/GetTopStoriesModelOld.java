package com.asiantimes.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000e\u0018\u00002\u00020\u0001:\u0003 !\"B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u001e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007R\u001e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0007R\"\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0013\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R \u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0007R\u001e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0007R\u001e\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0007\u00a8\u0006#"}, d2 = {"Lcom/asiantimes/model/GetTopStoriesModelOld;", "Ljava/io/Serializable;", "()V", "PostLista", "", "Lcom/asiantimes/model/GetTopStoriesModelOld$PostList;", "getPostLista", "()Ljava/util/List;", "breakingNews", "getBreakingNews", "cateList", "Lcom/asiantimes/model/GetTopStoriesModelOld$CategoryList;", "getCateList", "errorCode", "", "getErrorCode", "()Ljava/lang/Integer;", "setErrorCode", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "msg", "", "getMsg", "()Ljava/lang/String;", "setMsg", "(Ljava/lang/String;)V", "postList", "getPostList", "searchDataList", "getSearchDataList", "trendingNews", "getTrendingNews", "CategoryList", "PostDetail", "PostList", "app_productionDebug"})
public final class GetTopStoriesModelOld implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msg")
    private java.lang.String msg;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "errorCode")
    private java.lang.Integer errorCode;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "breakingnews")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> breakingNews = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postList")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> postList = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "PostList")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> PostLista = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "headlinestories")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> trendingNews = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "CateList")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.CategoryList> cateList = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "searchDataList")
    private final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> searchDataList = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMsg() {
        return null;
    }
    
    public final void setMsg(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getErrorCode() {
        return null;
    }
    
    public final void setErrorCode(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getBreakingNews() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getPostList() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getPostLista() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getTrendingNews() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.CategoryList> getCateList() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getSearchDataList() {
        return null;
    }
    
    public GetTopStoriesModelOld() {
        super();
    }
    
    @androidx.room.Entity(tableName = "home_page_breaking_news_table")
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\"\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0014\u001a\u0004\b\u000f\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\"\u0010\u0015\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0014\u001a\u0004\b\u0015\u0010\u0011\"\u0004\b\u0016\u0010\u0013R \u0010\u0017\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR \u0010\u001d\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001a\"\u0004\b\u001f\u0010\u001cR \u0010 \u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u001a\"\u0004\b\"\u0010\u001cR&\u0010#\u001a\n\u0012\u0004\u0012\u00020%\u0018\u00010$8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R \u0010*\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u001a\"\u0004\b,\u0010\u001cR \u0010-\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u001a\"\u0004\b/\u0010\u001cR \u00100\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u001a\"\u0004\b2\u0010\u001cR \u00103\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u001a\"\u0004\b5\u0010\u001c\u00a8\u00066"}, d2 = {"Lcom/asiantimes/model/GetTopStoriesModelOld$PostList;", "Ljava/io/Serializable;", "()V", "autoGenerateId", "", "getAutoGenerateId", "()I", "setAutoGenerateId", "(I)V", "errorCode", "getErrorCode", "()Ljava/lang/Integer;", "setErrorCode", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "isBookmark", "", "()Ljava/lang/Boolean;", "setBookmark", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "isPremium", "setPremium", "msg", "", "getMsg", "()Ljava/lang/String;", "setMsg", "(Ljava/lang/String;)V", "postCatName", "getPostCatName", "setPostCatName", "postDate", "getPostDate", "setPostDate", "postDetail", "", "Lcom/asiantimes/model/GetTopStoriesModelOld$PostDetail;", "getPostDetail", "()Ljava/util/List;", "setPostDetail", "(Ljava/util/List;)V", "postId", "getPostId", "setPostId", "postImage", "getPostImage", "setPostImage", "postTitle", "getPostTitle", "setPostTitle", "postUrl", "getPostUrl", "setPostUrl", "app_productionDebug"})
    public static final class PostList implements java.io.Serializable {
        @androidx.room.PrimaryKey(autoGenerate = true)
        private int autoGenerateId = 0;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "msg")
        private java.lang.String msg;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "errorCode")
        private java.lang.Integer errorCode;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postId")
        private java.lang.String postId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postUrl")
        private java.lang.String postUrl;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "isPremium")
        private java.lang.Boolean isPremium;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postTitle")
        private java.lang.String postTitle;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postDate")
        private java.lang.String postDate;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postImage")
        private java.lang.String postImage;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postCatName")
        private java.lang.String postCatName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "isBookmark")
        private java.lang.Boolean isBookmark;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "post_content")
        private java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostDetail> postDetail;
        
        public final int getAutoGenerateId() {
            return 0;
        }
        
        public final void setAutoGenerateId(int p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMsg() {
            return null;
        }
        
        public final void setMsg(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getErrorCode() {
            return null;
        }
        
        public final void setErrorCode(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostId() {
            return null;
        }
        
        public final void setPostId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostUrl() {
            return null;
        }
        
        public final void setPostUrl(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean isPremium() {
            return null;
        }
        
        public final void setPremium(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostTitle() {
            return null;
        }
        
        public final void setPostTitle(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostDate() {
            return null;
        }
        
        public final void setPostDate(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostImage() {
            return null;
        }
        
        public final void setPostImage(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostCatName() {
            return null;
        }
        
        public final void setPostCatName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean isBookmark() {
            return null;
        }
        
        public final void setBookmark(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostDetail> getPostDetail() {
            return null;
        }
        
        public final void setPostDetail(@org.jetbrains.annotations.Nullable()
        java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostDetail> p0) {
        }
        
        public PostList() {
            super();
        }
    }
    
    @androidx.room.Entity(tableName = "home_page_news_with_cat_table")
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R&\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00168\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001b\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/model/GetTopStoriesModelOld$CategoryList;", "", "()V", "CatColor", "", "getCatColor", "()Ljava/lang/String;", "setCatColor", "(Ljava/lang/String;)V", "CatID", "getCatID", "setCatID", "CatName", "getCatName", "setCatName", "autoGenerateId", "", "getAutoGenerateId", "()I", "setAutoGenerateId", "(I)V", "postList", "", "Lcom/asiantimes/model/GetTopStoriesModelOld$PostList;", "getPostList", "()Ljava/util/List;", "setPostList", "(Ljava/util/List;)V", "app_productionDebug"})
    public static final class CategoryList {
        @androidx.room.PrimaryKey(autoGenerate = true)
        private int autoGenerateId = 0;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "CatID")
        private java.lang.String CatID;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "CatName")
        private java.lang.String CatName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "CatColor")
        private java.lang.String CatColor;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postList")
        private java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> postList;
        
        public final int getAutoGenerateId() {
            return 0;
        }
        
        public final void setAutoGenerateId(int p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCatID() {
            return null;
        }
        
        public final void setCatID(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCatName() {
            return null;
        }
        
        public final void setCatName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCatColor() {
            return null;
        }
        
        public final void setCatColor(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> getPostList() {
            return null;
        }
        
        public final void setPostList(@org.jetbrains.annotations.Nullable()
        java.util.List<com.asiantimes.model.GetTopStoriesModelOld.PostList> p0) {
        }
        
        public CategoryList() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0010H\u0016R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR \u0010\f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\t\"\u0004\b\u000e\u0010\u000b\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/model/GetTopStoriesModelOld$PostDetail;", "Landroid/os/Parcelable;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "key", "", "getKey", "()Ljava/lang/String;", "setKey", "(Ljava/lang/String;)V", "types", "getTypes", "setTypes", "describeContents", "", "writeToParcel", "", "flags", "CREATOR", "app_productionDebug"})
    public static final class PostDetail implements android.os.Parcelable {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "key")
        private java.lang.String key;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "types")
        private java.lang.String types;
        @org.jetbrains.annotations.NotNull()
        public static final com.asiantimes.model.GetTopStoriesModelOld.PostDetail.CREATOR CREATOR = null;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getKey() {
            return null;
        }
        
        public final void setKey(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getTypes() {
            return null;
        }
        
        public final void setTypes(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @java.lang.Override()
        public void writeToParcel(@org.jetbrains.annotations.NotNull()
        android.os.Parcel parcel, int flags) {
        }
        
        @java.lang.Override()
        public int describeContents() {
            return 0;
        }
        
        public PostDetail() {
            super();
        }
        
        public PostDetail(@org.jetbrains.annotations.NotNull()
        android.os.Parcel parcel) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/model/GetTopStoriesModelOld$PostDetail$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lcom/asiantimes/model/GetTopStoriesModelOld$PostDetail;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", "size", "", "(I)[Lcom/asiantimes/model/GetTopStoriesModelOld$PostDetail;", "app_productionDebug"})
        public static final class CREATOR implements android.os.Parcelable.Creator<com.asiantimes.model.GetTopStoriesModelOld.PostDetail> {
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public com.asiantimes.model.GetTopStoriesModelOld.PostDetail createFromParcel(@org.jetbrains.annotations.NotNull()
            android.os.Parcel parcel) {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public com.asiantimes.model.GetTopStoriesModelOld.PostDetail[] newArray(int size) {
                return null;
            }
            
            private CREATOR() {
                super();
            }
        }
    }
}