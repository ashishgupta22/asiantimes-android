package com.asiantimes.ui.dashboard.video;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0016\u0010\u0013\u001a\u00020\f2\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/NewVideoAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/dashboard/video/NewVideoAdapter$ViewHolder;", "OnClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "(Lcom/asiantimes/interfaces/OnClickPositionViewListener;)V", "cateList", "", "Lcom/asiantimes/model/GetVideoListModel$NewsListBean;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class NewVideoAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.dashboard.video.NewVideoAdapter.ViewHolder> {
    private java.util.List<com.asiantimes.model.GetVideoListModel.NewsListBean> cateList;
    private final com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.ui.dashboard.video.NewVideoAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.dashboard.video.NewVideoAdapter.ViewHolder holder, int position) {
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetVideoListModel.NewsListBean> cateList) {
    }
    
    public NewVideoAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/NewVideoAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterNewVideoBinding;", "(Lcom/asiantimes/databinding/AdapterNewVideoBinding;)V", "bind", "", "OnClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "cateList", "Lcom/asiantimes/model/GetVideoListModel$NewsListBean;", "parentPosition", "", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.asiantimes.databinding.AdapterNewVideoBinding bindingView = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener, @org.jetbrains.annotations.NotNull()
        com.asiantimes.model.GetVideoListModel.NewsListBean cateList, int parentPosition) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterNewVideoBinding bindingView) {
            super(null);
        }
    }
}