package com.asiantimes.ui.contact_us;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0010\u001a\u00020\u0011J\u0012\u0010\u0012\u001a\u00020\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r\u00a8\u0006\u0016"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactUsActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "getConatctUs", "", "Lcom/asiantimes/model/ConatctU;", "getGetConatctUs", "()Ljava/util/List;", "setGetConatctUs", "(Ljava/util/List;)V", "viewModel", "Lcom/asiantimes/ui/contact_us/ContactUsViewModel;", "getViewModel", "()Lcom/asiantimes/ui/contact_us/ContactUsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getContactDetails", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "PagerAdapter", "app_productionDebug"})
public final class ContactUsActivity extends com.asiantimes.base.BaseActivity {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.asiantimes.model.ConatctU> getConatctUs;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.asiantimes.model.ConatctU> getGetConatctUs() {
        return null;
    }
    
    public final void setGetConatctUs(@org.jetbrains.annotations.NotNull()
    java.util.List<com.asiantimes.model.ConatctU> p0) {
    }
    
    private final com.asiantimes.ui.contact_us.ContactUsViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void getContactDetails() {
    }
    
    public ContactUsActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0015H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u0018\u001a\u00020\u0015H\u0016R\u001a\u0010\n\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001b"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactUsActivity$PagerAdapter;", "Landroidx/fragment/app/FragmentStatePagerAdapter;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "conatctUs", "", "Lcom/asiantimes/model/ConatctU;", "address", "", "(Landroidx/fragment/app/FragmentManager;Ljava/util/List;Ljava/lang/String;)V", "addressData", "getAddressData", "()Ljava/lang/String;", "setAddressData", "(Ljava/lang/String;)V", "listContact", "getListContact", "()Ljava/util/List;", "setListContact", "(Ljava/util/List;)V", "getCount", "", "getItem", "Landroidx/fragment/app/Fragment;", "position", "getPageTitle", "", "app_productionDebug"})
    public static final class PagerAdapter extends androidx.fragment.app.FragmentStatePagerAdapter {
        @org.jetbrains.annotations.NotNull()
        private java.util.List<com.asiantimes.model.ConatctU> listContact;
        @org.jetbrains.annotations.NotNull()
        private java.lang.String addressData;
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.asiantimes.model.ConatctU> getListContact() {
            return null;
        }
        
        public final void setListContact(@org.jetbrains.annotations.NotNull()
        java.util.List<com.asiantimes.model.ConatctU> p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getAddressData() {
            return null;
        }
        
        public final void setAddressData(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public androidx.fragment.app.Fragment getItem(int position) {
            return null;
        }
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.CharSequence getPageTitle(int position) {
            return null;
        }
        
        public PagerAdapter(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.FragmentManager fragmentManager, @org.jetbrains.annotations.NotNull()
        java.util.List<com.asiantimes.model.ConatctU> conatctUs, @org.jetbrains.annotations.NotNull()
        java.lang.String address) {
            super(null);
        }
    }
}