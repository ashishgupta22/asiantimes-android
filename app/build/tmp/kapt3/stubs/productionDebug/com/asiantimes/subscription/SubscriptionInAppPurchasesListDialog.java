package com.asiantimes.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 D2\u00020\u00012\u00020\u0002:\u0002DEB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010,\u001a\u00020\u00102\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0002J\u0006\u00101\u001a\u00020\u0010J\"\u00102\u001a\u00020\u00102\u0006\u00103\u001a\u0002002\u0006\u00104\u001a\u0002002\b\u00105\u001a\u0004\u0018\u000106H\u0014J\b\u00107\u001a\u00020\u0010H\u0016J\u0010\u00108\u001a\u00020\u00102\u0006\u00109\u001a\u00020:H\u0016J\u0012\u0010;\u001a\u00020\u00102\b\u0010<\u001a\u0004\u0018\u00010=H\u0014J\b\u0010>\u001a\u00020\u0010H\u0016J\b\u0010?\u001a\u00020\u0010H\u0002J$\u0010@\u001a\u00020\u00102\b\u0010A\u001a\u0004\u0018\u00010!2\b\u0010B\u001a\u0004\u0018\u00010!2\b\u0010C\u001a\u0004\u0018\u00010!R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+\u00a8\u0006F"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "getSubPlanUnit", "", "Lcom/asiantimes/subscriptionmodel/SubPlanUnit;", "imageviewCloseSubscriptionDialog", "Landroid/widget/ImageView;", "premiumPostArray", "", "getPremiumPostArray", "()Lkotlin/Unit;", "progress_bar", "Landroid/widget/ProgressBar;", "getProgress_bar", "()Landroid/widget/ProgressBar;", "setProgress_bar", "(Landroid/widget/ProgressBar;)V", "readyToPurchase", "", "recyclerViewPlanList", "Landroidx/recyclerview/widget/RecyclerView;", "subscriptionPlanData", "", "Lcom/asiantimes/subscriptionmodel/SubscriptionInAppPlanData;", "title", "", "getTitle", "()Ljava/lang/String;", "setTitle", "(Ljava/lang/String;)V", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "setViewModel", "(Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;)V", "getSubscription", "activity", "Landroid/app/Activity;", "position", "", "initializationView", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "redirectToMainActivity", "setReadyToPurchase", "transactionId", "productId", "deviceType", "Companion", "SubPlanUnitAdapter", "app_productionDebug"})
public final class SubscriptionInAppPurchasesListDialog extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    private boolean readyToPurchase = false;
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subscriptionPlanData = null;
    private java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> getSubPlanUnit;
    @org.jetbrains.annotations.Nullable()
    private android.widget.ProgressBar progress_bar;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String title;
    private android.widget.ImageView imageviewCloseSubscriptionDialog;
    private androidx.recyclerview.widget.RecyclerView recyclerViewPlanList;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.subscription.viewmodel.SubscriptionViewModel viewModel;
    private static final java.lang.String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB";
    private static final java.lang.String MERCHANT_ID = "15638088474552299429";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.subscription.SubscriptionInAppPurchasesListDialog.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ProgressBar getProgress_bar() {
        return null;
    }
    
    public final void setProgress_bar(@org.jetbrains.annotations.Nullable()
    android.widget.ProgressBar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTitle() {
        return null;
    }
    
    public final void setTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.asiantimes.subscription.viewmodel.SubscriptionViewModel p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initializationView() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final void setReadyToPurchase(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionId, @org.jetbrains.annotations.Nullable()
    java.lang.String productId, @org.jetbrains.annotations.Nullable()
    java.lang.String deviceType) {
    }
    
    private final kotlin.Unit getPremiumPostArray() {
        return null;
    }
    
    private final void redirectToMainActivity() {
    }
    
    private final void getSubscription(android.app.Activity activity, int position) {
    }
    
    public SubscriptionInAppPurchasesListDialog() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\u0010\u0012\f\u0012\n0\u0002R\u00060\u0000R\u00020\u00030\u0001:\u0001\u0016B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\f\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\u000f2\u000e\u0010\u0010\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0011\u001a\u00020\rH\u0016J \u0010\u0012\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016R \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0017"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog$SubPlanUnitAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog$SubPlanUnitAdapter$MyViewHolder;", "Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog;", "subPlanUnit", "", "Lcom/asiantimes/subscriptionmodel/SubscriptionInAppPlanData;", "(Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog;Ljava/util/List;)V", "getSubPlanUnit", "()Ljava/util/List;", "setSubPlanUnit", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MyViewHolder", "app_productionDebug"})
    public final class SubPlanUnitAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.subscription.SubscriptionInAppPurchasesListDialog.SubPlanUnitAdapter.MyViewHolder> {
        @org.jetbrains.annotations.NotNull()
        private java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subPlanUnit;
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> getSubPlanUnit() {
            return null;
        }
        
        public final void setSubPlanUnit(@org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.subscription.SubscriptionInAppPurchasesListDialog.SubPlanUnitAdapter.MyViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.subscription.SubscriptionInAppPurchasesListDialog.SubPlanUnitAdapter.MyViewHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        public SubPlanUnitAdapter(@org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subPlanUnit) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog$SubPlanUnitAdapter$MyViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog$SubPlanUnitAdapter;Landroid/view/View;)V", "btnPlanUnit", "Lcom/asiantimes/widget/TextViewRegularBold;", "getBtnPlanUnit", "()Lcom/asiantimes/widget/TextViewRegularBold;", "txtPlanPrice", "getTxtPlanPrice", "getView", "()Landroid/view/View;", "setView", "(Landroid/view/View;)V", "app_productionDebug"})
        public final class MyViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold txtPlanPrice = null;
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold btnPlanUnit = null;
            @org.jetbrains.annotations.NotNull()
            private android.view.View view;
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getTxtPlanPrice() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getBtnPlanUnit() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.view.View getView() {
                return null;
            }
            
            public final void setView(@org.jetbrains.annotations.NotNull()
            android.view.View p0) {
            }
            
            public MyViewHolder(@org.jetbrains.annotations.NotNull()
            android.view.View view) {
                super(null);
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesListDialog$Companion;", "", "()V", "LICENSE_KEY", "", "MERCHANT_ID", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}