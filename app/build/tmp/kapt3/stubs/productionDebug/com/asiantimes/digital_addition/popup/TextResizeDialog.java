package com.asiantimes.digital_addition.popup;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/asiantimes/digital_addition/popup/TextResizeDialog;", "Landroid/app/Dialog;", "mContext", "Landroid/content/Context;", "onClickListener", "Lcom/asiantimes/interfaces/OnClickListener;", "(Landroid/content/Context;Lcom/asiantimes/interfaces/OnClickListener;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_productionDebug"})
public final class TextResizeDialog extends android.app.Dialog {
    private final android.content.Context mContext = null;
    private final com.asiantimes.interfaces.OnClickListener onClickListener = null;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public TextResizeDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickListener onClickListener) {
        super(null);
    }
}