package com.asiantimes.ui.dashboard.saved_stories;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u000fH\u0016J\u0018\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000fH\u0016J\u001e\u0010\u0018\u001a\u00020\u00112\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tJ&\u0010\u0018\u001a\u00020\u00112\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\t2\u0006\u0010\u0013\u001a\u00020\u000fR*\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/asiantimes/ui/dashboard/saved_stories/SavedStoriesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/dashboard/saved_stories/SavedStoriesAdapter$ViewHolder;", "onClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "(Lcom/asiantimes/interfaces/OnClickPositionViewListener;)V", "bookmarksList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "Lkotlin/collections/ArrayList;", "getBookmarksList", "()Ljava/util/ArrayList;", "setBookmarksList", "(Ljava/util/ArrayList;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class SavedStoriesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.dashboard.saved_stories.SavedStoriesAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> bookmarksList;
    private final com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> getBookmarksList() {
        return null;
    }
    
    public final void setBookmarksList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.ui.dashboard.saved_stories.SavedStoriesAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.dashboard.saved_stories.SavedStoriesAdapter.ViewHolder holder, int position) {
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> bookmarksList) {
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> bookmarksList, int position) {
    }
    
    public SavedStoriesAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickPositionViewListener onClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/asiantimes/ui/dashboard/saved_stories/SavedStoriesAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterSavedStoriesBinding;", "(Lcom/asiantimes/databinding/AdapterSavedStoriesBinding;)V", "bind", "", "bookmarkData", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "onClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.asiantimes.databinding.AdapterSavedStoriesBinding bindingView = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.asiantimes.model.GetTopStoriesModel.PostList bookmarkData, @org.jetbrains.annotations.NotNull()
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterSavedStoriesBinding bindingView) {
            super(null);
        }
    }
}