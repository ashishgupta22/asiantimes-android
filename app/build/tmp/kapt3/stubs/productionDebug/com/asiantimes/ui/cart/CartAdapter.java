package com.asiantimes.ui.cart;

import java.lang.System;

/**
 * Created by Abhijeet-PC on 06-Jan-18.
 */
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u001bB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0010\u001a\u00020\u000fH\u0016J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J \u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0019H\u0017J\b\u0010\u001a\u001a\u00020\u000fH\u0016R\u000e\u0010\u000b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/ui/cart/CartAdapter;", "Landroid/widget/BaseAdapter;", "mContext", "Landroid/content/Context;", "mInfoList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/CartItem;", "Lkotlin/collections/ArrayList;", "activity", "Lcom/asiantimes/ui/cart/CartActivity;", "(Landroid/content/Context;Ljava/util/ArrayList;Lcom/asiantimes/ui/cart/CartActivity;)V", "cartActivity", "dbHelper", "Lcom/asiantimes/database/AppDatabase;", "itemCount", "", "getCount", "getItem", "position", "getItemId", "", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "getViewTypeCount", "ViewHolder", "app_productionDebug"})
public final class CartAdapter extends android.widget.BaseAdapter {
    private int itemCount = 0;
    private final com.asiantimes.database.AppDatabase dbHelper = null;
    private final com.asiantimes.ui.cart.CartActivity cartActivity = null;
    private final android.content.Context mContext = null;
    private final java.util.ArrayList<com.asiantimes.model.CartItem> mInfoList = null;
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.model.CartItem getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    @java.lang.Override()
    public int getViewTypeCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.NotNull()
    android.view.View convertView, @org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    public CartAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.CartItem> mInfoList, @org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.cart.CartActivity activity) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0017\"\u0004\b\u001c\u0010\u0019R\u001a\u0010\u001d\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0017\"\u0004\b\u001f\u0010\u0019R\u001a\u0010 \u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0017\"\u0004\b\"\u0010\u0019\u00a8\u0006#"}, d2 = {"Lcom/asiantimes/ui/cart/CartAdapter$ViewHolder;", "", "view", "Landroid/view/View;", "(Lcom/asiantimes/ui/cart/CartAdapter;Landroid/view/View;)V", "ib_item_less_cart_adpter", "Landroid/widget/ImageButton;", "getIb_item_less_cart_adpter", "()Landroid/widget/ImageButton;", "setIb_item_less_cart_adpter", "(Landroid/widget/ImageButton;)V", "ib_item_more_cart_adpter", "getIb_item_more_cart_adpter", "setIb_item_more_cart_adpter", "iv_icon", "Landroid/widget/ImageView;", "getIv_icon", "()Landroid/widget/ImageView;", "setIv_icon", "(Landroid/widget/ImageView;)V", "tv_edition_item_cart", "Lcom/asiantimes/widget/TextViewRegular;", "getTv_edition_item_cart", "()Lcom/asiantimes/widget/TextViewRegular;", "setTv_edition_item_cart", "(Lcom/asiantimes/widget/TextViewRegular;)V", "tv_item_count_cart_adpter", "getTv_item_count_cart_adpter", "setTv_item_count_cart_adpter", "tv_item_title_cart", "getTv_item_title_cart", "setTv_item_title_cart", "tv_price_item_cart", "getTv_price_item_cart", "setTv_price_item_cart", "app_productionDebug"})
    public final class ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private android.widget.ImageView iv_icon;
        @org.jetbrains.annotations.NotNull()
        private com.asiantimes.widget.TextViewRegular tv_item_title_cart;
        @org.jetbrains.annotations.NotNull()
        private com.asiantimes.widget.TextViewRegular tv_edition_item_cart;
        @org.jetbrains.annotations.NotNull()
        private com.asiantimes.widget.TextViewRegular tv_item_count_cart_adpter;
        @org.jetbrains.annotations.NotNull()
        private com.asiantimes.widget.TextViewRegular tv_price_item_cart;
        @org.jetbrains.annotations.NotNull()
        private android.widget.ImageButton ib_item_less_cart_adpter;
        @org.jetbrains.annotations.NotNull()
        private android.widget.ImageButton ib_item_more_cart_adpter;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getIv_icon() {
            return null;
        }
        
        public final void setIv_icon(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.widget.TextViewRegular getTv_item_title_cart() {
            return null;
        }
        
        public final void setTv_item_title_cart(@org.jetbrains.annotations.NotNull()
        com.asiantimes.widget.TextViewRegular p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.widget.TextViewRegular getTv_edition_item_cart() {
            return null;
        }
        
        public final void setTv_edition_item_cart(@org.jetbrains.annotations.NotNull()
        com.asiantimes.widget.TextViewRegular p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.widget.TextViewRegular getTv_item_count_cart_adpter() {
            return null;
        }
        
        public final void setTv_item_count_cart_adpter(@org.jetbrains.annotations.NotNull()
        com.asiantimes.widget.TextViewRegular p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.widget.TextViewRegular getTv_price_item_cart() {
            return null;
        }
        
        public final void setTv_price_item_cart(@org.jetbrains.annotations.NotNull()
        com.asiantimes.widget.TextViewRegular p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageButton getIb_item_less_cart_adpter() {
            return null;
        }
        
        public final void setIb_item_less_cart_adpter(@org.jetbrains.annotations.NotNull()
        android.widget.ImageButton p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageButton getIb_item_more_cart_adpter() {
            return null;
        }
        
        public final void setIb_item_more_cart_adpter(@org.jetbrains.annotations.NotNull()
        android.widget.ImageButton p0) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super();
        }
    }
}