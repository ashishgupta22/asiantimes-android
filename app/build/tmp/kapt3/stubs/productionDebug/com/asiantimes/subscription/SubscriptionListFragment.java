package com.asiantimes.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00c8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u0088\u00012\u00020\u0001:\u0002\u0088\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J$\u0010i\u001a\u00020A2\b\u0010j\u001a\u0004\u0018\u00010\u00102\b\u0010k\u001a\u0004\u0018\u00010\u00102\b\u0010l\u001a\u0004\u0018\u00010\u0010J\b\u0010m\u001a\u00020AH\u0002J\u0018\u0010n\u001a\u00020A2\b\u0010o\u001a\u0004\u0018\u00010\u00102\u0006\u0010p\u001a\u00020!J*\u0010q\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010b\u001a\u0004\u0018\u00010\u00102\u0006\u0010 \u001a\u00020!2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\"\u0010r\u001a\u00020A2\u0006\u0010s\u001a\u00020\u00182\u0006\u0010t\u001a\u00020\u00182\b\u0010u\u001a\u0004\u0018\u00010vH\u0016J\u0012\u0010w\u001a\u00020A2\b\u0010x\u001a\u0004\u0018\u00010yH\u0016J&\u0010z\u001a\u0004\u0018\u00010{2\u0006\u0010|\u001a\u00020}2\b\u0010~\u001a\u0004\u0018\u00010\u007f2\b\u0010x\u001a\u0004\u0018\u00010yH\u0016J\t\u0010\u0080\u0001\u001a\u00020AH\u0016J\u001c\u0010\u0081\u0001\u001a\u00020A2\u0007\u0010\u0082\u0001\u001a\u00020{2\b\u0010x\u001a\u0004\u0018\u00010yH\u0016J\t\u0010\u0083\u0001\u001a\u00020AH\u0002J\u0013\u0010\u0084\u0001\u001a\u00020A2\b\u0010Y\u001a\u0004\u0018\u000105H\u0002J\u0012\u0010\u0085\u0001\u001a\u00020A2\u0007\u0010\u0086\u0001\u001a\u00020\u0010H\u0002J\t\u0010\u0087\u0001\u001a\u00020AH\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0019R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u000e\u0010 \u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010#\u001a\u0004\u0018\u00010$X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u0012\u0010)\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0019R\u0010\u0010*\u001a\u0004\u0018\u00010+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010,\u001a\u0004\u0018\u00010-X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010-X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010/\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\u001c\u00104\u001a\u0004\u0018\u000105X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u00107\"\u0004\b8\u00109R\u001c\u0010:\u001a\u0004\u0018\u00010;X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u0011\u0010@\u001a\u00020A8F\u00a2\u0006\u0006\u001a\u0004\bB\u0010CR\"\u0010D\u001a\n\u0012\u0004\u0012\u00020;\u0018\u00010EX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010G\"\u0004\bH\u0010IR\u000e\u0010J\u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010K\u001a\u0004\u0018\u00010LX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bM\u0010N\"\u0004\bO\u0010PR\u0014\u0010Q\u001a\u00020A8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\bR\u0010CR\u0014\u0010S\u001a\b\u0012\u0004\u0012\u00020U0TX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010V\u001a\u0004\u0018\u000105X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bW\u00107\"\u0004\bX\u00109R&\u0010Y\u001a\u001a\u0012\u0006\u0012\u0004\u0018\u000105\u0018\u00010Zj\f\u0012\u0006\u0012\u0004\u0018\u000105\u0018\u0001`[X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\\\u001a\u0004\u0018\u00010]X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010_\"\u0004\b`\u0010aR\u0010\u0010b\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010c\u001a\u00020d8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bg\u0010h\u001a\u0004\be\u0010f\u00a8\u0006\u0089\u0001"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionListFragment;", "Landroidx/fragment/app/Fragment;", "()V", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "buttonViewBuy", "Landroid/widget/Button;", "getButtonViewBuy", "()Landroid/widget/Button;", "setButtonViewBuy", "(Landroid/widget/Button;)V", "callFrom", "", "descriptionAdapter", "Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter;", "getDescriptionAdapter", "()Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter;", "setDescriptionAdapter", "(Lcom/asianmedia/easterneye/adapter/subscription/DescriptionAdapter;)V", "id", "", "Ljava/lang/Integer;", "imageViewSubscription", "Landroid/widget/ImageView;", "getImageViewSubscription", "()Landroid/widget/ImageView;", "setImageViewSubscription", "(Landroid/widget/ImageView;)V", "isuk", "", "packageType", "planChooseAdapter", "Lcom/asianmedia/easterneye/adapter/subscription/PlanChooseAdapter;", "getPlanChooseAdapter", "()Lcom/asianmedia/easterneye/adapter/subscription/PlanChooseAdapter;", "setPlanChooseAdapter", "(Lcom/asianmedia/easterneye/adapter/subscription/PlanChooseAdapter;)V", "pos", "progress_bar", "Landroid/widget/ProgressBar;", "recycleViewChoosePlans", "Landroidx/recyclerview/widget/RecyclerView;", "recycleViewDescription", "recycleViewSubscription", "getRecycleViewSubscription", "()Landroidx/recyclerview/widget/RecyclerView;", "setRecycleViewSubscription", "(Landroidx/recyclerview/widget/RecyclerView;)V", "selectedPlanData", "Lcom/asiantimes/subscriptionmodel/SubscriptionPlanData;", "getSelectedPlanData", "()Lcom/asiantimes/subscriptionmodel/SubscriptionPlanData;", "setSelectedPlanData", "(Lcom/asiantimes/subscriptionmodel/SubscriptionPlanData;)V", "selectedSubPlanUnit", "Lcom/asiantimes/subscriptionmodel/SubPlanUnit;", "getSelectedSubPlanUnit", "()Lcom/asiantimes/subscriptionmodel/SubPlanUnit;", "setSelectedSubPlanUnit", "(Lcom/asiantimes/subscriptionmodel/SubPlanUnit;)V", "specialSubscriptionAPI", "", "getSpecialSubscriptionAPI", "()Lkotlin/Unit;", "subPlanUnitList", "", "getSubPlanUnitList", "()Ljava/util/List;", "setSubPlanUnitList", "(Ljava/util/List;)V", "sub_pay", "subsCriptionListAdapter", "Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter;", "getSubsCriptionListAdapter", "()Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter;", "setSubsCriptionListAdapter", "(Lcom/asianmedia/easterneye/adapter/subscription/SubscriptionListAdapter;)V", "subscription", "getSubscription", "subscriptionAppPlanData", "", "Lcom/asiantimes/subscriptionmodel/SubscriptionInAppPlanData;", "subscriptionPlan", "getSubscriptionPlan", "setSubscriptionPlan", "subscriptionPlanData", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "textViewDescription", "Landroid/widget/TextView;", "getTextViewDescription", "()Landroid/widget/TextView;", "setTextViewDescription", "(Landroid/widget/TextView;)V", "title", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "addTransactionAPI", "transactionId", "productId", "deviceType", "billingProcess", "getSubscriptionPlanDetailsAPI", "subscriptionName", "isUk", "newInstance", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroy", "onViewCreated", "view", "redirectToMainActivity", "redirectToStripe", "redirectToSubscriptionList", "subscriptionPrice", "setSelectedSubscriptionData", "Companion", "app_productionDebug"})
public final class SubscriptionListFragment extends androidx.fragment.app.Fragment {
    private java.lang.Integer pos;
    private java.lang.Integer id = 0;
    private java.lang.String packageType;
    private java.lang.String title;
    private java.lang.String callFrom;
    private boolean isuk = false;
    private androidx.recyclerview.widget.RecyclerView recycleViewChoosePlans;
    private androidx.recyclerview.widget.RecyclerView recycleViewDescription;
    private final java.util.ArrayList<com.asiantimes.subscriptionmodel.SubscriptionPlanData> subscriptionPlanData = null;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.subscriptionmodel.SubscriptionPlanData subscriptionPlan;
    private android.widget.ProgressBar progress_bar;
    @org.jetbrains.annotations.Nullable()
    private com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter subsCriptionListAdapter;
    @org.jetbrains.annotations.Nullable()
    private com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter descriptionAdapter;
    @org.jetbrains.annotations.Nullable()
    private com.asianmedia.easterneye.adapter.subscription.PlanChooseAdapter planChooseAdapter;
    @org.jetbrains.annotations.Nullable()
    private android.widget.ImageView imageViewSubscription;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView textViewDescription;
    @org.jetbrains.annotations.Nullable()
    private android.widget.Button buttonViewBuy;
    private boolean sub_pay = false;
    @org.jetbrains.annotations.Nullable()
    private java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> subPlanUnitList;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.subscriptionmodel.SubscriptionPlanData selectedPlanData;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.subscriptionmodel.SubPlanUnit selectedSubPlanUnit;
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subscriptionAppPlanData = null;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView recycleViewSubscription;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy viewModel$delegate = null;
    private static final java.lang.String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB";
    private static final java.lang.String MERCHANT_ID = "15638088474552299429";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.subscription.SubscriptionListFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.subscriptionmodel.SubscriptionPlanData getSubscriptionPlan() {
        return null;
    }
    
    public final void setSubscriptionPlan(@org.jetbrains.annotations.Nullable()
    com.asiantimes.subscriptionmodel.SubscriptionPlanData p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter getSubsCriptionListAdapter() {
        return null;
    }
    
    public final void setSubsCriptionListAdapter(@org.jetbrains.annotations.Nullable()
    com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter getDescriptionAdapter() {
        return null;
    }
    
    public final void setDescriptionAdapter(@org.jetbrains.annotations.Nullable()
    com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asianmedia.easterneye.adapter.subscription.PlanChooseAdapter getPlanChooseAdapter() {
        return null;
    }
    
    public final void setPlanChooseAdapter(@org.jetbrains.annotations.Nullable()
    com.asianmedia.easterneye.adapter.subscription.PlanChooseAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ImageView getImageViewSubscription() {
        return null;
    }
    
    public final void setImageViewSubscription(@org.jetbrains.annotations.Nullable()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTextViewDescription() {
        return null;
    }
    
    public final void setTextViewDescription(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.Button getButtonViewBuy() {
        return null;
    }
    
    public final void setButtonViewBuy(@org.jetbrains.annotations.Nullable()
    android.widget.Button p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.subscriptionmodel.SubPlanUnit> getSubPlanUnitList() {
        return null;
    }
    
    public final void setSubPlanUnitList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.subscriptionmodel.SubscriptionPlanData getSelectedPlanData() {
        return null;
    }
    
    public final void setSelectedPlanData(@org.jetbrains.annotations.Nullable()
    com.asiantimes.subscriptionmodel.SubscriptionPlanData p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.subscriptionmodel.SubPlanUnit getSelectedSubPlanUnit() {
        return null;
    }
    
    public final void setSelectedSubPlanUnit(@org.jetbrains.annotations.Nullable()
    com.asiantimes.subscriptionmodel.SubPlanUnit p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRecycleViewSubscription() {
        return null;
    }
    
    public final void setRecycleViewSubscription(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.SubscriptionListFragment newInstance(int id, @org.jetbrains.annotations.Nullable()
    java.lang.String title, boolean isuk, @org.jetbrains.annotations.Nullable()
    java.lang.String callFrom) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    private final kotlin.Unit getSubscription() {
        return null;
    }
    
    private final void setSelectedSubscriptionData() {
    }
    
    private final void billingProcess() {
    }
    
    private final void redirectToStripe(com.asiantimes.subscriptionmodel.SubscriptionPlanData subscriptionPlanData) {
    }
    
    private final void redirectToMainActivity() {
    }
    
    private final void redirectToSubscriptionList(java.lang.String subscriptionPrice) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Unit getSpecialSubscriptionAPI() {
        return null;
    }
    
    public final void getSubscriptionPlanDetailsAPI(@org.jetbrains.annotations.Nullable()
    java.lang.String subscriptionName, boolean isUk) {
    }
    
    public final void addTransactionAPI(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionId, @org.jetbrains.annotations.Nullable()
    java.lang.String productId, @org.jetbrains.annotations.Nullable()
    java.lang.String deviceType) {
    }
    
    public SubscriptionListFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionListFragment$Companion;", "", "()V", "LICENSE_KEY", "", "MERCHANT_ID", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}