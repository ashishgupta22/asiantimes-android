package com.asiantimes.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 I2\u00020\u00012\u00020\u0002:\u0002IJB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u00105\u001a\u00020\u001bJ\"\u00106\u001a\u00020\u001b2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u0002082\b\u0010:\u001a\u0004\u0018\u00010;H\u0014J\b\u0010<\u001a\u00020\u001bH\u0016J\u0010\u0010=\u001a\u00020\u001b2\u0006\u0010>\u001a\u00020?H\u0016J\u0012\u0010@\u001a\u00020\u001b2\b\u0010A\u001a\u0004\u0018\u00010BH\u0014J\b\u0010C\u001a\u00020\u001bH\u0016J\b\u0010D\u001a\u00020\u001bH\u0002J$\u0010E\u001a\u00020\u001b2\b\u0010F\u001a\u0004\u0018\u00010\u000e2\b\u0010G\u001a\u0004\u0018\u00010\u000e2\b\u0010H\u001a\u0004\u0018\u00010\u000eR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u00020\u001b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u000e\u0010$\u001a\u00020%X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\b\u0012\u0004\u0012\u00020\'0\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010(\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001a\u0010-\u001a\u00020.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0010\u00103\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00104\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006K"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "action_bar_title", "Lcom/asiantimes/widget/TextViewRegular;", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "getSubscriptionListingDetails", "Ljava/util/ArrayList;", "", "iv_menuBack", "Landroid/widget/ImageView;", "mAdapter", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "mLayoutManager", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "mRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "planListSerializes", "", "Lcom/asiantimes/model/PlanListSerialize;", "premiumPostArray", "", "getPremiumPostArray", "()Lkotlin/Unit;", "progress_bar", "Landroid/widget/ProgressBar;", "getProgress_bar", "()Landroid/widget/ProgressBar;", "setProgress_bar", "(Landroid/widget/ProgressBar;)V", "readyToPurchase", "", "subscriptionPlanData", "Lcom/asiantimes/subscriptionmodel/SubscriptionInAppPlanData;", "title", "getTitle", "()Ljava/lang/String;", "setTitle", "(Ljava/lang/String;)V", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "setViewModel", "(Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;)V", "visit_uk_site", "yes_continue", "initializationView", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "redirectToMainActivity", "setReadyToPurchase", "transactionId", "productId", "deviceType", "Companion", "MyAdapter", "app_productionDebug"})
public final class SubscriptionInAppPurchasesList extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    private boolean readyToPurchase = false;
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private final com.asiantimes.widget.TextViewRegular yes_continue = null;
    private final com.asiantimes.widget.TextViewRegular visit_uk_site = null;
    private com.asiantimes.widget.TextViewRegular action_bar_title;
    private android.widget.ImageView iv_menuBack;
    private final java.util.List<com.asiantimes.model.PlanListSerialize> planListSerializes = null;
    private androidx.recyclerview.widget.RecyclerView mRecyclerView;
    private androidx.recyclerview.widget.RecyclerView.Adapter<?> mAdapter;
    private androidx.recyclerview.widget.RecyclerView.LayoutManager mLayoutManager;
    private final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subscriptionPlanData = null;
    private final java.util.ArrayList<java.lang.String> getSubscriptionListingDetails = null;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.subscription.viewmodel.SubscriptionViewModel viewModel;
    @org.jetbrains.annotations.Nullable()
    private android.widget.ProgressBar progress_bar;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String title;
    private static final java.lang.String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB";
    private static final java.lang.String MERCHANT_ID = "15638088474552299429";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.subscription.SubscriptionInAppPurchasesList.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.asiantimes.subscription.viewmodel.SubscriptionViewModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ProgressBar getProgress_bar() {
        return null;
    }
    
    public final void setProgress_bar(@org.jetbrains.annotations.Nullable()
    android.widget.ProgressBar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTitle() {
        return null;
    }
    
    public final void setTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initializationView() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final void setReadyToPurchase(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionId, @org.jetbrains.annotations.Nullable()
    java.lang.String productId, @org.jetbrains.annotations.Nullable()
    java.lang.String deviceType) {
    }
    
    private final kotlin.Unit getPremiumPostArray() {
        return null;
    }
    
    private final void redirectToMainActivity() {
    }
    
    public SubscriptionInAppPurchasesList() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\u0010\u0012\f\u0012\n0\u0002R\u00060\u0000R\u00020\u00030\u0001:\u0001\u0014B\u001b\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u000bH\u0016J \u0010\f\u001a\u00020\r2\u000e\u0010\u000e\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000bH\u0016J \u0010\u0010\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000bH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList$MyAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList$MyAdapter$MyViewHolder;", "Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList;", "subscriptionPlanDataList", "", "Lcom/asiantimes/subscriptionmodel/SubscriptionInAppPlanData;", "context", "Landroid/content/Context;", "(Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList;Ljava/util/List;Landroid/content/Context;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MyViewHolder", "app_productionDebug"})
    public final class MyAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.subscription.SubscriptionInAppPurchasesList.MyAdapter.MyViewHolder> {
        private final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subscriptionPlanDataList = null;
        private final android.content.Context context = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.subscription.SubscriptionInAppPurchasesList.MyAdapter.MyViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.subscription.SubscriptionInAppPurchasesList.MyAdapter.MyViewHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        public MyAdapter(@org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData> subscriptionPlanDataList, @org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0013\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u000e\u0010\u0015\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001b\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList$MyAdapter$MyViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList$MyAdapter;Landroid/view/View;)V", "contentOfPlan", "Lcom/asiantimes/widget/TextViewRegular;", "getContentOfPlan", "()Lcom/asiantimes/widget/TextViewRegular;", "getForgroundSp", "Landroid/widget/ImageView;", "getsubTitle", "Lcom/asiantimes/widget/TextViewRegularBold;", "getGetsubTitle", "()Lcom/asiantimes/widget/TextViewRegularBold;", "plan_price", "getPlan_price", "seeall", "Landroid/widget/RelativeLayout;", "seealloption", "getSeealloption", "showtext", "subSubscriptionRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "getView", "()Landroid/view/View;", "setView", "(Landroid/view/View;)V", "app_productionDebug"})
        public final class MyViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold getsubTitle = null;
            private final android.widget.ImageView getForgroundSp = null;
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegular contentOfPlan = null;
            private final androidx.recyclerview.widget.RecyclerView subSubscriptionRecyclerView = null;
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold seealloption = null;
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold plan_price = null;
            private final android.widget.RelativeLayout seeall = null;
            private final com.asiantimes.widget.TextViewRegular showtext = null;
            @org.jetbrains.annotations.NotNull()
            private android.view.View view;
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getGetsubTitle() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegular getContentOfPlan() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getSeealloption() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getPlan_price() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.view.View getView() {
                return null;
            }
            
            public final void setView(@org.jetbrains.annotations.NotNull()
            android.view.View p0) {
            }
            
            public MyViewHolder(@org.jetbrains.annotations.NotNull()
            android.view.View view) {
                super(null);
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionInAppPurchasesList$Companion;", "", "()V", "LICENSE_KEY", "", "MERCHANT_ID", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}