package com.asiantimes.ui.splash;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0002J\u0012\u0010\u0013\u001a\u00020\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0014J\b\u0010\u0016\u001a\u00020\u0011H\u0002J\b\u0010\u0017\u001a\u00020\u0011H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0018"}, d2 = {"Lcom/asiantimes/ui/splash/SplashActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "bindingView", "Lcom/asiantimes/databinding/ActivitySplashBinding;", "viewModel", "Lcom/asiantimes/ui/splash/SplashViewModel;", "getViewModel", "()Lcom/asiantimes/ui/splash/SplashViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "viewModel1", "Lcom/asiantimes/ui/login/LogInViewModel;", "getViewModel1", "()Lcom/asiantimes/ui/login/LogInViewModel;", "viewModel1$delegate", "getAllCategoriesAPI", "", "getAllCategoriesObserver", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "updateDbVersionAPI", "updateDbVersionObserver", "app_productionDebug"})
public final class SplashActivity extends com.asiantimes.base.BaseActivity {
    private com.asiantimes.databinding.ActivitySplashBinding bindingView;
    private final kotlin.Lazy viewModel$delegate = null;
    private final kotlin.Lazy viewModel1$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.splash.SplashViewModel getViewModel() {
        return null;
    }
    
    private final com.asiantimes.ui.login.LogInViewModel getViewModel1() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void updateDbVersionAPI() {
    }
    
    private final void updateDbVersionObserver() {
    }
    
    private final void getAllCategoriesAPI() {
    }
    
    private final void getAllCategoriesObserver() {
    }
    
    public SplashActivity() {
        super();
    }
}