package com.asiantimes.ui.contact_us;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\u0018\u0000 \u001c2\u00020\u0001:\u0003\u001c\u001d\u001eB\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u001a\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J8\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u0016H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u001f"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment;", "Landroidx/fragment/app/Fragment;", "()V", "viewModel", "Lcom/asiantimes/ui/contact_us/ContactUsViewModel;", "getViewModel", "()Lcom/asiantimes/ui/contact_us/ContactUsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "submitContactDetailts", "first_name", "", "sur_name", "emailAddress", "phone_number", "address", "complain_detail", "Companion", "ContactInfoRecyclerAdapter", "ContactRecyclerAdapter", "app_productionDebug"})
public final class ContactFragment extends androidx.fragment.app.Fragment {
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.ui.contact_us.ContactFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.contact_us.ContactUsViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void submitContactDetailts(java.lang.String first_name, java.lang.String sur_name, java.lang.String emailAddress, java.lang.String phone_number, java.lang.String address, java.lang.String complain_detail) {
    }
    
    public ContactFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.ui.contact_us.ContactFragment newInstance(@org.jetbrains.annotations.NotNull()
    java.util.List<com.asiantimes.model.Detail> personDetail, @org.jetbrains.annotations.NotNull()
    java.lang.String address) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\fH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\fH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment$ContactRecyclerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/contact_us/ContactFragment$ContactRecyclerAdapter$Holder;", "context", "Landroid/content/Context;", "personDetail", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/Detail;", "(Landroid/content/Context;Ljava/util/ArrayList;)V", "getContext", "()Landroid/content/Context;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Holder", "app_productionDebug"})
    public static final class ContactRecyclerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.contact_us.ContactFragment.ContactRecyclerAdapter.Holder> {
        @org.jetbrains.annotations.NotNull()
        private final android.content.Context context = null;
        private final java.util.ArrayList<com.asiantimes.model.Detail> personDetail = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.ui.contact_us.ContactFragment.ContactRecyclerAdapter.Holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.ui.contact_us.ContactFragment.ContactRecyclerAdapter.Holder holder, int position) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Context getContext() {
            return null;
        }
        
        public ContactRecyclerAdapter(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        java.util.ArrayList<com.asiantimes.model.Detail> personDetail) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment$ContactRecyclerAdapter$Holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "recycleViewpPersonalInfoList", "Landroidx/recyclerview/widget/RecyclerView;", "getRecycleViewpPersonalInfoList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRecycleViewpPersonalInfoList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "txtPost", "Landroid/widget/TextView;", "getTxtPost", "()Landroid/widget/TextView;", "setTxtPost", "(Landroid/widget/TextView;)V", "app_productionDebug"})
        public static final class Holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private androidx.recyclerview.widget.RecyclerView recycleViewpPersonalInfoList;
            @org.jetbrains.annotations.NotNull()
            private android.widget.TextView txtPost;
            
            @org.jetbrains.annotations.NotNull()
            public final androidx.recyclerview.widget.RecyclerView getRecycleViewpPersonalInfoList() {
                return null;
            }
            
            public final void setRecycleViewpPersonalInfoList(@org.jetbrains.annotations.NotNull()
            androidx.recyclerview.widget.RecyclerView p0) {
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getTxtPost() {
                return null;
            }
            
            public final void setTxtPost(@org.jetbrains.annotations.NotNull()
            android.widget.TextView p0) {
            }
            
            public Holder(@org.jetbrains.annotations.NotNull()
            android.view.View view) {
                super(null);
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\fH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\fH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment$ContactInfoRecyclerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/contact_us/ContactFragment$ContactInfoRecyclerAdapter$Holder;", "context", "Landroid/content/Context;", "personDetail", "", "Lcom/asiantimes/model/PersonDetail;", "(Landroid/content/Context;Ljava/util/List;)V", "getContext", "()Landroid/content/Context;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Holder", "app_productionDebug"})
    public static final class ContactInfoRecyclerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.contact_us.ContactFragment.ContactInfoRecyclerAdapter.Holder> {
        @org.jetbrains.annotations.NotNull()
        private final android.content.Context context = null;
        private final java.util.List<com.asiantimes.model.PersonDetail> personDetail = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.ui.contact_us.ContactFragment.ContactInfoRecyclerAdapter.Holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.ui.contact_us.ContactFragment.ContactInfoRecyclerAdapter.Holder holder, int position) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Context getContext() {
            return null;
        }
        
        public ContactInfoRecyclerAdapter(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.util.List<com.asiantimes.model.PersonDetail> personDetail) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\n\u00a8\u0006\u0011"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment$ContactInfoRecyclerAdapter$Holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "txtEmail", "Landroid/widget/TextView;", "getTxtEmail", "()Landroid/widget/TextView;", "setTxtEmail", "(Landroid/widget/TextView;)V", "txtName", "getTxtName", "setTxtName", "txtNumber", "getTxtNumber", "setTxtNumber", "app_productionDebug"})
        public static final class Holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private android.widget.TextView txtName;
            @org.jetbrains.annotations.NotNull()
            private android.widget.TextView txtNumber;
            @org.jetbrains.annotations.NotNull()
            private android.widget.TextView txtEmail;
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getTxtName() {
                return null;
            }
            
            public final void setTxtName(@org.jetbrains.annotations.NotNull()
            android.widget.TextView p0) {
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getTxtNumber() {
                return null;
            }
            
            public final void setTxtNumber(@org.jetbrains.annotations.NotNull()
            android.widget.TextView p0) {
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getTxtEmail() {
                return null;
            }
            
            public final void setTxtEmail(@org.jetbrains.annotations.NotNull()
            android.widget.TextView p0) {
            }
            
            public Holder(@org.jetbrains.annotations.NotNull()
            android.view.View view) {
                super(null);
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tH\u0007\u00a8\u0006\n"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactFragment$Companion;", "", "()V", "newInstance", "Lcom/asiantimes/ui/contact_us/ContactFragment;", "personDetail", "", "Lcom/asiantimes/model/Detail;", "address", "", "app_productionDebug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.asiantimes.ui.contact_us.ContactFragment newInstance(@org.jetbrains.annotations.NotNull()
        java.util.List<com.asiantimes.model.Detail> personDetail, @org.jetbrains.annotations.NotNull()
        java.lang.String address) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}