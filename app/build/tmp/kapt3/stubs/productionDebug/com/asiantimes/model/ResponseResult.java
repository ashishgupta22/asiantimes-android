package com.asiantimes.model;

import java.lang.System;

/**
 * Created by Abhijeet-PC on 15-Dec-17.
 */
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u0002\n\u0002\b\u0012\u0018\u0000 \u007f2\u00020\u0001:\u0001\u007fB\u0019\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u000f\b\u0014\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010^\u001a\u00020\u0005H\u0016J\u000f\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u0007\u00a2\u0006\u0002\b_J\u000e\u0010`\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012J\u0006\u0010\u0015\u001a\u00020\u0005J\b\u0010a\u001a\u0004\u0018\u00010\u0003J\b\u0010b\u001a\u0004\u0018\u00010\u0003J\u0006\u00101\u001a\u00020\u0005J\b\u0010c\u001a\u0004\u0018\u00010\u0003J\b\u0010d\u001a\u0004\u0018\u00010\u0003J\u0010\u0010e\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010<\u0018\u00010\u0012J\b\u0010f\u001a\u0004\u0018\u00010\u0003J\b\u0010g\u001a\u0004\u0018\u00010\u0003J\u0010\u0010h\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010@\u0018\u00010\u0012J\b\u0010i\u001a\u0004\u0018\u00010\u0003J\u0010\u0010j\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010U\u0018\u00010\u0012J\b\u0010k\u001a\u0004\u0018\u00010\u0003J\b\u0010l\u001a\u0004\u0018\u00010\u0003J\u0016\u0010m\u001a\u00020n2\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012J\u000e\u0010\u0017\u001a\u00020n2\u0006\u0010\u0014\u001a\u00020\u0005J\u0010\u0010o\u001a\u00020n2\b\u0010&\u001a\u0004\u0018\u00010\u0003J\u0010\u0010p\u001a\u00020n2\b\u0010\'\u001a\u0004\u0018\u00010\u0003J\u000e\u00102\u001a\u00020n2\u0006\u0010\u0004\u001a\u00020\u0005J\u0019\u00102\u001a\u00020n2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0007\u00a2\u0006\u0004\bq\u0010\u0018J\u0010\u0010r\u001a\u00020n2\b\u00106\u001a\u0004\u0018\u00010\u0003J\u0010\u0010s\u001a\u00020n2\b\u00107\u001a\u0004\u0018\u00010\u0003J\u0014\u0010t\u001a\u00020n2\f\u0010;\u001a\b\u0012\u0004\u0012\u00020<0\u0012J\u0010\u0010u\u001a\u00020n2\b\u0010=\u001a\u0004\u0018\u00010\u0003J\u0010\u0010v\u001a\u00020n2\b\u0010>\u001a\u0004\u0018\u00010\u0003J\u0018\u0010w\u001a\u00020n2\u0010\u0010?\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010@\u0018\u00010\u0012J\u0010\u0010x\u001a\u00020n2\b\u0010N\u001a\u0004\u0018\u00010\u0003J\u0018\u0010y\u001a\u00020n2\u0010\u0010T\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010U\u0018\u00010\u0012J\u0010\u0010z\u001a\u00020n2\b\u0010V\u001a\u0004\u0018\u00010\u0003J\u0010\u0010{\u001a\u00020n2\b\u0010]\u001a\u0004\u0018\u00010\u0003J\u0018\u0010|\u001a\u00020n2\u0006\u0010}\u001a\u00020\b2\u0006\u0010~\u001a\u00020\u0005H\u0016R \u0010\n\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u001a\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R&\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR \u0010 \u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\f\"\u0004\b\"\u0010\u000eR \u0010#\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\f\"\u0004\b%\u0010\u000eR\u0014\u0010&\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010(\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\f\"\u0004\b*\u0010\u000eR \u0010+\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\f\"\u0004\b-\u0010\u000eR \u0010.\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\f\"\u0004\b0\u0010\u000eR\"\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b1\u0010\u0016\"\u0004\b2\u0010\u0018R \u00103\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\f\"\u0004\b5\u0010\u000eR\u0014\u00106\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00107\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R&\u00108\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\u001d\"\u0004\b:\u0010\u001fR\u001a\u0010;\u001a\n\u0012\u0004\u0012\u00020<\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010=\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010>\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010?\u001a\n\u0012\u0004\u0012\u00020@\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R&\u0010A\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010\u001d\"\u0004\bC\u0010\u001fR$\u0010D\u001a\b\u0012\u0004\u0012\u00020E0\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u001d\"\u0004\bG\u0010\u001fR \u0010H\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\f\"\u0004\bJ\u0010\u000eR \u0010K\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\f\"\u0004\bM\u0010\u000eR\u0014\u0010N\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010O\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010\f\"\u0004\bQ\u0010\u000eR \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010\f\"\u0004\bS\u0010\u000eR\u001a\u0010T\u001a\n\u0012\u0004\u0012\u00020U\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010V\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010W\u001a\u0004\u0018\u00010X8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bY\u0010Z\"\u0004\b[\u0010\\R\u0010\u0010]\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0080\u0001"}, d2 = {"Lcom/asiantimes/model/ResponseResult;", "Landroid/os/Parcelable;", "status", "", "errorCode", "", "(Ljava/lang/String;I)V", "in", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "aboutus", "getAboutus", "()Ljava/lang/String;", "setAboutus", "(Ljava/lang/String;)V", "address", "getAddress", "catpostlist", "", "Lcom/asiantimes/model/PostList;", "code", "getCode", "()Ljava/lang/Integer;", "setCode", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "conatctUs", "Lcom/asiantimes/model/ConatctU;", "getConatctUs", "()Ljava/util/List;", "setConatctUs", "(Ljava/util/List;)V", "content", "getContent", "setContent", "dBVersion", "getDBVersion", "setDBVersion", "detailsBottomAds", "detailsTopAds", "digitalFreeContent", "getDigitalFreeContent", "setDigitalFreeContent", "digitalPowerList", "getDigitalPowerList", "setDigitalPowerList", "digitalRichList", "getDigitalRichList", "setDigitalRichList", "getErrorCode", "setErrorCode", "goldMembership", "getGoldMembership", "setGoldMembership", "homeBottomAds", "homeListAds", "inActivepostList", "getInActivepostList", "setInActivepostList", "mainPostList", "Lcom/asiantimes/model/MainPostList;", "message", "nextPage", "planList", "Lcom/asiantimes/model/PlanList;", "postList", "getPostList", "setPostList", "powerList", "Lcom/asiantimes/model/PowerList;", "getPowerList", "setPowerList", "premiumPowerList", "getPremiumPowerList", "setPremiumPowerList", "premiumRichList", "getPremiumRichList", "setPremiumRichList", "specailvaltimeignore", "state", "getState", "setState", "getStatus", "setStatus", "subscriptionList", "Lcom/asiantimes/subscriptionmodel/SubscriptionPlanData;", "swipeRolloutAds", "userDetail", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserDetail", "()Lcom/asiantimes/model/CommonModel$UserDetail;", "setUserDetail", "(Lcom/asiantimes/model/CommonModel$UserDetail;)V", "versionCode", "describeContents", "getAddress1", "getCatPostList", "getDetailsBottomAds", "getDetailsTopAds", "getHomeBottomAds", "getHomeListAds", "getMainPostList", "getMessage", "getNextPage", "getPlanList", "getSpecailvaltimeignore", "getSubscriptionList", "getSwipeRolloutAds", "getVersionCode", "setCatPostList", "", "setDetailsBottomAds", "setDetailsTopAds", "setErrorCode1", "setHomeBottomAds", "setHomeListAds", "setMainPostList", "setMessage", "setNextPage", "setPlanList", "setSpecailvaltimeignore", "setSubscriptionList", "setSwipeRolloutAds", "setVersionCode", "writeToParcel", "dest", "flags", "CREATOR", "app_productionDebug"})
public final class ResponseResult implements android.os.Parcelable {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "status")
    private java.lang.String status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "errorCode")
    private java.lang.Integer errorCode = 0;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "code")
    private java.lang.Integer code;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postList")
    private java.util.List<? extends com.asiantimes.model.PostList> postList;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "catpostlist")
    private java.util.List<? extends com.asiantimes.model.PostList> catpostlist;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "inActivepostList")
    private java.util.List<? extends com.asiantimes.model.PostList> inActivepostList;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "powerList")
    private java.util.List<? extends com.asiantimes.model.PowerList> powerList;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "mainPostList")
    private java.util.List<? extends com.asiantimes.model.MainPostList> mainPostList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "state")
    private java.lang.String state;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "GoldMembership")
    private java.lang.String goldMembership;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "digitalPowerList")
    private java.lang.String digitalPowerList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "digitalRichList")
    private java.lang.String digitalRichList;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "subscriptionList")
    private java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionPlanData> subscriptionList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "digitalFreeContent")
    private java.lang.String digitalFreeContent;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "premiumPowerList")
    private java.lang.String premiumPowerList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "premiumRichList")
    private java.lang.String premiumRichList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userDetail")
    private com.asiantimes.model.CommonModel.UserDetail userDetail;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "DBversion")
    private java.lang.String dBVersion;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "aboutus")
    private java.lang.String aboutus;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "content")
    private java.lang.String content;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "planList")
    private java.util.List<? extends com.asiantimes.model.PlanList> planList;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "detailsBottomAds")
    private java.lang.String detailsBottomAds;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "detailsTopAds")
    private java.lang.String detailsTopAds;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "homeBottomAds")
    private java.lang.String homeBottomAds;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "swipeRolloutAds")
    private java.lang.String swipeRolloutAds;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "homeListAds")
    private java.lang.String homeListAds;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "specailvaltimeignore")
    private java.lang.String specailvaltimeignore;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "conatctUs")
    private java.util.List<? extends com.asiantimes.model.ConatctU> conatctUs;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "address")
    private final java.lang.String address = null;
    private java.lang.String message;
    private java.lang.String nextPage;
    private java.lang.String versionCode;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.model.ResponseResult.CREATOR CREATOR = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getErrorCode() {
        return null;
    }
    
    public final void setErrorCode(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCode() {
        return null;
    }
    
    public final void setCode(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.PostList> getPostList() {
        return null;
    }
    
    public final void setPostList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.model.PostList> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.PostList> getInActivepostList() {
        return null;
    }
    
    public final void setInActivepostList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.model.PostList> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.asiantimes.model.PowerList> getPowerList() {
        return null;
    }
    
    public final void setPowerList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.asiantimes.model.PowerList> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getState() {
        return null;
    }
    
    public final void setState(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGoldMembership() {
        return null;
    }
    
    public final void setGoldMembership(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDigitalPowerList() {
        return null;
    }
    
    public final void setDigitalPowerList(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDigitalRichList() {
        return null;
    }
    
    public final void setDigitalRichList(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDigitalFreeContent() {
        return null;
    }
    
    public final void setDigitalFreeContent(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPremiumPowerList() {
        return null;
    }
    
    public final void setPremiumPowerList(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPremiumRichList() {
        return null;
    }
    
    public final void setPremiumRichList(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.model.CommonModel.UserDetail getUserDetail() {
        return null;
    }
    
    public final void setUserDetail(@org.jetbrains.annotations.Nullable()
    com.asiantimes.model.CommonModel.UserDetail p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.subscriptionmodel.SubscriptionPlanData> getSubscriptionList() {
        return null;
    }
    
    public final void setSubscriptionList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.subscriptionmodel.SubscriptionPlanData> subscriptionList) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDBVersion() {
        return null;
    }
    
    public final void setDBVersion(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAboutus() {
        return null;
    }
    
    public final void setAboutus(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContent() {
        return null;
    }
    
    public final void setContent(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.ConatctU> getConatctUs() {
        return null;
    }
    
    public final void setConatctUs(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.model.ConatctU> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddress() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setErrorCode(int errorCode) {
    }
    
    public final void setCode(int code) {
    }
    
    public final int getErrorCode() {
        return 0;
    }
    
    public final int getCode() {
        return 0;
    }
    
    public final void setErrorCode1(@org.jetbrains.annotations.Nullable()
    java.lang.Integer errorCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.PostList> getCatPostList() {
        return null;
    }
    
    public final void setCatPostList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.model.PostList> catpostlist) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNextPage() {
        return null;
    }
    
    public final void setNextPage(@org.jetbrains.annotations.Nullable()
    java.lang.String nextPage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.MainPostList> getMainPostList() {
        return null;
    }
    
    public final void setMainPostList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.asiantimes.model.MainPostList> mainPostList) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getVersionCode() {
        return null;
    }
    
    public final void setVersionCode(@org.jetbrains.annotations.Nullable()
    java.lang.String versionCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddress1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDetailsTopAds() {
        return null;
    }
    
    public final void setDetailsTopAds(@org.jetbrains.annotations.Nullable()
    java.lang.String detailsTopAds) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHomeBottomAds() {
        return null;
    }
    
    public final void setHomeBottomAds(@org.jetbrains.annotations.Nullable()
    java.lang.String homeBottomAds) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSwipeRolloutAds() {
        return null;
    }
    
    public final void setSwipeRolloutAds(@org.jetbrains.annotations.Nullable()
    java.lang.String swipeRolloutAds) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHomeListAds() {
        return null;
    }
    
    public final void setHomeListAds(@org.jetbrains.annotations.Nullable()
    java.lang.String homeListAds) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDetailsBottomAds() {
        return null;
    }
    
    public final void setDetailsBottomAds(@org.jetbrains.annotations.Nullable()
    java.lang.String detailsBottomAds) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSpecailvaltimeignore() {
        return null;
    }
    
    public final void setSpecailvaltimeignore(@org.jetbrains.annotations.Nullable()
    java.lang.String specailvaltimeignore) {
    }
    
    @java.lang.Override()
    public int describeContents() {
        return 0;
    }
    
    @java.lang.Override()
    public void writeToParcel(@org.jetbrains.annotations.NotNull()
    android.os.Parcel dest, int flags) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.PlanList> getPlanList() {
        return null;
    }
    
    public final void setPlanList(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.asiantimes.model.PlanList> planList) {
    }
    
    public ResponseResult(@org.jetbrains.annotations.Nullable()
    java.lang.String status, int errorCode) {
        super();
    }
    
    protected ResponseResult(@org.jetbrains.annotations.NotNull()
    android.os.Parcel in) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/model/ResponseResult$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lcom/asiantimes/model/ResponseResult;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", "size", "", "(I)[Lcom/asiantimes/model/ResponseResult;", "app_productionDebug"})
    public static final class CREATOR implements android.os.Parcelable.Creator<com.asiantimes.model.ResponseResult> {
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.model.ResponseResult createFromParcel(@org.jetbrains.annotations.NotNull()
        android.os.Parcel parcel) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.model.ResponseResult[] newArray(int size) {
            return null;
        }
        
        private CREATOR() {
            super();
        }
    }
}