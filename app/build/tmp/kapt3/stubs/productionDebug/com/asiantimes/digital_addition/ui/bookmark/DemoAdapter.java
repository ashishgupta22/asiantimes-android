package com.asiantimes.digital_addition.ui.bookmark;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0002 !B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\b\u0010\u0014\u001a\u00020\u0010H\u0016J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0010H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0018\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0010H\u0016J\u0018\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0010H\u0016J\u001e\u0010\u001f\u001a\u00020\f2\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\t\u0018\u00010\bj\n\u0012\u0004\u0012\u00020\t\u0018\u0001`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/asiantimes/digital_addition/ui/bookmark/DemoAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/asiantimes/digital_addition/utility/StickHeaderItemDecoration$StickyHeaderInterface;", "clickListener", "Lcom/asiantimes/interfaces/OnClickListener;", "(Lcom/asiantimes/interfaces/OnClickListener;)V", "dataList", "Ljava/util/ArrayList;", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "Lkotlin/collections/ArrayList;", "bindHeaderData", "", "header", "Landroid/view/View;", "headerPosition", "", "getHeaderLayout", "getHeaderPositionForItem", "itemPosition", "getItemCount", "getItemViewType", "position", "isHeader", "", "onBindViewHolder", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ChildViewHolder", "ParentViewHolder", "app_productionDebug"})
public final class DemoAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> implements com.asiantimes.digital_addition.utility.StickHeaderItemDecoration.StickyHeaderInterface {
    private java.util.ArrayList<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> dataList;
    private final com.asiantimes.interfaces.OnClickListener clickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public int getHeaderPositionForItem(int itemPosition) {
        return 0;
    }
    
    @java.lang.Override()
    public int getHeaderLayout(int headerPosition) {
        return 0;
    }
    
    @java.lang.Override()
    public void bindHeaderData(@org.jetbrains.annotations.NotNull()
    android.view.View header, int headerPosition) {
    }
    
    @java.lang.Override()
    public boolean isHeader(int itemPosition) {
        return false;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> dataList) {
    }
    
    public DemoAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickListener clickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/asiantimes/digital_addition/ui/bookmark/DemoAdapter$ParentViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterBookmarkParentBinding;", "(Lcom/asiantimes/databinding/AdapterBookmarkParentBinding;)V", "bind", "", "header", "", "app_productionDebug"})
    public static final class ParentViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.asiantimes.databinding.AdapterBookmarkParentBinding bindingView = null;
        
        public final void bind(@org.jetbrains.annotations.Nullable()
        java.lang.String header) {
        }
        
        public ParentViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterBookmarkParentBinding bindingView) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/asiantimes/digital_addition/ui/bookmark/DemoAdapter$ChildViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterBookmarkChildBinding;", "(Lcom/asiantimes/databinding/AdapterBookmarkChildBinding;)V", "bind", "", "clickListener", "", "postCatNewsList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "app_productionDebug"})
    public static final class ChildViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.asiantimes.databinding.AdapterBookmarkChildBinding bindingView = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        java.lang.Object clickListener, @org.jetbrains.annotations.NotNull()
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList) {
        }
        
        public ChildViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterBookmarkChildBinding bindingView) {
            super(null);
        }
    }
}