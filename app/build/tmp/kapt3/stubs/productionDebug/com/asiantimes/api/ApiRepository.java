package com.asiantimes.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bJ\u001c\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\bJ\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0004J\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\bJ\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0004J\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u00042\u0006\u0010\u0015\u001a\u00020\u0016J\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00140\u00042\u0006\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0016J\u001c\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00140\u00042\u0006\u0010\u001a\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0016J\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00140\u0004J\u001c\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u00042\u0006\u0010\u0018\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\bJ\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001d0\u0004J \u0010 \u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\bJ \u0010\"\u001a\b\u0012\u0004\u0012\u00020\b0\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\bJ4\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u0010$\u001a\u0004\u0018\u00010\b2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010%\u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\bJ\u001c\u0010&\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\'\u001a\u00020\u0016J*\u0010(\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010$\u001a\u0004\u0018\u00010\b2\b\u0010%\u001a\u0004\u0018\u00010\bJ<\u0010)\u001a\b\u0012\u0004\u0012\u00020\r0\u00042\u0006\u0010*\u001a\u00020\b2\u0006\u0010+\u001a\u00020\b2\u0006\u0010,\u001a\u00020\b2\u0006\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020\b2\u0006\u0010/\u001a\u00020\bJ\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0004J*\u00101\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u00102\u001a\u0004\u0018\u00010\b2\b\u00103\u001a\u0004\u0018\u00010\b2\b\u0010!\u001a\u0004\u0018\u00010\bJ4\u00104\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00042\b\u00102\u001a\u0004\u0018\u0001052\b\u00106\u001a\u0004\u0018\u0001052\b\u00107\u001a\u0004\u0018\u0001052\b\u00108\u001a\u0004\u0018\u000109J\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010;\u001a\u00020\b\u00a8\u0006<"}, d2 = {"Lcom/asiantimes/api/ApiRepository;", "", "()V", "aboutUs", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/ResponseResult;", "commentList", "postId", "", "nextPage", "commentReply", "comment", "contactList", "Lcom/asiantimes/model/ResponseResultNew;", "forgotPassword", "Lcom/asiantimes/model/CommonModel;", "email", "getAllCategories", "Lcom/asiantimes/model/GetAllCategoriesModel;", "getBookmark", "Lcom/asiantimes/model/GetTopStoriesModel;", "page", "", "getPostListByCateID", "catId", "getSearchResult", "searchText", "getTopStories", "getVideoCatResult", "Lcom/asiantimes/model/GetVideoListModel;", "nextpage", "getVideoResult", "login", "password", "loginStr", "register", "name", "number", "saveBookmark", "isRemoved", "socialLogin", "submitContact", "emailAddress", "first_name", "sur_name", "phone_number", "address", "complain_detail", "updateDbVersion", "updatePassword", "userId", "otp", "updateProfile", "Lokhttp3/RequestBody;", "userName", "mobile", "img", "Lokhttp3/MultipartBody$Part;", "viewMorePhotos", "cateID", "app_productionDebug"})
public final class ApiRepository {
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.api.ApiRepository INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> updateDbVersion() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetAllCategoriesModel> getAllCategories() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> register(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String number, @org.jetbrains.annotations.Nullable()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> login(@org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> loginStr(@org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> updateProfile(@org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody userId, @org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody userName, @org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody mobile, @org.jetbrains.annotations.Nullable()
    okhttp3.MultipartBody.Part img) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> socialLogin(@org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String number) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> forgotPassword(@org.jetbrains.annotations.Nullable()
    java.lang.String email) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> updatePassword(@org.jetbrains.annotations.Nullable()
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    java.lang.String otp, @org.jetbrains.annotations.Nullable()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> getSearchResult(@org.jetbrains.annotations.NotNull()
    java.lang.String searchText, int page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetVideoListModel> getVideoResult() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetVideoListModel> getVideoCatResult(@org.jetbrains.annotations.NotNull()
    java.lang.String catId, @org.jetbrains.annotations.NotNull()
    java.lang.String nextpage) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> getBookmark(int page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> saveBookmark(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, int isRemoved) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> commentList(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, @org.jetbrains.annotations.NotNull()
    java.lang.String nextPage) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> aboutUs() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> commentReply(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, @org.jetbrains.annotations.NotNull()
    java.lang.String comment) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResultNew> contactList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResultNew> submitContact(@org.jetbrains.annotations.NotNull()
    java.lang.String emailAddress, @org.jetbrains.annotations.NotNull()
    java.lang.String first_name, @org.jetbrains.annotations.NotNull()
    java.lang.String sur_name, @org.jetbrains.annotations.NotNull()
    java.lang.String phone_number, @org.jetbrains.annotations.NotNull()
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    java.lang.String complain_detail) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> getPostListByCateID(@org.jetbrains.annotations.NotNull()
    java.lang.String catId, int page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> getTopStories() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> viewMorePhotos(@org.jetbrains.annotations.NotNull()
    java.lang.String cateID) {
        return null;
    }
    
    private ApiRepository() {
        super();
    }
}