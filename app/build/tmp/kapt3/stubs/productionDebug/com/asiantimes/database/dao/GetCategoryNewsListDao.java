package com.asiantimes.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bg\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H\'J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00032\u0006\u0010\n\u001a\u00020\u0004H\'J\u0010\u0010\u000b\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u0004H\'J\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\u00032\u0006\u0010\u0007\u001a\u00020\u0004H\'J\u001e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u00032\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004H\'J \u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00042\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0003H\u0016J(\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00042\u0016\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u00140\u0013j\b\u0012\u0004\u0012\u00020\u0014`\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0011H\u0016J\u0012\u0010\u0018\u001a\u00020\u00062\b\u0010\u0019\u001a\u0004\u0018\u00010\tH\'J(\u0010\u001a\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00042\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0003H\'J \u0010\u001b\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00042\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0003H\'\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/database/dao/GetCategoryNewsListDao;", "", "allType", "", "", "delete", "", "type", "getTopStoriesList_AllPST_ID", "Lcom/asiantimes/model/GetTableTypeModel;", "postId", "getTopStoriesList_new", "getTopStoriesList_newAll", "getTopStoriesList_newAllPST_ID", "inesrtUpdate", "tableType", "breakingNews", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "inesrtUpdateNew", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetTopStoriesModel$CategoryList;", "Lkotlin/collections/ArrayList;", "inesrtUpdateNewWithPostID", "data", "insert_new", "getAllCategories", "udpateWithPostCatID", "udpateWithType", "app_productionDebug"})
public abstract interface GetCategoryNewsListDao {
    
    @androidx.room.Insert()
    public abstract void insert_new(@org.jetbrains.annotations.Nullable()
    com.asiantimes.model.GetTableTypeModel getAllCategories);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type")
    public abstract com.asiantimes.model.GetTableTypeModel getTopStoriesList_new(@org.jetbrains.annotations.NotNull()
    java.lang.String type);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type")
    public abstract java.util.List<com.asiantimes.model.GetTableTypeModel> getTopStoriesList_newAll(@org.jetbrains.annotations.NotNull()
    java.lang.String type);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type AND postCatID = :postId")
    public abstract java.util.List<com.asiantimes.model.GetTableTypeModel> getTopStoriesList_newAllPST_ID(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String postId);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where  postCatID = :postId")
    public abstract java.util.List<com.asiantimes.model.GetTableTypeModel> getTopStoriesList_AllPST_ID(@org.jetbrains.annotations.NotNull()
    java.lang.String postId);
    
    @androidx.room.Query(value = "DELETE FROM HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE WHERE tableType =:type")
    public abstract void delete(@org.jetbrains.annotations.NotNull()
    java.lang.String type);
    
    @androidx.room.Query(value = "UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=:breakingNews WHERE tableType = :tableType")
    public abstract void udpateWithType(@org.jetbrains.annotations.NotNull()
    java.lang.String tableType, @org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews);
    
    @androidx.room.Query(value = "UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=:breakingNews WHERE tableType = :tableType AND postCatID = :postId")
    public abstract void udpateWithPostCatID(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, @org.jetbrains.annotations.NotNull()
    java.lang.String tableType, @org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT tableType from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE ")
    public abstract java.util.List<java.lang.String> allType();
    
    public abstract void inesrtUpdate(@org.jetbrains.annotations.NotNull()
    java.lang.String tableType, @org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews);
    
    public abstract void inesrtUpdateNew(@org.jetbrains.annotations.NotNull()
    java.lang.String tableType, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.CategoryList> breakingNews);
    
    public abstract void inesrtUpdateNewWithPostID(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.GetTopStoriesModel.PostList data);
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        public static void inesrtUpdate(@org.jetbrains.annotations.NotNull()
        com.asiantimes.database.dao.GetCategoryNewsListDao $this, @org.jetbrains.annotations.NotNull()
        java.lang.String tableType, @org.jetbrains.annotations.Nullable()
        java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews) {
        }
        
        public static void inesrtUpdateNew(@org.jetbrains.annotations.NotNull()
        com.asiantimes.database.dao.GetCategoryNewsListDao $this, @org.jetbrains.annotations.NotNull()
        java.lang.String tableType, @org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.CategoryList> breakingNews) {
        }
        
        public static void inesrtUpdateNewWithPostID(@org.jetbrains.annotations.NotNull()
        com.asiantimes.database.dao.GetCategoryNewsListDao $this, @org.jetbrains.annotations.NotNull()
        com.asiantimes.model.GetTopStoriesModel.PostList data) {
        }
    }
}