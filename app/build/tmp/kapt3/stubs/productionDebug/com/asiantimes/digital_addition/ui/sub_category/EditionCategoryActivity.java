package com.asiantimes.digital_addition.ui.sub_category;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\"\u001a\u00020#H\u0002J0\u0010$\u001a\u00020#2\u0006\u0010\u0019\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u00062\u0006\u0010\'\u001a\u00020\u00062\u0006\u0010(\u001a\u00020\u0006H\u0002J\b\u0010)\u001a\u00020#H\u0002J\u0012\u0010*\u001a\u00020#2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0012\u0010-\u001a\u00020#2\b\u0010.\u001a\u0004\u0018\u00010/H\u0014J\u0010\u00100\u001a\u00020\u00182\u0006\u00101\u001a\u000202H\u0016J\b\u00103\u001a\u00020#H\u0014J\b\u00104\u001a\u00020#H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001c\u001a\u00020\u001d8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\u001e\u0010\u001f\u00a8\u00065"}, d2 = {"Lcom/asiantimes/digital_addition/ui/sub_category/EditionCategoryActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "Landroidx/appcompat/widget/PopupMenu$OnMenuItemClickListener;", "()V", "LICENSE_KEY", "", "MERCHANT_ID", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "editionData", "Lcom/asiantimes/digital_addition/model/EditionListModel$DigitalEditionList;", "editionPostList", "", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList;", "edition_Id", "isSubscribed", "localDB", "Lcom/asiantimes/digital_addition/database/AppDatabase;", "readyToPurchase", "", "subscriptionId", "userData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "viewModel", "Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "billingProcess", "", "buySingleProductApi", "productId", "deviceType", "userId", "email", "getEditionListAPI", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onMenuItemClick", "item", "Landroid/view/MenuItem;", "onResume", "setAdapter", "app_productionDebug"})
public final class EditionCategoryActivity extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener, androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener {
    private com.asiantimes.digital_addition.database.AppDatabase localDB;
    private java.lang.String edition_Id;
    private java.lang.String isSubscribed;
    private java.lang.String subscriptionId;
    private final java.lang.String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB";
    private final java.lang.String MERCHANT_ID = "15638088474552299429";
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private boolean readyToPurchase = false;
    private com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList editionData;
    private java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList> editionPostList;
    private com.asiantimes.model.CommonModel.UserDetail userData;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    private final com.asiantimes.digital_addition.ui.download.DownloadViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    @java.lang.Override()
    public boolean onMenuItemClick(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void setAdapter() {
    }
    
    private final void billingProcess() {
    }
    
    private final void buySingleProductApi(java.lang.String subscriptionId, java.lang.String productId, java.lang.String deviceType, java.lang.String userId, java.lang.String email) {
    }
    
    private final void getEditionListAPI() {
    }
    
    public EditionCategoryActivity() {
        super();
    }
}