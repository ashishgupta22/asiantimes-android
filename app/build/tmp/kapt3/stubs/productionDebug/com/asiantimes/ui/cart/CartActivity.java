package com.asiantimes.ui.cart;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 ^2\u00020\u00012\u00020\u0002:\u0001^B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010D\u001a\u00020\u00052\u0006\u0010E\u001a\u00020\u0005H\u0002J\u0012\u0010F\u001a\u00020%2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\b\u0010G\u001a\u00020%H\u0003J\"\u0010H\u001a\u00020%2\u0006\u0010I\u001a\u00020\u00052\u0006\u0010J\u001a\u00020\u00052\b\u0010K\u001a\u0004\u0018\u00010LH\u0014J\u0010\u0010M\u001a\u00020%2\u0006\u0010N\u001a\u00020OH\u0016J\u0012\u0010P\u001a\u00020%2\b\u0010Q\u001a\u0004\u0018\u00010RH\u0015J\b\u0010S\u001a\u00020%H\u0014J\b\u0010T\u001a\u00020%H\u0002J\u0006\u0010U\u001a\u00020%J\u0010\u0010V\u001a\u00020%2\u0006\u0010W\u001a\u00020XH\u0002J\u0018\u0010<\u001a\u00020%2\u0006\u00109\u001a\u00020/2\u0006\u0010Y\u001a\u00020/H\u0007J\b\u0010Z\u001a\u00020%H\u0002J\u001e\u0010[\u001a\u00020%2\u0006\u00106\u001a\u00020/2\u0006\u0010\\\u001a\u00020\u00052\u0006\u0010]\u001a\u00020\u0005R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u00020%8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b&\u0010\'R\u001c\u0010(\u001a\u0004\u0018\u00010)X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u0016\u0010.\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u00100\u001a\u0004\u0018\u000101X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u00103\"\u0004\b4\u00105R\u0016\u00106\u001a\n\u0012\u0004\u0012\u00020/\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00107\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00109\u001a\u00020/X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u00020?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010C\u00a8\u0006_"}, d2 = {"Lcom/asiantimes/ui/cart/CartActivity;", "Lcom/asiantimes/base/BaseActivity;", "Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;", "()V", "LOGIN_REGISTER", "", "buttonProceedCart", "Lcom/asiantimes/base/RippleButton;", "cartAdapter", "Lcom/asiantimes/ui/cart/CartAdapter;", "cartItemList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/CartItem;", "dbHelper", "Lcom/asiantimes/database/AppDatabase;", "getDbHelper", "()Lcom/asiantimes/database/AppDatabase;", "setDbHelper", "(Lcom/asiantimes/database/AppDatabase;)V", "footerText", "Lcom/asiantimes/widget/TextViewRegular;", "layoutInflater", "Landroid/view/View;", "getLayoutInflater", "()Landroid/view/View;", "setLayoutInflater", "(Landroid/view/View;)V", "listView", "Lcom/baoyz/swipemenulistview/SwipeMenuListView;", "getListView", "()Lcom/baoyz/swipemenulistview/SwipeMenuListView;", "setListView", "(Lcom/baoyz/swipemenulistview/SwipeMenuListView;)V", "premiumPlanLists", "", "Lcom/asiantimes/model/PremiumPlanList;", "premiumPostArray", "", "getPremiumPostArray", "()Lkotlin/Unit;", "rl_actionbar", "Landroid/widget/RelativeLayout;", "getRl_actionbar", "()Landroid/widget/RelativeLayout;", "setRl_actionbar", "(Landroid/widget/RelativeLayout;)V", "subscriptionCount", "", "subscriptionId", "", "getSubscriptionId", "()Ljava/lang/String;", "setSubscriptionId", "(Ljava/lang/String;)V", "subscriptionPrice", "textSubtotalPriceCart", "textTotalPriceCart", "totalPrice", "getTotalPrice", "()F", "setTotalPrice", "(F)V", "viewModel", "Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "getViewModel", "()Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "setViewModel", "(Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;)V", "dp2px", "dp", "initView", "initialisationView", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onConnectionFailed", "connectionResult", "Lcom/google/android/gms/common/ConnectionResult;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "redirectToPaymentActivity", "setCartItemList", "setPreferencesData", "responseResult", "Lcom/asiantimes/model/ResponseResult;", "subTotalPrice", "setViewEvent", "updateCart", "subscriptionCountOld", "subscriptionCountNew", "Companion", "app_productionDebug"})
public final class CartActivity extends com.asiantimes.base.BaseActivity implements com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
    private java.util.ArrayList<com.asiantimes.model.CartItem> cartItemList;
    private java.util.ArrayList<java.lang.Float> subscriptionPrice;
    private java.util.ArrayList<java.lang.Float> subscriptionCount;
    private com.asiantimes.ui.cart.CartAdapter cartAdapter;
    @org.jetbrains.annotations.Nullable()
    private com.baoyz.swipemenulistview.SwipeMenuListView listView;
    @org.jetbrains.annotations.Nullable()
    private android.view.View layoutInflater;
    @org.jetbrains.annotations.Nullable()
    private android.widget.RelativeLayout rl_actionbar;
    private final int LOGIN_REGISTER = 0;
    private final java.util.List<com.asiantimes.model.PremiumPlanList> premiumPlanLists = null;
    @org.jetbrains.annotations.Nullable()
    private com.asiantimes.database.AppDatabase dbHelper;
    private float totalPrice = 0.0F;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String subscriptionId;
    private com.asiantimes.widget.TextViewRegular textSubtotalPriceCart;
    private com.asiantimes.widget.TextViewRegular footerText;
    private com.asiantimes.widget.TextViewRegular textTotalPriceCart;
    private com.asiantimes.base.RippleButton buttonProceedCart;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.subscription.viewmodel.SubscriptionViewModel viewModel;
    private static final java.lang.String selectedPremiumPlanId = null;
    private static final java.lang.String selectedPremiumPlanPrice = null;
    private static final java.lang.String selectedPremiumPlanTitle = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.ui.cart.CartActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.baoyz.swipemenulistview.SwipeMenuListView getListView() {
        return null;
    }
    
    public final void setListView(@org.jetbrains.annotations.Nullable()
    com.baoyz.swipemenulistview.SwipeMenuListView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.view.View getLayoutInflater() {
        return null;
    }
    
    public final void setLayoutInflater(@org.jetbrains.annotations.Nullable()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.RelativeLayout getRl_actionbar() {
        return null;
    }
    
    public final void setRl_actionbar(@org.jetbrains.annotations.Nullable()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.database.AppDatabase getDbHelper() {
        return null;
    }
    
    public final void setDbHelper(@org.jetbrains.annotations.Nullable()
    com.asiantimes.database.AppDatabase p0) {
    }
    
    public final float getTotalPrice() {
        return 0.0F;
    }
    
    public final void setTotalPrice(float p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubscriptionId() {
        return null;
    }
    
    public final void setSubscriptionId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.subscription.viewmodel.SubscriptionViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.asiantimes.subscription.viewmodel.SubscriptionViewModel p0) {
    }
    
    @android.annotation.SuppressLint(value = {"InflateParams"})
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"InflateParams"})
    private final void initialisationView() {
    }
    
    private final void setViewEvent() {
    }
    
    public final void setCartItemList() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final int dp2px(int dp) {
        return 0;
    }
    
    public final void updateCart(float subscriptionPrice, int subscriptionCountOld, int subscriptionCountNew) {
    }
    
    @android.annotation.SuppressLint(value = {"DefaultLocale", "SetTextI18n"})
    public final void setTotalPrice(float totalPrice, float subTotalPrice) {
    }
    
    private final void setPreferencesData(com.asiantimes.model.ResponseResult responseResult) {
    }
    
    private final kotlin.Unit getPremiumPostArray() {
        return null;
    }
    
    private final void redirectToPaymentActivity() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onConnectionFailed(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.common.ConnectionResult connectionResult) {
    }
    
    private final void initView(android.view.View layoutInflater) {
    }
    
    public CartActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/asiantimes/ui/cart/CartActivity$Companion;", "", "()V", "selectedPremiumPlanId", "", "selectedPremiumPlanPrice", "selectedPremiumPlanTitle", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}