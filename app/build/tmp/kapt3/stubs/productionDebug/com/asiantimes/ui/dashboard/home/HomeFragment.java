package com.asiantimes.ui.dashboard.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u001e\u001a\u00020\u000fH\u0002J\b\u0010\u001f\u001a\u00020 H\u0002J\b\u0010!\u001a\u00020 H\u0002J\u0012\u0010\"\u001a\u00020 2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J&\u0010%\u001a\u0004\u0018\u00010$2\u0006\u0010&\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010)2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\b\u0010,\u001a\u00020 H\u0016J\u001a\u0010-\u001a\u00020 2\u0006\u0010#\u001a\u00020$2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\u0018\u0010.\u001a\u00020 2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0002J(\u00103\u001a\u00020 2\u0006\u00104\u001a\u00020\n2\u0006\u00105\u001a\u0002002\u0006\u00106\u001a\u0002022\u0006\u00107\u001a\u000202H\u0002J\b\u00108\u001a\u00020 H\u0002J\u0010\u00109\u001a\u00020 2\u0006\u00104\u001a\u00020\nH\u0002J\b\u0010:\u001a\u00020 H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\f\u001a\u0012\u0012\u0004\u0012\u00020\r0\tj\b\u0012\u0004\u0012\u00020\r`\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0018\u001a\u00020\u00198BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001a\u0010\u001b\u00a8\u0006;"}, d2 = {"Lcom/asiantimes/ui/dashboard/home/HomeFragment;", "Lcom/asiantimes/base/BaseFragment;", "Landroid/view/View$OnClickListener;", "()V", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/FragmentHomeBinding;", "breakingNewsList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "Lkotlin/collections/ArrayList;", "catNewsList", "Lcom/asiantimes/model/GetTopStoriesModel$CategoryList;", "firstTime", "", "homeBreakingNewsAdapter", "Lcom/asiantimes/ui/dashboard/home/HomeBreakingNewsAdapter;", "homeNewsAdapter", "Lcom/asiantimes/ui/dashboard/home/HomeNewsParentAdapter;", "homeTrendingAdapter", "Lcom/asiantimes/ui/dashboard/home/HomeTrendingAdapter;", "isRefreshing", "trendingNewsList", "viewModel", "Lcom/asiantimes/ui/dashboard/home/HomeViewModel;", "getViewModel", "()Lcom/asiantimes/ui/dashboard/home/HomeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "checkLoginStatus", "getTopStoriesAPI", "", "getTopStoriesObserver", "onClick", "view", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "saveBookmarkAPI", "postId", "", "isRemoved", "", "saveBookmarkObserver", "data", "tableType", "group_post", "position", "setAdapter", "setHomeNewBookmarkDatabase", "setHomeNewDatabase", "app_productionDebug"})
public final class HomeFragment extends com.asiantimes.base.BaseFragment implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.FragmentHomeBinding bindingView;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.asiantimes.ui.dashboard.home.HomeTrendingAdapter homeTrendingAdapter;
    private com.asiantimes.ui.dashboard.home.HomeBreakingNewsAdapter homeBreakingNewsAdapter;
    private com.asiantimes.ui.dashboard.home.HomeNewsParentAdapter homeNewsAdapter;
    private boolean isRefreshing = false;
    private com.asiantimes.database.AppDatabase appDatabase;
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNewsList;
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> trendingNewsList;
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.CategoryList> catNewsList;
    private boolean firstTime = false;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.dashboard.home.HomeViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    private final void setHomeNewDatabase() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void getTopStoriesAPI() {
    }
    
    private final void getTopStoriesObserver() {
    }
    
    private final void setAdapter() {
    }
    
    private final void saveBookmarkAPI(java.lang.String postId, int isRemoved) {
    }
    
    private final boolean checkLoginStatus() {
        return false;
    }
    
    private final void saveBookmarkObserver(com.asiantimes.model.GetTopStoriesModel.PostList data, java.lang.String tableType, int group_post, int position) {
    }
    
    private final void setHomeNewBookmarkDatabase(com.asiantimes.model.GetTopStoriesModel.PostList data) {
    }
    
    public HomeFragment() {
        super();
    }
}