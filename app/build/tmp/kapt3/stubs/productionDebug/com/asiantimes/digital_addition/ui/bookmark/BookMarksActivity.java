package com.asiantimes.digital_addition.ui.bookmark;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\u0012\u0010\u000b\u001a\u00020\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014J\b\u0010\u000e\u001a\u00020\nH\u0014J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0012"}, d2 = {"Lcom/asiantimes/digital_addition/ui/bookmark/BookMarksActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "viewModel", "Lcom/asiantimes/digital_addition/ui/bookmark/BookmarkViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/bookmark/BookmarkViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getATDigitalEditionBookmarksAPI", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "setAdapter", "response", "Lcom/asiantimes/digital_addition/model/BookmarksModel;", "app_productionDebug"})
public final class BookMarksActivity extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.digital_addition.ui.bookmark.BookmarkViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void setAdapter(com.asiantimes.digital_addition.model.BookmarksModel response) {
    }
    
    private final void getATDigitalEditionBookmarksAPI() {
    }
    
    public BookMarksActivity() {
        super();
    }
}