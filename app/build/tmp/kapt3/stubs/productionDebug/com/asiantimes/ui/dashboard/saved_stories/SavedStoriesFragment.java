package com.asiantimes.ui.dashboard.saved_stories;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020#H\u0002J&\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\b\u0010-\u001a\u00020#H\u0016J\u001a\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u00020&2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0010\u00100\u001a\u00020#2\u0006\u00101\u001a\u000202H\u0002J\u0010\u00103\u001a\u00020#2\u0006\u00104\u001a\u00020\u0016H\u0002J\b\u00105\u001a\u00020#H\u0002J\u0010\u00106\u001a\u00020#2\u0006\u00107\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\nj\b\u0012\u0004\u0012\u00020\u000b`\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u001e\u0010\u0013\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\u0013\u0010\u000e\"\u0004\b\u0014\u0010\u0010R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001eR\u000e\u0010!\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00068"}, d2 = {"Lcom/asiantimes/ui/dashboard/saved_stories/SavedStoriesFragment;", "Lcom/asiantimes/base/BaseFragment;", "()V", "_hasLoadedOnce", "", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/FragmentSavedStoriesBinding;", "bookmarksList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "Lkotlin/collections/ArrayList;", "isFirstTime", "()Ljava/lang/Boolean;", "setFirstTime", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "isPagination", "isVisible", "setVisible", "page", "", "pastVisibleItems", "savedStoriesAdapter", "Lcom/asiantimes/ui/dashboard/saved_stories/SavedStoriesAdapter;", "totalItemCount", "viewModel", "Lcom/asiantimes/ui/dashboard/saved_stories/SavedArticleViewModel;", "getViewModel", "()Lcom/asiantimes/ui/dashboard/saved_stories/SavedArticleViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "visibleItemCount", "getBookmarkAPI", "", "getBookmarkObserver", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "saveBookmarkAPI", "postId", "", "saveBookmarkObserver", "position", "setAdapter", "setUserVisibleHint", "isVisibleToUser", "app_productionDebug"})
public final class SavedStoriesFragment extends com.asiantimes.base.BaseFragment {
    private com.asiantimes.databinding.FragmentSavedStoriesBinding bindingView;
    private com.asiantimes.ui.dashboard.saved_stories.SavedStoriesAdapter savedStoriesAdapter;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> bookmarksList;
    private java.lang.Boolean isPagination;
    private int page = 1;
    private int pastVisibleItems = 0;
    private int visibleItemCount = 0;
    private int totalItemCount = 0;
    private com.asiantimes.database.AppDatabase appDatabase;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isVisible = false;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isFirstTime = false;
    private boolean _hasLoadedOnce = false;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.dashboard.saved_stories.SavedArticleViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isVisible() {
        return null;
    }
    
    public final void setVisible(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isFirstTime() {
        return null;
    }
    
    public final void setFirstTime(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setAdapter() {
    }
    
    private final void getBookmarkAPI() {
    }
    
    private final void getBookmarkObserver() {
    }
    
    private final void saveBookmarkAPI(java.lang.String postId) {
    }
    
    private final void saveBookmarkObserver(int position) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void setUserVisibleHint(boolean isVisibleToUser) {
    }
    
    public SavedStoriesFragment() {
        super();
    }
}