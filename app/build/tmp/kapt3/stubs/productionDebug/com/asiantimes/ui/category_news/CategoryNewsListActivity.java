package com.asiantimes.ui.category_news;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u001f\u001a\u00020 H\u0002J\b\u0010!\u001a\u00020 H\u0002J\u0012\u0010\"\u001a\u00020 2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0012\u0010%\u001a\u00020 2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0014J\b\u0010(\u001a\u00020 H\u0014J\u0018\u0010)\u001a\u00020 2\u0006\u0010*\u001a\u00020\t2\u0006\u0010+\u001a\u00020\u0015H\u0002J\u0010\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\fH\u0002J\b\u0010.\u001a\u00020 H\u0002J\b\u0010/\u001a\u00020 H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0018\u001a\u00020\u00198BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001e\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/asiantimes/ui/category_news/CategoryNewsListActivity;", "Lcom/asiantimes/base/BaseActivity;", "Landroid/view/View$OnClickListener;", "()V", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/ActivityCategoryNewsListBinding;", "catId", "", "catNewsList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "Lkotlin/collections/ArrayList;", "categoriesNewsListAdapter", "Lcom/asiantimes/ui/category_news/CategoriesNewsListAdapter;", "isFirst", "", "isPagination", "Ljava/lang/Boolean;", "page", "", "pastVisibleItems", "totalItemCount", "viewModel", "Lcom/asiantimes/ui/category_news/CategoriesNewsViewModel;", "getViewModel", "()Lcom/asiantimes/ui/category_news/CategoriesNewsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "visibleItemCount", "getPostListByCateIDAPI", "", "getPostListByCateIDObserver", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "saveBookmarkAPI", "postId", "isRemoved", "saveBookmarkObserver", "data", "setAdapter", "setHomeNewBookmarkDatabase", "app_productionDebug"})
public final class CategoryNewsListActivity extends com.asiantimes.base.BaseActivity implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.ActivityCategoryNewsListBinding bindingView;
    private com.asiantimes.ui.category_news.CategoriesNewsListAdapter categoriesNewsListAdapter;
    private java.util.ArrayList<com.asiantimes.model.GetTopStoriesModel.PostList> catNewsList;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.lang.String catId;
    private java.lang.Boolean isPagination;
    private int page = 1;
    private int pastVisibleItems = 0;
    private int visibleItemCount = 0;
    private int totalItemCount = 0;
    private com.asiantimes.database.AppDatabase appDatabase;
    private boolean isFirst = false;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.category_news.CategoriesNewsViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void setAdapter() {
    }
    
    private final void getPostListByCateIDAPI() {
    }
    
    private final void getPostListByCateIDObserver() {
    }
    
    private final void setHomeNewBookmarkDatabase() {
    }
    
    private final void saveBookmarkAPI(java.lang.String postId, int isRemoved) {
    }
    
    private final void saveBookmarkObserver(com.asiantimes.model.GetTopStoriesModel.PostList data) {
    }
    
    public CategoryNewsListActivity() {
        super();
    }
}