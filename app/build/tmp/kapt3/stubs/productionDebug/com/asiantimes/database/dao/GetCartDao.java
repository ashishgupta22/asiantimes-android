package com.asiantimes.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bg\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004H\'J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\'J\b\u0010\f\u001a\u00020\u0006H\'J\u0018\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\tH\'\u00a8\u0006\u0010"}, d2 = {"Lcom/asiantimes/database/dao/GetCartDao;", "", "getAllData", "", "Lcom/asiantimes/model/CartItem;", "insert_new", "", "getAllCategories", "removeCartItem", "", "subscriptionID", "", "removeCartItemALL", "updateEasternEyeCart", "subscriptionID1", "subscriptionCount1", "app_productionDebug"})
public abstract interface GetCartDao {
    
    @androidx.room.Insert()
    public abstract void insert_new(@org.jetbrains.annotations.Nullable()
    com.asiantimes.model.CartItem getAllCategories);
    
    @androidx.room.Query(value = "DELETE FROM CART_MANAGE_TABLE WHERE subscriptionID =:subscriptionID")
    public abstract int removeCartItem(@org.jetbrains.annotations.NotNull()
    java.lang.String subscriptionID);
    
    @androidx.room.Query(value = "DELETE FROM CART_MANAGE_TABLE")
    public abstract void removeCartItemALL();
    
    @androidx.room.Query(value = "UPDATE CART_MANAGE_TABLE SET subscriptionCount=:subscriptionCount1 WHERE  subscriptionID= :subscriptionID1")
    public abstract void updateEasternEyeCart(@org.jetbrains.annotations.NotNull()
    java.lang.String subscriptionID1, int subscriptionCount1);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from CART_MANAGE_TABLE")
    public abstract java.util.List<com.asiantimes.model.CartItem> getAllData();
}