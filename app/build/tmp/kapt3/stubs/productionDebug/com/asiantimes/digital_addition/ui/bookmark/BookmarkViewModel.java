package com.asiantimes.digital_addition.ui.bookmark;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\b"}, d2 = {"Lcom/asiantimes/digital_addition/ui/bookmark/BookmarkViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "getATDigitalEditionBookmarks", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/digital_addition/model/BookmarksModel;", "requestBean", "Lcom/asiantimes/model/RequestBean;", "app_productionDebug"})
public final class BookmarkViewModel extends androidx.lifecycle.ViewModel {
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.BookmarksModel> getATDigitalEditionBookmarks(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    public BookmarkViewModel() {
        super();
    }
}