package com.asiantimes.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0001H&\u00a8\u0006\u0005"}, d2 = {"Lcom/asiantimes/interfaces/OnClickListener;", "", "onClickListener", "", "object", "app_productionDebug"})
public abstract interface OnClickListener {
    
    public abstract void onClickListener(@org.jetbrains.annotations.Nullable()
    java.lang.Object object);
}