package com.asiantimes.digital_addition.model;

import java.lang.System;

@androidx.room.Entity(tableName = "edition_post_list")
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\b\u0007\u0018\u00002\u00020\u0001:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\n\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u000b\u0010\u0006\"\u0004\b\f\u0010\bR\u001e\u0010\r\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R&\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00198\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR \u0010\u001f\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0015\"\u0004\b!\u0010\u0017R \u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0015\"\u0004\b$\u0010\u0017\u00a8\u0006&"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "", "()V", "editionId", "", "getEditionId", "()Ljava/lang/Integer;", "setEditionId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "errorCode", "getErrorCode", "setErrorCode", "id", "getId", "()I", "setId", "(I)V", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "postList", "", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList;", "getPostList", "()Ljava/util/List;", "setPostList", "(Ljava/util/List;)V", "state", "getState", "setState", "status", "getStatus", "setStatus", "PostList", "app_productionDebug"})
public final class EditionPostListModel {
    @androidx.room.PrimaryKey(autoGenerate = true)
    private int id = 0;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "status")
    private java.lang.String status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "errorCode")
    private java.lang.Integer errorCode;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String message;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postList")
    private java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList> postList;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "state")
    private java.lang.String state;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "editionId")
    private java.lang.Integer editionId;
    
    public final int getId() {
        return 0;
    }
    
    public final void setId(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getErrorCode() {
        return null;
    }
    
    public final void setErrorCode(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList> getPostList() {
        return null;
    }
    
    public final void setPostList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getState() {
        return null;
    }
    
    public final void setState(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getEditionId() {
        return null;
    }
    
    public final void setEditionId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public EditionPostListModel() {
        super();
    }
    
    @androidx.room.TypeConverters(value = {com.asiantimes.digital_addition.database.converter.DataConverter.class})
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001:\u0001#B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\r\"\u0004\b\u0012\u0010\u000fR\"\u0010\u0013\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0014\u0010\u0006\"\u0004\b\u0015\u0010\bR \u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\r\"\u0004\b\u0018\u0010\u000fR&\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001a8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR \u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\r\"\u0004\b\"\u0010\u000f\u00a8\u0006$"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList;", "", "()V", "errorCode", "", "getErrorCode", "()Ljava/lang/Integer;", "setErrorCode", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "postCatColor", "getPostCatColor", "setPostCatColor", "postCatID", "getPostCatID", "setPostCatID", "postCatName", "getPostCatName", "setPostCatName", "postCatNewsList", "", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "getPostCatNewsList", "()Ljava/util/List;", "setPostCatNewsList", "(Ljava/util/List;)V", "status", "getStatus", "setStatus", "PostCatNewsList", "app_productionDebug"})
    public static final class PostList {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "status")
        private java.lang.String status;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "errorCode")
        private java.lang.Integer errorCode;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "message")
        private java.lang.String message;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postCatID")
        private java.lang.Integer postCatID;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postCatName")
        private java.lang.String postCatName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postCatColor")
        private java.lang.String postCatColor;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "postCatNewsList")
        private java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> postCatNewsList;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStatus() {
            return null;
        }
        
        public final void setStatus(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getErrorCode() {
            return null;
        }
        
        public final void setErrorCode(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMessage() {
            return null;
        }
        
        public final void setMessage(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getPostCatID() {
            return null;
        }
        
        public final void setPostCatID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostCatName() {
            return null;
        }
        
        public final void setPostCatName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPostCatColor() {
            return null;
        }
        
        public final void setPostCatColor(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> getPostCatNewsList() {
            return null;
        }
        
        public final void setPostCatNewsList(@org.jetbrains.annotations.Nullable()
        java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> p0) {
        }
        
        public PostList() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b!\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u0000 M2\u00020\u0001:\u0002MNB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010I\u001a\u00020\u0015H\u0016J\u0018\u0010J\u001a\u00020K2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010L\u001a\u00020\u0015H\u0016R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR \u0010\f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\t\"\u0004\b\u000e\u0010\u000bR \u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\t\"\u0004\b\u0011\u0010\u000bR \u0010\u0012\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\t\"\u0004\b\u0013\u0010\u000bR\"\u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\u0014\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\"\u0010\u001a\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\u001a\u0010\u0016\"\u0004\b\u001b\u0010\u0018R \u0010\u001c\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\t\"\u0004\b\u001d\u0010\u000bR \u0010\u001e\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\t\"\u0004\b \u0010\u000bR\"\u0010!\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\"\u0010\u0016\"\u0004\b#\u0010\u0018R \u0010$\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\t\"\u0004\b&\u0010\u000bR \u0010\'\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\t\"\u0004\b)\u0010\u000bR \u0010*\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\t\"\u0004\b,\u0010\u000bR \u0010-\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\t\"\u0004\b/\u0010\u000bR \u00100\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\t\"\u0004\b2\u0010\u000bR \u00103\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\t\"\u0004\b5\u0010\u000bR&\u00106\u001a\n\u0012\u0004\u0012\u000208\u0018\u0001078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R \u0010=\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010\t\"\u0004\b?\u0010\u000bR \u0010@\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010\t\"\u0004\bB\u0010\u000bR \u0010C\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\t\"\u0004\bE\u0010\u000bR \u0010F\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010\t\"\u0004\bH\u0010\u000b\u00a8\u0006O"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "Landroid/os/Parcelable;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "bannerImage", "", "getBannerImage", "()Ljava/lang/String;", "setBannerImage", "(Ljava/lang/String;)V", "date", "getDate", "setDate", "editionId", "getEditionId", "setEditionId", "isBookmark", "setBookmark", "isHeader", "", "()Ljava/lang/Integer;", "setHeader", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "isPosition", "setPosition", "isPremium", "setPremium", "postCatColor", "getPostCatColor", "setPostCatColor", "postCatID", "getPostCatID", "setPostCatID", "postCatName", "getPostCatName", "setPostCatName", "postDate", "getPostDate", "setPostDate", "postDescription", "getPostDescription", "setPostDescription", "postId", "getPostId", "setPostId", "postImage", "getPostImage", "setPostImage", "postTitle", "getPostTitle", "setPostTitle", "productList", "", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "getProductList", "()Ljava/util/List;", "setProductList", "(Ljava/util/List;)V", "shareUrl", "getShareUrl", "setShareUrl", "type", "getType", "setType", "url", "getUrl", "setUrl", "videoUrl", "getVideoUrl", "setVideoUrl", "describeContents", "writeToParcel", "", "flags", "CREATOR", "ProductList", "app_productionDebug"})
        public static final class PostCatNewsList implements android.os.Parcelable {
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "isPremium")
            private java.lang.String isPremium;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "isHeader")
            private java.lang.Integer isHeader;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "isPosition")
            private java.lang.Integer isPosition;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "date")
            private java.lang.String date;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postCatID")
            private java.lang.Integer postCatID;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postCatName")
            private java.lang.String postCatName;
            @org.jetbrains.annotations.Nullable()
            @androidx.room.TypeConverters(value = {})
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postDate")
            private java.lang.String postDate;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postCatColor")
            private java.lang.String postCatColor;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postId")
            private java.lang.String postId;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "shareUrl")
            private java.lang.String shareUrl;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "videoUrl")
            private java.lang.String videoUrl;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postImage")
            private java.lang.String postImage;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "editionId")
            private java.lang.String editionId;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postTitle")
            private java.lang.String postTitle;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "isBookmark")
            private java.lang.String isBookmark;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "postDescription")
            private java.lang.String postDescription;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "productList")
            private java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> productList;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "type")
            private java.lang.String type;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "banner_image")
            private java.lang.String bannerImage;
            @org.jetbrains.annotations.Nullable()
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "url")
            private java.lang.String url;
            @org.jetbrains.annotations.NotNull()
            public static final com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.CREATOR CREATOR = null;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String isPremium() {
                return null;
            }
            
            public final void setPremium(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer isHeader() {
                return null;
            }
            
            public final void setHeader(@org.jetbrains.annotations.Nullable()
            java.lang.Integer p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer isPosition() {
                return null;
            }
            
            public final void setPosition(@org.jetbrains.annotations.Nullable()
            java.lang.Integer p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getDate() {
                return null;
            }
            
            public final void setDate(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getPostCatID() {
                return null;
            }
            
            public final void setPostCatID(@org.jetbrains.annotations.Nullable()
            java.lang.Integer p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostCatName() {
                return null;
            }
            
            public final void setPostCatName(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostDate() {
                return null;
            }
            
            public final void setPostDate(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostCatColor() {
                return null;
            }
            
            public final void setPostCatColor(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostId() {
                return null;
            }
            
            public final void setPostId(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getShareUrl() {
                return null;
            }
            
            public final void setShareUrl(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getVideoUrl() {
                return null;
            }
            
            public final void setVideoUrl(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostImage() {
                return null;
            }
            
            public final void setPostImage(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getEditionId() {
                return null;
            }
            
            public final void setEditionId(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostTitle() {
                return null;
            }
            
            public final void setPostTitle(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String isBookmark() {
                return null;
            }
            
            public final void setBookmark(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getPostDescription() {
                return null;
            }
            
            public final void setPostDescription(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> getProductList() {
                return null;
            }
            
            public final void setProductList(@org.jetbrains.annotations.Nullable()
            java.util.List<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getType() {
                return null;
            }
            
            public final void setType(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getBannerImage() {
                return null;
            }
            
            public final void setBannerImage(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getUrl() {
                return null;
            }
            
            public final void setUrl(@org.jetbrains.annotations.Nullable()
            java.lang.String p0) {
            }
            
            @java.lang.Override()
            public void writeToParcel(@org.jetbrains.annotations.NotNull()
            android.os.Parcel parcel, int flags) {
            }
            
            @java.lang.Override()
            public int describeContents() {
                return 0;
            }
            
            public PostCatNewsList() {
                super();
            }
            
            public PostCatNewsList(@org.jetbrains.annotations.NotNull()
            android.os.Parcel parcel) {
                super();
            }
            
            @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b#\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 /2\u00020\u0001:\u0001/B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010*\u001a\u00020+H\u0016J\u0018\u0010,\u001a\u00020-2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010.\u001a\u00020+H\u0016R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR \u0010\f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\t\"\u0004\b\u000e\u0010\u000bR \u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\t\"\u0004\b\u0011\u0010\u000bR \u0010\u0012\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\t\"\u0004\b\u0014\u0010\u000bR \u0010\u0015\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\t\"\u0004\b\u0017\u0010\u000bR \u0010\u0018\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\t\"\u0004\b\u001a\u0010\u000bR \u0010\u001b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\t\"\u0004\b\u001d\u0010\u000bR \u0010\u001e\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\t\"\u0004\b \u0010\u000bR \u0010!\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\t\"\u0004\b#\u0010\u000bR \u0010$\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\t\"\u0004\b&\u0010\u000bR \u0010\'\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\t\"\u0004\b)\u0010\u000b\u00a8\u00060"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "Landroid/os/Parcelable;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "key", "", "getKey", "()Ljava/lang/String;", "setKey", "(Ljava/lang/String;)V", "numberComments", "getNumberComments", "setNumberComments", "postCatName", "getPostCatName", "setPostCatName", "postDate", "getPostDate", "setPostDate", "postDescription", "getPostDescription", "setPostDescription", "postId", "getPostId", "setPostId", "postImage", "getPostImage", "setPostImage", "postImageCaption", "getPostImageCaption", "setPostImageCaption", "shareUrl", "getShareUrl", "setShareUrl", "title", "getTitle", "setTitle", "types", "getTypes", "setTypes", "describeContents", "", "writeToParcel", "", "flags", "CREATOR", "app_productionDebug"})
            public static final class ProductList implements android.os.Parcelable {
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postId")
                private java.lang.String postId;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "types")
                private java.lang.String types;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "numberComments")
                private java.lang.String numberComments;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "Title")
                private java.lang.String title;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "shareUrl")
                private java.lang.String shareUrl;
                @org.jetbrains.annotations.Nullable()
                @androidx.room.TypeConverters(value = {})
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postDate")
                private java.lang.String postDate;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postCatName")
                private java.lang.String postCatName;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postImageCaption")
                private java.lang.String postImageCaption;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postImage")
                private java.lang.String postImage;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "postDescription")
                private java.lang.String postDescription;
                @org.jetbrains.annotations.Nullable()
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "key")
                private java.lang.String key;
                @org.jetbrains.annotations.NotNull()
                public static final com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList.CREATOR CREATOR = null;
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostId() {
                    return null;
                }
                
                public final void setPostId(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getTypes() {
                    return null;
                }
                
                public final void setTypes(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getNumberComments() {
                    return null;
                }
                
                public final void setNumberComments(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getTitle() {
                    return null;
                }
                
                public final void setTitle(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getShareUrl() {
                    return null;
                }
                
                public final void setShareUrl(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostDate() {
                    return null;
                }
                
                public final void setPostDate(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostCatName() {
                    return null;
                }
                
                public final void setPostCatName(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostImageCaption() {
                    return null;
                }
                
                public final void setPostImageCaption(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostImage() {
                    return null;
                }
                
                public final void setPostImage(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getPostDescription() {
                    return null;
                }
                
                public final void setPostDescription(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getKey() {
                    return null;
                }
                
                public final void setKey(@org.jetbrains.annotations.Nullable()
                java.lang.String p0) {
                }
                
                @java.lang.Override()
                public void writeToParcel(@org.jetbrains.annotations.NotNull()
                android.os.Parcel parcel, int flags) {
                }
                
                @java.lang.Override()
                public int describeContents() {
                    return 0;
                }
                
                public ProductList() {
                    super();
                }
                
                public ProductList(@org.jetbrains.annotations.NotNull()
                android.os.Parcel parcel) {
                    super();
                }
                
                @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", "size", "", "(I)[Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$ProductList;", "app_productionDebug"})
                public static final class CREATOR implements android.os.Parcelable.Creator<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList> {
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList createFromParcel(@org.jetbrains.annotations.NotNull()
                    android.os.Parcel parcel) {
                        return null;
                    }
                    
                    @org.jetbrains.annotations.NotNull()
                    @java.lang.Override()
                    public com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList[] newArray(int size) {
                        return null;
                    }
                    
                    private CREATOR() {
                        super();
                    }
                }
            }
            
            @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", "size", "", "(I)[Lcom/asiantimes/digital_addition/model/EditionPostListModel$PostList$PostCatNewsList;", "app_productionDebug"})
            public static final class CREATOR implements android.os.Parcelable.Creator<com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList> {
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList createFromParcel(@org.jetbrains.annotations.NotNull()
                android.os.Parcel parcel) {
                    return null;
                }
                
                @org.jetbrains.annotations.NotNull()
                @java.lang.Override()
                public com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList[] newArray(int size) {
                    return null;
                }
                
                private CREATOR() {
                    super();
                }
            }
        }
    }
}