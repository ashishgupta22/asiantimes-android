package com.asiantimes.digital_addition.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007J\u001a\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\bH\u0007\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/digital_addition/utility/BindingAdapters;", "", "()V", "htmlText", "", "view", "Landroid/widget/TextView;", "html", "", "imageUrl", "Landroid/widget/ImageView;", "imageURL", "app_productionDebug"})
public final class BindingAdapters {
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.digital_addition.utility.BindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"imageUrl"})
    public static final void imageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, @org.jetbrains.annotations.Nullable()
    java.lang.String imageURL) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"htmlText"})
    public static final void htmlText(@org.jetbrains.annotations.NotNull()
    android.widget.TextView view, @org.jetbrains.annotations.Nullable()
    java.lang.String html) {
    }
    
    private BindingAdapters() {
        super();
    }
}