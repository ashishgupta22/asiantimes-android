package com.asiantimes.model;

import java.lang.System;

@androidx.room.Entity(tableName = "home_page_news_with_cat_table_type")
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000eR&\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R \u0010\u0019\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\f\"\u0004\b\u001b\u0010\u000e\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/model/GetTableTypeModel;", "", "()V", "autoGenerateId", "", "getAutoGenerateId", "()I", "setAutoGenerateId", "(I)V", "cateName", "", "getCateName", "()Ljava/lang/String;", "setCateName", "(Ljava/lang/String;)V", "postCatID", "getPostCatID", "setPostCatID", "postList", "", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "getPostList", "()Ljava/util/List;", "setPostList", "(Ljava/util/List;)V", "tableType", "getTableType", "setTableType", "app_productionDebug"})
public final class GetTableTypeModel {
    @androidx.room.PrimaryKey(autoGenerate = true)
    private int autoGenerateId = 0;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postList")
    private java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> postList;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "tableType")
    private java.lang.String tableType;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "cateName")
    private java.lang.String cateName;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "postCatID")
    private java.lang.String postCatID;
    
    public final int getAutoGenerateId() {
        return 0;
    }
    
    public final void setAutoGenerateId(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> getPostList() {
        return null;
    }
    
    public final void setPostList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTableType() {
        return null;
    }
    
    public final void setTableType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCateName() {
        return null;
    }
    
    public final void setCateName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPostCatID() {
        return null;
    }
    
    public final void setPostCatID(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public GetTableTypeModel() {
        super();
    }
}