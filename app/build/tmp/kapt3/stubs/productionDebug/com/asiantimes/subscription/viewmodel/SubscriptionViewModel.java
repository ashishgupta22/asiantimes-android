package com.asiantimes.subscription.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\u000b"}, d2 = {"Lcom/asiantimes/subscription/viewmodel/SubscriptionViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "addTransaction", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/ResponseResult;", "requestBean", "Lcom/asiantimes/model/RequestBean;", "getPremiumPostRequestNew", "getSpecialSubscription", "getSubScriptionPlanDetails", "app_productionDebug"})
public final class SubscriptionViewModel extends androidx.lifecycle.ViewModel {
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getSubScriptionPlanDetails(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getSpecialSubscription(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> addTransaction(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getPremiumPostRequestNew(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    public SubscriptionViewModel() {
        super();
    }
}