package com.asiantimes.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0019X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0019X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0019X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/asiantimes/base/Constants;", "", "()V", "ANDROID", "", "APP_DATABASE_NAME", "BRAKING_NEWS_CATEGOTY_TYPE", "CART_MANAGE_TABLE", "CATEGORY_ID", "CATEGORY_NAME", "CAT_LIST_NEWS_CATEGOTY_TYPE", "CAT_LIST_NEWS_HOME_TYPE", "DEVICE_TYPE", "getDEVICE_TYPE", "()Ljava/lang/String;", "setDEVICE_TYPE", "(Ljava/lang/String;)V", "GET_ALL_CATEGORIES_TABLE", "HOME_PAGE_BREAKING_NEWS_TABLE", "HOME_PAGE_NEWS_WITH_CAT_TABLE", "HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE", "HOME_PAGE_TRENDING_NEWS_TABLE", "HTTP_AUTHENTICATION_KEY", "HTTP_AUTH_KEY", "NEWS_DETAIL_COMMAN", "", "NEWS_DETAIL_DATA", "NEWS_DETAIL_TYPE", "NEWS_FROM_NEWS_LIST", "NEWS_FROM_NOTIFIATION", "NEWS_POST_CATID", "NEWS_PST_CAT_ID", "POSITION", "SAVE_CATEGOTY_TYPE", "SEARCH_NEWS_CATEGOTY_TYPE", "SPLASH_DELAY_TIME", "", "TRADING_NEWS_CATEGOTY_TYPE", "USER_DATA_TABLE", "app_productionDebug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String DEVICE_TYPE = "ANDROID";
    public static final long SPLASH_DELAY_TIME = 1000L;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ANDROID = "android";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CATEGORY_ID = "category_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CATEGORY_NAME = "category_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NEWS_DETAIL_DATA = "news_detail_data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NEWS_DETAIL_TYPE = "news_detail_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NEWS_POST_CATID = "news_postcatId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String POSITION = "position";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HTTP_AUTHENTICATION_KEY = "ciVxvtAsfWU:APA91bFd";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HTTP_AUTH_KEY = "t~E/x^sB9m>7CG1==k}M3N4iSmB-UY1~Om6rE{yiMwk1r=tVv}]MPdMR4Du?^77";
    public static final int NEWS_DETAIL_COMMAN = 1;
    public static final int NEWS_FROM_NEWS_LIST = 2;
    public static final int NEWS_FROM_NOTIFIATION = 3;
    public static final int NEWS_PST_CAT_ID = 4;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String APP_DATABASE_NAME = "asian_times";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_DATA_TABLE = "user_data_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ALL_CATEGORIES_TABLE = "get_all_categories_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOME_PAGE_BREAKING_NEWS_TABLE = "home_page_breaking_news_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOME_PAGE_TRENDING_NEWS_TABLE = "home_page_trending_news_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOME_PAGE_NEWS_WITH_CAT_TABLE = "home_page_news_with_cat_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE = "home_page_news_with_cat_table_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CART_MANAGE_TABLE = "cart_manage_table";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SAVE_CATEGOTY_TYPE = "savedCategory";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BRAKING_NEWS_CATEGOTY_TYPE = "brakingNews";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TRADING_NEWS_CATEGOTY_TYPE = "tradingNews";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SEARCH_NEWS_CATEGOTY_TYPE = "searchNews";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CAT_LIST_NEWS_CATEGOTY_TYPE = "catlist_News";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CAT_LIST_NEWS_HOME_TYPE = "catlist_News_Home";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.base.Constants INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDEVICE_TYPE() {
        return null;
    }
    
    public final void setDEVICE_TYPE(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    private Constants() {
        super();
    }
}