package com.asiantimes.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/asiantimes/ui/detail/NewsDetailActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/ActivityNewsDetailBinding;", "dataList", "", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_productionDebug"})
public final class NewsDetailActivity extends com.asiantimes.base.BaseActivity {
    private com.asiantimes.databinding.ActivityNewsDetailBinding bindingView;
    private java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> dataList;
    private com.asiantimes.database.AppDatabase appDatabase;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public NewsDetailActivity() {
        super();
    }
}