package com.asiantimes.subscription;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010\u000f\u001a\u00020\u0010J\b\u0010\u0011\u001a\u00020\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0012\u0010\u0015\u001a\u00020\u00102\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionPrintListDialog;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "getSubPlanUnit", "Ljava/util/ArrayList;", "Lcom/asiantimes/subscriptionmodel/SubPlanUnit;", "getGetSubPlanUnit", "()Ljava/util/ArrayList;", "setGetSubPlanUnit", "(Ljava/util/ArrayList;)V", "imageviewCloseSubscriptionDialog", "Landroid/widget/ImageView;", "recyclerViewPlanList", "Landroidx/recyclerview/widget/RecyclerView;", "initializationView", "", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "redirectToSubscriptionList", "subscriptionPrice", "", "SubPrintUnitList", "app_productionDebug"})
public final class SubscriptionPrintListDialog extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    private android.widget.ImageView imageviewCloseSubscriptionDialog;
    private androidx.recyclerview.widget.RecyclerView recyclerViewPlanList;
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<com.asiantimes.subscriptionmodel.SubPlanUnit> getSubPlanUnit;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.asiantimes.subscriptionmodel.SubPlanUnit> getGetSubPlanUnit() {
        return null;
    }
    
    public final void setGetSubPlanUnit(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.asiantimes.subscriptionmodel.SubPlanUnit> p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initializationView() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    private final void redirectToSubscriptionList(java.lang.String subscriptionPrice) {
    }
    
    public SubscriptionPrintListDialog() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\u0010\u0012\f\u0012\n0\u0002R\u00060\u0000R\u00020\u00030\u0001:\u0001\u0016B\u0015\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\f\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\u000f2\u000e\u0010\u0010\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0011\u001a\u00020\rH\u0016J \u0010\u0012\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0017"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionPrintListDialog$SubPrintUnitList;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/subscription/SubscriptionPrintListDialog$SubPrintUnitList$MyViewHolder;", "Lcom/asiantimes/subscription/SubscriptionPrintListDialog;", "subPlanUnit", "", "Lcom/asiantimes/subscriptionmodel/SubPlanUnit;", "(Lcom/asiantimes/subscription/SubscriptionPrintListDialog;Ljava/util/List;)V", "getSubPlanUnit", "()Ljava/util/List;", "setSubPlanUnit", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MyViewHolder", "app_productionDebug"})
    public final class SubPrintUnitList extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.subscription.SubscriptionPrintListDialog.SubPrintUnitList.MyViewHolder> {
        @org.jetbrains.annotations.Nullable()
        private java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> subPlanUnit;
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<com.asiantimes.subscriptionmodel.SubPlanUnit> getSubPlanUnit() {
            return null;
        }
        
        public final void setSubPlanUnit(@org.jetbrains.annotations.Nullable()
        java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.asiantimes.subscription.SubscriptionPrintListDialog.SubPrintUnitList.MyViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.subscription.SubscriptionPrintListDialog.SubPrintUnitList.MyViewHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        public SubPrintUnitList(@org.jetbrains.annotations.Nullable()
        java.util.List<? extends com.asiantimes.subscriptionmodel.SubPlanUnit> subPlanUnit) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/asiantimes/subscription/SubscriptionPrintListDialog$SubPrintUnitList$MyViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lcom/asiantimes/subscription/SubscriptionPrintListDialog$SubPrintUnitList;Landroid/view/View;)V", "btnPlanUnit", "Lcom/asiantimes/widget/TextViewRegularBold;", "getBtnPlanUnit", "()Lcom/asiantimes/widget/TextViewRegularBold;", "txtPlanPrice", "getTxtPlanPrice", "getView", "()Landroid/view/View;", "setView", "(Landroid/view/View;)V", "app_productionDebug"})
        public final class MyViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold txtPlanPrice = null;
            @org.jetbrains.annotations.NotNull()
            private final com.asiantimes.widget.TextViewRegularBold btnPlanUnit = null;
            @org.jetbrains.annotations.NotNull()
            private android.view.View view;
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getTxtPlanPrice() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.asiantimes.widget.TextViewRegularBold getBtnPlanUnit() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.view.View getView() {
                return null;
            }
            
            public final void setView(@org.jetbrains.annotations.NotNull()
            android.view.View p0) {
            }
            
            public MyViewHolder(@org.jetbrains.annotations.NotNull()
            android.view.View view) {
                super(null);
            }
        }
    }
}