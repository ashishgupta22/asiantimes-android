package com.asiantimes.ui.dashboard.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0016\u0010\u0013\u001a\u00020\f2\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007J\u001e\u0010\u0013\u001a\u00020\f2\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00072\u0006\u0010\u000e\u001a\u00020\nR\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/asiantimes/ui/dashboard/home/HomeBreakingNewsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/dashboard/home/HomeBreakingNewsAdapter$ViewHolder;", "onClickListener", "Lcom/asiantimes/interfaces/OnClickPositionViewListener;", "(Lcom/asiantimes/interfaces/OnClickPositionViewListener;)V", "breakingNews", "", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "ViewHolder", "app_productionDebug"})
public final class HomeBreakingNewsAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.dashboard.home.HomeBreakingNewsAdapter.ViewHolder> {
    private java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews;
    private final com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.ui.dashboard.home.HomeBreakingNewsAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.dashboard.home.HomeBreakingNewsAdapter.ViewHolder holder, int position) {
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews) {
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> breakingNews, int position) {
    }
    
    public HomeBreakingNewsAdapter(@org.jetbrains.annotations.NotNull()
    com.asiantimes.interfaces.OnClickPositionViewListener onClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0010"}, d2 = {"Lcom/asiantimes/ui/dashboard/home/HomeBreakingNewsAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindingView", "Lcom/asiantimes/databinding/AdapterHomeBreakingNewsBinding;", "(Lcom/asiantimes/databinding/AdapterHomeBreakingNewsBinding;)V", "imageViewBookmark", "Landroidx/appcompat/widget/AppCompatImageView;", "getImageViewBookmark", "()Landroidx/appcompat/widget/AppCompatImageView;", "bind", "", "breakingNews", "", "position", "", "onClickListener", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final androidx.appcompat.widget.AppCompatImageView imageViewBookmark = null;
        private final com.asiantimes.databinding.AdapterHomeBreakingNewsBinding bindingView = null;
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.widget.AppCompatImageView getImageViewBookmark() {
            return null;
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        java.lang.Object breakingNews, int position, @org.jetbrains.annotations.NotNull()
        java.lang.Object onClickListener) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.asiantimes.databinding.AdapterHomeBreakingNewsBinding bindingView) {
            super(null);
        }
    }
}