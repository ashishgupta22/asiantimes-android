package com.asiantimes.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u000e\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\'J\u0016\u0010\u0007\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\'\u00a8\u0006\b"}, d2 = {"Lcom/asiantimes/database/dao/GetAllCategoryDao;", "", "delete", "", "getAllCategories", "", "Lcom/asiantimes/model/GetAllCategoriesModel$Post;", "insert", "app_productionDebug"})
public abstract interface GetAllCategoryDao {
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.NotNull()
    java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post> getAllCategories);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from GET_ALL_CATEGORIES_TABLE")
    public abstract java.util.List<com.asiantimes.model.GetAllCategoriesModel.Post> getAllCategories();
    
    @androidx.room.Query(value = "DELETE FROM GET_ALL_CATEGORIES_TABLE")
    public abstract void delete();
}