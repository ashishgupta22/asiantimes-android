package com.asiantimes.widget;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/asiantimes/widget/CustomProgressDialog;", "Landroid/app/Dialog;", "context", "Landroid/content/Context;", "resourceIdOfImage", "", "(Landroid/content/Context;I)V", "iv", "Landroid/widget/ImageView;", "show", "", "app_productionDebug"})
public final class CustomProgressDialog extends android.app.Dialog {
    private final android.widget.ImageView iv = null;
    
    @java.lang.Override()
    public void show() {
    }
    
    public CustomProgressDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int resourceIdOfImage) {
        super(null);
    }
}