package com.asiantimes.ui.dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\b\u0010\r\u001a\u00020\nH\u0016J\u0012\u0010\u000e\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\b\u0010\u0011\u001a\u00020\nH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/asiantimes/ui/dashboard/DashboardActivity;", "Lcom/asiantimes/base/BaseActivity;", "()V", "bindingView", "Lcom/asiantimes/databinding/ActivityDashBoardBinding;", "dashboardAdapter", "Lcom/asiantimes/ui/dashboard/DashboardAdapter;", "doubleBackToExitPressedOnce", "", "logout", "", "pos", "", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setupPager", "app_productionDebug"})
public final class DashboardActivity extends com.asiantimes.base.BaseActivity {
    private com.asiantimes.databinding.ActivityDashBoardBinding bindingView;
    private com.asiantimes.ui.dashboard.DashboardAdapter dashboardAdapter;
    private boolean doubleBackToExitPressedOnce = false;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupPager() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void logout(int pos) {
    }
    
    public DashboardActivity() {
        super();
    }
}