package com.asiantimes.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004J\n\u0010\u0015\u001a\u0004\u0018\u00010\u0004H\u0007J\u0006\u0010\u0016\u001a\u00020\u0013J\u0006\u0010\u0017\u001a\u00020\u0013J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00112\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cJ\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\b2\b\u0010!\u001a\u0004\u0018\u00010\"J\u0010\u0010#\u001a\u00020\b2\b\u0010$\u001a\u0004\u0018\u00010\u0004J\u000e\u0010%\u001a\u00020\b2\u0006\u0010!\u001a\u00020\u0011J\u000e\u0010&\u001a\u00020\b2\u0006\u0010\u001a\u001a\u00020\u0011J\u0010\u0010\'\u001a\u00020\b2\b\u0010(\u001a\u0004\u0018\u00010\u0004J\u0016\u0010)\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u0004J\u0010\u0010+\u001a\u00020\u00192\b\u0010,\u001a\u0004\u0018\u00010\u0004R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\t\"\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\t\"\u0004\b\r\u0010\u000b\u00a8\u0006-"}, d2 = {"Lcom/asiantimes/base/Utils;", "", "()V", "COPYRIGHT_TXT", "", "getCOPYRIGHT_TXT", "()Ljava/lang/String;", "isDebugModeOn", "", "()Z", "setDebugModeOn", "(Z)V", "isToastShow", "setToastShow", "drawTextToBitmap", "Landroid/graphics/Bitmap;", "gContext", "Landroid/content/Context;", "gResId", "", "gText", "getDeviceId", "getHeight", "getWidth", "hideSoftKeyBoard", "", "mContext", "view", "Landroid/view/View;", "htmlToSppanned", "Landroid/text/Spanned;", "s", "isActivityDestroyed", "context", "Landroid/app/Activity;", "isEmailValid", "email", "isLandscape", "isOnline", "isValidString", "str", "showSnackBarAlert", "msg", "showToast", "message", "app_productionDebug"})
public final class Utils {
    @org.jetbrains.annotations.Nullable()
    private static final java.lang.String COPYRIGHT_TXT = "";
    private static boolean isToastShow = false;
    private static boolean isDebugModeOn = false;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.base.Utils INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCOPYRIGHT_TXT() {
        return null;
    }
    
    public final boolean isToastShow() {
        return false;
    }
    
    public final void setToastShow(boolean p0) {
    }
    
    public final boolean isDebugModeOn() {
        return false;
    }
    
    public final void setDebugModeOn(boolean p0) {
    }
    
    public final void showToast(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public final void showSnackBarAlert(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @android.annotation.SuppressLint(value = {"HardwareIds"})
    public final java.lang.String getDeviceId() {
        return null;
    }
    
    public final int getHeight() {
        return 0;
    }
    
    public final int getWidth() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.text.Spanned htmlToSppanned(@org.jetbrains.annotations.Nullable()
    java.lang.String s) {
        return null;
    }
    
    public final boolean isEmailValid(@org.jetbrains.annotations.Nullable()
    java.lang.String email) {
        return false;
    }
    
    public final boolean isOnline(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext) {
        return false;
    }
    
    public final boolean isLandscape(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final boolean isValidString(@org.jetbrains.annotations.Nullable()
    java.lang.String str) {
        return false;
    }
    
    public final boolean isActivityDestroyed(@org.jetbrains.annotations.Nullable()
    android.app.Activity context) {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.Bitmap drawTextToBitmap(@org.jetbrains.annotations.NotNull()
    android.content.Context gContext, int gResId, @org.jetbrains.annotations.NotNull()
    java.lang.String gText) {
        return null;
    }
    
    public final void hideSoftKeyBoard(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private Utils() {
        super();
    }
}