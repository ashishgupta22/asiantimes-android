package com.asiantimes.ui.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u001fH\u0002J\"\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u000b2\u0006\u0010\"\u001a\u00020\u000b2\b\u0010#\u001a\u0004\u0018\u00010$H\u0014J\u0012\u0010%\u001a\u00020\u001f2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0012\u0010(\u001a\u00020\u001f2\b\u0010)\u001a\u0004\u0018\u00010*H\u0014J-\u0010+\u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u000b2\u000e\u0010,\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050-2\u0006\u0010.\u001a\u00020/H\u0016\u00a2\u0006\u0002\u00100J\u001a\u00101\u001a\u0004\u0018\u0001022\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0005H\u0002J\u0006\u00106\u001a\u00020\u001fR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u0018\u0010\u0019\u00a8\u00067"}, d2 = {"Lcom/asiantimes/ui/profile/ProfileActivity;", "Lcom/asiantimes/base/BaseActivity;", "Landroid/view/View$OnClickListener;", "()V", "CAMERA_PERMISSION", "", "getCAMERA_PERMISSION", "()Ljava/lang/String;", "setCAMERA_PERMISSION", "(Ljava/lang/String;)V", "MY_CAMERA_REQUEST_CODE", "", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/ActivityProfileBinding;", "imageBody", "Lokhttp3/MultipartBody$Part;", "getImageBody", "()Lokhttp3/MultipartBody$Part;", "setImageBody", "(Lokhttp3/MultipartBody$Part;)V", "viewModel", "Lcom/asiantimes/ui/profile/ProfileViewModel;", "getViewModel", "()Lcom/asiantimes/ui/profile/ProfileViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "checkLoginStatus", "", "observLiveData", "", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "persistImage", "Ljava/io/File;", "bitmap", "Landroid/graphics/Bitmap;", "name", "updateProfileAPI", "app_productionDebug"})
public final class ProfileActivity extends com.asiantimes.base.BaseActivity implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.ActivityProfileBinding bindingView;
    private com.asiantimes.database.AppDatabase appDatabase;
    @org.jetbrains.annotations.Nullable()
    private okhttp3.MultipartBody.Part imageBody;
    private final kotlin.Lazy viewModel$delegate = null;
    private final int MY_CAMERA_REQUEST_CODE = 100;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String CAMERA_PERMISSION = "android.permission.CAMERA";
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final okhttp3.MultipartBody.Part getImageBody() {
        return null;
    }
    
    public final void setImageBody(@org.jetbrains.annotations.Nullable()
    okhttp3.MultipartBody.Part p0) {
    }
    
    private final com.asiantimes.ui.profile.ProfileViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCAMERA_PERMISSION() {
        return null;
    }
    
    public final void setCAMERA_PERMISSION(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    public final void updateProfileAPI() {
    }
    
    private final void observLiveData() {
    }
    
    private final boolean checkLoginStatus() {
        return false;
    }
    
    private final java.io.File persistImage(android.graphics.Bitmap bitmap, java.lang.String name) {
        return null;
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public ProfileActivity() {
        super();
    }
}