package com.asiantimes.digital_addition.ui.download;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010$\u001a\u00020%H\u0002J0\u0010&\u001a\u00020%2\u0006\u0010\u0017\u001a\u00020\u00052\u0006\u0010\'\u001a\u00020\u00052\u0006\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\u00052\u0006\u0010*\u001a\u00020\u0005H\u0002J\b\u0010+\u001a\u00020%H\u0002J\b\u0010,\u001a\u00020%H\u0002J\"\u0010-\u001a\u00020%2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/2\b\u00101\u001a\u0004\u0018\u000102H\u0014J\u0012\u00103\u001a\u00020%2\b\u00104\u001a\u0004\u0018\u000105H\u0016J\u0012\u00106\u001a\u00020%2\b\u00107\u001a\u0004\u0018\u000108H\u0014J\b\u00109\u001a\u00020%H\u0014R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u00020\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u00020\u001f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b \u0010!\u00a8\u0006:"}, d2 = {"Lcom/asiantimes/digital_addition/ui/download/DownloadActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/view/View$OnClickListener;", "()V", "LICENSE_KEY", "", "MERCHANT_ID", "appDatabase", "Lcom/asiantimes/database/AppDatabase;", "bindingView", "Lcom/asiantimes/databinding/ActivityDownloadBinding;", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "editionId", "isSubscribed", "localDB", "Lcom/asiantimes/digital_addition/database/AppDatabase;", "readyToPurchase", "", "subscriptionId", "userData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserData", "()Lcom/asiantimes/model/CommonModel$UserDetail;", "setUserData", "(Lcom/asiantimes/model/CommonModel$UserDetail;)V", "viewModel", "Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "getViewModel", "()Lcom/asiantimes/digital_addition/ui/download/DownloadViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "billingProcess", "", "buySingleProductApi", "productId", "deviceType", "userId", "email", "getATDigitalEditionPostListAPI", "getEditionListAPI", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "app_productionDebug"})
public final class DownloadActivity extends androidx.appcompat.app.AppCompatActivity implements android.view.View.OnClickListener {
    private com.asiantimes.databinding.ActivityDownloadBinding bindingView;
    private com.asiantimes.digital_addition.database.AppDatabase localDB;
    private java.lang.String isSubscribed;
    private java.lang.String editionId;
    private java.lang.String subscriptionId;
    private final java.lang.String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB";
    private final java.lang.String MERCHANT_ID = "15638088474552299429";
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private boolean readyToPurchase = false;
    @org.jetbrains.annotations.NotNull()
    private com.asiantimes.model.CommonModel.UserDetail userData;
    private com.asiantimes.database.AppDatabase appDatabase;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.asiantimes.model.CommonModel.UserDetail getUserData() {
        return null;
    }
    
    public final void setUserData(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.CommonModel.UserDetail p0) {
    }
    
    private final com.asiantimes.digital_addition.ui.download.DownloadViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View view) {
    }
    
    private final void getATDigitalEditionPostListAPI() {
    }
    
    private final void billingProcess() {
    }
    
    private final void buySingleProductApi(java.lang.String subscriptionId, java.lang.String productId, java.lang.String deviceType, java.lang.String userId, java.lang.String email) {
    }
    
    private final void getEditionListAPI() {
    }
    
    public DownloadActivity() {
        super();
    }
}