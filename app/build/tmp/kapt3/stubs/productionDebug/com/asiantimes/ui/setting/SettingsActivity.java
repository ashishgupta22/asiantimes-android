package com.asiantimes.ui.setting;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b<\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010R\u001a\u00020SJ\b\u0010T\u001a\u00020SH\u0016J\u0010\u0010U\u001a\u00020S2\u0006\u0010V\u001a\u00020WH\u0016J\u0010\u0010X\u001a\u00020S2\u0006\u0010Y\u001a\u00020ZH\u0016J\u0012\u0010[\u001a\u00020S2\b\u0010\\\u001a\u0004\u0018\u00010]H\u0014J\u0010\u0010^\u001a\u00020S2\b\u0010_\u001a\u0004\u0018\u00010`J\u0006\u0010a\u001a\u00020SR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\b\"\u0004\b\u001b\u0010\nR\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\b\"\u0004\b\u001e\u0010\nR\u001c\u0010\u001f\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\b\"\u0004\b!\u0010\nR\u001c\u0010\"\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\b\"\u0004\b$\u0010\nR\u001c\u0010%\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\b\"\u0004\b\'\u0010\nR\u001c\u0010(\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\b\"\u0004\b*\u0010\nR\u001c\u0010+\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\b\"\u0004\b-\u0010\nR\u001c\u0010.\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\b\"\u0004\b0\u0010\nR\u001c\u00101\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\b\"\u0004\b3\u0010\nR\u001c\u00104\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\b\"\u0004\b6\u0010\nR\u001c\u00107\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\b\"\u0004\b9\u0010\nR\u001c\u0010:\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010\b\"\u0004\b<\u0010\nR\u001c\u0010=\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010\b\"\u0004\b?\u0010\nR\u001c\u0010@\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010\b\"\u0004\bB\u0010\nR\u001c\u0010C\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\b\"\u0004\bE\u0010\nR\u001c\u0010F\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010\b\"\u0004\bH\u0010\nR\u001c\u0010I\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\b\"\u0004\bK\u0010\nR\u001c\u0010L\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bM\u0010\b\"\u0004\bN\u0010\nR\u001c\u0010O\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010\b\"\u0004\bQ\u0010\n\u00a8\u0006b"}, d2 = {"Lcom/asiantimes/ui/setting/SettingsActivity;", "Lcom/asiantimes/base/BaseActivity;", "Landroid/view/View$OnClickListener;", "Lcom/asiantimes/interfaces/OnClickButtonListener;", "()V", "action_bar_title", "Landroid/widget/TextView;", "getAction_bar_title", "()Landroid/widget/TextView;", "setAction_bar_title", "(Landroid/widget/TextView;)V", "iv_huge_settings", "Landroid/widget/ImageView;", "iv_large_settings", "iv_normal_settings", "iv_small_settings", "rb_background_sync", "Landroid/widget/Switch;", "rb_breaking_notification", "rb_image_sync", "rb_mobile_network_sync", "rb_never", "Landroid/widget/RadioButton;", "rb_onwifi", "rb_wifi_mobile", "tv_background_message_settings", "getTv_background_message_settings", "setTv_background_message_settings", "tv_background_settings", "getTv_background_settings", "setTv_background_settings", "tv_breaking_news_message_settings", "getTv_breaking_news_message_settings", "setTv_breaking_news_message_settings", "tv_breaking_news_settings", "getTv_breaking_news_settings", "setTv_breaking_news_settings", "tv_copy_right_settings", "getTv_copy_right_settings", "setTv_copy_right_settings", "tv_sync_images_message_settings", "getTv_sync_images_message_settings", "setTv_sync_images_message_settings", "tv_sync_images_settings", "getTv_sync_images_settings", "setTv_sync_images_settings", "tv_sync_mobile_message_settings", "getTv_sync_mobile_message_settings", "setTv_sync_mobile_message_settings", "tv_sync_mobile_settings", "getTv_sync_mobile_settings", "setTv_sync_mobile_settings", "tv_textSize", "getTv_textSize", "setTv_textSize", "tv_text_size_huge_settings", "getTv_text_size_huge_settings", "setTv_text_size_huge_settings", "tv_text_size_large_settings", "getTv_text_size_large_settings", "setTv_text_size_large_settings", "tv_text_size_normal_settings", "getTv_text_size_normal_settings", "setTv_text_size_normal_settings", "tv_text_size_settings", "getTv_text_size_settings", "setTv_text_size_settings", "tv_text_size_small_settings", "getTv_text_size_small_settings", "setTv_text_size_small_settings", "tv_waer_message_settings", "getTv_waer_message_settings", "setTv_waer_message_settings", "tv_wear_settings", "getTv_wear_settings", "setTv_wear_settings", "tv_widget_message_settings", "getTv_widget_message_settings", "setTv_widget_message_settings", "tv_widget_settings", "getTv_widget_settings", "setTv_widget_settings", "initialisationView", "", "onBackPressed", "onButtonClick", "object", "", "onClick", "view", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setBreakingNewsNotification", "isBreakingNotification", "", "setValues", "app_productionDebug"})
public final class SettingsActivity extends com.asiantimes.base.BaseActivity implements android.view.View.OnClickListener, com.asiantimes.interfaces.OnClickButtonListener {
    private android.widget.Switch rb_breaking_notification;
    private android.widget.Switch rb_background_sync;
    private android.widget.Switch rb_image_sync;
    private android.widget.Switch rb_mobile_network_sync;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_textSize;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_copy_right_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_waer_message_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_wear_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_widget_message_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_widget_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_text_size_huge_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_text_size_large_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_text_size_normal_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_text_size_small_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_text_size_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_breaking_news_message_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_breaking_news_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_sync_mobile_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_sync_mobile_message_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_sync_images_message_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_sync_images_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_background_settings;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView action_bar_title;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tv_background_message_settings;
    private android.widget.RadioButton rb_never;
    private android.widget.RadioButton rb_onwifi;
    private android.widget.RadioButton rb_wifi_mobile;
    private android.widget.ImageView iv_huge_settings;
    private android.widget.ImageView iv_large_settings;
    private android.widget.ImageView iv_normal_settings;
    private android.widget.ImageView iv_small_settings;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_textSize() {
        return null;
    }
    
    public final void setTv_textSize(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_copy_right_settings() {
        return null;
    }
    
    public final void setTv_copy_right_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_waer_message_settings() {
        return null;
    }
    
    public final void setTv_waer_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_wear_settings() {
        return null;
    }
    
    public final void setTv_wear_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_widget_message_settings() {
        return null;
    }
    
    public final void setTv_widget_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_widget_settings() {
        return null;
    }
    
    public final void setTv_widget_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_text_size_huge_settings() {
        return null;
    }
    
    public final void setTv_text_size_huge_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_text_size_large_settings() {
        return null;
    }
    
    public final void setTv_text_size_large_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_text_size_normal_settings() {
        return null;
    }
    
    public final void setTv_text_size_normal_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_text_size_small_settings() {
        return null;
    }
    
    public final void setTv_text_size_small_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_text_size_settings() {
        return null;
    }
    
    public final void setTv_text_size_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_breaking_news_message_settings() {
        return null;
    }
    
    public final void setTv_breaking_news_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_breaking_news_settings() {
        return null;
    }
    
    public final void setTv_breaking_news_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_sync_mobile_settings() {
        return null;
    }
    
    public final void setTv_sync_mobile_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_sync_mobile_message_settings() {
        return null;
    }
    
    public final void setTv_sync_mobile_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_sync_images_message_settings() {
        return null;
    }
    
    public final void setTv_sync_images_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_sync_images_settings() {
        return null;
    }
    
    public final void setTv_sync_images_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_background_settings() {
        return null;
    }
    
    public final void setTv_background_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getAction_bar_title() {
        return null;
    }
    
    public final void setAction_bar_title(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTv_background_message_settings() {
        return null;
    }
    
    public final void setTv_background_message_settings(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initialisationView() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void setBreakingNewsNotification(@org.jetbrains.annotations.Nullable()
    java.lang.String isBreakingNotification) {
    }
    
    public final void setValues() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onButtonClick(@org.jetbrains.annotations.NotNull()
    java.lang.Object object) {
    }
    
    public SettingsActivity() {
        super();
    }
}