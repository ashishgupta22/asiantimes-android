package com.asiantimes.ui.dashboard.video;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u000b\u001a\u00020\bH\u0016J\u0018\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\bH\u0017J\u0018\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bH\u0016J\u0014\u0010\u0014\u001a\u00020\r2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/CategoryVideoAdapterNew;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/asiantimes/ui/dashboard/video/CategoryVideoAdapterNew$ViewHolder;", "childCatList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetVideoListModel$VideoSubCatListBean;", "(Ljava/util/ArrayList;)V", "checkedCount", "", "context", "Landroid/content/Context;", "getItemCount", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "catList", "ViewHolder", "app_productionDebug"})
public final class CategoryVideoAdapterNew extends androidx.recyclerview.widget.RecyclerView.Adapter<com.asiantimes.ui.dashboard.video.CategoryVideoAdapterNew.ViewHolder> {
    private android.content.Context context;
    private int checkedCount = 0;
    private java.util.ArrayList<com.asiantimes.model.GetVideoListModel.VideoSubCatListBean> childCatList;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.asiantimes.ui.dashboard.video.CategoryVideoAdapterNew.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.asiantimes.ui.dashboard.video.CategoryVideoAdapterNew.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetVideoListModel.VideoSubCatListBean> catList) {
    }
    
    public CategoryVideoAdapterNew(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.asiantimes.model.GetVideoListModel.VideoSubCatListBean> childCatList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/CategoryVideoAdapterNew$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "buttonChildCat", "Landroid/widget/TextView;", "getButtonChildCat", "()Landroid/widget/TextView;", "ccMain", "Landroid/widget/LinearLayout;", "getCcMain", "()Landroid/widget/LinearLayout;", "imgCate", "Landroid/widget/ImageView;", "getImgCate", "()Landroid/widget/ImageView;", "app_productionDebug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView buttonChildCat = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.LinearLayout ccMain = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView imgCate = null;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getButtonChildCat() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getCcMain() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getImgCate() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}