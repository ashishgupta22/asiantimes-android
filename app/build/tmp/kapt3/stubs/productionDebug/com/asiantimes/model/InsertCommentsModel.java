package com.asiantimes.model;

import java.lang.System;

/**
 * Created by Abhijeet-PC on 05-Feb-18.
 */
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001BM\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\nB\u0005\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\f\u001a\u0004\u0018\u00010\u0003J\b\u0010\r\u001a\u0004\u0018\u00010\u0003J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0003J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0003J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0013\u001a\u00020\u00142\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0015\u001a\u00020\u00142\b\u0010\b\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0016\u001a\u00020\u00142\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0017\u001a\u00020\u00142\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0018\u001a\u00020\u00142\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0019\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u001a\u001a\u00020\u00142\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/asiantimes/model/InsertCommentsModel;", "", "name", "", "comments", "time", "postcommentId", "commentId", "commentTitle", "shareUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "()V", "getCommentId", "getCommentTitle", "getComments", "getName", "getPostcommentId", "getShareUrl", "getTime", "setCommentId", "", "setCommentTitle", "setComments", "setName", "setPostcommentId", "setShareUrl", "setTime", "app_productionDebug"})
public final class InsertCommentsModel {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "name")
    private java.lang.String name;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "comments")
    private java.lang.String comments;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "time")
    private java.lang.String time;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postcommentId")
    private java.lang.String postcommentId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "commentId")
    private java.lang.String commentId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "commentTitle")
    private java.lang.String commentTitle;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "shareUrl")
    private java.lang.String shareUrl;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.Nullable()
    java.lang.String name) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getComments() {
        return null;
    }
    
    public final void setComments(@org.jetbrains.annotations.Nullable()
    java.lang.String comments) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTime() {
        return null;
    }
    
    public final void setTime(@org.jetbrains.annotations.Nullable()
    java.lang.String time) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPostcommentId() {
        return null;
    }
    
    public final void setPostcommentId(@org.jetbrains.annotations.Nullable()
    java.lang.String postcommentId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCommentTitle() {
        return null;
    }
    
    public final void setCommentTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String commentTitle) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCommentId() {
        return null;
    }
    
    public final void setCommentId(@org.jetbrains.annotations.Nullable()
    java.lang.String commentId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getShareUrl() {
        return null;
    }
    
    public final void setShareUrl(@org.jetbrains.annotations.Nullable()
    java.lang.String shareUrl) {
    }
    
    public InsertCommentsModel() {
        super();
    }
    
    public InsertCommentsModel(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String comments, @org.jetbrains.annotations.Nullable()
    java.lang.String time, @org.jetbrains.annotations.Nullable()
    java.lang.String postcommentId, @org.jetbrains.annotations.Nullable()
    java.lang.String commentId, @org.jetbrains.annotations.Nullable()
    java.lang.String commentTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String shareUrl) {
        super();
    }
}