package com.asiantimes.digital_addition.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u000e\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\'J\u0018\u0010\u0007\u001a\u00020\u00032\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\'\u00a8\u0006\t"}, d2 = {"Lcom/asiantimes/digital_addition/database/dao/EditionCategoryDao;", "", "delete", "", "getEditionCategoryList", "", "Lcom/asiantimes/digital_addition/model/EditionCategoryModel$DigitalEditionCategoryList;", "insert", "categoryListEntity", "app_productionDebug"})
public abstract interface EditionCategoryDao {
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.digital_addition.model.EditionCategoryModel.DigitalEditionCategoryList> categoryListEntity);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from edition_category_list_table")
    public abstract java.util.List<com.asiantimes.digital_addition.model.EditionCategoryModel.DigitalEditionCategoryList> getEditionCategoryList();
    
    @androidx.room.Query(value = "delete from edition_category_list_table")
    public abstract void delete();
}