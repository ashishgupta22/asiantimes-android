package com.asiantimes.digital_addition.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/asiantimes/digital_addition/utility/DigitalConstants;", "", "()V", "PRIVACY_POLICY_URL", "", "getPRIVACY_POLICY_URL", "()Ljava/lang/String;", "setPRIVACY_POLICY_URL", "(Ljava/lang/String;)V", "TERMS_OF_USE_URL", "getTERMS_OF_USE_URL", "setTERMS_OF_USE_URL", "app_productionDebug"})
public final class DigitalConstants {
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String PRIVACY_POLICY_URL = "https://www.easterneye.biz/privacy-policy-mobile/";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String TERMS_OF_USE_URL = "https://www.easterneye.biz/terms-conditions-mobile/";
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.digital_addition.utility.DigitalConstants INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPRIVACY_POLICY_URL() {
        return null;
    }
    
    public final void setPRIVACY_POLICY_URL(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTERMS_OF_USE_URL() {
        return null;
    }
    
    public final void setTERMS_OF_USE_URL(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    private DigitalConstants() {
        super();
    }
}