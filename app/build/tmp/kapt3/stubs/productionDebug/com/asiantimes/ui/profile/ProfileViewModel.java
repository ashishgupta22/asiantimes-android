package com.asiantimes.ui.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J.\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u0012"}, d2 = {"Lcom/asiantimes/ui/profile/ProfileViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "profileLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/CommonModel;", "getProfileLiveData", "()Landroidx/lifecycle/MutableLiveData;", "setProfileLiveData", "(Landroidx/lifecycle/MutableLiveData;)V", "updateProfile", "", "userId", "Lokhttp3/RequestBody;", "userName", "mobile", "img", "Lokhttp3/MultipartBody$Part;", "app_productionDebug"})
public final class ProfileViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> profileLiveData;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> getProfileLiveData() {
        return null;
    }
    
    public final void setProfileLiveData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> p0) {
    }
    
    public final void updateProfile(@org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody userId, @org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody userName, @org.jetbrains.annotations.Nullable()
    okhttp3.RequestBody mobile, @org.jetbrains.annotations.Nullable()
    okhttp3.MultipartBody.Part img) {
    }
    
    public ProfileViewModel() {
        super();
    }
}