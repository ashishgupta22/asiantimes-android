package com.asiantimes.ui.comment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010J\u0016\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\t\u00a8\u0006\u0014"}, d2 = {"Lcom/asiantimes/ui/comment/CommentViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "liveDataComment", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/ResponseResult;", "getLiveDataComment", "()Landroidx/lifecycle/MutableLiveData;", "setLiveDataComment", "(Landroidx/lifecycle/MutableLiveData;)V", "liveDataInsert", "getLiveDataInsert", "setLiveDataInsert", "getAllComments", "", "postId", "", "nextPage", "getCommentReply", "comment", "app_productionDebug"})
public final class CommentViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> liveDataComment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> liveDataInsert;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getLiveDataComment() {
        return null;
    }
    
    public final void setLiveDataComment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> getLiveDataInsert() {
        return null;
    }
    
    public final void setLiveDataInsert(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResult> p0) {
    }
    
    public final void getAllComments(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, @org.jetbrains.annotations.NotNull()
    java.lang.String nextPage) {
    }
    
    public final void getCommentReply(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, @org.jetbrains.annotations.NotNull()
    java.lang.String comment) {
    }
    
    public CommentViewModel() {
        super();
    }
}