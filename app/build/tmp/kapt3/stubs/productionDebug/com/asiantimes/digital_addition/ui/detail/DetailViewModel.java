package com.asiantimes.digital_addition.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\n"}, d2 = {"Lcom/asiantimes/digital_addition/ui/detail/DetailViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "addBookmark", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/CommonModel;", "requestBean", "Lcom/asiantimes/model/RequestBean;", "getATDigitalEditionPostList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "app_productionDebug"})
public final class DetailViewModel extends androidx.lifecycle.ViewModel {
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> addBookmark(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.digital_addition.model.EditionPostListModel> getATDigitalEditionPostList(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.RequestBean requestBean) {
        return null;
    }
    
    public DetailViewModel() {
        super();
    }
}