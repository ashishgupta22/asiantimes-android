package com.asiantimes.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001d\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010%\u001a\u00020&H\u0007J\u0006\u0010\'\u001a\u00020&J\b\u0010(\u001a\u0004\u0018\u00010\u0004J\u000e\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u0004J\b\u0010,\u001a\u0004\u0018\u00010\u0004J\b\u0010-\u001a\u0004\u0018\u00010\u0004J\b\u0010.\u001a\u0004\u0018\u00010\u0004J\b\u0010/\u001a\u0004\u0018\u00010\u0004J\b\u00100\u001a\u0004\u0018\u00010\u0004J\b\u00101\u001a\u0004\u0018\u00010\u0004J\b\u00102\u001a\u0004\u0018\u00010\u0004J\u000e\u00103\u001a\u0002042\u0006\u0010+\u001a\u00020\u0004J\b\u00105\u001a\u0004\u0018\u00010\u0004J\b\u00106\u001a\u0004\u0018\u00010\u0004J\b\u00107\u001a\u0004\u0018\u00010\u0004J\u0006\u00108\u001a\u00020*J\u000e\u00109\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u0004J\b\u0010:\u001a\u0004\u0018\u00010;J\b\u0010<\u001a\u0004\u0018\u00010\u0004J\b\u0010=\u001a\u0004\u0018\u00010\u0004J\u000e\u0010>\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u0016\u0010@\u001a\u00020&2\u0006\u0010+\u001a\u00020\u00042\u0006\u0010?\u001a\u00020*J\u000e\u0010A\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u0012\u0010B\u001a\u00020&2\b\u0010C\u001a\u0004\u0018\u00010\u0004H\u0007J\u0012\u0010D\u001a\u00020&2\b\u0010E\u001a\u0004\u0018\u00010\u0004H\u0007J\u000e\u0010F\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u000e\u0010G\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u0012\u0010H\u001a\u00020&2\b\u0010I\u001a\u0004\u0018\u00010\u0004H\u0007J\u0016\u0010J\u001a\u00020&2\u0006\u0010+\u001a\u00020\u00042\u0006\u0010?\u001a\u000204J\u000e\u0010K\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u000e\u0010L\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u000e\u0010M\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004J\u0012\u0010N\u001a\u00020&2\b\u0010O\u001a\u0004\u0018\u00010\u0004H\u0007J\u000e\u0010P\u001a\u00020&2\u0006\u0010?\u001a\u00020*JD\u0010Q\u001a\u00020&2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0012\u001a\u0004\u0018\u00010\u00042\b\u0010\u0013\u001a\u0004\u0018\u00010\u00042\b\u0010\u0011\u001a\u0004\u0018\u00010\u00042\b\u0010\"\u001a\u0004\u0018\u00010\u00042\b\u0010#\u001a\u0004\u0018\u00010\u0004H\u0007J\u0018\u0010R\u001a\u00020&2\u0006\u0010+\u001a\u00020\u00042\b\u0010?\u001a\u0004\u0018\u00010\u0004J\u000e\u0010S\u001a\u00020&2\u0006\u0010T\u001a\u00020;J\u0010\u0010U\u001a\u00020&2\b\u0010V\u001a\u0004\u0018\u00010\u0004J\u000e\u0010W\u001a\u00020&2\u0006\u0010?\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010$\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006X"}, d2 = {"Lcom/asiantimes/base/AppPreferences;", "", "()V", "FONT_SCALE", "", "GoldMembership", "HTTP_SPLALGOVAL", "ImageURl", "IsPremium", "LOGIN_STATUS", "USER_ID", "USER_IMAGE", "allowVideo", "backgroundOnMobileNetwork", "backgroundSyncing", "breakingNewsNotifications", "breakingNewsSound", "digitalFreeContent", "digitalPowerList", "digitalRichList", "digitalTextLineSpacing", "digitalTextSize", "editor", "Landroid/content/SharedPreferences$Editor;", "fcmToken", "imageOffline", "isBackground", "isCarouselsLayout", "isImageOffLine", "isMobileNetwork", "isUserDigitalSubscription", "msPreferences", "pref", "Landroid/content/SharedPreferences;", "premiumPowerList", "premiumRichList", "userDetails", "cleanGetPremiumArr", "", "clearData", "getAllowVideo", "getBoolean", "", "preferenceKey", "getBreakingNewsNotifications", "getDigitalTextLineSpacing", "getDigitalTextSize", "getFONT_SCALE", "getFcmToken", "getImageOffLine", "getImageURl", "getInteger", "", "getIsBackground", "getIsMobileNetwork", "getIsPremium", "getLoginStatus", "getString", "getUserDetails", "Lcom/asiantimes/model/CommonModel$UserDetail;", "getUserDigitalSubscription", "getUserId", "setAllowVideo", "data", "setBoolean", "setBreakingNewsNotifications", "setDigitalTextLineSpacing", "textSpacing", "setDigitalTextSize", "textSize", "setFONT_SCALE", "setFcmToken", "setImageURl", "imageURl", "setInteger", "setIsBackground", "setIsImageOffLine", "setIsMobileNetwork", "setIsPremium", "isPremium", "setLoginStatus", "setPremiumArr", "setString", "setUserDeatils", "userdetails", "setUserDigitalSubscription", "isSubscribe", "setUserId", "app_productionDebug"})
public final class AppPreferences {
    private static final java.lang.String msPreferences = "com.indiaWeekly";
    private static final java.lang.String LOGIN_STATUS = "login_status";
    private static final java.lang.String USER_ID = "user_id";
    private static final java.lang.String USER_IMAGE = "user_image";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HTTP_SPLALGOVAL = "http_splalgoval";
    private static final java.lang.String fcmToken = "fcmToken";
    private static final java.lang.String allowVideo = "allowVideo";
    private static final java.lang.String FONT_SCALE = "fontScale";
    private static final java.lang.String breakingNewsNotifications = "breaking_news_notifications";
    private static final java.lang.String breakingNewsSound = "breaking_news_sound";
    private static final java.lang.String backgroundSyncing = "background_syncing";
    private static final java.lang.String backgroundOnMobileNetwork = "background_on_mobile_network";
    private static final java.lang.String isCarouselsLayout = "isCarouselsLayout";
    private static final java.lang.String isImageOffLine = "isImageOffLine";
    private static final java.lang.String isMobileNetwork = "isMobileNetwork";
    private static final java.lang.String isBackground = "isBackground";
    private static final java.lang.String imageOffline = "image_offline";
    private static final java.lang.String ImageURl = "image";
    private static final java.lang.String digitalTextSize = "digital_text_size";
    private static final java.lang.String digitalTextLineSpacing = "digital_text_line_spacing";
    private static final java.lang.String IsPremium = "isPremium";
    private static final java.lang.String isUserDigitalSubscription = "isUserDigitalSubscription";
    private static final java.lang.String GoldMembership = "GoldMembership";
    private static java.lang.String digitalPowerList = "digitalPowerList";
    private static java.lang.String digitalRichList = "digitalRichList";
    private static java.lang.String digitalFreeContent = "digitalFreeContent";
    private static java.lang.String premiumPowerList = "premiumPowerList";
    private static java.lang.String premiumRichList = "premiumRichList";
    private static java.lang.String userDetails = "userDetails";
    private static final android.content.SharedPreferences pref = null;
    private static final android.content.SharedPreferences.Editor editor = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.base.AppPreferences INSTANCE = null;
    
    public final void setBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey, boolean data) {
    }
    
    public final void setFONT_SCALE(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFONT_SCALE() {
        return null;
    }
    
    public final boolean getBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey) {
        return false;
    }
    
    public final void setString(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey, @org.jetbrains.annotations.Nullable()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getString(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey) {
        return null;
    }
    
    public final void setInteger(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey, int data) {
    }
    
    public final int getInteger(@org.jetbrains.annotations.NotNull()
    java.lang.String preferenceKey) {
        return 0;
    }
    
    public final void clearData() {
    }
    
    public final void setLoginStatus(boolean data) {
    }
    
    public final void setFcmToken(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFcmToken() {
        return null;
    }
    
    public final boolean getLoginStatus() {
        return false;
    }
    
    public final void setUserId(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserId() {
        return null;
    }
    
    public final void setAllowVideo(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAllowVideo() {
        return null;
    }
    
    public final void setBreakingNewsNotifications(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBreakingNewsNotifications() {
        return null;
    }
    
    public final void setIsBackground(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getIsBackground() {
        return null;
    }
    
    public final void setIsImageOffLine(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageOffLine() {
        return null;
    }
    
    public final void setIsMobileNetwork(@org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getIsMobileNetwork() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void setPremiumArr(@org.jetbrains.annotations.Nullable()
    java.lang.String GoldMembership, @org.jetbrains.annotations.Nullable()
    java.lang.String digitalPowerList, @org.jetbrains.annotations.Nullable()
    java.lang.String digitalRichList, @org.jetbrains.annotations.Nullable()
    java.lang.String digitalFreeContent, @org.jetbrains.annotations.Nullable()
    java.lang.String premiumPowerList, @org.jetbrains.annotations.Nullable()
    java.lang.String premiumRichList) {
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void cleanGetPremiumArr() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageURl() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void setImageURl(@org.jetbrains.annotations.Nullable()
    java.lang.String imageURl) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDigitalTextLineSpacing() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void setDigitalTextLineSpacing(@org.jetbrains.annotations.Nullable()
    java.lang.String textSpacing) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDigitalTextSize() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void setDigitalTextSize(@org.jetbrains.annotations.Nullable()
    java.lang.String textSize) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getIsPremium() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"ApplySharedPref"})
    public final void setIsPremium(@org.jetbrains.annotations.Nullable()
    java.lang.String isPremium) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserDigitalSubscription() {
        return null;
    }
    
    public final void setUserDigitalSubscription(@org.jetbrains.annotations.Nullable()
    java.lang.String isSubscribe) {
    }
    
    public final void setUserDeatils(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.CommonModel.UserDetail userdetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.asiantimes.model.CommonModel.UserDetail getUserDetails() {
        return null;
    }
    
    private AppPreferences() {
        super();
    }
}