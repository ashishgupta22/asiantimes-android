package com.asiantimes.ui.contact_us;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\n\u001a\u00020\u000bJ6\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000eR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u0014"}, d2 = {"Lcom/asiantimes/ui/contact_us/ContactUsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "liveDataUpdateDbVersion", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/ResponseResultNew;", "getLiveDataUpdateDbVersion", "()Landroidx/lifecycle/MutableLiveData;", "setLiveDataUpdateDbVersion", "(Landroidx/lifecycle/MutableLiveData;)V", "contactList", "", "contactSubmit", "emailAddress", "", "first_name", "sur_name", "phone_number", "address", "complain_detail", "app_productionDebug"})
public final class ContactUsViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResultNew> liveDataUpdateDbVersion;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResultNew> getLiveDataUpdateDbVersion() {
        return null;
    }
    
    public final void setLiveDataUpdateDbVersion(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.ResponseResultNew> p0) {
    }
    
    public final void contactSubmit(@org.jetbrains.annotations.NotNull()
    java.lang.String emailAddress, @org.jetbrains.annotations.NotNull()
    java.lang.String first_name, @org.jetbrains.annotations.NotNull()
    java.lang.String sur_name, @org.jetbrains.annotations.NotNull()
    java.lang.String phone_number, @org.jetbrains.annotations.NotNull()
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    java.lang.String complain_detail) {
    }
    
    public final void contactList() {
    }
    
    public ContactUsViewModel() {
        super();
    }
}