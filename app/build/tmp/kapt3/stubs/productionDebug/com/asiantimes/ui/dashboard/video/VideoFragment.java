package com.asiantimes.ui.dashboard.video;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001bH\u0002J\b\u0010\u001e\u001a\u00020\u0019H\u0002J\b\u0010\u001f\u001a\u00020\u0019H\u0002J&\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u001a\u0010(\u001a\u00020\u00192\u0006\u0010)\u001a\u00020!2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\b\u0010*\u001a\u00020\u0019H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006+"}, d2 = {"Lcom/asiantimes/ui/dashboard/video/VideoFragment;", "Lcom/asiantimes/base/BaseFragment;", "()V", "bindingView", "Lcom/asiantimes/databinding/FragmentVideoBinding;", "catVideoList", "Ljava/util/ArrayList;", "Lcom/asiantimes/model/GetVideoListModel$VideoSubCatListBean;", "categoriesVideoAdapter", "Lcom/asiantimes/ui/dashboard/video/CategoryVideoAdapterNew;", "newVideoAdapter", "Lcom/asiantimes/ui/dashboard/video/NewVideoAdapter;", "newVideoList", "Lcom/asiantimes/model/GetVideoListModel$NewsListBean;", "trendingVideoAdapter", "Lcom/asiantimes/ui/dashboard/video/TrendingVideoAdapter;", "trendingVideoList", "Lcom/asiantimes/model/GetVideoListModel$TrandListBean;", "viewModel", "Lcom/asiantimes/ui/dashboard/video/VideoViewModel;", "getViewModel", "()Lcom/asiantimes/ui/dashboard/video/VideoViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "callVideoDetailsIntent", "", "videoUrl", "", "postTitle", "postContent", "getTopStoriesAPI", "getTopStoriesObserver", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "setAdapter", "app_productionDebug"})
public final class VideoFragment extends com.asiantimes.base.BaseFragment {
    private java.util.ArrayList<com.asiantimes.model.GetVideoListModel.VideoSubCatListBean> catVideoList;
    private java.util.ArrayList<com.asiantimes.model.GetVideoListModel.TrandListBean> trendingVideoList;
    private java.util.ArrayList<com.asiantimes.model.GetVideoListModel.NewsListBean> newVideoList;
    private com.asiantimes.databinding.FragmentVideoBinding bindingView;
    private com.asiantimes.ui.dashboard.video.NewVideoAdapter newVideoAdapter;
    private com.asiantimes.ui.dashboard.video.TrendingVideoAdapter trendingVideoAdapter;
    private com.asiantimes.ui.dashboard.video.CategoryVideoAdapterNew categoriesVideoAdapter;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.asiantimes.ui.dashboard.video.VideoViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void getTopStoriesAPI() {
    }
    
    private final void getTopStoriesObserver() {
    }
    
    private final void setAdapter() {
    }
    
    private final void callVideoDetailsIntent(java.lang.String videoUrl, java.lang.String postTitle, java.lang.String postContent) {
    }
    
    public VideoFragment() {
        super();
    }
}