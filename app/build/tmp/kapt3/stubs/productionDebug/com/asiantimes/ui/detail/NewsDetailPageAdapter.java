package com.asiantimes.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000bH\u0016J\u0010\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/asiantimes/ui/detail/NewsDetailPageAdapter;", "Landroidx/fragment/app/FragmentStatePagerAdapter;", "activytType", "", "manager", "Landroidx/fragment/app/FragmentManager;", "dataList", "", "Lcom/asiantimes/model/GetTopStoriesModel$PostList;", "(Ljava/lang/String;Landroidx/fragment/app/FragmentManager;Ljava/util/List;)V", "getCount", "", "getItem", "Landroidx/fragment/app/Fragment;", "position", "getItemPosition", "object", "", "app_productionDebug"})
public final class NewsDetailPageAdapter extends androidx.fragment.app.FragmentStatePagerAdapter {
    private final java.lang.String activytType = null;
    private final java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> dataList = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.fragment.app.Fragment getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemPosition(@org.jetbrains.annotations.NotNull()
    java.lang.Object object) {
        return 0;
    }
    
    public NewsDetailPageAdapter(@org.jetbrains.annotations.NotNull()
    java.lang.String activytType, @org.jetbrains.annotations.Nullable()
    androidx.fragment.app.FragmentManager manager, @org.jetbrains.annotations.Nullable()
    java.util.List<com.asiantimes.model.GetTopStoriesModel.PostList> dataList) {
        super(null);
    }
}