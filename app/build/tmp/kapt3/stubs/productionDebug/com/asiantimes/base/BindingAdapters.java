package com.asiantimes.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\u001a\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\nH\u0007J\u0018\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J \u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/asiantimes/base/BindingAdapters;", "", "()V", "imageLoader", "Lcom/nostra13/universalimageloader/core/ImageLoader;", "htmlStrip", "", "view", "Landroid/widget/TextView;", "html", "", "imageUrl", "Landroid/widget/ImageView;", "imageURL", "setBookmark", "isBookmark", "", "setBookmarkNewsDetails", "setMargin", "Landroid/view/View;", "position", "", "spanCount", "app_productionDebug"})
public final class BindingAdapters {
    private static com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.base.BindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"imageUrl"})
    public static final void imageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, @org.jetbrains.annotations.Nullable()
    java.lang.String imageURL) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"htmlStrip"})
    public static final void htmlStrip(@org.jetbrains.annotations.NotNull()
    android.widget.TextView view, @org.jetbrains.annotations.NotNull()
    java.lang.String html) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"setMargin:position", "setMargin:spanCount"})
    public static final void setMargin(@org.jetbrains.annotations.NotNull()
    android.view.View view, int position, int spanCount) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"setBookmark"})
    public static final void setBookmark(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, boolean isBookmark) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"setBookmarkDetails"})
    public static final void setBookmarkNewsDetails(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, boolean isBookmark) {
    }
    
    private BindingAdapters() {
        super();
    }
}