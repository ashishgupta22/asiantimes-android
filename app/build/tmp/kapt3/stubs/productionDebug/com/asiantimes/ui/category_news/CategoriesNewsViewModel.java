package com.asiantimes.ui.category_news;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0016\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0013R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\u0007\"\u0004\b\r\u0010\t\u00a8\u0006\u0017"}, d2 = {"Lcom/asiantimes/ui/category_news/CategoriesNewsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "commonLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/asiantimes/model/CommonModel;", "getCommonLiveData", "()Landroidx/lifecycle/MutableLiveData;", "setCommonLiveData", "(Landroidx/lifecycle/MutableLiveData;)V", "liveData", "Lcom/asiantimes/model/GetTopStoriesModel;", "getLiveData", "setLiveData", "getPostListByCateID", "", "catId", "", "page", "", "saveBookmark", "postId", "isRemoved", "app_productionDebug"})
public final class CategoriesNewsViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> liveData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> commonLiveData;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> getLiveData() {
        return null;
    }
    
    public final void setLiveData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.GetTopStoriesModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> getCommonLiveData() {
        return null;
    }
    
    public final void setCommonLiveData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.asiantimes.model.CommonModel> p0) {
    }
    
    public final void getPostListByCateID(@org.jetbrains.annotations.NotNull()
    java.lang.String catId, int page) {
    }
    
    public final void saveBookmark(@org.jetbrains.annotations.NotNull()
    java.lang.String postId, int isRemoved) {
    }
    
    public CategoriesNewsViewModel() {
        super();
    }
}