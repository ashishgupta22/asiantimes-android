package com.asiantimes.database.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\b\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0005H\'\u00a8\u0006\b"}, d2 = {"Lcom/asiantimes/database/dao/UserDataDao;", "", "delete", "", "getUserData", "Lcom/asiantimes/model/CommonModel$UserDetail;", "insert", "userData", "app_productionDebug"})
public abstract interface UserDataDao {
    
    @androidx.room.Insert()
    public abstract void insert(@org.jetbrains.annotations.NotNull()
    com.asiantimes.model.CommonModel.UserDetail userData);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * from USER_DATA_TABLE")
    public abstract com.asiantimes.model.CommonModel.UserDetail getUserData();
    
    @androidx.room.Query(value = "DELETE FROM USER_DATA_TABLE")
    public abstract void delete();
}