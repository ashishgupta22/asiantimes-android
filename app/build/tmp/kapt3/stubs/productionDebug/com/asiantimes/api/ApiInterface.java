package com.asiantimes.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\bf\u0018\u0000 j2\u00020\u0001:\u0001jJ\u001b\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\'\u0010\u0006\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u001e\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\'\u0010\r\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\'\u0010\u000f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ?\u0010\u0011\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0015\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J?\u0010\u0017\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J\u001b\u0010\u0019\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u001a\u0018\u00010\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\'\u0010\u001b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ\'\u0010\u001e\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u001f\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\'\u0010 \u001a\f\u0012\u0006\u0012\u0004\u0018\u00010!\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\'\u0010\"\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010#\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u001b\u0010$\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010%\u0018\u00010\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J3\u0010&\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\'\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010(H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010)J\u001b\u0010*\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010+\u0018\u00010\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\'\u0010,\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010-\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ?\u0010.\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\'\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010(2\n\b\u0001\u0010/\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00100J\u001e\u00101\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u0001022\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\u001e\u00103\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\'\u00104\u001a\f\u0012\u0006\u0012\u0004\u0018\u000105\u0018\u00010\u00032\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ?\u00104\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\'\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010(2\n\b\u0001\u0010/\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00100J\u001e\u00106\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\u001e\u00107\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\u001e\u00108\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J\'\u00109\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ%\u0010:\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\'\u0018\u00010\u00032\b\b\u0001\u0010\u0012\u001a\u00020\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ/\u0010;\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010<\u0018\u00010\u00032\b\b\u0001\u0010\u0012\u001a\u00020\u00132\b\b\u0001\u0010\u0018\u001a\u00020\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010=J%\u0010>\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010<\u0018\u00010\u00032\b\b\u0001\u0010\u0012\u001a\u00020\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ3\u0010?\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010@\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010A\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010=J3\u0010B\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0013\u0018\u00010\u00032\n\b\u0001\u0010@\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010A\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010=J?\u0010C\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010D\u001a\u0004\u0018\u00010(H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010EJ\u001e\u0010F\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00032\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\tH\'J?\u0010G\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010H\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010I\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016Jc\u0010J\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u001a\u0018\u00010\u00032\n\b\u0001\u0010K\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010L\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010M\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010I\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010N\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010O\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010PJ?\u0010Q\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u0015\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010K\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010S\u001a\u0004\u0018\u00010TH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010UJ?\u0010V\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010W\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010X\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010Y\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J?\u0010Z\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010[\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\\\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J{\u0010]\u001a\b\u0012\u0004\u0012\u00020\u00070\u00032\b\b\u0001\u0010H\u001a\u00020R2\b\b\u0001\u0010^\u001a\u00020R2\b\b\u0001\u0010_\u001a\u00020R2\n\b\u0001\u0010`\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010a\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010b\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010c\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010R2\b\b\u0001\u0010S\u001a\u00020TH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010dJK\u0010]\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010e\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010f\u001a\u0004\u0018\u00010R2\n\b\u0001\u0010S\u001a\u0004\u0018\u00010TH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010gJK\u0010h\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u00032\n\b\u0001\u0010H\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010I\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u00132\n\b\u0001\u0010A\u001a\u0004\u0018\u00010\u0013H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010i\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006k"}, d2 = {"Lcom/asiantimes/api/ApiInterface;", "", "aboutUs", "Lretrofit2/Response;", "Lcom/asiantimes/model/ResponseResult;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addBookmark", "Lcom/asiantimes/model/CommonModel;", "requestWebAPIBean", "Lcom/asiantimes/model/RequestBean;", "(Lcom/asiantimes/model/RequestBean;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addTransaction", "requestBean", "buySingleProduct", "Lcom/asiantimes/digital_addition/model/BuySingleProductModel;", "checkSubscriptionPlan", "Lcom/asiantimes/digital_addition/model/CheckSubscriptionModel;", "commentInsert", "userId", "", "postId", "comment", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "commentList", "nextPage", "contactList", "Lcom/asiantimes/model/ResponseResultNew;", "forgotPassword", "email_address", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getATDigitalEditionBookmarks", "Lcom/asiantimes/digital_addition/model/BookmarksModel;", "getATDigitalEditionList", "Lcom/asiantimes/digital_addition/model/EditionListModel;", "getATDigitalEditionPostList", "Lcom/asiantimes/digital_addition/model/EditionPostListModel;", "getAllCategories", "Lcom/asiantimes/model/GetAllCategoriesModel;", "getBookmark", "Lcom/asiantimes/model/GetTopStoriesModel;", "", "(Ljava/lang/String;Ljava/lang/Integer;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getDigitalEditionCategory", "Lcom/asiantimes/digital_addition/model/EditionCategoryModel;", "getMorePhotos", "categoryId", "getPostListByCateID", "term_id", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPremiumPostRequest", "Lrx/Observable;", "getPremiumPostRequestNew", "getSearchResult", "Lcom/asiantimes/digital_addition/model/SearchModel;", "getShippingPrice", "getSpecialSubscription", "getSubScriptionPlan", "getSubScriptionPlanDetails", "getTopStories", "getVideoCatResult", "Lcom/asiantimes/model/GetVideoListModel;", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getVideoResult", "login", "eamil", "password", "loginStr", "saveBookmark", "isRemoved", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "setSubmitPayment", "socialLogin", "name", "phone_number", "submitContact", "emailAddress", "first_name", "sur_name", "address", "complain_detail", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "submitFeedback", "Lokhttp3/RequestBody;", "image", "Lokhttp3/MultipartBody$Part;", "(Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/MultipartBody$Part;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateDBVERSION", "deviceType", "deviceToken", "versionType", "updatePassword", "token", "newPassword", "updateProfile", "dob", "betCatId", "paypalemail", "facebookLink", "linkedinLink", "twitterLink", "(Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/MultipartBody$Part;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "userName", "mobileNumber", "(Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/MultipartBody$Part;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "userRegister", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "app_productionDebug"})
public abstract interface ApiInterface {
    @org.jetbrains.annotations.NotNull()
    public static final com.asiantimes.api.ApiInterface.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATE_DB_VERSION = "updateDBVersion";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ALL_CATEGORIES = "getAllcategories";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_REGISTER = "userRegister";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN = "login";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SOCIAL_LOGIN = "socialLogin";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FORGOT_PASSWORD = "forgotPassword";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATE_PASSWORD = "updatePassword";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_TOP_STORIES = "getTopStories";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_POST_LIST_BY_ID = "getPostListByCateID";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_BOOKMARK = "getBookmark";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SAVE_BOOKMARK = "saveBookmark";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_SEARCH_RESULT = "getSearchResult";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_UPDATE_PROFILE = "updateProfile";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_VIDEO = "getVideoList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_VIDEO_CAT = "getCatVideolist";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_COMMENTLIST = "getcommentlist";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_COMMENTINSERT = "insertPostComment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_GET_CONTACT = "getcontactlist";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_SUBMIT_CONTACT = "submitContact";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_PHOTO_SUB_CATEGORY = "getPhotoVideo";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_ABOUT_US = "getaboutus";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_PREMIUM_SUBSCRIPTION = "premiumSubscription";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_GET_SHIPPING_PRICE = "getShippingPrice";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_CATEGORY = "getATDigitalEditionCategory";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_EDITION_LIST = "getATDigitalEditionList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_EDITION_POST_LIST = "getDigitalEditonPostList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_EDITION_GET_BOOKMARK = "at_articleLIstByUser";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_EDITION_ADD_BOOKMARK = "at_saveBookmarkPost";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_DIGITAL_EDITION_SEARCH_RESULT = "at_getSearchResult";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DIGITAL_EDITION_BUY_SINGLE_PRODUCT = "buySingleProduct";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DIGITAL_EDITION_CHECK_SUBSCRIPTION = "checkSubscriptionPlan";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SUBMIT_FEEDBACK = "submitFeedback";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_PREMIUM_POST = "getPremimumArr";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_GET_SPECIAL_PLAN = "getSpecialSubscription";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_SUBSCRIPTION_PLAN_DETAILS = "getSubscriptionList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_ORDER_DETAILS = "submitDigitalPackage";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_URL_SUBSCRIPTION_PLAN = "getSubscriptionPlans";
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "updateDBVersion")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object updateDBVERSION(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "deviceType")
    java.lang.String deviceType, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "deviceToken")
    java.lang.String deviceToken, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "versionType")
    java.lang.String versionType, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "getAllcategories")
    public abstract java.lang.Object getAllCategories(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetAllCategoriesModel>> p0);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "userRegister")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object userRegister(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "name")
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "phone_number")
    java.lang.String phone_number, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "email_address")
    java.lang.String email_address, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p4);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "login")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object login(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "emailaddress")
    java.lang.String eamil, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "login")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object loginStr(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "emailaddress")
    java.lang.String eamil, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<java.lang.String>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "socialLogin")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object socialLogin(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "email_address")
    java.lang.String email_address, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "name")
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "phone_number")
    java.lang.String phone_number, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "updateProfile")
    @retrofit2.http.Multipart()
    public abstract java.lang.Object updateProfile(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "user_Id")
    okhttp3.RequestBody userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "userName")
    okhttp3.RequestBody userName, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "mobileNumber")
    okhttp3.RequestBody mobileNumber, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part()
    okhttp3.MultipartBody.Part image, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p4);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "forgotPassword")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object forgotPassword(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "emailaddress")
    java.lang.String email_address, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "updatePassword")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object updatePassword(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "user_id")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "token")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "newpassword")
    java.lang.String newPassword, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getSearchResult")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getSearchResult(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "userId")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "nextPage")
    java.lang.Integer nextPage, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "searchWord")
    java.lang.String term_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetTopStoriesModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getVideoList")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getVideoResult(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "userId")
    java.lang.String userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetVideoListModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getCatVideolist")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getVideoCatResult(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "catId")
    java.lang.String userId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "nextPage")
    java.lang.String nextPage, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetVideoListModel>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getPostListByCateID")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getPostListByCateID(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "userId")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "nextPage")
    java.lang.Integer nextPage, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "categoryId")
    java.lang.String term_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetTopStoriesModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getTopStories")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getTopStories(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "userId")
    java.lang.String userId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetTopStoriesModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getBookmark")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getBookmark(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "user_id")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "nextPage")
    java.lang.Integer nextPage, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.GetTopStoriesModel>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "saveBookmark")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object saveBookmark(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "user_id")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "postID")
    java.lang.String postId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "isRemoved")
    java.lang.Integer isRemoved, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getcommentlist")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object commentList(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "userid")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "postId")
    java.lang.String postId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "nextPage")
    java.lang.String nextPage, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResult>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "getaboutus")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object aboutUs(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResult>> p0);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "insertPostComment")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object commentInsert(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "userid")
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "postId")
    java.lang.String postId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "comment")
    java.lang.String comment, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResult>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getPhotoVideo")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object getMorePhotos(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "categoryId")
    java.lang.String categoryId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResult>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "getcontactlist")
    public abstract java.lang.Object contactList(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResultNew>> p0);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "submitContact")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object submitContact(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "emailAddress")
    java.lang.String emailAddress, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "first_name")
    java.lang.String first_name, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "sur_name")
    java.lang.String sur_name, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "phone_number")
    java.lang.String phone_number, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "address")
    java.lang.String address, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Field(value = "complain_detail")
    java.lang.String complain_detail, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResultNew>> p6);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "SIGN_IN")
    @retrofit2.http.Multipart()
    public abstract java.lang.Object updateProfile(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Part(value = "name")
    okhttp3.RequestBody name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Part(value = "dob")
    okhttp3.RequestBody dob, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Part(value = "betCatId")
    okhttp3.RequestBody betCatId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "paypalEmail")
    okhttp3.RequestBody paypalemail, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "facebookLink")
    okhttp3.RequestBody facebookLink, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "linkedinLink")
    okhttp3.RequestBody linkedinLink, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "twitterLink")
    okhttp3.RequestBody twitterLink, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "userId")
    okhttp3.RequestBody userId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Part()
    okhttp3.MultipartBody.Part image, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p9);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "getATDigitalEditionCategory")
    public abstract java.lang.Object getDigitalEditionCategory(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.EditionCategoryModel>> p0);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getATDigitalEditionList")
    public abstract java.lang.Object getATDigitalEditionList(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.EditionListModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getDigitalEditonPostList")
    public abstract java.lang.Object getATDigitalEditionPostList(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.EditionPostListModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getSubscriptionList")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract java.lang.Object getSubScriptionPlanDetails(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.ResponseResult>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "at_articleLIstByUser")
    public abstract java.lang.Object getATDigitalEditionBookmarks(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.BookmarksModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "at_getSearchResult")
    public abstract java.lang.Object getSearchResult(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.SearchModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "at_saveBookmarkPost")
    public abstract java.lang.Object addBookmark(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "buySingleProduct")
    public abstract java.lang.Object buySingleProduct(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.BuySingleProductModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "checkSubscriptionPlan")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract java.lang.Object checkSubscriptionPlan(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestWebAPIBean, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.digital_addition.model.CheckSubscriptionModel>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "submitFeedback")
    @retrofit2.http.Multipart()
    public abstract java.lang.Object submitFeedback(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "comment")
    okhttp3.RequestBody comment, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part(value = "emailAddress")
    okhttp3.RequestBody emailAddress, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part()
    okhttp3.MultipartBody.Part image, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.asiantimes.model.CommonModel>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getPremimumArr")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract rx.Observable<com.asiantimes.model.ResponseResult> getPremiumPostRequest(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getPremimumArr")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> getPremiumPostRequestNew(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getSpecialSubscription")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> getSpecialSubscription(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "submitDigitalPackage")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> addTransaction(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getSubscriptionPlans")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> getSubScriptionPlan(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "premiumSubscription")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> setSubmitPayment(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "getShippingPrice")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Response<com.asiantimes.model.ResponseResult> getShippingPrice(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.asiantimes.model.RequestBean requestBean);
    
    @kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b%\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/asiantimes/api/ApiInterface$Companion;", "", "()V", "API_URL_ABOUT_US", "", "API_URL_COMMENTINSERT", "API_URL_COMMENTLIST", "API_URL_GET_CONTACT", "API_URL_GET_SHIPPING_PRICE", "API_URL_GET_SPECIAL_PLAN", "API_URL_ORDER_DETAILS", "API_URL_PHOTO_SUB_CATEGORY", "API_URL_PREMIUM_POST", "API_URL_PREMIUM_SUBSCRIPTION", "API_URL_SUBMIT_CONTACT", "API_URL_SUBSCRIPTION_PLAN", "API_URL_SUBSCRIPTION_PLAN_DETAILS", "API_URL_UPDATE_PROFILE", "API_URL_VIDEO", "API_URL_VIDEO_CAT", "DIGITAL_EDITION_BUY_SINGLE_PRODUCT", "DIGITAL_EDITION_CHECK_SUBSCRIPTION", "FORGOT_PASSWORD", "GET_ALL_CATEGORIES", "GET_BOOKMARK", "GET_DIGITAL_CATEGORY", "GET_DIGITAL_EDITION_ADD_BOOKMARK", "GET_DIGITAL_EDITION_GET_BOOKMARK", "GET_DIGITAL_EDITION_LIST", "GET_DIGITAL_EDITION_POST_LIST", "GET_DIGITAL_EDITION_SEARCH_RESULT", "GET_POST_LIST_BY_ID", "GET_SEARCH_RESULT", "GET_TOP_STORIES", "LOGIN", "SAVE_BOOKMARK", "SOCIAL_LOGIN", "SUBMIT_FEEDBACK", "UPDATE_DB_VERSION", "UPDATE_PASSWORD", "USER_REGISTER", "app_productionDebug"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UPDATE_DB_VERSION = "updateDBVersion";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_ALL_CATEGORIES = "getAllcategories";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String USER_REGISTER = "userRegister";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LOGIN = "login";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SOCIAL_LOGIN = "socialLogin";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String FORGOT_PASSWORD = "forgotPassword";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UPDATE_PASSWORD = "updatePassword";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_TOP_STORIES = "getTopStories";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_POST_LIST_BY_ID = "getPostListByCateID";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_BOOKMARK = "getBookmark";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SAVE_BOOKMARK = "saveBookmark";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_SEARCH_RESULT = "getSearchResult";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_UPDATE_PROFILE = "updateProfile";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_VIDEO = "getVideoList";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_VIDEO_CAT = "getCatVideolist";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_COMMENTLIST = "getcommentlist";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_COMMENTINSERT = "insertPostComment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_GET_CONTACT = "getcontactlist";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_SUBMIT_CONTACT = "submitContact";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_PHOTO_SUB_CATEGORY = "getPhotoVideo";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_ABOUT_US = "getaboutus";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_PREMIUM_SUBSCRIPTION = "premiumSubscription";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_GET_SHIPPING_PRICE = "getShippingPrice";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_CATEGORY = "getATDigitalEditionCategory";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_EDITION_LIST = "getATDigitalEditionList";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_EDITION_POST_LIST = "getDigitalEditonPostList";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_EDITION_GET_BOOKMARK = "at_articleLIstByUser";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_EDITION_ADD_BOOKMARK = "at_saveBookmarkPost";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_DIGITAL_EDITION_SEARCH_RESULT = "at_getSearchResult";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DIGITAL_EDITION_BUY_SINGLE_PRODUCT = "buySingleProduct";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DIGITAL_EDITION_CHECK_SUBSCRIPTION = "checkSubscriptionPlan";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SUBMIT_FEEDBACK = "submitFeedback";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_PREMIUM_POST = "getPremimumArr";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_GET_SPECIAL_PLAN = "getSpecialSubscription";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_SUBSCRIPTION_PLAN_DETAILS = "getSubscriptionList";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_ORDER_DETAILS = "submitDigitalPackage";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_URL_SUBSCRIPTION_PLAN = "getSubscriptionPlans";
        
        private Companion() {
            super();
        }
    }
}