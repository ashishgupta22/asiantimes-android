package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int adsBanner = 1;

  public static final int bookmarkData = 2;

  public static final int breakingNews = 3;

  public static final int catList = 4;

  public static final int catName = 5;

  public static final int catNewsData = 6;

  public static final int catPostList = 7;

  public static final int cateList = 8;

  public static final int categoriesData = 9;

  public static final int clickListener = 10;

  public static final int copyRight = 11;

  public static final int digitalEditionList = 12;

  public static final int email = 13;

  public static final int groupPosition = 14;

  public static final int header = 15;

  public static final int name = 16;

  public static final int newsDetailData = 17;

  public static final int onClickListener = 18;

  public static final int position = 19;

  public static final int postCatNewsList = 20;

  public static final int postDetailText = 21;

  public static final int postHeader = 22;

  public static final int searchData = 23;

  public static final int subCategoriesData = 24;

  public static final int tradingList = 25;

  public static final int trendingNews = 26;

  public static final int userData = 27;

  public static final int videoList = 28;
}
