package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterSideMenuChildBindingImpl extends AdapterSideMenuChildBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.view_top_horizontal, 2);
        sViewsWithIds.put(R.id.view_top_vertical, 3);
        sViewsWithIds.put(R.id.view_round, 4);
        sViewsWithIds.put(R.id.view_bottom_vertical, 5);
        sViewsWithIds.put(R.id.view_horizontal, 6);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterSideMenuChildBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private AdapterSideMenuChildBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (android.view.View) bindings[5]
            , (android.view.View) bindings[6]
            , (android.view.View) bindings[4]
            , (android.view.View) bindings[2]
            , (android.view.View) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textViewChildTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.subCategoriesData == variableId) {
            setSubCategoriesData((com.asiantimes.model.GetAllCategoriesModel.Post.SubCatArr) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setSubCategoriesData(@Nullable com.asiantimes.model.GetAllCategoriesModel.Post.SubCatArr SubCategoriesData) {
        this.mSubCategoriesData = SubCategoriesData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.subCategoriesData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String subCategoriesDataSubCat = null;
        com.asiantimes.model.GetAllCategoriesModel.Post.SubCatArr subCategoriesData = mSubCategoriesData;

        if ((dirtyFlags & 0x3L) != 0) {



                if (subCategoriesData != null) {
                    // read subCategoriesData.subCat
                    subCategoriesDataSubCat = subCategoriesData.getSubCat();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewChildTitle, subCategoriesDataSubCat);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): subCategoriesData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}