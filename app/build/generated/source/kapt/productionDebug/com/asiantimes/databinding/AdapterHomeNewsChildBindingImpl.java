package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterHomeNewsChildBindingImpl extends AdapterHomeNewsChildBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.view, 13);
        sViewsWithIds.put(R.id.text_view_breaking_description, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    @Nullable
    private final android.view.View.OnClickListener mCallback13;
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    @Nullable
    private final android.view.View.OnClickListener mCallback10;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterHomeNewsChildBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private AdapterHomeNewsChildBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (com.asiantimes.widget.RoundCornerImageView) bindings[8]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (com.asiantimes.widget.TopAlignedImageView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[9]
            , (android.view.View) bindings[13]
            );
        this.imageBreakingViewBookmark.setTag(null);
        this.imageViewBookmark.setTag(null);
        this.imageViewBreakingShare.setTag(null);
        this.imageViewNews.setTag(null);
        this.imageViewShare.setTag(null);
        this.imageViewTop.setTag(null);
        this.layoutNews.setTag(null);
        this.layoutTop.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textBreakingViewTime.setTag(null);
        this.textViewBreakingTitle.setTag(null);
        this.textViewTime.setTag(null);
        this.textViewTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback15 = new com.asiantimes.generated.callback.OnClickListener(this, 6);
        mCallback13 = new com.asiantimes.generated.callback.OnClickListener(this, 4);
        mCallback11 = new com.asiantimes.generated.callback.OnClickListener(this, 2);
        mCallback14 = new com.asiantimes.generated.callback.OnClickListener(this, 5);
        mCallback12 = new com.asiantimes.generated.callback.OnClickListener(this, 3);
        mCallback10 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.groupPosition == variableId) {
            setGroupPosition((java.lang.Integer) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickGroupPositionViewListener) variable);
        }
        else if (BR.catPostList == variableId) {
            setCatPostList((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setGroupPosition(@Nullable java.lang.Integer GroupPosition) {
        this.mGroupPosition = GroupPosition;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.groupPosition);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickGroupPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }
    public void setCatPostList(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList CatPostList) {
        this.mCatPostList = CatPostList;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.catPostList);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String catPostListPostImage = null;
        java.lang.Integer position = mPosition;
        java.lang.Integer groupPosition = mGroupPosition;
        java.lang.String catPostListPostDate = null;
        com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
        boolean androidxDatabindingViewDataBindingSafeUnboxCatPostListIsBookmark = false;
        java.lang.String catPostListPostTitle = null;
        com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
        java.lang.Boolean catPostListIsBookmark = null;

        if ((dirtyFlags & 0x18L) != 0) {



                if (catPostList != null) {
                    // read catPostList.postImage
                    catPostListPostImage = catPostList.getPostImage();
                    // read catPostList.postDate
                    catPostListPostDate = catPostList.getPostDate();
                    // read catPostList.postTitle
                    catPostListPostTitle = catPostList.getPostTitle();
                    // read catPostList.isBookmark()
                    catPostListIsBookmark = catPostList.isBookmark();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(catPostList.isBookmark())
                androidxDatabindingViewDataBindingSafeUnboxCatPostListIsBookmark = androidx.databinding.ViewDataBinding.safeUnbox(catPostListIsBookmark);
        }
        // batch finished
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            com.asiantimes.base.BindingAdapters.setBookmark(this.imageBreakingViewBookmark, androidxDatabindingViewDataBindingSafeUnboxCatPostListIsBookmark);
            com.asiantimes.base.BindingAdapters.setBookmark(this.imageViewBookmark, androidxDatabindingViewDataBindingSafeUnboxCatPostListIsBookmark);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewNews, catPostListPostImage);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewTop, catPostListPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textBreakingViewTime, catPostListPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewBreakingTitle, catPostListPostTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewTime, catPostListPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewTitle, catPostListPostTitle);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.imageBreakingViewBookmark.setOnClickListener(mCallback12);
            this.imageViewBookmark.setOnClickListener(mCallback15);
            this.imageViewBreakingShare.setOnClickListener(mCallback11);
            this.imageViewShare.setOnClickListener(mCallback14);
            this.layoutNews.setOnClickListener(mCallback13);
            this.layoutTop.setOnClickListener(mCallback10);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 6: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 1);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 2);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 2);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 1);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // catPostList
                com.asiantimes.model.GetTopStoriesModel.PostList catPostList = mCatPostList;
                // onClickListener
                com.asiantimes.interfaces.OnClickGroupPositionViewListener onClickListener = mOnClickListener;
                // groupPosition
                java.lang.Integer groupPosition = mGroupPosition;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {






                    onClickListener.onClickPositionViewListener(catPostList, groupPosition, position, 0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): position
        flag 1 (0x2L): groupPosition
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): catPostList
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}