package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterEditionDetailBindingImpl extends AdapterEditionDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_header, 6);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterEditionDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private AdapterEditionDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            );
        this.imageViewDetail.setTag(null);
        this.imageViewEdition.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.textViewDetail.setTag(null);
        this.textViewTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.postHeader == variableId) {
            setPostHeader((com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList) variable);
        }
        else if (BR.postDetailText == variableId) {
            setPostDetailText((com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPostHeader(@Nullable com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList PostHeader) {
        this.mPostHeader = PostHeader;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.postHeader);
        super.requestRebind();
    }
    public void setPostDetailText(@Nullable com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList PostDetailText) {
        this.mPostDetailText = PostDetailText;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.postDetailText);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList postHeader = mPostHeader;
        boolean postHeaderPostDescriptionJavaLangObjectNull = false;
        int postHeaderPostDescriptionJavaLangObjectNullViewGONEViewVISIBLE = 0;
        java.lang.String postHeaderTitle = null;
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList.ProductList postDetailText = mPostDetailText;
        java.lang.String postHeaderPostImage = null;
        java.lang.String postDetailTextKey = null;
        java.lang.String postHeaderPostDescription = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (postHeader != null) {
                    // read postHeader.title
                    postHeaderTitle = postHeader.getTitle();
                    // read postHeader.postImage
                    postHeaderPostImage = postHeader.getPostImage();
                    // read postHeader.postDescription
                    postHeaderPostDescription = postHeader.getPostDescription();
                }


                // read postHeader.postDescription == null
                postHeaderPostDescriptionJavaLangObjectNull = (postHeaderPostDescription) == (null);
            if((dirtyFlags & 0x5L) != 0) {
                if(postHeaderPostDescriptionJavaLangObjectNull) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read postHeader.postDescription == null ? View.GONE : View.VISIBLE
                postHeaderPostDescriptionJavaLangObjectNullViewGONEViewVISIBLE = ((postHeaderPostDescriptionJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (postDetailText != null) {
                    // read postDetailText.key
                    postDetailTextKey = postDetailText.getKey();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewDetail, postDetailTextKey);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.textViewDetail, postDetailTextKey);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewEdition, postHeaderPostImage);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.mboundView3, postHeaderPostDescription);
            this.mboundView3.setVisibility(postHeaderPostDescriptionJavaLangObjectNullViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewTitle, postHeaderTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): postHeader
        flag 1 (0x2L): postDetailText
        flag 2 (0x3L): null
        flag 3 (0x4L): postHeader.postDescription == null ? View.GONE : View.VISIBLE
        flag 4 (0x5L): postHeader.postDescription == null ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}