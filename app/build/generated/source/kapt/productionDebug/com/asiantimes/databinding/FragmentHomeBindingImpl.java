package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentHomeBindingImpl extends FragmentHomeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_toolbar, 3);
        sViewsWithIds.put(R.id.image_view_search, 4);
        sViewsWithIds.put(R.id.image_view_no_internet, 5);
        sViewsWithIds.put(R.id.image_view_no_data, 6);
        sViewsWithIds.put(R.id.layout_breaking_news, 7);
        sViewsWithIds.put(R.id.text_view_breaking_news, 8);
        sViewsWithIds.put(R.id.recycle_view_breaking_news, 9);
        sViewsWithIds.put(R.id.viewBraking, 10);
        sViewsWithIds.put(R.id.layout_trending_news, 11);
        sViewsWithIds.put(R.id.text_view_trending_title, 12);
        sViewsWithIds.put(R.id.viewRed, 13);
        sViewsWithIds.put(R.id.recycle_view_trending, 14);
        sViewsWithIds.put(R.id.recycle_view_news, 15);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentHomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private FragmentHomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (com.facebook.shimmer.ShimmerFrameLayout) bindings[2]
            , (androidx.swiperefreshlayout.widget.SwipeRefreshLayout) bindings[0]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[11]
            , (androidx.recyclerview.widget.RecyclerView) bindings[9]
            , (androidx.recyclerview.widget.RecyclerView) bindings[15]
            , (androidx.recyclerview.widget.RecyclerView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[12]
            , (android.view.View) bindings[10]
            , (android.view.View) bindings[13]
            );
        this.imageViewProfile.setTag(null);
        this.layoutShimmer.setTag(null);
        this.layoutSwipeRefresh.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.userData == variableId) {
            setUserData((com.asiantimes.model.CommonModel.UserDetail) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUserData(@Nullable com.asiantimes.model.CommonModel.UserDetail UserData) {
        this.mUserData = UserData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.userData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String userDataImage = null;
        com.asiantimes.model.CommonModel.UserDetail userData = mUserData;

        if ((dirtyFlags & 0x3L) != 0) {



                if (userData != null) {
                    // read userData.image
                    userDataImage = userData.getImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewProfile, userDataImage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): userData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}