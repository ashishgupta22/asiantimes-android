package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterSideMenuGroupBindingImpl extends AdapterSideMenuGroupBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.image_view_group_indicator, 3);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterSideMenuGroupBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private AdapterSideMenuGroupBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.TextView) bindings[2]
            );
        this.imageViewIcon.setTag(null);
        this.layoutParent.setTag(null);
        this.textViewGroupTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.categoriesData == variableId) {
            setCategoriesData((com.asiantimes.model.GetAllCategoriesModel.Post) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCategoriesData(@Nullable com.asiantimes.model.GetAllCategoriesModel.Post CategoriesData) {
        this.mCategoriesData = CategoriesData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.categoriesData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.model.GetAllCategoriesModel.Post categoriesData = mCategoriesData;
        java.lang.String categoriesDataCat = null;
        java.lang.String categoriesDataImage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (categoriesData != null) {
                    // read categoriesData.cat
                    categoriesDataCat = categoriesData.getCat();
                    // read categoriesData.image
                    categoriesDataImage = categoriesData.getImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewIcon, categoriesDataImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewGroupTitle, categoriesDataCat);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): categoriesData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}