package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityProfileBindingImpl extends ActivityProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_login, 6);
        sViewsWithIds.put(R.id.text_input_layout_name, 7);
        sViewsWithIds.put(R.id.text_input_layout_email, 8);
        sViewsWithIds.put(R.id.text_input_layout_phone_number, 9);
        sViewsWithIds.put(R.id.button_continue, 10);
        sViewsWithIds.put(R.id.img_edit, 11);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private ActivityProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatButton) bindings[10]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[5]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            );
        this.imageViewProfile.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatEditText) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.userData == variableId) {
            setUserData((com.asiantimes.model.CommonModel.UserDetail) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUserData(@Nullable com.asiantimes.model.CommonModel.UserDetail UserData) {
        this.mUserData = UserData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.userData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String userDataMobileNumber = null;
        java.lang.String userDataUserName = null;
        java.lang.String userDataImage = null;
        com.asiantimes.model.CommonModel.UserDetail userData = mUserData;
        java.lang.String userDataEmailAddress = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (userData != null) {
                    // read userData.mobileNumber
                    userDataMobileNumber = userData.getMobileNumber();
                    // read userData.userName
                    userDataUserName = userData.getUserName();
                    // read userData.image
                    userDataImage = userData.getImage();
                    // read userData.emailAddress
                    userDataEmailAddress = userData.getEmailAddress();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewProfile, userDataImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, userDataUserName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, userDataEmailAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, userDataMobileNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): userData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}