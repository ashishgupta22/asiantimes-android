package com.asiantimes.digital_addition.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.digital_addition.database.converter.DataConverter;
import com.asiantimes.digital_addition.model.EditionPostListModel;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class EditionPostListDao_Impl implements EditionPostListDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<EditionPostListModel> __insertionAdapterOfEditionPostListModel;

  private final DataConverter __dataConverter = new DataConverter();

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public EditionPostListDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfEditionPostListModel = new EntityInsertionAdapter<EditionPostListModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `edition_post_list` (`id`,`status`,`errorCode`,`message`,`postList`,`state`,`editionId`) VALUES (nullif(?, 0),?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, EditionPostListModel value) {
        stmt.bindLong(1, value.getId());
        if (value.getStatus() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getStatus());
        }
        if (value.getErrorCode() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, value.getErrorCode());
        }
        if (value.getMessage() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getMessage());
        }
        final String _tmp;
        _tmp = __dataConverter.fromOptionValuesList(value.getPostList());
        if (_tmp == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, _tmp);
        }
        if (value.getState() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getState());
        }
        if (value.getEditionId() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindLong(7, value.getEditionId());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from edition_post_list";
        return _query;
      }
    };
  }

  @Override
  public void insert(final EditionPostListModel editionPostListEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfEditionPostListModel.insert(editionPostListEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public EditionPostListModel getEditionPostById(final int editionId) {
    final String _sql = "select * from edition_post_list where editionId in (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, editionId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
      final int _cursorIndexOfErrorCode = CursorUtil.getColumnIndexOrThrow(_cursor, "errorCode");
      final int _cursorIndexOfMessage = CursorUtil.getColumnIndexOrThrow(_cursor, "message");
      final int _cursorIndexOfPostList = CursorUtil.getColumnIndexOrThrow(_cursor, "postList");
      final int _cursorIndexOfState = CursorUtil.getColumnIndexOrThrow(_cursor, "state");
      final int _cursorIndexOfEditionId = CursorUtil.getColumnIndexOrThrow(_cursor, "editionId");
      final EditionPostListModel _result;
      if(_cursor.moveToFirst()) {
        _result = new EditionPostListModel();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpStatus;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmpStatus = null;
        } else {
          _tmpStatus = _cursor.getString(_cursorIndexOfStatus);
        }
        _result.setStatus(_tmpStatus);
        final Integer _tmpErrorCode;
        if (_cursor.isNull(_cursorIndexOfErrorCode)) {
          _tmpErrorCode = null;
        } else {
          _tmpErrorCode = _cursor.getInt(_cursorIndexOfErrorCode);
        }
        _result.setErrorCode(_tmpErrorCode);
        final String _tmpMessage;
        if (_cursor.isNull(_cursorIndexOfMessage)) {
          _tmpMessage = null;
        } else {
          _tmpMessage = _cursor.getString(_cursorIndexOfMessage);
        }
        _result.setMessage(_tmpMessage);
        final List<EditionPostListModel.PostList> _tmpPostList;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfPostList)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfPostList);
        }
        _tmpPostList = __dataConverter.toOptionValuesList(_tmp);
        _result.setPostList(_tmpPostList);
        final String _tmpState;
        if (_cursor.isNull(_cursorIndexOfState)) {
          _tmpState = null;
        } else {
          _tmpState = _cursor.getString(_cursorIndexOfState);
        }
        _result.setState(_tmpState);
        final Integer _tmpEditionId;
        if (_cursor.isNull(_cursorIndexOfEditionId)) {
          _tmpEditionId = null;
        } else {
          _tmpEditionId = _cursor.getInt(_cursorIndexOfEditionId);
        }
        _result.setEditionId(_tmpEditionId);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
