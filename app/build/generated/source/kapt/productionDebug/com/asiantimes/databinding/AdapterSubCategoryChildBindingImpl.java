package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterSubCategoryChildBindingImpl extends AdapterSubCategoryChildBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_items, 9);
        sViewsWithIds.put(R.id.image_view_lock, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final com.asiantimes.widget.TextViewRegular mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterSubCategoryChildBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private AdapterSubCategoryChildBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[10]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (com.asiantimes.widget.TextViewRegular) bindings[4]
            , (com.asiantimes.widget.TextViewRegular) bindings[5]
            , (com.asiantimes.widget.TextViewRegularBold) bindings[7]
            , (com.asiantimes.widget.TextViewRegularBold) bindings[3]
            );
        this.imageViewPost.setTag(null);
        this.imageViewPostFirst.setTag(null);
        this.layoutTopItem.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView8 = (com.asiantimes.widget.TextViewRegular) bindings[8];
        this.mboundView8.setTag(null);
        this.textViewDate.setTag(null);
        this.textViewDescription.setTag(null);
        this.textViewTitle.setTag(null);
        this.textViewTitleFirst.setTag(null);
        setRootTag(root);
        // listeners
        mCallback5 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.postCatNewsList == variableId) {
            setPostCatNewsList((com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList) variable);
        }
        else if (BR.clickListener == variableId) {
            setClickListener((com.asiantimes.interfaces.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPostCatNewsList(@Nullable com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList PostCatNewsList) {
        this.mPostCatNewsList = PostCatNewsList;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.postCatNewsList);
        super.requestRebind();
    }
    public void setClickListener(@Nullable com.asiantimes.interfaces.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String postCatNewsListPostImage = null;
        java.lang.String postCatNewsListPostTitle = null;
        java.lang.String postCatNewsListPostDescription = null;
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList = mPostCatNewsList;
        com.asiantimes.interfaces.OnClickListener clickListener = mClickListener;
        java.lang.String postCatNewsListPostDate = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (postCatNewsList != null) {
                    // read postCatNewsList.postImage
                    postCatNewsListPostImage = postCatNewsList.getPostImage();
                    // read postCatNewsList.postTitle
                    postCatNewsListPostTitle = postCatNewsList.getPostTitle();
                    // read postCatNewsList.postDescription
                    postCatNewsListPostDescription = postCatNewsList.getPostDescription();
                    // read postCatNewsList.postDate
                    postCatNewsListPostDate = postCatNewsList.getPostDate();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewPost, postCatNewsListPostImage);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewPostFirst, postCatNewsListPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, postCatNewsListPostDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewDate, postCatNewsListPostDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewDescription, postCatNewsListPostDescription);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.textViewTitle, postCatNewsListPostTitle);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.textViewTitleFirst, postCatNewsListPostTitle);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.layoutTopItem.setOnClickListener(mCallback5);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // postCatNewsList
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList = mPostCatNewsList;
        // clickListener
        com.asiantimes.interfaces.OnClickListener clickListener = mClickListener;
        // clickListener != null
        boolean clickListenerJavaLangObjectNull = false;



        clickListenerJavaLangObjectNull = (clickListener) != (null);
        if (clickListenerJavaLangObjectNull) {



            clickListener.onClickListener(postCatNewsList);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): postCatNewsList
        flag 1 (0x2L): clickListener
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}