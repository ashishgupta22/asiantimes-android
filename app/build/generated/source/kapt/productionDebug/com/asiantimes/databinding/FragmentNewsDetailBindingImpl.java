package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentNewsDetailBindingImpl extends FragmentNewsDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.image_view_back, 6);
        sViewsWithIds.put(R.id.ll_title, 7);
        sViewsWithIds.put(R.id.tv_ispremium_details, 8);
        sViewsWithIds.put(R.id.ll_content_details_fragment, 9);
        sViewsWithIds.put(R.id.card_view_footer, 10);
        sViewsWithIds.put(R.id.image_view_comment, 11);
        sViewsWithIds.put(R.id.tv_number_comments, 12);
        sViewsWithIds.put(R.id.image_view_share, 13);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentNewsDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 14, sIncludes, sViewsWithIds));
    }
    private FragmentNewsDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[10]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[13]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[12]
            );
        this.imageViewBookmark.setTag(null);
        this.imageViewNews.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.textViewNewsCat.setTag(null);
        this.textViewNewsTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.newsDetailData == variableId) {
            setNewsDetailData((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setNewsDetailData(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList NewsDetailData) {
        this.mNewsDetailData = NewsDetailData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.newsDetailData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String newsDetailDataPostDate = null;
        java.lang.String newsDetailDataPostCatName = null;
        java.lang.String newsDetailDataPostTitle = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxNewsDetailDataIsBookmark = false;
        java.lang.String newsDetailDataPostImage = null;
        java.lang.Boolean newsDetailDataIsBookmark = null;
        com.asiantimes.model.GetTopStoriesModel.PostList newsDetailData = mNewsDetailData;

        if ((dirtyFlags & 0x3L) != 0) {



                if (newsDetailData != null) {
                    // read newsDetailData.postDate
                    newsDetailDataPostDate = newsDetailData.getPostDate();
                    // read newsDetailData.postCatName
                    newsDetailDataPostCatName = newsDetailData.getPostCatName();
                    // read newsDetailData.postTitle
                    newsDetailDataPostTitle = newsDetailData.getPostTitle();
                    // read newsDetailData.postImage
                    newsDetailDataPostImage = newsDetailData.getPostImage();
                    // read newsDetailData.isBookmark()
                    newsDetailDataIsBookmark = newsDetailData.isBookmark();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(newsDetailData.isBookmark())
                androidxDatabindingViewDataBindingSafeUnboxNewsDetailDataIsBookmark = androidx.databinding.ViewDataBinding.safeUnbox(newsDetailDataIsBookmark);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.asiantimes.base.BindingAdapters.setBookmarkNewsDetails(this.imageViewBookmark, androidxDatabindingViewDataBindingSafeUnboxNewsDetailDataIsBookmark);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewNews, newsDetailDataPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, newsDetailDataPostDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewNewsCat, newsDetailDataPostCatName);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewNewsTitle, newsDetailDataPostTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): newsDetailData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}