package com.asiantimes.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.database.DataConverter;
import com.asiantimes.model.GetAllCategoriesModel;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GetAllCategoryDao_Impl implements GetAllCategoryDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<GetAllCategoriesModel.Post> __insertionAdapterOfPost;

  private final DataConverter __dataConverter = new DataConverter();

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public GetAllCategoryDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfPost = new EntityInsertionAdapter<GetAllCategoriesModel.Post>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `get_all_categories_table` (`autoGenerateId`,`cat`,`catId`,`catColor`,`catUrl`,`categoryType`,`image`,`subCatArr`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, GetAllCategoriesModel.Post value) {
        stmt.bindLong(1, value.getAutoGenerateId());
        if (value.getCat() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCat());
        }
        if (value.getCatId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCatId());
        }
        if (value.getCatColor() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCatColor());
        }
        if (value.getCatUrl() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getCatUrl());
        }
        if (value.getCategoryType() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getCategoryType());
        }
        if (value.getImage() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getImage());
        }
        final String _tmp;
        _tmp = __dataConverter.fromOptionValuesList(value.getSubCatArr());
        if (_tmp == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, _tmp);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM GET_ALL_CATEGORIES_TABLE";
        return _query;
      }
    };
  }

  @Override
  public void insert(final List<GetAllCategoriesModel.Post> getAllCategories) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfPost.insert(getAllCategories);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<GetAllCategoriesModel.Post> getAllCategories() {
    final String _sql = "SELECT * from GET_ALL_CATEGORIES_TABLE";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfCat = CursorUtil.getColumnIndexOrThrow(_cursor, "cat");
      final int _cursorIndexOfCatId = CursorUtil.getColumnIndexOrThrow(_cursor, "catId");
      final int _cursorIndexOfCatColor = CursorUtil.getColumnIndexOrThrow(_cursor, "catColor");
      final int _cursorIndexOfCatUrl = CursorUtil.getColumnIndexOrThrow(_cursor, "catUrl");
      final int _cursorIndexOfCategoryType = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryType");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfSubCatArr = CursorUtil.getColumnIndexOrThrow(_cursor, "subCatArr");
      final List<GetAllCategoriesModel.Post> _result = new ArrayList<GetAllCategoriesModel.Post>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final GetAllCategoriesModel.Post _item;
        _item = new GetAllCategoriesModel.Post();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _item.setAutoGenerateId(_tmpAutoGenerateId);
        final String _tmpCat;
        if (_cursor.isNull(_cursorIndexOfCat)) {
          _tmpCat = null;
        } else {
          _tmpCat = _cursor.getString(_cursorIndexOfCat);
        }
        _item.setCat(_tmpCat);
        final String _tmpCatId;
        if (_cursor.isNull(_cursorIndexOfCatId)) {
          _tmpCatId = null;
        } else {
          _tmpCatId = _cursor.getString(_cursorIndexOfCatId);
        }
        _item.setCatId(_tmpCatId);
        final String _tmpCatColor;
        if (_cursor.isNull(_cursorIndexOfCatColor)) {
          _tmpCatColor = null;
        } else {
          _tmpCatColor = _cursor.getString(_cursorIndexOfCatColor);
        }
        _item.setCatColor(_tmpCatColor);
        final String _tmpCatUrl;
        if (_cursor.isNull(_cursorIndexOfCatUrl)) {
          _tmpCatUrl = null;
        } else {
          _tmpCatUrl = _cursor.getString(_cursorIndexOfCatUrl);
        }
        _item.setCatUrl(_tmpCatUrl);
        final String _tmpCategoryType;
        if (_cursor.isNull(_cursorIndexOfCategoryType)) {
          _tmpCategoryType = null;
        } else {
          _tmpCategoryType = _cursor.getString(_cursorIndexOfCategoryType);
        }
        _item.setCategoryType(_tmpCategoryType);
        final String _tmpImage;
        if (_cursor.isNull(_cursorIndexOfImage)) {
          _tmpImage = null;
        } else {
          _tmpImage = _cursor.getString(_cursorIndexOfImage);
        }
        _item.setImage(_tmpImage);
        final List<GetAllCategoriesModel.Post.SubCatArr> _tmpSubCatArr;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfSubCatArr)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfSubCatArr);
        }
        _tmpSubCatArr = __dataConverter.toOptionValuesList(_tmp);
        _item.setSubCatArr(_tmpSubCatArr);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
