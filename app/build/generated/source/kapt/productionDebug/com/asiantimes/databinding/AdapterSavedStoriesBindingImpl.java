package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterSavedStoriesBindingImpl extends AdapterSavedStoriesBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.view, 6);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterSavedStoriesBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private AdapterSavedStoriesBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (com.asiantimes.widget.RoundCornerImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (android.view.View) bindings[6]
            );
        this.imageViewBookmark.setTag(null);
        this.imageViewSavedStories.setTag(null);
        this.imageViewShare.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textViewTime.setTag(null);
        this.textViewTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback3 = new com.asiantimes.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        mCallback2 = new com.asiantimes.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.bookmarkData == variableId) {
            setBookmarkData((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionViewListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setBookmarkData(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList BookmarkData) {
        this.mBookmarkData = BookmarkData;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.bookmarkData);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer position = mPosition;
        java.lang.String bookmarkDataPostImage = null;
        java.lang.String bookmarkDataPostDate = null;
        com.asiantimes.model.GetTopStoriesModel.PostList bookmarkData = mBookmarkData;
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        java.lang.String bookmarkDataPostTitle = null;

        if ((dirtyFlags & 0xaL) != 0) {



                if (bookmarkData != null) {
                    // read bookmarkData.postImage
                    bookmarkDataPostImage = bookmarkData.getPostImage();
                    // read bookmarkData.postDate
                    bookmarkDataPostDate = bookmarkData.getPostDate();
                    // read bookmarkData.postTitle
                    bookmarkDataPostTitle = bookmarkData.getPostTitle();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.imageViewBookmark.setOnClickListener(mCallback3);
            this.imageViewShare.setOnClickListener(mCallback2);
            this.mboundView0.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewSavedStories, bookmarkDataPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewTime, bookmarkDataPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewTitle, bookmarkDataPostTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // bookmarkData
                com.asiantimes.model.GetTopStoriesModel.PostList bookmarkData = mBookmarkData;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(bookmarkData, position, 1);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // bookmarkData
                com.asiantimes.model.GetTopStoriesModel.PostList bookmarkData = mBookmarkData;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(bookmarkData, position, 0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // bookmarkData
                com.asiantimes.model.GetTopStoriesModel.PostList bookmarkData = mBookmarkData;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(bookmarkData, position, 2);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): position
        flag 1 (0x2L): bookmarkData
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}