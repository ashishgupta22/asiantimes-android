package com.asiantimes.digital_addition.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.asiantimes.digital_addition.database.dao.EditionCategoryDao;
import com.asiantimes.digital_addition.database.dao.EditionCategoryDao_Impl;
import com.asiantimes.digital_addition.database.dao.EditionListDao;
import com.asiantimes.digital_addition.database.dao.EditionListDao_Impl;
import com.asiantimes.digital_addition.database.dao.EditionPostListDao;
import com.asiantimes.digital_addition.database.dao.EditionPostListDao_Impl;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile EditionCategoryDao _editionCategoryDao;

  private volatile EditionListDao _editionListDao;

  private volatile EditionPostListDao _editionPostListDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `get_all_categories_table` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `cat` TEXT, `catId` TEXT, `catColor` TEXT, `catUrl` TEXT, `categoryType` TEXT, `image` TEXT, `subCatArr` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `edition_category_list_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `categoryId` TEXT, `categoryName` TEXT, `categoryType` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `edition_list_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `productId` TEXT, `isDownloaded` INTEGER, `date` TEXT, `image` TEXT, `subscribed` TEXT, `subscriptionPrice` TEXT, `editionDescription` TEXT, `subscriptionId` TEXT, `categoryType` TEXT, `editionName` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `edition_post_list` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `status` TEXT, `errorCode` INTEGER, `message` TEXT, `postList` TEXT, `state` TEXT, `editionId` INTEGER)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '55347852b3368fda3eac498db35ae5dc')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `get_all_categories_table`");
        _db.execSQL("DROP TABLE IF EXISTS `edition_category_list_table`");
        _db.execSQL("DROP TABLE IF EXISTS `edition_list_table`");
        _db.execSQL("DROP TABLE IF EXISTS `edition_post_list`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsGetAllCategoriesTable = new HashMap<String, TableInfo.Column>(8);
        _columnsGetAllCategoriesTable.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("cat", new TableInfo.Column("cat", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catId", new TableInfo.Column("catId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catColor", new TableInfo.Column("catColor", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catUrl", new TableInfo.Column("catUrl", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("categoryType", new TableInfo.Column("categoryType", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("subCatArr", new TableInfo.Column("subCatArr", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysGetAllCategoriesTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesGetAllCategoriesTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoGetAllCategoriesTable = new TableInfo("get_all_categories_table", _columnsGetAllCategoriesTable, _foreignKeysGetAllCategoriesTable, _indicesGetAllCategoriesTable);
        final TableInfo _existingGetAllCategoriesTable = TableInfo.read(_db, "get_all_categories_table");
        if (! _infoGetAllCategoriesTable.equals(_existingGetAllCategoriesTable)) {
          return new RoomOpenHelper.ValidationResult(false, "get_all_categories_table(com.asiantimes.model.GetAllCategoriesModel.Post).\n"
                  + " Expected:\n" + _infoGetAllCategoriesTable + "\n"
                  + " Found:\n" + _existingGetAllCategoriesTable);
        }
        final HashMap<String, TableInfo.Column> _columnsEditionCategoryListTable = new HashMap<String, TableInfo.Column>(4);
        _columnsEditionCategoryListTable.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionCategoryListTable.put("categoryId", new TableInfo.Column("categoryId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionCategoryListTable.put("categoryName", new TableInfo.Column("categoryName", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionCategoryListTable.put("categoryType", new TableInfo.Column("categoryType", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysEditionCategoryListTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesEditionCategoryListTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoEditionCategoryListTable = new TableInfo("edition_category_list_table", _columnsEditionCategoryListTable, _foreignKeysEditionCategoryListTable, _indicesEditionCategoryListTable);
        final TableInfo _existingEditionCategoryListTable = TableInfo.read(_db, "edition_category_list_table");
        if (! _infoEditionCategoryListTable.equals(_existingEditionCategoryListTable)) {
          return new RoomOpenHelper.ValidationResult(false, "edition_category_list_table(com.asiantimes.digital_addition.model.EditionCategoryModel.DigitalEditionCategoryList).\n"
                  + " Expected:\n" + _infoEditionCategoryListTable + "\n"
                  + " Found:\n" + _existingEditionCategoryListTable);
        }
        final HashMap<String, TableInfo.Column> _columnsEditionListTable = new HashMap<String, TableInfo.Column>(11);
        _columnsEditionListTable.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("productId", new TableInfo.Column("productId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("isDownloaded", new TableInfo.Column("isDownloaded", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("date", new TableInfo.Column("date", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("subscribed", new TableInfo.Column("subscribed", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("subscriptionPrice", new TableInfo.Column("subscriptionPrice", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("editionDescription", new TableInfo.Column("editionDescription", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("subscriptionId", new TableInfo.Column("subscriptionId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("categoryType", new TableInfo.Column("categoryType", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionListTable.put("editionName", new TableInfo.Column("editionName", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysEditionListTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesEditionListTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoEditionListTable = new TableInfo("edition_list_table", _columnsEditionListTable, _foreignKeysEditionListTable, _indicesEditionListTable);
        final TableInfo _existingEditionListTable = TableInfo.read(_db, "edition_list_table");
        if (! _infoEditionListTable.equals(_existingEditionListTable)) {
          return new RoomOpenHelper.ValidationResult(false, "edition_list_table(com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList).\n"
                  + " Expected:\n" + _infoEditionListTable + "\n"
                  + " Found:\n" + _existingEditionListTable);
        }
        final HashMap<String, TableInfo.Column> _columnsEditionPostList = new HashMap<String, TableInfo.Column>(7);
        _columnsEditionPostList.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("status", new TableInfo.Column("status", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("errorCode", new TableInfo.Column("errorCode", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("message", new TableInfo.Column("message", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("postList", new TableInfo.Column("postList", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("state", new TableInfo.Column("state", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsEditionPostList.put("editionId", new TableInfo.Column("editionId", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysEditionPostList = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesEditionPostList = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoEditionPostList = new TableInfo("edition_post_list", _columnsEditionPostList, _foreignKeysEditionPostList, _indicesEditionPostList);
        final TableInfo _existingEditionPostList = TableInfo.read(_db, "edition_post_list");
        if (! _infoEditionPostList.equals(_existingEditionPostList)) {
          return new RoomOpenHelper.ValidationResult(false, "edition_post_list(com.asiantimes.digital_addition.model.EditionPostListModel).\n"
                  + " Expected:\n" + _infoEditionPostList + "\n"
                  + " Found:\n" + _existingEditionPostList);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "55347852b3368fda3eac498db35ae5dc", "3bac217bd883b55e9f72246c0c56f0ae");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "get_all_categories_table","edition_category_list_table","edition_list_table","edition_post_list");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `get_all_categories_table`");
      _db.execSQL("DELETE FROM `edition_category_list_table`");
      _db.execSQL("DELETE FROM `edition_list_table`");
      _db.execSQL("DELETE FROM `edition_post_list`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(EditionCategoryDao.class, EditionCategoryDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(EditionListDao.class, EditionListDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(EditionPostListDao.class, EditionPostListDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public EditionCategoryDao editionCategoryDao() {
    if (_editionCategoryDao != null) {
      return _editionCategoryDao;
    } else {
      synchronized(this) {
        if(_editionCategoryDao == null) {
          _editionCategoryDao = new EditionCategoryDao_Impl(this);
        }
        return _editionCategoryDao;
      }
    }
  }

  @Override
  public EditionListDao editionListDao() {
    if (_editionListDao != null) {
      return _editionListDao;
    } else {
      synchronized(this) {
        if(_editionListDao == null) {
          _editionListDao = new EditionListDao_Impl(this);
        }
        return _editionListDao;
      }
    }
  }

  @Override
  public EditionPostListDao editionPostListDao() {
    if (_editionPostListDao != null) {
      return _editionPostListDao;
    } else {
      synchronized(this) {
        if(_editionPostListDao == null) {
          _editionPostListDao = new EditionPostListDao_Impl(this);
        }
        return _editionPostListDao;
      }
    }
  }
}
