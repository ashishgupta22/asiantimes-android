package com.asiantimes.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.model.CartItem;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GetCartDao_Impl implements GetCartDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<CartItem> __insertionAdapterOfCartItem;

  private final SharedSQLiteStatement __preparedStmtOfRemoveCartItem;

  private final SharedSQLiteStatement __preparedStmtOfRemoveCartItemALL;

  private final SharedSQLiteStatement __preparedStmtOfUpdateEasternEyeCart;

  public GetCartDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfCartItem = new EntityInsertionAdapter<CartItem>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `cart_manage_table` (`autoGenerateId`,`subscriptionID`,`subscriptionTitle`,`subscriptionDate`,`subscriptionSubtitle`,`subscriptionPrice`,`subscriptionImage`,`subscriptionCount`,`id`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CartItem value) {
        stmt.bindLong(1, value.autoGenerateId);
        if (value.getSubscriptionID() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getSubscriptionID());
        }
        if (value.getSubscriptionTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getSubscriptionTitle());
        }
        if (value.getSubscriptionDate() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getSubscriptionDate());
        }
        if (value.getSubscriptionSubtitle() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getSubscriptionSubtitle());
        }
        if (value.getSubscriptionPrice() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getSubscriptionPrice());
        }
        if (value.getSubscriptionImage() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getSubscriptionImage());
        }
        stmt.bindLong(8, value.getSubscriptionCount());
        stmt.bindLong(9, value.getId());
      }
    };
    this.__preparedStmtOfRemoveCartItem = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM CART_MANAGE_TABLE WHERE subscriptionID =?";
        return _query;
      }
    };
    this.__preparedStmtOfRemoveCartItemALL = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM CART_MANAGE_TABLE";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateEasternEyeCart = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE CART_MANAGE_TABLE SET subscriptionCount=? WHERE  subscriptionID= ?";
        return _query;
      }
    };
  }

  @Override
  public void insert_new(final CartItem getAllCategories) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfCartItem.insert(getAllCategories);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int removeCartItem(final String subscriptionID) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfRemoveCartItem.acquire();
    int _argIndex = 1;
    if (subscriptionID == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, subscriptionID);
    }
    __db.beginTransaction();
    try {
      final int _result = _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
      __preparedStmtOfRemoveCartItem.release(_stmt);
    }
  }

  @Override
  public void removeCartItemALL() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfRemoveCartItemALL.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfRemoveCartItemALL.release(_stmt);
    }
  }

  @Override
  public void updateEasternEyeCart(final String subscriptionID1, final int subscriptionCount1) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateEasternEyeCart.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, subscriptionCount1);
    _argIndex = 2;
    if (subscriptionID1 == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, subscriptionID1);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateEasternEyeCart.release(_stmt);
    }
  }

  @Override
  public List<CartItem> getAllData() {
    final String _sql = "SELECT * from CART_MANAGE_TABLE";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfSubscriptionID = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionID");
      final int _cursorIndexOfSubscriptionTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionTitle");
      final int _cursorIndexOfSubscriptionDate = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionDate");
      final int _cursorIndexOfSubscriptionSubtitle = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionSubtitle");
      final int _cursorIndexOfSubscriptionPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionPrice");
      final int _cursorIndexOfSubscriptionImage = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionImage");
      final int _cursorIndexOfSubscriptionCount = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionCount");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final List<CartItem> _result = new ArrayList<CartItem>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final CartItem _item;
        _item = new CartItem();
        _item.autoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        final String _tmpSubscriptionID;
        if (_cursor.isNull(_cursorIndexOfSubscriptionID)) {
          _tmpSubscriptionID = null;
        } else {
          _tmpSubscriptionID = _cursor.getString(_cursorIndexOfSubscriptionID);
        }
        _item.setSubscriptionID(_tmpSubscriptionID);
        final String _tmpSubscriptionTitle;
        if (_cursor.isNull(_cursorIndexOfSubscriptionTitle)) {
          _tmpSubscriptionTitle = null;
        } else {
          _tmpSubscriptionTitle = _cursor.getString(_cursorIndexOfSubscriptionTitle);
        }
        _item.setSubscriptionTitle(_tmpSubscriptionTitle);
        final String _tmpSubscriptionDate;
        if (_cursor.isNull(_cursorIndexOfSubscriptionDate)) {
          _tmpSubscriptionDate = null;
        } else {
          _tmpSubscriptionDate = _cursor.getString(_cursorIndexOfSubscriptionDate);
        }
        _item.setSubscriptionDate(_tmpSubscriptionDate);
        final String _tmpSubscriptionSubtitle;
        if (_cursor.isNull(_cursorIndexOfSubscriptionSubtitle)) {
          _tmpSubscriptionSubtitle = null;
        } else {
          _tmpSubscriptionSubtitle = _cursor.getString(_cursorIndexOfSubscriptionSubtitle);
        }
        _item.setSubscriptionSubtitle(_tmpSubscriptionSubtitle);
        final String _tmpSubscriptionPrice;
        if (_cursor.isNull(_cursorIndexOfSubscriptionPrice)) {
          _tmpSubscriptionPrice = null;
        } else {
          _tmpSubscriptionPrice = _cursor.getString(_cursorIndexOfSubscriptionPrice);
        }
        _item.setSubscriptionPrice(_tmpSubscriptionPrice);
        final String _tmpSubscriptionImage;
        if (_cursor.isNull(_cursorIndexOfSubscriptionImage)) {
          _tmpSubscriptionImage = null;
        } else {
          _tmpSubscriptionImage = _cursor.getString(_cursorIndexOfSubscriptionImage);
        }
        _item.setSubscriptionImage(_tmpSubscriptionImage);
        final int _tmpSubscriptionCount;
        _tmpSubscriptionCount = _cursor.getInt(_cursorIndexOfSubscriptionCount);
        _item.setSubscriptionCount(_tmpSubscriptionCount);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
