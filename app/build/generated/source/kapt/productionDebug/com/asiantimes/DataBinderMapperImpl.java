package com.asiantimes;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.asiantimes.databinding.ActivityCategoryNewsListBindingImpl;
import com.asiantimes.databinding.ActivityCategoryVideoListBindingImpl;
import com.asiantimes.databinding.ActivityDashBoardBindingImpl;
import com.asiantimes.databinding.ActivityDigitalHomeBindingImpl;
import com.asiantimes.databinding.ActivityDownloadBindingImpl;
import com.asiantimes.databinding.ActivityForgotPasswordBindingImpl;
import com.asiantimes.databinding.ActivityLogInBindingImpl;
import com.asiantimes.databinding.ActivityNewsDetailBindingImpl;
import com.asiantimes.databinding.ActivityProfileBindingImpl;
import com.asiantimes.databinding.ActivitySearchBindingImpl;
import com.asiantimes.databinding.ActivitySignUpActivityBindingImpl;
import com.asiantimes.databinding.ActivitySplashBindingImpl;
import com.asiantimes.databinding.ActivityWelcomeActivityBindingImpl;
import com.asiantimes.databinding.AdapterBookmarkChildBindingImpl;
import com.asiantimes.databinding.AdapterBookmarkParentBindingImpl;
import com.asiantimes.databinding.AdapterCategoriesBindingImpl;
import com.asiantimes.databinding.AdapterCategoriesNewsChildBindingImpl;
import com.asiantimes.databinding.AdapterCategoriesNewsParentBindingImpl;
import com.asiantimes.databinding.AdapterCategoriesVideoBindingImpl;
import com.asiantimes.databinding.AdapterCategoriesVideoChildBindingImpl;
import com.asiantimes.databinding.AdapterDigitalAdditionBindingImpl;
import com.asiantimes.databinding.AdapterEditionDetailBindingImpl;
import com.asiantimes.databinding.AdapterHomeBreakingNewsBindingImpl;
import com.asiantimes.databinding.AdapterHomeNewsChildBindingImpl;
import com.asiantimes.databinding.AdapterHomeNewsParentBindingImpl;
import com.asiantimes.databinding.AdapterHomeTrendingBindingImpl;
import com.asiantimes.databinding.AdapterNewVideoBindingImpl;
import com.asiantimes.databinding.AdapterSavedStoriesBindingImpl;
import com.asiantimes.databinding.AdapterSearchBindingImpl;
import com.asiantimes.databinding.AdapterSideMenuChildBindingImpl;
import com.asiantimes.databinding.AdapterSideMenuGroupBindingImpl;
import com.asiantimes.databinding.AdapterSubCategoryChildBindingImpl;
import com.asiantimes.databinding.AdapterSubCategoryParentBindingImpl;
import com.asiantimes.databinding.AdapterTrendingVideoBindingImpl;
import com.asiantimes.databinding.DigitalActionBarBindingImpl;
import com.asiantimes.databinding.DigitalDrawerBindingImpl;
import com.asiantimes.databinding.FragmentCategoriesBindingImpl;
import com.asiantimes.databinding.FragmentDetailBindingImpl;
import com.asiantimes.databinding.FragmentDigitalHomeBindingImpl;
import com.asiantimes.databinding.FragmentHomeBindingImpl;
import com.asiantimes.databinding.FragmentNewsDetailBindingImpl;
import com.asiantimes.databinding.FragmentSavedStoriesBindingImpl;
import com.asiantimes.databinding.FragmentSideMenuBindingImpl;
import com.asiantimes.databinding.FragmentVideoBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYCATEGORYNEWSLIST = 1;

  private static final int LAYOUT_ACTIVITYCATEGORYVIDEOLIST = 2;

  private static final int LAYOUT_ACTIVITYDASHBOARD = 3;

  private static final int LAYOUT_ACTIVITYDIGITALHOME = 4;

  private static final int LAYOUT_ACTIVITYDOWNLOAD = 5;

  private static final int LAYOUT_ACTIVITYFORGOTPASSWORD = 6;

  private static final int LAYOUT_ACTIVITYLOGIN = 7;

  private static final int LAYOUT_ACTIVITYNEWSDETAIL = 8;

  private static final int LAYOUT_ACTIVITYPROFILE = 9;

  private static final int LAYOUT_ACTIVITYSEARCH = 10;

  private static final int LAYOUT_ACTIVITYSIGNUPACTIVITY = 11;

  private static final int LAYOUT_ACTIVITYSPLASH = 12;

  private static final int LAYOUT_ACTIVITYWELCOMEACTIVITY = 13;

  private static final int LAYOUT_ADAPTERBOOKMARKCHILD = 14;

  private static final int LAYOUT_ADAPTERBOOKMARKPARENT = 15;

  private static final int LAYOUT_ADAPTERCATEGORIES = 16;

  private static final int LAYOUT_ADAPTERCATEGORIESNEWSCHILD = 17;

  private static final int LAYOUT_ADAPTERCATEGORIESNEWSPARENT = 18;

  private static final int LAYOUT_ADAPTERCATEGORIESVIDEO = 19;

  private static final int LAYOUT_ADAPTERCATEGORIESVIDEOCHILD = 20;

  private static final int LAYOUT_ADAPTERDIGITALADDITION = 21;

  private static final int LAYOUT_ADAPTEREDITIONDETAIL = 22;

  private static final int LAYOUT_ADAPTERHOMEBREAKINGNEWS = 23;

  private static final int LAYOUT_ADAPTERHOMENEWSCHILD = 24;

  private static final int LAYOUT_ADAPTERHOMENEWSPARENT = 25;

  private static final int LAYOUT_ADAPTERHOMETRENDING = 26;

  private static final int LAYOUT_ADAPTERNEWVIDEO = 27;

  private static final int LAYOUT_ADAPTERSAVEDSTORIES = 28;

  private static final int LAYOUT_ADAPTERSEARCH = 29;

  private static final int LAYOUT_ADAPTERSIDEMENUCHILD = 30;

  private static final int LAYOUT_ADAPTERSIDEMENUGROUP = 31;

  private static final int LAYOUT_ADAPTERSUBCATEGORYCHILD = 32;

  private static final int LAYOUT_ADAPTERSUBCATEGORYPARENT = 33;

  private static final int LAYOUT_ADAPTERTRENDINGVIDEO = 34;

  private static final int LAYOUT_DIGITALACTIONBAR = 35;

  private static final int LAYOUT_DIGITALDRAWER = 36;

  private static final int LAYOUT_FRAGMENTCATEGORIES = 37;

  private static final int LAYOUT_FRAGMENTDETAIL = 38;

  private static final int LAYOUT_FRAGMENTDIGITALHOME = 39;

  private static final int LAYOUT_FRAGMENTHOME = 40;

  private static final int LAYOUT_FRAGMENTNEWSDETAIL = 41;

  private static final int LAYOUT_FRAGMENTSAVEDSTORIES = 42;

  private static final int LAYOUT_FRAGMENTSIDEMENU = 43;

  private static final int LAYOUT_FRAGMENTVIDEO = 44;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(44);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_category_news_list, LAYOUT_ACTIVITYCATEGORYNEWSLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_category_video_list, LAYOUT_ACTIVITYCATEGORYVIDEOLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_dash_board, LAYOUT_ACTIVITYDASHBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_digital_home, LAYOUT_ACTIVITYDIGITALHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_download, LAYOUT_ACTIVITYDOWNLOAD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_forgot_password, LAYOUT_ACTIVITYFORGOTPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_log_in, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_news_detail, LAYOUT_ACTIVITYNEWSDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_profile, LAYOUT_ACTIVITYPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_search, LAYOUT_ACTIVITYSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_sign_up_activity, LAYOUT_ACTIVITYSIGNUPACTIVITY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.activity_welcome_activity, LAYOUT_ACTIVITYWELCOMEACTIVITY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_bookmark_child, LAYOUT_ADAPTERBOOKMARKCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_bookmark_parent, LAYOUT_ADAPTERBOOKMARKPARENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_categories, LAYOUT_ADAPTERCATEGORIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_categories_news_child, LAYOUT_ADAPTERCATEGORIESNEWSCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_categories_news_parent, LAYOUT_ADAPTERCATEGORIESNEWSPARENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_categories_video, LAYOUT_ADAPTERCATEGORIESVIDEO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_categories_video_child, LAYOUT_ADAPTERCATEGORIESVIDEOCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_digital_addition, LAYOUT_ADAPTERDIGITALADDITION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_edition_detail, LAYOUT_ADAPTEREDITIONDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_home_breaking_news, LAYOUT_ADAPTERHOMEBREAKINGNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_home_news_child, LAYOUT_ADAPTERHOMENEWSCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_home_news_parent, LAYOUT_ADAPTERHOMENEWSPARENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_home_trending, LAYOUT_ADAPTERHOMETRENDING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_new_video, LAYOUT_ADAPTERNEWVIDEO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_saved_stories, LAYOUT_ADAPTERSAVEDSTORIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_search, LAYOUT_ADAPTERSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_side_menu_child, LAYOUT_ADAPTERSIDEMENUCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_side_menu_group, LAYOUT_ADAPTERSIDEMENUGROUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_sub_category_child, LAYOUT_ADAPTERSUBCATEGORYCHILD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_sub_category_parent, LAYOUT_ADAPTERSUBCATEGORYPARENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.adapter_trending_video, LAYOUT_ADAPTERTRENDINGVIDEO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.digital_action_bar, LAYOUT_DIGITALACTIONBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.digital_drawer, LAYOUT_DIGITALDRAWER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_categories, LAYOUT_FRAGMENTCATEGORIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_detail, LAYOUT_FRAGMENTDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_digital_home, LAYOUT_FRAGMENTDIGITALHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_news_detail, LAYOUT_FRAGMENTNEWSDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_saved_stories, LAYOUT_FRAGMENTSAVEDSTORIES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_side_menu, LAYOUT_FRAGMENTSIDEMENU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.asiantimes.R.layout.fragment_video, LAYOUT_FRAGMENTVIDEO);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYCATEGORYNEWSLIST: {
          if ("layout/activity_category_news_list_0".equals(tag)) {
            return new ActivityCategoryNewsListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_category_news_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYCATEGORYVIDEOLIST: {
          if ("layout/activity_category_video_list_0".equals(tag)) {
            return new ActivityCategoryVideoListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_category_video_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYDASHBOARD: {
          if ("layout/activity_dash_board_0".equals(tag)) {
            return new ActivityDashBoardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_dash_board is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYDIGITALHOME: {
          if ("layout/activity_digital_home_0".equals(tag)) {
            return new ActivityDigitalHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_digital_home is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYDOWNLOAD: {
          if ("layout/activity_download_0".equals(tag)) {
            return new ActivityDownloadBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_download is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYFORGOTPASSWORD: {
          if ("layout/activity_forgot_password_0".equals(tag)) {
            return new ActivityForgotPasswordBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_forgot_password is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLOGIN: {
          if ("layout/activity_log_in_0".equals(tag)) {
            return new ActivityLogInBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_log_in is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYNEWSDETAIL: {
          if ("layout/activity_news_detail_0".equals(tag)) {
            return new ActivityNewsDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_news_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPROFILE: {
          if ("layout/activity_profile_0".equals(tag)) {
            return new ActivityProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSEARCH: {
          if ("layout/activity_search_0".equals(tag)) {
            return new ActivitySearchBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_search is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSIGNUPACTIVITY: {
          if ("layout/activity_sign_up_activity_0".equals(tag)) {
            return new ActivitySignUpActivityBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_sign_up_activity is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSPLASH: {
          if ("layout/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYWELCOMEACTIVITY: {
          if ("layout/activity_welcome_activity_0".equals(tag)) {
            return new ActivityWelcomeActivityBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_welcome_activity is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERBOOKMARKCHILD: {
          if ("layout/adapter_bookmark_child_0".equals(tag)) {
            return new AdapterBookmarkChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_bookmark_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERBOOKMARKPARENT: {
          if ("layout/adapter_bookmark_parent_0".equals(tag)) {
            return new AdapterBookmarkParentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_bookmark_parent is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERCATEGORIES: {
          if ("layout/adapter_categories_0".equals(tag)) {
            return new AdapterCategoriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_categories is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERCATEGORIESNEWSCHILD: {
          if ("layout/adapter_categories_news_child_0".equals(tag)) {
            return new AdapterCategoriesNewsChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_categories_news_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERCATEGORIESNEWSPARENT: {
          if ("layout/adapter_categories_news_parent_0".equals(tag)) {
            return new AdapterCategoriesNewsParentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_categories_news_parent is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERCATEGORIESVIDEO: {
          if ("layout/adapter_categories_video_0".equals(tag)) {
            return new AdapterCategoriesVideoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_categories_video is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERCATEGORIESVIDEOCHILD: {
          if ("layout/adapter_categories_video_child_0".equals(tag)) {
            return new AdapterCategoriesVideoChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_categories_video_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERDIGITALADDITION: {
          if ("layout/adapter_digital_addition_0".equals(tag)) {
            return new AdapterDigitalAdditionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_digital_addition is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTEREDITIONDETAIL: {
          if ("layout/adapter_edition_detail_0".equals(tag)) {
            return new AdapterEditionDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_edition_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERHOMEBREAKINGNEWS: {
          if ("layout/adapter_home_breaking_news_0".equals(tag)) {
            return new AdapterHomeBreakingNewsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_home_breaking_news is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERHOMENEWSCHILD: {
          if ("layout/adapter_home_news_child_0".equals(tag)) {
            return new AdapterHomeNewsChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_home_news_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERHOMENEWSPARENT: {
          if ("layout/adapter_home_news_parent_0".equals(tag)) {
            return new AdapterHomeNewsParentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_home_news_parent is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERHOMETRENDING: {
          if ("layout/adapter_home_trending_0".equals(tag)) {
            return new AdapterHomeTrendingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_home_trending is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERNEWVIDEO: {
          if ("layout/adapter_new_video_0".equals(tag)) {
            return new AdapterNewVideoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_new_video is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSAVEDSTORIES: {
          if ("layout/adapter_saved_stories_0".equals(tag)) {
            return new AdapterSavedStoriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_saved_stories is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSEARCH: {
          if ("layout/adapter_search_0".equals(tag)) {
            return new AdapterSearchBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_search is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSIDEMENUCHILD: {
          if ("layout/adapter_side_menu_child_0".equals(tag)) {
            return new AdapterSideMenuChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_side_menu_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSIDEMENUGROUP: {
          if ("layout/adapter_side_menu_group_0".equals(tag)) {
            return new AdapterSideMenuGroupBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_side_menu_group is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSUBCATEGORYCHILD: {
          if ("layout/adapter_sub_category_child_0".equals(tag)) {
            return new AdapterSubCategoryChildBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_sub_category_child is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSUBCATEGORYPARENT: {
          if ("layout/adapter_sub_category_parent_0".equals(tag)) {
            return new AdapterSubCategoryParentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_sub_category_parent is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERTRENDINGVIDEO: {
          if ("layout/adapter_trending_video_0".equals(tag)) {
            return new AdapterTrendingVideoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_trending_video is invalid. Received: " + tag);
        }
        case  LAYOUT_DIGITALACTIONBAR: {
          if ("layout/digital_action_bar_0".equals(tag)) {
            return new DigitalActionBarBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for digital_action_bar is invalid. Received: " + tag);
        }
        case  LAYOUT_DIGITALDRAWER: {
          if ("layout/digital_drawer_0".equals(tag)) {
            return new DigitalDrawerBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for digital_drawer is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCATEGORIES: {
          if ("layout/fragment_categories_0".equals(tag)) {
            return new FragmentCategoriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_categories is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTDETAIL: {
          if ("layout/fragment_detail_0".equals(tag)) {
            return new FragmentDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTDIGITALHOME: {
          if ("layout/fragment_digital_home_0".equals(tag)) {
            return new FragmentDigitalHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_digital_home is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTHOME: {
          if ("layout/fragment_home_0".equals(tag)) {
            return new FragmentHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTNEWSDETAIL: {
          if ("layout/fragment_news_detail_0".equals(tag)) {
            return new FragmentNewsDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_news_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSAVEDSTORIES: {
          if ("layout/fragment_saved_stories_0".equals(tag)) {
            return new FragmentSavedStoriesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_saved_stories is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSIDEMENU: {
          if ("layout/fragment_side_menu_0".equals(tag)) {
            return new FragmentSideMenuBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_side_menu is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTVIDEO: {
          if ("layout/fragment_video_0".equals(tag)) {
            return new FragmentVideoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_video is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(29);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "adsBanner");
      sKeys.put(2, "bookmarkData");
      sKeys.put(3, "breakingNews");
      sKeys.put(4, "catList");
      sKeys.put(5, "catName");
      sKeys.put(6, "catNewsData");
      sKeys.put(7, "catPostList");
      sKeys.put(8, "cateList");
      sKeys.put(9, "categoriesData");
      sKeys.put(10, "clickListener");
      sKeys.put(11, "copyRight");
      sKeys.put(12, "digitalEditionList");
      sKeys.put(13, "email");
      sKeys.put(14, "groupPosition");
      sKeys.put(15, "header");
      sKeys.put(16, "name");
      sKeys.put(17, "newsDetailData");
      sKeys.put(18, "onClickListener");
      sKeys.put(19, "position");
      sKeys.put(20, "postCatNewsList");
      sKeys.put(21, "postDetailText");
      sKeys.put(22, "postHeader");
      sKeys.put(23, "searchData");
      sKeys.put(24, "subCategoriesData");
      sKeys.put(25, "tradingList");
      sKeys.put(26, "trendingNews");
      sKeys.put(27, "userData");
      sKeys.put(28, "videoList");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(44);

    static {
      sKeys.put("layout/activity_category_news_list_0", com.asiantimes.R.layout.activity_category_news_list);
      sKeys.put("layout/activity_category_video_list_0", com.asiantimes.R.layout.activity_category_video_list);
      sKeys.put("layout/activity_dash_board_0", com.asiantimes.R.layout.activity_dash_board);
      sKeys.put("layout/activity_digital_home_0", com.asiantimes.R.layout.activity_digital_home);
      sKeys.put("layout/activity_download_0", com.asiantimes.R.layout.activity_download);
      sKeys.put("layout/activity_forgot_password_0", com.asiantimes.R.layout.activity_forgot_password);
      sKeys.put("layout/activity_log_in_0", com.asiantimes.R.layout.activity_log_in);
      sKeys.put("layout/activity_news_detail_0", com.asiantimes.R.layout.activity_news_detail);
      sKeys.put("layout/activity_profile_0", com.asiantimes.R.layout.activity_profile);
      sKeys.put("layout/activity_search_0", com.asiantimes.R.layout.activity_search);
      sKeys.put("layout/activity_sign_up_activity_0", com.asiantimes.R.layout.activity_sign_up_activity);
      sKeys.put("layout/activity_splash_0", com.asiantimes.R.layout.activity_splash);
      sKeys.put("layout/activity_welcome_activity_0", com.asiantimes.R.layout.activity_welcome_activity);
      sKeys.put("layout/adapter_bookmark_child_0", com.asiantimes.R.layout.adapter_bookmark_child);
      sKeys.put("layout/adapter_bookmark_parent_0", com.asiantimes.R.layout.adapter_bookmark_parent);
      sKeys.put("layout/adapter_categories_0", com.asiantimes.R.layout.adapter_categories);
      sKeys.put("layout/adapter_categories_news_child_0", com.asiantimes.R.layout.adapter_categories_news_child);
      sKeys.put("layout/adapter_categories_news_parent_0", com.asiantimes.R.layout.adapter_categories_news_parent);
      sKeys.put("layout/adapter_categories_video_0", com.asiantimes.R.layout.adapter_categories_video);
      sKeys.put("layout/adapter_categories_video_child_0", com.asiantimes.R.layout.adapter_categories_video_child);
      sKeys.put("layout/adapter_digital_addition_0", com.asiantimes.R.layout.adapter_digital_addition);
      sKeys.put("layout/adapter_edition_detail_0", com.asiantimes.R.layout.adapter_edition_detail);
      sKeys.put("layout/adapter_home_breaking_news_0", com.asiantimes.R.layout.adapter_home_breaking_news);
      sKeys.put("layout/adapter_home_news_child_0", com.asiantimes.R.layout.adapter_home_news_child);
      sKeys.put("layout/adapter_home_news_parent_0", com.asiantimes.R.layout.adapter_home_news_parent);
      sKeys.put("layout/adapter_home_trending_0", com.asiantimes.R.layout.adapter_home_trending);
      sKeys.put("layout/adapter_new_video_0", com.asiantimes.R.layout.adapter_new_video);
      sKeys.put("layout/adapter_saved_stories_0", com.asiantimes.R.layout.adapter_saved_stories);
      sKeys.put("layout/adapter_search_0", com.asiantimes.R.layout.adapter_search);
      sKeys.put("layout/adapter_side_menu_child_0", com.asiantimes.R.layout.adapter_side_menu_child);
      sKeys.put("layout/adapter_side_menu_group_0", com.asiantimes.R.layout.adapter_side_menu_group);
      sKeys.put("layout/adapter_sub_category_child_0", com.asiantimes.R.layout.adapter_sub_category_child);
      sKeys.put("layout/adapter_sub_category_parent_0", com.asiantimes.R.layout.adapter_sub_category_parent);
      sKeys.put("layout/adapter_trending_video_0", com.asiantimes.R.layout.adapter_trending_video);
      sKeys.put("layout/digital_action_bar_0", com.asiantimes.R.layout.digital_action_bar);
      sKeys.put("layout/digital_drawer_0", com.asiantimes.R.layout.digital_drawer);
      sKeys.put("layout/fragment_categories_0", com.asiantimes.R.layout.fragment_categories);
      sKeys.put("layout/fragment_detail_0", com.asiantimes.R.layout.fragment_detail);
      sKeys.put("layout/fragment_digital_home_0", com.asiantimes.R.layout.fragment_digital_home);
      sKeys.put("layout/fragment_home_0", com.asiantimes.R.layout.fragment_home);
      sKeys.put("layout/fragment_news_detail_0", com.asiantimes.R.layout.fragment_news_detail);
      sKeys.put("layout/fragment_saved_stories_0", com.asiantimes.R.layout.fragment_saved_stories);
      sKeys.put("layout/fragment_side_menu_0", com.asiantimes.R.layout.fragment_side_menu);
      sKeys.put("layout/fragment_video_0", com.asiantimes.R.layout.fragment_video);
    }
  }
}
