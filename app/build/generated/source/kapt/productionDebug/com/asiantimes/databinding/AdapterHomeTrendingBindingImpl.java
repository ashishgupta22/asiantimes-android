package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterHomeTrendingBindingImpl extends AdapterHomeTrendingBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterHomeTrendingBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private AdapterHomeTrendingBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.asiantimes.widget.TopAlignedImageView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            );
        this.imageViewTrending.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.textViewBreakingTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback4 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.trendingNews == variableId) {
            setTrendingNews((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionViewListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setTrendingNews(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList TrendingNews) {
        this.mTrendingNews = TrendingNews;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.trendingNews);
        super.requestRebind();
    }
    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String trendingNewsPostTitle = null;
        com.asiantimes.model.GetTopStoriesModel.PostList trendingNews = mTrendingNews;
        java.lang.Integer position = mPosition;
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        java.lang.String trendingNewsPostImage = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (trendingNews != null) {
                    // read trendingNews.postTitle
                    trendingNewsPostTitle = trendingNews.getPostTitle();
                    // read trendingNews.postImage
                    trendingNewsPostImage = trendingNews.getPostImage();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewTrending, trendingNewsPostImage);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewBreakingTitle, trendingNewsPostTitle);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback4);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // trendingNews
        com.asiantimes.model.GetTopStoriesModel.PostList trendingNews = mTrendingNews;
        // position
        java.lang.Integer position = mPosition;
        // onClickListener
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        // onClickListener != null
        boolean onClickListenerJavaLangObjectNull = false;



        onClickListenerJavaLangObjectNull = (onClickListener) != (null);
        if (onClickListenerJavaLangObjectNull) {





            onClickListener.onClickPositionViewListener(trendingNews, position, 0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): trendingNews
        flag 1 (0x2L): position
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}