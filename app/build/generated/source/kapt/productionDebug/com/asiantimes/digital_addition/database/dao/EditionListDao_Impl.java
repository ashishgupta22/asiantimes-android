package com.asiantimes.digital_addition.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.digital_addition.model.EditionListModel;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class EditionListDao_Impl implements EditionListDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<EditionListModel.DigitalEditionList> __insertionAdapterOfDigitalEditionList;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public EditionListDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDigitalEditionList = new EntityInsertionAdapter<EditionListModel.DigitalEditionList>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `edition_list_table` (`id`,`productId`,`isDownloaded`,`date`,`image`,`subscribed`,`subscriptionPrice`,`editionDescription`,`subscriptionId`,`categoryType`,`editionName`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, EditionListModel.DigitalEditionList value) {
        stmt.bindLong(1, value.getId());
        if (value.getProductId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getProductId());
        }
        final Integer _tmp;
        _tmp = value.isDownloaded() == null ? null : (value.isDownloaded() ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, _tmp);
        }
        if (value.getDate() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDate());
        }
        if (value.getImage() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getImage());
        }
        if (value.getSubscribed() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getSubscribed());
        }
        if (value.getSubscriptionPrice() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getSubscriptionPrice());
        }
        if (value.getEditionDescription() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getEditionDescription());
        }
        if (value.getSubscriptionId() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getSubscriptionId());
        }
        if (value.getCategoryType() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getCategoryType());
        }
        if (value.getEditionName() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getEditionName());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from edition_list_table";
        return _query;
      }
    };
  }

  @Override
  public void insert(final List<EditionListModel.DigitalEditionList> editionListEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfDigitalEditionList.insert(editionListEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<EditionListModel.DigitalEditionList> getEditionList() {
    final String _sql = "SELECT * from edition_list_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "productId");
      final int _cursorIndexOfIsDownloaded = CursorUtil.getColumnIndexOrThrow(_cursor, "isDownloaded");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfSubscribed = CursorUtil.getColumnIndexOrThrow(_cursor, "subscribed");
      final int _cursorIndexOfSubscriptionPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionPrice");
      final int _cursorIndexOfEditionDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "editionDescription");
      final int _cursorIndexOfSubscriptionId = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionId");
      final int _cursorIndexOfCategoryType = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryType");
      final int _cursorIndexOfEditionName = CursorUtil.getColumnIndexOrThrow(_cursor, "editionName");
      final List<EditionListModel.DigitalEditionList> _result = new ArrayList<EditionListModel.DigitalEditionList>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final EditionListModel.DigitalEditionList _item;
        _item = new EditionListModel.DigitalEditionList();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpProductId;
        if (_cursor.isNull(_cursorIndexOfProductId)) {
          _tmpProductId = null;
        } else {
          _tmpProductId = _cursor.getString(_cursorIndexOfProductId);
        }
        _item.setProductId(_tmpProductId);
        final Boolean _tmpIsDownloaded;
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfIsDownloaded)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfIsDownloaded);
        }
        _tmpIsDownloaded = _tmp == null ? null : _tmp != 0;
        _item.setDownloaded(_tmpIsDownloaded);
        final String _tmpDate;
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _tmpDate = null;
        } else {
          _tmpDate = _cursor.getString(_cursorIndexOfDate);
        }
        _item.setDate(_tmpDate);
        final String _tmpImage;
        if (_cursor.isNull(_cursorIndexOfImage)) {
          _tmpImage = null;
        } else {
          _tmpImage = _cursor.getString(_cursorIndexOfImage);
        }
        _item.setImage(_tmpImage);
        final String _tmpSubscribed;
        if (_cursor.isNull(_cursorIndexOfSubscribed)) {
          _tmpSubscribed = null;
        } else {
          _tmpSubscribed = _cursor.getString(_cursorIndexOfSubscribed);
        }
        _item.setSubscribed(_tmpSubscribed);
        final String _tmpSubscriptionPrice;
        if (_cursor.isNull(_cursorIndexOfSubscriptionPrice)) {
          _tmpSubscriptionPrice = null;
        } else {
          _tmpSubscriptionPrice = _cursor.getString(_cursorIndexOfSubscriptionPrice);
        }
        _item.setSubscriptionPrice(_tmpSubscriptionPrice);
        final String _tmpEditionDescription;
        if (_cursor.isNull(_cursorIndexOfEditionDescription)) {
          _tmpEditionDescription = null;
        } else {
          _tmpEditionDescription = _cursor.getString(_cursorIndexOfEditionDescription);
        }
        _item.setEditionDescription(_tmpEditionDescription);
        final String _tmpSubscriptionId;
        if (_cursor.isNull(_cursorIndexOfSubscriptionId)) {
          _tmpSubscriptionId = null;
        } else {
          _tmpSubscriptionId = _cursor.getString(_cursorIndexOfSubscriptionId);
        }
        _item.setSubscriptionId(_tmpSubscriptionId);
        final String _tmpCategoryType;
        if (_cursor.isNull(_cursorIndexOfCategoryType)) {
          _tmpCategoryType = null;
        } else {
          _tmpCategoryType = _cursor.getString(_cursorIndexOfCategoryType);
        }
        _item.setCategoryType(_tmpCategoryType);
        final String _tmpEditionName;
        if (_cursor.isNull(_cursorIndexOfEditionName)) {
          _tmpEditionName = null;
        } else {
          _tmpEditionName = _cursor.getString(_cursorIndexOfEditionName);
        }
        _item.setEditionName(_tmpEditionName);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public EditionListModel.DigitalEditionList getEditionById(final int productId) {
    final String _sql = "select * from edition_list_table where productId in (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, productId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "productId");
      final int _cursorIndexOfIsDownloaded = CursorUtil.getColumnIndexOrThrow(_cursor, "isDownloaded");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfSubscribed = CursorUtil.getColumnIndexOrThrow(_cursor, "subscribed");
      final int _cursorIndexOfSubscriptionPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionPrice");
      final int _cursorIndexOfEditionDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "editionDescription");
      final int _cursorIndexOfSubscriptionId = CursorUtil.getColumnIndexOrThrow(_cursor, "subscriptionId");
      final int _cursorIndexOfCategoryType = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryType");
      final int _cursorIndexOfEditionName = CursorUtil.getColumnIndexOrThrow(_cursor, "editionName");
      final EditionListModel.DigitalEditionList _result;
      if(_cursor.moveToFirst()) {
        _result = new EditionListModel.DigitalEditionList();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _result.setId(_tmpId);
        final String _tmpProductId;
        if (_cursor.isNull(_cursorIndexOfProductId)) {
          _tmpProductId = null;
        } else {
          _tmpProductId = _cursor.getString(_cursorIndexOfProductId);
        }
        _result.setProductId(_tmpProductId);
        final Boolean _tmpIsDownloaded;
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfIsDownloaded)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfIsDownloaded);
        }
        _tmpIsDownloaded = _tmp == null ? null : _tmp != 0;
        _result.setDownloaded(_tmpIsDownloaded);
        final String _tmpDate;
        if (_cursor.isNull(_cursorIndexOfDate)) {
          _tmpDate = null;
        } else {
          _tmpDate = _cursor.getString(_cursorIndexOfDate);
        }
        _result.setDate(_tmpDate);
        final String _tmpImage;
        if (_cursor.isNull(_cursorIndexOfImage)) {
          _tmpImage = null;
        } else {
          _tmpImage = _cursor.getString(_cursorIndexOfImage);
        }
        _result.setImage(_tmpImage);
        final String _tmpSubscribed;
        if (_cursor.isNull(_cursorIndexOfSubscribed)) {
          _tmpSubscribed = null;
        } else {
          _tmpSubscribed = _cursor.getString(_cursorIndexOfSubscribed);
        }
        _result.setSubscribed(_tmpSubscribed);
        final String _tmpSubscriptionPrice;
        if (_cursor.isNull(_cursorIndexOfSubscriptionPrice)) {
          _tmpSubscriptionPrice = null;
        } else {
          _tmpSubscriptionPrice = _cursor.getString(_cursorIndexOfSubscriptionPrice);
        }
        _result.setSubscriptionPrice(_tmpSubscriptionPrice);
        final String _tmpEditionDescription;
        if (_cursor.isNull(_cursorIndexOfEditionDescription)) {
          _tmpEditionDescription = null;
        } else {
          _tmpEditionDescription = _cursor.getString(_cursorIndexOfEditionDescription);
        }
        _result.setEditionDescription(_tmpEditionDescription);
        final String _tmpSubscriptionId;
        if (_cursor.isNull(_cursorIndexOfSubscriptionId)) {
          _tmpSubscriptionId = null;
        } else {
          _tmpSubscriptionId = _cursor.getString(_cursorIndexOfSubscriptionId);
        }
        _result.setSubscriptionId(_tmpSubscriptionId);
        final String _tmpCategoryType;
        if (_cursor.isNull(_cursorIndexOfCategoryType)) {
          _tmpCategoryType = null;
        } else {
          _tmpCategoryType = _cursor.getString(_cursorIndexOfCategoryType);
        }
        _result.setCategoryType(_tmpCategoryType);
        final String _tmpEditionName;
        if (_cursor.isNull(_cursorIndexOfEditionName)) {
          _tmpEditionName = null;
        } else {
          _tmpEditionName = _cursor.getString(_cursorIndexOfEditionName);
        }
        _result.setEditionName(_tmpEditionName);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
