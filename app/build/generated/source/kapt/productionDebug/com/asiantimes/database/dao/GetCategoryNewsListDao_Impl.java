package com.asiantimes.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.database.DataConverter;
import com.asiantimes.model.GetTableTypeModel;
import com.asiantimes.model.GetTopStoriesModel;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GetCategoryNewsListDao_Impl implements GetCategoryNewsListDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<GetTableTypeModel> __insertionAdapterOfGetTableTypeModel;

  private final DataConverter __dataConverter = new DataConverter();

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  private final SharedSQLiteStatement __preparedStmtOfUdpateWithType;

  private final SharedSQLiteStatement __preparedStmtOfUdpateWithPostCatID;

  public GetCategoryNewsListDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfGetTableTypeModel = new EntityInsertionAdapter<GetTableTypeModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `home_page_news_with_cat_table_type` (`autoGenerateId`,`postList`,`tableType`,`cateName`,`postCatID`) VALUES (nullif(?, 0),?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, GetTableTypeModel value) {
        stmt.bindLong(1, value.getAutoGenerateId());
        final String _tmp;
        _tmp = __dataConverter.fromOptionValuesList1(value.getPostList());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
        if (value.getTableType() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTableType());
        }
        if (value.getCateName() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCateName());
        }
        if (value.getPostCatID() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getPostCatID());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE WHERE tableType =?";
        return _query;
      }
    };
    this.__preparedStmtOfUdpateWithType = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=? WHERE tableType = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUdpateWithPostCatID = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=? WHERE tableType = ? AND postCatID = ?";
        return _query;
      }
    };
  }

  @Override
  public void insert_new(final GetTableTypeModel getAllCategories) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfGetTableTypeModel.insert(getAllCategories);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final String type) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    int _argIndex = 1;
    if (type == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, type);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public void udpateWithType(final String tableType,
      final List<GetTopStoriesModel.PostList> breakingNews) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUdpateWithType.acquire();
    int _argIndex = 1;
    final String _tmp;
    _tmp = __dataConverter.fromOptionValuesList1(breakingNews);
    if (_tmp == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, _tmp);
    }
    _argIndex = 2;
    if (tableType == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, tableType);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUdpateWithType.release(_stmt);
    }
  }

  @Override
  public void udpateWithPostCatID(final String postId, final String tableType,
      final List<GetTopStoriesModel.PostList> breakingNews) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUdpateWithPostCatID.acquire();
    int _argIndex = 1;
    final String _tmp;
    _tmp = __dataConverter.fromOptionValuesList1(breakingNews);
    if (_tmp == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, _tmp);
    }
    _argIndex = 2;
    if (tableType == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, tableType);
    }
    _argIndex = 3;
    if (postId == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, postId);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUdpateWithPostCatID.release(_stmt);
    }
  }

  @Override
  public GetTableTypeModel getTopStoriesList_new(final String type) {
    final String _sql = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (type == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, type);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfPostList = CursorUtil.getColumnIndexOrThrow(_cursor, "postList");
      final int _cursorIndexOfTableType = CursorUtil.getColumnIndexOrThrow(_cursor, "tableType");
      final int _cursorIndexOfCateName = CursorUtil.getColumnIndexOrThrow(_cursor, "cateName");
      final int _cursorIndexOfPostCatID = CursorUtil.getColumnIndexOrThrow(_cursor, "postCatID");
      final GetTableTypeModel _result;
      if(_cursor.moveToFirst()) {
        _result = new GetTableTypeModel();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _result.setAutoGenerateId(_tmpAutoGenerateId);
        final List<GetTopStoriesModel.PostList> _tmpPostList;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfPostList)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfPostList);
        }
        _tmpPostList = __dataConverter.toOptionValuesList1(_tmp);
        _result.setPostList(_tmpPostList);
        final String _tmpTableType;
        if (_cursor.isNull(_cursorIndexOfTableType)) {
          _tmpTableType = null;
        } else {
          _tmpTableType = _cursor.getString(_cursorIndexOfTableType);
        }
        _result.setTableType(_tmpTableType);
        final String _tmpCateName;
        if (_cursor.isNull(_cursorIndexOfCateName)) {
          _tmpCateName = null;
        } else {
          _tmpCateName = _cursor.getString(_cursorIndexOfCateName);
        }
        _result.setCateName(_tmpCateName);
        final String _tmpPostCatID;
        if (_cursor.isNull(_cursorIndexOfPostCatID)) {
          _tmpPostCatID = null;
        } else {
          _tmpPostCatID = _cursor.getString(_cursorIndexOfPostCatID);
        }
        _result.setPostCatID(_tmpPostCatID);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<GetTableTypeModel> getTopStoriesList_newAll(final String type) {
    final String _sql = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (type == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, type);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfPostList = CursorUtil.getColumnIndexOrThrow(_cursor, "postList");
      final int _cursorIndexOfTableType = CursorUtil.getColumnIndexOrThrow(_cursor, "tableType");
      final int _cursorIndexOfCateName = CursorUtil.getColumnIndexOrThrow(_cursor, "cateName");
      final int _cursorIndexOfPostCatID = CursorUtil.getColumnIndexOrThrow(_cursor, "postCatID");
      final List<GetTableTypeModel> _result = new ArrayList<GetTableTypeModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final GetTableTypeModel _item;
        _item = new GetTableTypeModel();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _item.setAutoGenerateId(_tmpAutoGenerateId);
        final List<GetTopStoriesModel.PostList> _tmpPostList;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfPostList)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfPostList);
        }
        _tmpPostList = __dataConverter.toOptionValuesList1(_tmp);
        _item.setPostList(_tmpPostList);
        final String _tmpTableType;
        if (_cursor.isNull(_cursorIndexOfTableType)) {
          _tmpTableType = null;
        } else {
          _tmpTableType = _cursor.getString(_cursorIndexOfTableType);
        }
        _item.setTableType(_tmpTableType);
        final String _tmpCateName;
        if (_cursor.isNull(_cursorIndexOfCateName)) {
          _tmpCateName = null;
        } else {
          _tmpCateName = _cursor.getString(_cursorIndexOfCateName);
        }
        _item.setCateName(_tmpCateName);
        final String _tmpPostCatID;
        if (_cursor.isNull(_cursorIndexOfPostCatID)) {
          _tmpPostCatID = null;
        } else {
          _tmpPostCatID = _cursor.getString(_cursorIndexOfPostCatID);
        }
        _item.setPostCatID(_tmpPostCatID);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<GetTableTypeModel> getTopStoriesList_newAllPST_ID(final String type,
      final String postId) {
    final String _sql = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =? AND postCatID = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    if (type == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, type);
    }
    _argIndex = 2;
    if (postId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, postId);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfPostList = CursorUtil.getColumnIndexOrThrow(_cursor, "postList");
      final int _cursorIndexOfTableType = CursorUtil.getColumnIndexOrThrow(_cursor, "tableType");
      final int _cursorIndexOfCateName = CursorUtil.getColumnIndexOrThrow(_cursor, "cateName");
      final int _cursorIndexOfPostCatID = CursorUtil.getColumnIndexOrThrow(_cursor, "postCatID");
      final List<GetTableTypeModel> _result = new ArrayList<GetTableTypeModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final GetTableTypeModel _item;
        _item = new GetTableTypeModel();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _item.setAutoGenerateId(_tmpAutoGenerateId);
        final List<GetTopStoriesModel.PostList> _tmpPostList;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfPostList)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfPostList);
        }
        _tmpPostList = __dataConverter.toOptionValuesList1(_tmp);
        _item.setPostList(_tmpPostList);
        final String _tmpTableType;
        if (_cursor.isNull(_cursorIndexOfTableType)) {
          _tmpTableType = null;
        } else {
          _tmpTableType = _cursor.getString(_cursorIndexOfTableType);
        }
        _item.setTableType(_tmpTableType);
        final String _tmpCateName;
        if (_cursor.isNull(_cursorIndexOfCateName)) {
          _tmpCateName = null;
        } else {
          _tmpCateName = _cursor.getString(_cursorIndexOfCateName);
        }
        _item.setCateName(_tmpCateName);
        final String _tmpPostCatID;
        if (_cursor.isNull(_cursorIndexOfPostCatID)) {
          _tmpPostCatID = null;
        } else {
          _tmpPostCatID = _cursor.getString(_cursorIndexOfPostCatID);
        }
        _item.setPostCatID(_tmpPostCatID);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<GetTableTypeModel> getTopStoriesList_AllPST_ID(final String postId) {
    final String _sql = "SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where  postCatID = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (postId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, postId);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfPostList = CursorUtil.getColumnIndexOrThrow(_cursor, "postList");
      final int _cursorIndexOfTableType = CursorUtil.getColumnIndexOrThrow(_cursor, "tableType");
      final int _cursorIndexOfCateName = CursorUtil.getColumnIndexOrThrow(_cursor, "cateName");
      final int _cursorIndexOfPostCatID = CursorUtil.getColumnIndexOrThrow(_cursor, "postCatID");
      final List<GetTableTypeModel> _result = new ArrayList<GetTableTypeModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final GetTableTypeModel _item;
        _item = new GetTableTypeModel();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _item.setAutoGenerateId(_tmpAutoGenerateId);
        final List<GetTopStoriesModel.PostList> _tmpPostList;
        final String _tmp;
        if (_cursor.isNull(_cursorIndexOfPostList)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(_cursorIndexOfPostList);
        }
        _tmpPostList = __dataConverter.toOptionValuesList1(_tmp);
        _item.setPostList(_tmpPostList);
        final String _tmpTableType;
        if (_cursor.isNull(_cursorIndexOfTableType)) {
          _tmpTableType = null;
        } else {
          _tmpTableType = _cursor.getString(_cursorIndexOfTableType);
        }
        _item.setTableType(_tmpTableType);
        final String _tmpCateName;
        if (_cursor.isNull(_cursorIndexOfCateName)) {
          _tmpCateName = null;
        } else {
          _tmpCateName = _cursor.getString(_cursorIndexOfCateName);
        }
        _item.setCateName(_tmpCateName);
        final String _tmpPostCatID;
        if (_cursor.isNull(_cursorIndexOfPostCatID)) {
          _tmpPostCatID = null;
        } else {
          _tmpPostCatID = _cursor.getString(_cursorIndexOfPostCatID);
        }
        _item.setPostCatID(_tmpPostCatID);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<String> allType() {
    final String _sql = "SELECT tableType from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final List<String> _result = new ArrayList<String>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final String _item;
        if (_cursor.isNull(0)) {
          _item = null;
        } else {
          _item = _cursor.getString(0);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public void inesrtUpdate(final String tableType,
      final List<GetTopStoriesModel.PostList> breakingNews) {
    GetCategoryNewsListDao.DefaultImpls.inesrtUpdate(GetCategoryNewsListDao_Impl.this, tableType, breakingNews);
  }

  @Override
  public void inesrtUpdateNew(final String tableType,
      final ArrayList<GetTopStoriesModel.CategoryList> breakingNews) {
    GetCategoryNewsListDao.DefaultImpls.inesrtUpdateNew(GetCategoryNewsListDao_Impl.this, tableType, breakingNews);
  }

  @Override
  public void inesrtUpdateNewWithPostID(final GetTopStoriesModel.PostList data) {
    GetCategoryNewsListDao.DefaultImpls.inesrtUpdateNewWithPostID(GetCategoryNewsListDao_Impl.this, data);
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
