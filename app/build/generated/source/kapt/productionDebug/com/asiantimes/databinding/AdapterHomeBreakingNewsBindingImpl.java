package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterHomeBreakingNewsBindingImpl extends AdapterHomeBreakingNewsBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback28;
    @Nullable
    private final android.view.View.OnClickListener mCallback29;
    @Nullable
    private final android.view.View.OnClickListener mCallback30;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterHomeBreakingNewsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private AdapterHomeBreakingNewsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (com.asiantimes.widget.TopAlignedImageView) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.imageViewBookmark.setTag(null);
        this.imageViewBreakingShare.setTag(null);
        this.imageViewTop.setTag(null);
        this.layoutTop.setTag(null);
        this.textBreakingViewTime.setTag(null);
        this.textViewBreakingDescription.setTag(null);
        this.textViewBreakingTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback28 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        mCallback29 = new com.asiantimes.generated.callback.OnClickListener(this, 2);
        mCallback30 = new com.asiantimes.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.breakingNews == variableId) {
            setBreakingNews((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionViewListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setBreakingNews(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList BreakingNews) {
        this.mBreakingNews = BreakingNews;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.breakingNews);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean breakingNewsIsBookmark = null;
        java.lang.String breakingNewsPostTitle = null;
        java.lang.Integer position = mPosition;
        boolean androidxDatabindingViewDataBindingSafeUnboxBreakingNewsIsBookmark = false;
        java.lang.String breakingNewsPostImage = null;
        com.asiantimes.model.GetTopStoriesModel.PostList breakingNews = mBreakingNews;
        java.lang.String breakingNewsPostDetailGetInt0Key = null;
        com.asiantimes.model.GetTopStoriesModel.PostDetail breakingNewsPostDetailGetInt0 = null;
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        java.util.List<com.asiantimes.model.GetTopStoriesModel.PostDetail> breakingNewsPostDetail = null;
        java.lang.String breakingNewsPostDate = null;

        if ((dirtyFlags & 0xaL) != 0) {



                if (breakingNews != null) {
                    // read breakingNews.isBookmark()
                    breakingNewsIsBookmark = breakingNews.isBookmark();
                    // read breakingNews.postTitle
                    breakingNewsPostTitle = breakingNews.getPostTitle();
                    // read breakingNews.postImage
                    breakingNewsPostImage = breakingNews.getPostImage();
                    // read breakingNews.postDetail
                    breakingNewsPostDetail = breakingNews.getPostDetail();
                    // read breakingNews.postDate
                    breakingNewsPostDate = breakingNews.getPostDate();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(breakingNews.isBookmark())
                androidxDatabindingViewDataBindingSafeUnboxBreakingNewsIsBookmark = androidx.databinding.ViewDataBinding.safeUnbox(breakingNewsIsBookmark);
                if (breakingNewsPostDetail != null) {
                    // read breakingNews.postDetail.get(0)
                    breakingNewsPostDetailGetInt0 = breakingNewsPostDetail.get(0);
                }


                if (breakingNewsPostDetailGetInt0 != null) {
                    // read breakingNews.postDetail.get(0).key
                    breakingNewsPostDetailGetInt0Key = breakingNewsPostDetailGetInt0.getKey();
                }
        }
        // batch finished
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            com.asiantimes.base.BindingAdapters.setBookmark(this.imageViewBookmark, androidxDatabindingViewDataBindingSafeUnboxBreakingNewsIsBookmark);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewTop, breakingNewsPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textBreakingViewTime, breakingNewsPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewBreakingDescription, breakingNewsPostDetailGetInt0Key);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewBreakingTitle, breakingNewsPostTitle);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.imageViewBookmark.setOnClickListener(mCallback30);
            this.imageViewBreakingShare.setOnClickListener(mCallback29);
            this.layoutTop.setOnClickListener(mCallback28);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // breakingNews
                com.asiantimes.model.GetTopStoriesModel.PostList breakingNews = mBreakingNews;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(breakingNews, position, 0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // breakingNews
                com.asiantimes.model.GetTopStoriesModel.PostList breakingNews = mBreakingNews;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(breakingNews, position, 2);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // breakingNews
                com.asiantimes.model.GetTopStoriesModel.PostList breakingNews = mBreakingNews;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(breakingNews, position, 1);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): position
        flag 1 (0x2L): breakingNews
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}