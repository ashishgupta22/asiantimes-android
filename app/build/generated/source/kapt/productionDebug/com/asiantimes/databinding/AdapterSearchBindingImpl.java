package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterSearchBindingImpl extends AdapterSearchBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.image_view_search, 3);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback9;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterSearchBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private AdapterSearchBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textViewDate.setTag(null);
        this.textViewTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback9 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.searchData == variableId) {
            setSearchData((com.asiantimes.model.GetTopStoriesModel.PostList) variable);
        }
        else if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setSearchData(@Nullable com.asiantimes.model.GetTopStoriesModel.PostList SearchData) {
        this.mSearchData = SearchData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.searchData);
        super.requestRebind();
    }
    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.model.GetTopStoriesModel.PostList searchData = mSearchData;
        java.lang.Integer position = mPosition;
        java.lang.String searchDataPostDate = null;
        com.asiantimes.interfaces.OnClickPositionListener onClickListener = mOnClickListener;
        java.lang.String searchDataPostTitle = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (searchData != null) {
                    // read searchData.postDate
                    searchDataPostDate = searchData.getPostDate();
                    // read searchData.postTitle
                    searchDataPostTitle = searchData.getPostTitle();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback9);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewDate, searchDataPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewTitle, searchDataPostTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // searchData
        com.asiantimes.model.GetTopStoriesModel.PostList searchData = mSearchData;
        // position
        java.lang.Integer position = mPosition;
        // onClickListener
        com.asiantimes.interfaces.OnClickPositionListener onClickListener = mOnClickListener;
        // onClickListener != null
        boolean onClickListenerJavaLangObjectNull = false;



        onClickListenerJavaLangObjectNull = (onClickListener) != (null);
        if (onClickListenerJavaLangObjectNull) {




            onClickListener.onClickPositionListener(searchData, position);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): searchData
        flag 1 (0x2L): position
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}