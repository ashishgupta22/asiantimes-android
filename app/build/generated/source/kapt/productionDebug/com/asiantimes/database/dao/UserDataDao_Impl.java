package com.asiantimes.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.model.CommonModel;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class UserDataDao_Impl implements UserDataDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<CommonModel.UserDetail> __insertionAdapterOfUserDetail;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public UserDataDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUserDetail = new EntityInsertionAdapter<CommonModel.UserDetail>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `user_data_table` (`autoGenerateId`,`userName`,`userId`,`emailAddress`,`isSubscribe`,`mobileNumber`,`image`,`isExpired`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CommonModel.UserDetail value) {
        stmt.bindLong(1, value.getAutoGenerateId());
        if (value.getUserName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getUserName());
        }
        if (value.getUserId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getUserId());
        }
        if (value.getEmailAddress() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getEmailAddress());
        }
        final Integer _tmp;
        _tmp = value.isSubscribe() == null ? null : (value.isSubscribe() ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindLong(5, _tmp);
        }
        if (value.getMobileNumber() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getMobileNumber());
        }
        if (value.getImage() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getImage());
        }
        final Integer _tmp_1;
        _tmp_1 = value.isExpired() == null ? null : (value.isExpired() ? 1 : 0);
        if (_tmp_1 == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindLong(8, _tmp_1);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM USER_DATA_TABLE";
        return _query;
      }
    };
  }

  @Override
  public void insert(final CommonModel.UserDetail userData) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfUserDetail.insert(userData);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public CommonModel.UserDetail getUserData() {
    final String _sql = "SELECT * from USER_DATA_TABLE";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfAutoGenerateId = CursorUtil.getColumnIndexOrThrow(_cursor, "autoGenerateId");
      final int _cursorIndexOfUserName = CursorUtil.getColumnIndexOrThrow(_cursor, "userName");
      final int _cursorIndexOfUserId = CursorUtil.getColumnIndexOrThrow(_cursor, "userId");
      final int _cursorIndexOfEmailAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "emailAddress");
      final int _cursorIndexOfIsSubscribe = CursorUtil.getColumnIndexOrThrow(_cursor, "isSubscribe");
      final int _cursorIndexOfMobileNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "mobileNumber");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfIsExpired = CursorUtil.getColumnIndexOrThrow(_cursor, "isExpired");
      final CommonModel.UserDetail _result;
      if(_cursor.moveToFirst()) {
        _result = new CommonModel.UserDetail();
        final int _tmpAutoGenerateId;
        _tmpAutoGenerateId = _cursor.getInt(_cursorIndexOfAutoGenerateId);
        _result.setAutoGenerateId(_tmpAutoGenerateId);
        final String _tmpUserName;
        if (_cursor.isNull(_cursorIndexOfUserName)) {
          _tmpUserName = null;
        } else {
          _tmpUserName = _cursor.getString(_cursorIndexOfUserName);
        }
        _result.setUserName(_tmpUserName);
        final String _tmpUserId;
        if (_cursor.isNull(_cursorIndexOfUserId)) {
          _tmpUserId = null;
        } else {
          _tmpUserId = _cursor.getString(_cursorIndexOfUserId);
        }
        _result.setUserId(_tmpUserId);
        final String _tmpEmailAddress;
        if (_cursor.isNull(_cursorIndexOfEmailAddress)) {
          _tmpEmailAddress = null;
        } else {
          _tmpEmailAddress = _cursor.getString(_cursorIndexOfEmailAddress);
        }
        _result.setEmailAddress(_tmpEmailAddress);
        final Boolean _tmpIsSubscribe;
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfIsSubscribe)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfIsSubscribe);
        }
        _tmpIsSubscribe = _tmp == null ? null : _tmp != 0;
        _result.setSubscribe(_tmpIsSubscribe);
        final String _tmpMobileNumber;
        if (_cursor.isNull(_cursorIndexOfMobileNumber)) {
          _tmpMobileNumber = null;
        } else {
          _tmpMobileNumber = _cursor.getString(_cursorIndexOfMobileNumber);
        }
        _result.setMobileNumber(_tmpMobileNumber);
        final String _tmpImage;
        if (_cursor.isNull(_cursorIndexOfImage)) {
          _tmpImage = null;
        } else {
          _tmpImage = _cursor.getString(_cursorIndexOfImage);
        }
        _result.setImage(_tmpImage);
        final Boolean _tmpIsExpired;
        final Integer _tmp_1;
        if (_cursor.isNull(_cursorIndexOfIsExpired)) {
          _tmp_1 = null;
        } else {
          _tmp_1 = _cursor.getInt(_cursorIndexOfIsExpired);
        }
        _tmpIsExpired = _tmp_1 == null ? null : _tmp_1 != 0;
        _result.setExpired(_tmpIsExpired);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
