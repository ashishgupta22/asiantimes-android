package com.asiantimes.database.dao;

import androidx.room.RoomDatabase;
import java.lang.Class;
import java.lang.SuppressWarnings;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GetHomePageNewsDaoNew_Impl implements GetHomePageNewsDaoNew {
  private final RoomDatabase __db;

  public GetHomePageNewsDaoNew_Impl(RoomDatabase __db) {
    this.__db = __db;
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
