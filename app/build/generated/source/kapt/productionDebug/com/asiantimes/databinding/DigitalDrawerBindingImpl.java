package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class DigitalDrawerBindingImpl extends DigitalDrawerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_header, 3);
        sViewsWithIds.put(R.id.image_view_close_drawer, 4);
        sViewsWithIds.put(R.id.layout_profile, 5);
        sViewsWithIds.put(R.id.layout_image, 6);
        sViewsWithIds.put(R.id.image_view_profile, 7);
        sViewsWithIds.put(R.id.text_view_profile_name, 8);
        sViewsWithIds.put(R.id.image_view_linked_in, 9);
        sViewsWithIds.put(R.id.image_view_google, 10);
        sViewsWithIds.put(R.id.image_view_twitter, 11);
        sViewsWithIds.put(R.id.image_view_facebook, 12);
        sViewsWithIds.put(R.id.layout_login, 13);
        sViewsWithIds.put(R.id.button_login, 14);
        sViewsWithIds.put(R.id.buttonSignUp, 15);
        sViewsWithIds.put(R.id.view_header, 16);
        sViewsWithIds.put(R.id.layout_bookmark, 17);
        sViewsWithIds.put(R.id.image_view_bookmark, 18);
        sViewsWithIds.put(R.id.layout_news_app, 19);
        sViewsWithIds.put(R.id.image_view_news, 20);
        sViewsWithIds.put(R.id.layout_setting, 21);
        sViewsWithIds.put(R.id.image_view_setting, 22);
        sViewsWithIds.put(R.id.layout_logout, 23);
        sViewsWithIds.put(R.id.view, 24);
        sViewsWithIds.put(R.id.layoutLogout, 25);
        sViewsWithIds.put(R.id.iv_listTitle_icon, 26);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public DigitalDrawerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 27, sIncludes, sViewsWithIds));
    }
    private DigitalDrawerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatButton) bindings[14]
            , (androidx.appcompat.widget.AppCompatButton) bindings[15]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[18]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[10]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[20]
            , (com.asiantimes.base.RoundRectCornerImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[22]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[26]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[17]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[23]
            , (android.widget.LinearLayout) bindings[25]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[19]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[21]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (android.view.View) bindings[24]
            , (android.view.View) bindings[16]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textViewEmail.setTag(null);
        this.textViewName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.name == variableId) {
            setName((java.lang.String) variable);
        }
        else if (BR.email == variableId) {
            setEmail((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setName(@Nullable java.lang.String Name) {
        this.mName = Name;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.name);
        super.requestRebind();
    }
    public void setEmail(@Nullable java.lang.String Email) {
        this.mEmail = Email;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.email);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String name = mName;
        java.lang.String email = mEmail;

        if ((dirtyFlags & 0x5L) != 0) {
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewEmail, email);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewName, name);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): name
        flag 1 (0x2L): email
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}