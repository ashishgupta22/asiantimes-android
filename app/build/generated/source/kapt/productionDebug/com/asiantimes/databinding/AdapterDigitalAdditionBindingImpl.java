package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterDigitalAdditionBindingImpl extends AdapterDigitalAdditionBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layoutDownload, 7);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback25;
    @Nullable
    private final android.view.View.OnClickListener mCallback26;
    @Nullable
    private final android.view.View.OnClickListener mCallback23;
    @Nullable
    private final android.view.View.OnClickListener mCallback24;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterDigitalAdditionBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private AdapterDigitalAdditionBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            );
        this.imageViewDownload.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatImageView) bindings[3];
        this.mboundView3.setTag(null);
        this.textViewPrice.setTag(null);
        this.textViewRead.setTag(null);
        this.textViewSubscribe.setTag(null);
        setRootTag(root);
        // listeners
        mCallback25 = new com.asiantimes.generated.callback.OnClickListener(this, 3);
        mCallback26 = new com.asiantimes.generated.callback.OnClickListener(this, 4);
        mCallback23 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        mCallback24 = new com.asiantimes.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.digitalEditionList == variableId) {
            setDigitalEditionList((com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList) variable);
        }
        else if (BR.clickListener == variableId) {
            setClickListener((com.asiantimes.interfaces.OnClickPositionListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDigitalEditionList(@Nullable com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList DigitalEditionList) {
        this.mDigitalEditionList = DigitalEditionList;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.digitalEditionList);
        super.requestRebind();
    }
    public void setClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
        java.lang.String digitalEditionListDate = null;
        java.lang.String javaLangStringDigitalEditionListSubscriptionPrice = null;
        com.asiantimes.interfaces.OnClickPositionListener clickListener = mClickListener;
        java.lang.String digitalEditionListImage = null;
        java.lang.String digitalEditionListSubscriptionPrice = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (digitalEditionList != null) {
                    // read digitalEditionList.date
                    digitalEditionListDate = digitalEditionList.getDate();
                    // read digitalEditionList.image
                    digitalEditionListImage = digitalEditionList.getImage();
                    // read digitalEditionList.subscriptionPrice
                    digitalEditionListSubscriptionPrice = digitalEditionList.getSubscriptionPrice();
                }


                // read ("£ ") + (digitalEditionList.subscriptionPrice)
                javaLangStringDigitalEditionListSubscriptionPrice = ("£ ") + (digitalEditionListSubscriptionPrice);
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.imageViewDownload.setOnClickListener(mCallback26);
            this.mboundView3.setOnClickListener(mCallback23);
            this.textViewRead.setOnClickListener(mCallback25);
            this.textViewSubscribe.setOnClickListener(mCallback24);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, digitalEditionListDate);
            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.mboundView3, digitalEditionListImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewPrice, javaLangStringDigitalEditionListSubscriptionPrice);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // digitalEditionList
                com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
                // clickListener
                com.asiantimes.interfaces.OnClickPositionListener clickListener = mClickListener;
                // clickListener != null
                boolean clickListenerJavaLangObjectNull = false;



                clickListenerJavaLangObjectNull = (clickListener) != (null);
                if (clickListenerJavaLangObjectNull) {




                    clickListener.onClickPositionListener(digitalEditionList, 2);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // digitalEditionList
                com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
                // clickListener
                com.asiantimes.interfaces.OnClickPositionListener clickListener = mClickListener;
                // clickListener != null
                boolean clickListenerJavaLangObjectNull = false;



                clickListenerJavaLangObjectNull = (clickListener) != (null);
                if (clickListenerJavaLangObjectNull) {




                    clickListener.onClickPositionListener(digitalEditionList, 3);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // digitalEditionList
                com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
                // clickListener
                com.asiantimes.interfaces.OnClickPositionListener clickListener = mClickListener;
                // clickListener != null
                boolean clickListenerJavaLangObjectNull = false;



                clickListenerJavaLangObjectNull = (clickListener) != (null);
                if (clickListenerJavaLangObjectNull) {




                    clickListener.onClickPositionListener(digitalEditionList, 0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // digitalEditionList
                com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
                // clickListener
                com.asiantimes.interfaces.OnClickPositionListener clickListener = mClickListener;
                // clickListener != null
                boolean clickListenerJavaLangObjectNull = false;



                clickListenerJavaLangObjectNull = (clickListener) != (null);
                if (clickListenerJavaLangObjectNull) {




                    clickListener.onClickPositionListener(digitalEditionList, 1);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): digitalEditionList
        flag 1 (0x2L): clickListener
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}