package com.asiantimes.digital_addition.database.dao;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.asiantimes.digital_addition.model.EditionCategoryModel;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class EditionCategoryDao_Impl implements EditionCategoryDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<EditionCategoryModel.DigitalEditionCategoryList> __insertionAdapterOfDigitalEditionCategoryList;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public EditionCategoryDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfDigitalEditionCategoryList = new EntityInsertionAdapter<EditionCategoryModel.DigitalEditionCategoryList>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `edition_category_list_table` (`id`,`categoryId`,`categoryName`,`categoryType`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt,
          EditionCategoryModel.DigitalEditionCategoryList value) {
        stmt.bindLong(1, value.getId());
        if (value.getCategoryId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCategoryId());
        }
        if (value.getCategoryName() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCategoryName());
        }
        if (value.getCategoryType() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCategoryType());
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from edition_category_list_table";
        return _query;
      }
    };
  }

  @Override
  public void insert(
      final List<EditionCategoryModel.DigitalEditionCategoryList> categoryListEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfDigitalEditionCategoryList.insert(categoryListEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<EditionCategoryModel.DigitalEditionCategoryList> getEditionCategoryList() {
    final String _sql = "SELECT * from edition_category_list_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryId");
      final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryName");
      final int _cursorIndexOfCategoryType = CursorUtil.getColumnIndexOrThrow(_cursor, "categoryType");
      final List<EditionCategoryModel.DigitalEditionCategoryList> _result = new ArrayList<EditionCategoryModel.DigitalEditionCategoryList>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final EditionCategoryModel.DigitalEditionCategoryList _item;
        _item = new EditionCategoryModel.DigitalEditionCategoryList();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getString(_cursorIndexOfCategoryId);
        }
        _item.setCategoryId(_tmpCategoryId);
        final String _tmpCategoryName;
        if (_cursor.isNull(_cursorIndexOfCategoryName)) {
          _tmpCategoryName = null;
        } else {
          _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
        }
        _item.setCategoryName(_tmpCategoryName);
        final String _tmpCategoryType;
        if (_cursor.isNull(_cursorIndexOfCategoryType)) {
          _tmpCategoryType = null;
        } else {
          _tmpCategoryType = _cursor.getString(_cursorIndexOfCategoryType);
        }
        _item.setCategoryType(_tmpCategoryType);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
