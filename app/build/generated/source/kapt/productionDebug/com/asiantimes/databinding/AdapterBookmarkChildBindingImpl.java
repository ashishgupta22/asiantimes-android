package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterBookmarkChildBindingImpl extends AdapterBookmarkChildBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback6;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterBookmarkChildBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private AdapterBookmarkChildBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.imageViewSaved.setTag(null);
        this.linearLayoutMainSavedArticleAdapter.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.textViewDescription.setTag(null);
        this.textViewTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback6 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.postCatNewsList == variableId) {
            setPostCatNewsList((com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList) variable);
        }
        else if (BR.clickListener == variableId) {
            setClickListener((com.asiantimes.interfaces.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPostCatNewsList(@Nullable com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList PostCatNewsList) {
        this.mPostCatNewsList = PostCatNewsList;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.postCatNewsList);
        super.requestRebind();
    }
    public void setClickListener(@Nullable com.asiantimes.interfaces.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String postCatNewsListPostImage = null;
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList = mPostCatNewsList;
        java.lang.String postCatNewsListPostTitle = null;
        com.asiantimes.interfaces.OnClickListener clickListener = mClickListener;
        java.lang.String postCatNewsListPostDescription = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (postCatNewsList != null) {
                    // read postCatNewsList.postImage
                    postCatNewsListPostImage = postCatNewsList.getPostImage();
                    // read postCatNewsList.postTitle
                    postCatNewsListPostTitle = postCatNewsList.getPostTitle();
                    // read postCatNewsList.postDescription
                    postCatNewsListPostDescription = postCatNewsList.getPostDescription();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewSaved, postCatNewsListPostImage);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.textViewDescription, postCatNewsListPostDescription);
            com.asiantimes.digital_addition.utility.BindingAdapters.htmlText(this.textViewTitle, postCatNewsListPostTitle);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback6);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // postCatNewsList
        com.asiantimes.digital_addition.model.EditionPostListModel.PostList.PostCatNewsList postCatNewsList = mPostCatNewsList;
        // clickListener
        com.asiantimes.interfaces.OnClickListener clickListener = mClickListener;
        // clickListener != null
        boolean clickListenerJavaLangObjectNull = false;



        clickListenerJavaLangObjectNull = (clickListener) != (null);
        if (clickListenerJavaLangObjectNull) {



            clickListener.onClickListener(postCatNewsList);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): postCatNewsList
        flag 1 (0x2L): clickListener
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}