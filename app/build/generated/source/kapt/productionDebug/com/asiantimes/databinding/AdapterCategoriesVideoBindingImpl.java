package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterCategoriesVideoBindingImpl extends AdapterCategoriesVideoBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_view_trending_title, 2);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView1;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterCategoriesVideoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private AdapterCategoriesVideoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatImageView) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        mCallback7 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.catList == variableId) {
            setCatList((com.asiantimes.model.GetVideoListModel.VideoSubCatListBean) variable);
        }
        else if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionViewListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCatList(@Nullable com.asiantimes.model.GetVideoListModel.VideoSubCatListBean CatList) {
        this.mCatList = CatList;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.catList);
        super.requestRebind();
    }
    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.model.GetVideoListModel.VideoSubCatListBean catList = mCatList;
        java.lang.String catListCatImage = null;
        java.lang.Integer position = mPosition;
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        int androidxDatabindingViewDataBindingSafeUnboxPosition = 0;

        if ((dirtyFlags & 0x9L) != 0) {



                if (catList != null) {
                    // read catList.catImage
                    catListCatImage = catList.getCatImage();
                }
        }
        if ((dirtyFlags & 0xaL) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(position)
                androidxDatabindingViewDataBindingSafeUnboxPosition = androidx.databinding.ViewDataBinding.safeUnbox(position);
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback7);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            com.asiantimes.base.BindingAdapters.setMargin(this.mboundView0, androidxDatabindingViewDataBindingSafeUnboxPosition, 3);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.mboundView1, catListCatImage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // catList
        com.asiantimes.model.GetVideoListModel.VideoSubCatListBean catList = mCatList;
        // position
        java.lang.Integer position = mPosition;
        // onClickListener
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
        // onClickListener != null
        boolean onClickListenerJavaLangObjectNull = false;



        onClickListenerJavaLangObjectNull = (onClickListener) != (null);
        if (onClickListenerJavaLangObjectNull) {





            onClickListener.onClickPositionViewListener(catList, position, 0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): catList
        flag 1 (0x2L): position
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}