package com.asiantimes.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.asiantimes.database.dao.GetAllCategoryDao;
import com.asiantimes.database.dao.GetAllCategoryDao_Impl;
import com.asiantimes.database.dao.GetCartDao;
import com.asiantimes.database.dao.GetCartDao_Impl;
import com.asiantimes.database.dao.GetCategoryNewsListDao;
import com.asiantimes.database.dao.GetCategoryNewsListDao_Impl;
import com.asiantimes.database.dao.GetHomePageNewsDaoNew;
import com.asiantimes.database.dao.GetHomePageNewsDaoNew_Impl;
import com.asiantimes.database.dao.UserDataDao;
import com.asiantimes.database.dao.UserDataDao_Impl;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile UserDataDao _userDataDao;

  private volatile GetAllCategoryDao _getAllCategoryDao;

  private volatile GetHomePageNewsDaoNew _getHomePageNewsDaoNew;

  private volatile GetCategoryNewsListDao _getCategoryNewsListDao;

  private volatile GetCartDao _getCartDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(3) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `get_all_categories_table` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `cat` TEXT, `catId` TEXT, `catColor` TEXT, `catUrl` TEXT, `categoryType` TEXT, `image` TEXT, `subCatArr` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `home_page_news_with_cat_table_type` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `postList` TEXT, `tableType` TEXT, `cateName` TEXT, `postCatID` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `user_data_table` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `userName` TEXT, `userId` TEXT, `emailAddress` TEXT, `isSubscribe` INTEGER, `mobileNumber` TEXT, `image` TEXT, `isExpired` INTEGER)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `home_page_breaking_news_table` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `msg` TEXT, `errorCode` INTEGER, `numberComments` INTEGER, `postId` TEXT, `postCatID` TEXT, `postCatColor` TEXT, `postUrl` TEXT, `isPremium` INTEGER, `postTitle` TEXT, `postDate` TEXT, `postImage` TEXT, `postCatName` TEXT, `isBookmark` INTEGER, `postDetail` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `cart_manage_table` (`autoGenerateId` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `subscriptionID` TEXT, `subscriptionTitle` TEXT, `subscriptionDate` TEXT, `subscriptionSubtitle` TEXT, `subscriptionPrice` TEXT, `subscriptionImage` TEXT, `subscriptionCount` INTEGER NOT NULL, `id` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '450e79667c5c14e49e4b01e61c51a481')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `get_all_categories_table`");
        _db.execSQL("DROP TABLE IF EXISTS `home_page_news_with_cat_table_type`");
        _db.execSQL("DROP TABLE IF EXISTS `user_data_table`");
        _db.execSQL("DROP TABLE IF EXISTS `home_page_breaking_news_table`");
        _db.execSQL("DROP TABLE IF EXISTS `cart_manage_table`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsGetAllCategoriesTable = new HashMap<String, TableInfo.Column>(8);
        _columnsGetAllCategoriesTable.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("cat", new TableInfo.Column("cat", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catId", new TableInfo.Column("catId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catColor", new TableInfo.Column("catColor", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("catUrl", new TableInfo.Column("catUrl", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("categoryType", new TableInfo.Column("categoryType", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsGetAllCategoriesTable.put("subCatArr", new TableInfo.Column("subCatArr", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysGetAllCategoriesTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesGetAllCategoriesTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoGetAllCategoriesTable = new TableInfo("get_all_categories_table", _columnsGetAllCategoriesTable, _foreignKeysGetAllCategoriesTable, _indicesGetAllCategoriesTable);
        final TableInfo _existingGetAllCategoriesTable = TableInfo.read(_db, "get_all_categories_table");
        if (! _infoGetAllCategoriesTable.equals(_existingGetAllCategoriesTable)) {
          return new RoomOpenHelper.ValidationResult(false, "get_all_categories_table(com.asiantimes.model.GetAllCategoriesModel.Post).\n"
                  + " Expected:\n" + _infoGetAllCategoriesTable + "\n"
                  + " Found:\n" + _existingGetAllCategoriesTable);
        }
        final HashMap<String, TableInfo.Column> _columnsHomePageNewsWithCatTableType = new HashMap<String, TableInfo.Column>(5);
        _columnsHomePageNewsWithCatTableType.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageNewsWithCatTableType.put("postList", new TableInfo.Column("postList", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageNewsWithCatTableType.put("tableType", new TableInfo.Column("tableType", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageNewsWithCatTableType.put("cateName", new TableInfo.Column("cateName", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageNewsWithCatTableType.put("postCatID", new TableInfo.Column("postCatID", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysHomePageNewsWithCatTableType = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesHomePageNewsWithCatTableType = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoHomePageNewsWithCatTableType = new TableInfo("home_page_news_with_cat_table_type", _columnsHomePageNewsWithCatTableType, _foreignKeysHomePageNewsWithCatTableType, _indicesHomePageNewsWithCatTableType);
        final TableInfo _existingHomePageNewsWithCatTableType = TableInfo.read(_db, "home_page_news_with_cat_table_type");
        if (! _infoHomePageNewsWithCatTableType.equals(_existingHomePageNewsWithCatTableType)) {
          return new RoomOpenHelper.ValidationResult(false, "home_page_news_with_cat_table_type(com.asiantimes.model.GetTableTypeModel).\n"
                  + " Expected:\n" + _infoHomePageNewsWithCatTableType + "\n"
                  + " Found:\n" + _existingHomePageNewsWithCatTableType);
        }
        final HashMap<String, TableInfo.Column> _columnsUserDataTable = new HashMap<String, TableInfo.Column>(8);
        _columnsUserDataTable.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("userName", new TableInfo.Column("userName", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("userId", new TableInfo.Column("userId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("emailAddress", new TableInfo.Column("emailAddress", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("isSubscribe", new TableInfo.Column("isSubscribe", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("mobileNumber", new TableInfo.Column("mobileNumber", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUserDataTable.put("isExpired", new TableInfo.Column("isExpired", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUserDataTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUserDataTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUserDataTable = new TableInfo("user_data_table", _columnsUserDataTable, _foreignKeysUserDataTable, _indicesUserDataTable);
        final TableInfo _existingUserDataTable = TableInfo.read(_db, "user_data_table");
        if (! _infoUserDataTable.equals(_existingUserDataTable)) {
          return new RoomOpenHelper.ValidationResult(false, "user_data_table(com.asiantimes.model.CommonModel.UserDetail).\n"
                  + " Expected:\n" + _infoUserDataTable + "\n"
                  + " Found:\n" + _existingUserDataTable);
        }
        final HashMap<String, TableInfo.Column> _columnsHomePageBreakingNewsTable = new HashMap<String, TableInfo.Column>(15);
        _columnsHomePageBreakingNewsTable.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("msg", new TableInfo.Column("msg", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("errorCode", new TableInfo.Column("errorCode", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("numberComments", new TableInfo.Column("numberComments", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postId", new TableInfo.Column("postId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postCatID", new TableInfo.Column("postCatID", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postCatColor", new TableInfo.Column("postCatColor", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postUrl", new TableInfo.Column("postUrl", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("isPremium", new TableInfo.Column("isPremium", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postTitle", new TableInfo.Column("postTitle", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postDate", new TableInfo.Column("postDate", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postImage", new TableInfo.Column("postImage", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postCatName", new TableInfo.Column("postCatName", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("isBookmark", new TableInfo.Column("isBookmark", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsHomePageBreakingNewsTable.put("postDetail", new TableInfo.Column("postDetail", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysHomePageBreakingNewsTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesHomePageBreakingNewsTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoHomePageBreakingNewsTable = new TableInfo("home_page_breaking_news_table", _columnsHomePageBreakingNewsTable, _foreignKeysHomePageBreakingNewsTable, _indicesHomePageBreakingNewsTable);
        final TableInfo _existingHomePageBreakingNewsTable = TableInfo.read(_db, "home_page_breaking_news_table");
        if (! _infoHomePageBreakingNewsTable.equals(_existingHomePageBreakingNewsTable)) {
          return new RoomOpenHelper.ValidationResult(false, "home_page_breaking_news_table(com.asiantimes.model.GetTopStoriesModel.PostList).\n"
                  + " Expected:\n" + _infoHomePageBreakingNewsTable + "\n"
                  + " Found:\n" + _existingHomePageBreakingNewsTable);
        }
        final HashMap<String, TableInfo.Column> _columnsCartManageTable = new HashMap<String, TableInfo.Column>(9);
        _columnsCartManageTable.put("autoGenerateId", new TableInfo.Column("autoGenerateId", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionID", new TableInfo.Column("subscriptionID", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionTitle", new TableInfo.Column("subscriptionTitle", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionDate", new TableInfo.Column("subscriptionDate", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionSubtitle", new TableInfo.Column("subscriptionSubtitle", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionPrice", new TableInfo.Column("subscriptionPrice", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionImage", new TableInfo.Column("subscriptionImage", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("subscriptionCount", new TableInfo.Column("subscriptionCount", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCartManageTable.put("id", new TableInfo.Column("id", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCartManageTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCartManageTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCartManageTable = new TableInfo("cart_manage_table", _columnsCartManageTable, _foreignKeysCartManageTable, _indicesCartManageTable);
        final TableInfo _existingCartManageTable = TableInfo.read(_db, "cart_manage_table");
        if (! _infoCartManageTable.equals(_existingCartManageTable)) {
          return new RoomOpenHelper.ValidationResult(false, "cart_manage_table(com.asiantimes.model.CartItem).\n"
                  + " Expected:\n" + _infoCartManageTable + "\n"
                  + " Found:\n" + _existingCartManageTable);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "450e79667c5c14e49e4b01e61c51a481", "ccc31e9d1d8313aceba3c7b067997000");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "get_all_categories_table","home_page_news_with_cat_table_type","user_data_table","home_page_breaking_news_table","cart_manage_table");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `get_all_categories_table`");
      _db.execSQL("DELETE FROM `home_page_news_with_cat_table_type`");
      _db.execSQL("DELETE FROM `user_data_table`");
      _db.execSQL("DELETE FROM `home_page_breaking_news_table`");
      _db.execSQL("DELETE FROM `cart_manage_table`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(UserDataDao.class, UserDataDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(GetAllCategoryDao.class, GetAllCategoryDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(GetHomePageNewsDaoNew.class, GetHomePageNewsDaoNew_Impl.getRequiredConverters());
    _typeConvertersMap.put(GetCategoryNewsListDao.class, GetCategoryNewsListDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(GetCartDao.class, GetCartDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public UserDataDao userDataDao() {
    if (_userDataDao != null) {
      return _userDataDao;
    } else {
      synchronized(this) {
        if(_userDataDao == null) {
          _userDataDao = new UserDataDao_Impl(this);
        }
        return _userDataDao;
      }
    }
  }

  @Override
  public GetAllCategoryDao getAllCategoryDao() {
    if (_getAllCategoryDao != null) {
      return _getAllCategoryDao;
    } else {
      synchronized(this) {
        if(_getAllCategoryDao == null) {
          _getAllCategoryDao = new GetAllCategoryDao_Impl(this);
        }
        return _getAllCategoryDao;
      }
    }
  }

  @Override
  public GetHomePageNewsDaoNew getHomePageNewsDao() {
    if (_getHomePageNewsDaoNew != null) {
      return _getHomePageNewsDaoNew;
    } else {
      synchronized(this) {
        if(_getHomePageNewsDaoNew == null) {
          _getHomePageNewsDaoNew = new GetHomePageNewsDaoNew_Impl(this);
        }
        return _getHomePageNewsDaoNew;
      }
    }
  }

  @Override
  public GetCategoryNewsListDao getCateNewsList() {
    if (_getCategoryNewsListDao != null) {
      return _getCategoryNewsListDao;
    } else {
      synchronized(this) {
        if(_getCategoryNewsListDao == null) {
          _getCategoryNewsListDao = new GetCategoryNewsListDao_Impl(this);
        }
        return _getCategoryNewsListDao;
      }
    }
  }

  @Override
  public GetCartDao getCartManangeDao() {
    if (_getCartDao != null) {
      return _getCartDao;
    } else {
      synchronized(this) {
        if(_getCartDao == null) {
          _getCartDao = new GetCartDao_Impl(this);
        }
        return _getCartDao;
      }
    }
  }
}
