package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterCategoriesVideoChildBindingImpl extends AdapterCategoriesVideoChildBinding implements com.asiantimes.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_breaking_news, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    @Nullable
    private final android.view.View.OnClickListener mCallback17;
    @Nullable
    private final android.view.View.OnClickListener mCallback18;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterCategoriesVideoChildBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private AdapterCategoriesVideoChildBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.imageViewNews.setTag(null);
        this.imageViewShare.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatImageView) bindings[5];
        this.mboundView5.setTag(null);
        this.textViewBreakingTitle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback16 = new com.asiantimes.generated.callback.OnClickListener(this, 1);
        mCallback17 = new com.asiantimes.generated.callback.OnClickListener(this, 2);
        mCallback18 = new com.asiantimes.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.catNewsData == variableId) {
            setCatNewsData((com.asiantimes.model.GetVideoListModel.NewsListBean) variable);
        }
        else if (BR.onClickListener == variableId) {
            setOnClickListener((com.asiantimes.interfaces.OnClickPositionViewListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setCatNewsData(@Nullable com.asiantimes.model.GetVideoListModel.NewsListBean CatNewsData) {
        this.mCatNewsData = CatNewsData;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.catNewsData);
        super.requestRebind();
    }
    public void setOnClickListener(@Nullable com.asiantimes.interfaces.OnClickPositionViewListener OnClickListener) {
        this.mOnClickListener = OnClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.onClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer position = mPosition;
        java.lang.String catNewsDataPostImage = null;
        java.lang.String catNewsDataPostTitle = null;
        java.lang.String catNewsDataPostDate = null;
        com.asiantimes.model.GetVideoListModel.NewsListBean catNewsData = mCatNewsData;
        com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;

        if ((dirtyFlags & 0xaL) != 0) {



                if (catNewsData != null) {
                    // read catNewsData.postImage
                    catNewsDataPostImage = catNewsData.getPostImage();
                    // read catNewsData.postTitle
                    catNewsDataPostTitle = catNewsData.getPostTitle();
                    // read catNewsData.postDate
                    catNewsDataPostDate = catNewsData.getPostDate();
                }
        }
        // batch finished
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewNews, catNewsDataPostImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, catNewsDataPostDate);
            com.asiantimes.base.BindingAdapters.htmlStrip(this.textViewBreakingTitle, catNewsDataPostTitle);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.imageViewShare.setOnClickListener(mCallback17);
            this.mboundView0.setOnClickListener(mCallback16);
            this.mboundView5.setOnClickListener(mCallback18);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // catNewsData
                com.asiantimes.model.GetVideoListModel.NewsListBean catNewsData = mCatNewsData;
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(catNewsData, position, 0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // catNewsData
                com.asiantimes.model.GetVideoListModel.NewsListBean catNewsData = mCatNewsData;
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(catNewsData, position, 2);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // catNewsData
                com.asiantimes.model.GetVideoListModel.NewsListBean catNewsData = mCatNewsData;
                // position
                java.lang.Integer position = mPosition;
                // onClickListener
                com.asiantimes.interfaces.OnClickPositionViewListener onClickListener = mOnClickListener;
                // onClickListener != null
                boolean onClickListenerJavaLangObjectNull = false;



                onClickListenerJavaLangObjectNull = (onClickListener) != (null);
                if (onClickListenerJavaLangObjectNull) {





                    onClickListener.onClickPositionViewListener(catNewsData, position, 1);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): position
        flag 1 (0x2L): catNewsData
        flag 2 (0x3L): onClickListener
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}