package com.asiantimes.databinding;
import com.asiantimes.R;
import com.asiantimes.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityDownloadBindingImpl extends ActivityDownloadBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(11);
        sIncludes.setIncludes(1, 
            new String[] {"digital_action_bar"},
            new int[] {6},
            new int[] {com.asiantimes.R.layout.digital_action_bar});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.text_view_download, 7);
        sViewsWithIds.put(R.id.gif_downloading, 8);
        sViewsWithIds.put(R.id.layout_subscribe, 9);
        sViewsWithIds.put(R.id.ripple_subscribe, 10);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityDownloadBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private ActivityDownloadBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (com.asiantimes.widget.GifImageView) bindings[8]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (com.asiantimes.databinding.DigitalActionBarBinding) bindings[6]
            , (com.asiantimes.base.RippleButton) bindings[5]
            , (com.asiantimes.base.RippleButton) bindings[10]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            );
        this.imageViewEdition.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.rippleBuySingle.setTag(null);
        this.textViewDate.setTag(null);
        this.textViewPrice.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        layoutToolbar.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (layoutToolbar.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.digitalEditionList == variableId) {
            setDigitalEditionList((com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setDigitalEditionList(@Nullable com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList DigitalEditionList) {
        this.mDigitalEditionList = DigitalEditionList;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.digitalEditionList);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        layoutToolbar.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeLayoutToolbar((com.asiantimes.databinding.DigitalActionBarBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeLayoutToolbar(com.asiantimes.databinding.DigitalActionBarBinding LayoutToolbar, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.asiantimes.digital_addition.model.EditionListModel.DigitalEditionList digitalEditionList = mDigitalEditionList;
        java.lang.String digitalEditionListDate = null;
        java.lang.String javaLangStringBuySingleDigitalEditionListSubscriptionPrice = null;
        java.lang.String javaLangStringDigitalEditionListSubscriptionPrice = null;
        java.lang.String digitalEditionListImage = null;
        java.lang.String digitalEditionListSubscriptionPrice = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (digitalEditionList != null) {
                    // read digitalEditionList.date
                    digitalEditionListDate = digitalEditionList.getDate();
                    // read digitalEditionList.image
                    digitalEditionListImage = digitalEditionList.getImage();
                    // read digitalEditionList.subscriptionPrice
                    digitalEditionListSubscriptionPrice = digitalEditionList.getSubscriptionPrice();
                }


                // read ("Buy Single £ ") + (digitalEditionList.subscriptionPrice)
                javaLangStringBuySingleDigitalEditionListSubscriptionPrice = ("Buy Single £ ") + (digitalEditionListSubscriptionPrice);
                // read ("£ ") + (digitalEditionList.subscriptionPrice)
                javaLangStringDigitalEditionListSubscriptionPrice = ("£ ") + (digitalEditionListSubscriptionPrice);
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.asiantimes.digital_addition.utility.BindingAdapters.imageUrl(this.imageViewEdition, digitalEditionListImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.rippleBuySingle, javaLangStringBuySingleDigitalEditionListSubscriptionPrice);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewDate, digitalEditionListDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textViewPrice, javaLangStringDigitalEditionListSubscriptionPrice);
        }
        executeBindingsOn(layoutToolbar);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): layoutToolbar
        flag 1 (0x2L): digitalEditionList
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}