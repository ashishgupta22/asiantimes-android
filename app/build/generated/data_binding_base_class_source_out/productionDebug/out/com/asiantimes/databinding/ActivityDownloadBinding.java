// Generated by data binding compiler. Do not edit!
package com.asiantimes.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.asiantimes.R;
import com.asiantimes.base.RippleButton;
import com.asiantimes.digital_addition.model.EditionListModel;
import com.asiantimes.widget.GifImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityDownloadBinding extends ViewDataBinding {
  @NonNull
  public final GifImageView gifDownloading;

  @NonNull
  public final AppCompatImageView imageViewEdition;

  @NonNull
  public final ConstraintLayout layoutSubscribe;

  @NonNull
  public final DigitalActionBarBinding layoutToolbar;

  @NonNull
  public final RippleButton rippleBuySingle;

  @NonNull
  public final RippleButton rippleSubscribe;

  @NonNull
  public final AppCompatTextView textViewDate;

  @NonNull
  public final AppCompatTextView textViewDownload;

  @NonNull
  public final AppCompatTextView textViewPrice;

  @Bindable
  protected EditionListModel.DigitalEditionList mDigitalEditionList;

  protected ActivityDownloadBinding(Object _bindingComponent, View _root, int _localFieldCount,
      GifImageView gifDownloading, AppCompatImageView imageViewEdition,
      ConstraintLayout layoutSubscribe, DigitalActionBarBinding layoutToolbar,
      RippleButton rippleBuySingle, RippleButton rippleSubscribe, AppCompatTextView textViewDate,
      AppCompatTextView textViewDownload, AppCompatTextView textViewPrice) {
    super(_bindingComponent, _root, _localFieldCount);
    this.gifDownloading = gifDownloading;
    this.imageViewEdition = imageViewEdition;
    this.layoutSubscribe = layoutSubscribe;
    this.layoutToolbar = layoutToolbar;
    setContainedBinding(this.layoutToolbar);
    this.rippleBuySingle = rippleBuySingle;
    this.rippleSubscribe = rippleSubscribe;
    this.textViewDate = textViewDate;
    this.textViewDownload = textViewDownload;
    this.textViewPrice = textViewPrice;
  }

  public abstract void setDigitalEditionList(
      @Nullable EditionListModel.DigitalEditionList digitalEditionList);

  @Nullable
  public EditionListModel.DigitalEditionList getDigitalEditionList() {
    return mDigitalEditionList;
  }

  @NonNull
  public static ActivityDownloadBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_download, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityDownloadBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityDownloadBinding>inflateInternal(inflater, R.layout.activity_download, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityDownloadBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_download, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityDownloadBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityDownloadBinding>inflateInternal(inflater, R.layout.activity_download, null, false, component);
  }

  public static ActivityDownloadBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityDownloadBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityDownloadBinding)bind(component, view, R.layout.activity_download);
  }
}
