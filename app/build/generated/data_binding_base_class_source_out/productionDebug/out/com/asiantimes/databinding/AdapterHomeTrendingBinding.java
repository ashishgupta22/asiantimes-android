// Generated by data binding compiler. Do not edit!
package com.asiantimes.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.asiantimes.R;
import com.asiantimes.interfaces.OnClickPositionViewListener;
import com.asiantimes.model.GetTopStoriesModel;
import com.asiantimes.widget.TopAlignedImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class AdapterHomeTrendingBinding extends ViewDataBinding {
  @NonNull
  public final TopAlignedImageView imageViewTrending;

  @NonNull
  public final AppCompatTextView textViewBreakingTitle;

  @Bindable
  protected GetTopStoriesModel.PostList mTrendingNews;

  @Bindable
  protected Integer mPosition;

  @Bindable
  protected OnClickPositionViewListener mOnClickListener;

  protected AdapterHomeTrendingBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TopAlignedImageView imageViewTrending, AppCompatTextView textViewBreakingTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageViewTrending = imageViewTrending;
    this.textViewBreakingTitle = textViewBreakingTitle;
  }

  public abstract void setTrendingNews(@Nullable GetTopStoriesModel.PostList trendingNews);

  @Nullable
  public GetTopStoriesModel.PostList getTrendingNews() {
    return mTrendingNews;
  }

  public abstract void setPosition(@Nullable Integer position);

  @Nullable
  public Integer getPosition() {
    return mPosition;
  }

  public abstract void setOnClickListener(@Nullable OnClickPositionViewListener onClickListener);

  @Nullable
  public OnClickPositionViewListener getOnClickListener() {
    return mOnClickListener;
  }

  @NonNull
  public static AdapterHomeTrendingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_home_trending, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static AdapterHomeTrendingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<AdapterHomeTrendingBinding>inflateInternal(inflater, R.layout.adapter_home_trending, root, attachToRoot, component);
  }

  @NonNull
  public static AdapterHomeTrendingBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_home_trending, null, false, component)
   */
  @NonNull
  @Deprecated
  public static AdapterHomeTrendingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<AdapterHomeTrendingBinding>inflateInternal(inflater, R.layout.adapter_home_trending, null, false, component);
  }

  public static AdapterHomeTrendingBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static AdapterHomeTrendingBinding bind(@NonNull View view, @Nullable Object component) {
    return (AdapterHomeTrendingBinding)bind(component, view, R.layout.adapter_home_trending);
  }
}
