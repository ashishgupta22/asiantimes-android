// Generated by data binding compiler. Do not edit!
package com.asiantimes.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.asiantimes.R;
import com.asiantimes.widget.TextViewRegularBold;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class AdapterSubCategoryParentBinding extends ViewDataBinding {
  @NonNull
  public final TextViewRegularBold textViewHeader;

  @Bindable
  protected String mHeader;

  protected AdapterSubCategoryParentBinding(Object _bindingComponent, View _root,
      int _localFieldCount, TextViewRegularBold textViewHeader) {
    super(_bindingComponent, _root, _localFieldCount);
    this.textViewHeader = textViewHeader;
  }

  public abstract void setHeader(@Nullable String header);

  @Nullable
  public String getHeader() {
    return mHeader;
  }

  @NonNull
  public static AdapterSubCategoryParentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_sub_category_parent, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static AdapterSubCategoryParentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<AdapterSubCategoryParentBinding>inflateInternal(inflater, R.layout.adapter_sub_category_parent, root, attachToRoot, component);
  }

  @NonNull
  public static AdapterSubCategoryParentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_sub_category_parent, null, false, component)
   */
  @NonNull
  @Deprecated
  public static AdapterSubCategoryParentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<AdapterSubCategoryParentBinding>inflateInternal(inflater, R.layout.adapter_sub_category_parent, null, false, component);
  }

  public static AdapterSubCategoryParentBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static AdapterSubCategoryParentBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (AdapterSubCategoryParentBinding)bind(component, view, R.layout.adapter_sub_category_parent);
  }
}
