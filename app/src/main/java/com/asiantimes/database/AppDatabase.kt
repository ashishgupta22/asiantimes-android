package com.asiantimes.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.asiantimes.base.Constants.APP_DATABASE_NAME
import com.asiantimes.database.dao.*
import com.asiantimes.model.*

@Database(
    entities = [GetAllCategoriesModel.Post::class, GetTableTypeModel::class,
        CommonModel.UserDetail::class, GetTopStoriesModel.PostList::class, CartItem::class],
    version = 3, exportSchema = false
)
@TypeConverters(DataConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDataDao(): UserDataDao
    abstract fun getAllCategoryDao(): GetAllCategoryDao
    abstract fun getHomePageNewsDao(): GetHomePageNewsDaoNew
    abstract fun getCateNewsList(): GetCategoryNewsListDao
    abstract fun getCartManangeDao(): GetCartDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext, AppDatabase::class.java, APP_DATABASE_NAME
                ).fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}