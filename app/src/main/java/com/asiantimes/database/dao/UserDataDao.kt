package com.asiantimes.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.model.CommonModel

@Dao
interface   UserDataDao {
    @Insert
    fun insert(userData: CommonModel.UserDetail)

    @Query("SELECT * from USER_DATA_TABLE")
    fun getUserData(): CommonModel.UserDetail

    @Query("DELETE FROM USER_DATA_TABLE")
    fun delete()
}