package com.asiantimes.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.model.GetAllCategoriesModel

@Dao
interface GetAllCategoryDao {
    @Insert
    fun insert(getAllCategories: List<GetAllCategoriesModel.Post>)

    @Query("SELECT * from GET_ALL_CATEGORIES_TABLE")
    fun getAllCategories(): List<GetAllCategoriesModel.Post>

    @Query("DELETE FROM GET_ALL_CATEGORIES_TABLE")
    fun delete()
}