package com.asiantimes.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.model.GetTableTypeModel
import com.asiantimes.model.GetTopStoriesModel

@Dao
interface GetCategoryNewsListDao {

    @Insert
    fun insert_new(getAllCategories: GetTableTypeModel?)
//
//    @Query("SELECT * from HOME_PAGE_BREAKING_NEWS_TABLE")
//    fun getTopStoriesList(): List<GetTopStoriesModel.PostList>

    @Query("SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type")
    fun getTopStoriesList_new(type: String): GetTableTypeModel

    @Query("SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type")
    fun getTopStoriesList_newAll(type: String): List<GetTableTypeModel>

    @Query("SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where tableType =:type AND postCatID = :postId")
    fun getTopStoriesList_newAllPST_ID(type: String, postId: String): List<GetTableTypeModel>

    @Query("SELECT * from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE where  postCatID = :postId")
    fun getTopStoriesList_AllPST_ID(postId: String): List<GetTableTypeModel>

    @Query("DELETE FROM HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE WHERE tableType =:type")
    fun delete(type: String)

    @Query("UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=:breakingNews WHERE tableType = :tableType")
    fun udpateWithType(tableType: String, breakingNews: List<GetTopStoriesModel.PostList>?)

    @Query("UPDATE HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE SET postList=:breakingNews WHERE tableType = :tableType AND postCatID = :postId")
    fun udpateWithPostCatID(
        postId: String,
        tableType: String,
        breakingNews: List<GetTopStoriesModel.PostList>?
    )

//    @Update(entity = GetTopStoriesModel.PostList::class)
//    fun update(postData: GetTopStoriesModel.PostList)

    @Query("SELECT tableType from HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE ")
    fun allType(): List<String>

    fun inesrtUpdate(tableType: String, breakingNews: List<GetTopStoriesModel.PostList>?) {
        val getList = allType();
        if (getList.isNullOrEmpty()) {
            val tableTypeData = GetTableTypeModel()
            tableTypeData.tableType = tableType
            tableTypeData.postList = breakingNews!!
            tableTypeData.cateName = ""
            tableTypeData.postCatID = ""
            insert_new(tableTypeData)
        } else {
            if (getList.contains(tableType)) {
                udpateWithType(tableType, breakingNews)
            } else {
                val tableTypeData = GetTableTypeModel()
                tableTypeData.tableType = tableType
                tableTypeData.postList = breakingNews!!
                tableTypeData.cateName = ""
                tableTypeData.postCatID = ""
                insert_new(tableTypeData)
            }
        }
    }

    fun inesrtUpdateNew(
        tableType: String,
        breakingNews: ArrayList<GetTopStoriesModel.CategoryList>
    ) {
        delete(tableType)
        for (i in breakingNews) {
            val tableTypeData = GetTableTypeModel()
            tableTypeData.tableType = tableType
            tableTypeData.cateName = i.CatName
            tableTypeData.postList = i.postList!!
            tableTypeData.postCatID = i.CatID!!
            insert_new(tableTypeData)
        }
    }

    fun inesrtUpdateNewWithPostID(data: GetTopStoriesModel.PostList) {
        val dataList = data.postCatID?.let { getTopStoriesList_AllPST_ID(it) }
        if (!dataList.isNullOrEmpty()) {
            dataList[0].tableType?.let { inesrtUpdate(it,dataList[0].postList) }
        }
    }
}