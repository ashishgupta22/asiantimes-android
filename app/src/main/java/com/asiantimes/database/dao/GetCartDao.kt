package com.asiantimes.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.model.CartItem

@Dao
interface GetCartDao {

    @Insert
    fun insert_new(getAllCategories: CartItem?)

    @Query("DELETE FROM CART_MANAGE_TABLE WHERE subscriptionID =:subscriptionID")
    fun removeCartItem(subscriptionID: String): Int

    @Query("DELETE FROM CART_MANAGE_TABLE")
    fun removeCartItemALL()

    @Query("UPDATE CART_MANAGE_TABLE SET subscriptionCount=:subscriptionCount1 WHERE  subscriptionID= :subscriptionID1")
    fun updateEasternEyeCart(
        subscriptionID1: String,
        subscriptionCount1: Int
    )
    @Query("SELECT * from CART_MANAGE_TABLE")
    fun getAllData(): List<CartItem>
//

//
//    fun inesrtUpdate(tableType: String, breakingNews: List<GetTopStoriesModel.PostList>?) {
//        val getList = allType();
//        if (getList.isNullOrEmpty()) {
//            val tableTypeData = GetTableTypeModel()
//            tableTypeData.tableType = tableType
//            tableTypeData.postList = breakingNews!!
//            tableTypeData.cateName = ""
//            tableTypeData.postCatID = ""
//            insert_new(tableTypeData)
//        } else {
//            if (getList.contains(tableType)) {
//                udpateWithType(tableType, breakingNews)
//            } else {
//                val tableTypeData = GetTableTypeModel()
//                tableTypeData.tableType = tableType
//                tableTypeData.postList = breakingNews!!
//                tableTypeData.cateName = ""
//                tableTypeData.postCatID = ""
//                insert_new(tableTypeData)
//            }
//        }
//    }

//    fun inesrtUpdateNew(
//        tableType: String,
//        breakingNews: ArrayList<GetTopStoriesModel.CategoryList>
//    ) {
//        delete(tableType)
//        for (i in breakingNews) {
//            val tableTypeData = GetTableTypeModel()
//            tableTypeData.tableType = tableType
//            tableTypeData.cateName = i.CatName
//            tableTypeData.postList = i.postList!!
//            tableTypeData.postCatID = i.CatID!!
//            insert_new(tableTypeData)
//        }
//    }
}