package com.asiantimes.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.asiantimes.model.GetAllCategoriesModel
import com.asiantimes.model.GetTopStoriesModel
import java.io.Serializable
import java.lang.reflect.Type

class DataConverter : Serializable {
    @TypeConverter // note this annotation
    fun fromOptionValuesList(optionValues: List<GetAllCategoriesModel.Post.SubCatArr>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<GetAllCategoriesModel.Post.SubCatArr>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList(optionValuesString: String?): List<GetAllCategoriesModel.Post.SubCatArr>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<GetAllCategoriesModel.Post.SubCatArr?>?>() {}.type
        return gson.fromJson<List<GetAllCategoriesModel.Post.SubCatArr>>(optionValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesList1(optionValues: List<GetTopStoriesModel.PostList>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<GetTopStoriesModel.PostList>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList1(optionValuesString: String?): List<GetTopStoriesModel.PostList>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type =
            object : TypeToken<List<GetTopStoriesModel.PostList?>?>() {}.type
        return gson.fromJson<List<GetTopStoriesModel.PostList>>(optionValuesString, type)
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesList2(optionValues: List<GetTopStoriesModel.CategoryList>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type =
            object : TypeToken<List<GetTopStoriesModel.CategoryList>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList2(optionValuesString: String?): List<GetTopStoriesModel.CategoryList>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type =
            object : TypeToken<List<GetTopStoriesModel.CategoryList?>?>() {}.type
        return gson.fromJson<List<GetTopStoriesModel.CategoryList>>(
            optionValuesString,
            type
        )
    }

    @TypeConverter // note this annotation
    fun fromOptionValuesList3(optionValues: List<GetTopStoriesModel.PostDetail>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type =
            object : TypeToken<List<GetTopStoriesModel.PostDetail>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList3(optionValuesString: String?): List<GetTopStoriesModel.PostDetail>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type =
            object : TypeToken<List<GetTopStoriesModel.PostDetail?>?>() {}.type
        return gson.fromJson<List<GetTopStoriesModel.PostDetail>>(
            optionValuesString,
            type
        )
    }
}