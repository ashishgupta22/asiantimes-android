package com.asiantimes

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Constants
import com.asiantimes.fcm.Config
import com.asiantimes.fcm.NotificationUtils
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.detail.NewsDetailActivity

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var mNotificationManager: NotificationManager? = null
    var builder: NotificationCompat.Builder? = null
    var intent: Intent? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.data.isNotEmpty()) {
            try {
                val data = remoteMessage.data
                if (data.isEmpty()) {
                    val messageNotification = remoteMessage.notification!!.body
                    if (messageNotification != null) {
                        handleNotification(
                            messageNotification,
                            remoteMessage.notification!!.title!!,
                            "",
                            "",
                            data["type"]!!,
                            data["subscriptionId"]!!,
                            data["url"]!!
                        )
                    }
                } else {
                    handleNotification(
                        data["message"]!!,
                        data["title"]!!,
                        data["postId"]!!,
                        data["categoryId"]!!,
                        data["type"]!!,
                        data["subscriptionId"]!!,
                        data["url"]!!
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("TAG", "Message Notification Body: ${it.body}")
        }

        if (Looper.getMainLooper().thread == Thread.currentThread()) {
            //do some stuff on receive
        }
    }

    override fun onNewToken(token: String) {
        Log.d("TAG", "Refreshed token: $token")
        sendRegistrationToServer(token)
    }

    private fun handleNow() {
        Log.d("TAG", "Short lived task is done.")
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d("TAG", "sendRegistrationTokenToServer($token)")
        token?.let { AppPreferences.setFcmToken(it) }
    }

    private fun handleNotification(
        message: String,
        title: String,
        postId: String,
        categoryIds: String,
        type: String,
        subscriptionId: String,
        url: String
    ) {
//        Log.v("notification", "---4" + message + "---" + title);
        var url = url
        if (!NotificationUtils.isAppIsInBackground(this)) {
            try {
                var contentIntent: PendingIntent? = null
                if (type.equals("articlePage", ignoreCase = true)) {
                    if (TextUtils.isEmpty(postId)) {
                        intent = Intent(this, DashboardActivity::class.java)
                        intent!!.putExtra("select", "0")
                        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                        )
                    } else {
                        intent = Intent(this, NewsDetailActivity::class.java)
                        intent!!.putExtra("postID", postId)
                        intent!!.putExtra("categoryID", categoryIds)
                        intent!!.putExtra(Constants.POSITION,0)
                        intent!!.putExtra(
                            Constants.NEWS_DETAIL_TYPE,
                            Constants.NEWS_FROM_NOTIFIATION
                        )
                        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                        )
                    }
                } else if (type.equals("subscription", ignoreCase = true)) {
//                    intent = Intent(this, NotificationHandlerActivity::class.java)
//                    intent!!.putExtra("type", type)
//                    intent!!.putExtra("subscriptionId", subscriptionId)
//                    intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    contentIntent = PendingIntent.getActivity(
//                        this,
//                        0,
//                        intent,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                    )
                } else if (type.equals("thirdParty", ignoreCase = true)) {
                    try {
                        if (!TextUtils.isEmpty(url)) {
                            if (!url.startsWith("http://") && !url.startsWith("https://")) url =
                                "http://$url"
                            // pending implicit intent to view url
                            val resultIntent = Intent(Intent.ACTION_VIEW)
                            resultIntent.data = Uri.parse(url)
                            contentIntent = PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                            )
                        }
                    } catch (ex: java.lang.Exception) {
                        Toast.makeText(
                            this,
                            "Browser not found! Open URL Manually.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    intent = Intent(this, DashboardActivity::class.java)
                    intent!!.putExtra("select", "0")
                    intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    contentIntent = PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                }
                val manager = getSystemService(AUDIO_SERVICE) as AudioManager
                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0)
                val uri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/" + R.raw.notification_sound)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (Build.VERSION.SDK_INT < 26) {
                            return
                        }
                        val attributes = AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build()
                        val notificationManager =
                            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                        val channel = NotificationChannel(
                            "default",
                            "Channel name",
                            NotificationManager.IMPORTANCE_DEFAULT
                        )
                        channel.description = "Channel description"
                        channel.setSound(uri, attributes)
                        notificationManager.createNotificationChannel(channel)
                    }
                    builder = NotificationCompat.Builder(this, "default")
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setColor(resources.getColor(R.color.colorPrimary))
                        .setColor(Color.TRANSPARENT)
                        .setContentText(message)
                        .setSound(uri)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                } else {
                    builder = NotificationCompat.Builder(this)
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText(message)
                        .setSound(uri)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                    val notificationUtils = NotificationUtils(applicationContext)
                    notificationUtils.playNotificationSound()
                }
                builder!!.setContentIntent(contentIntent)
                mNotificationManager =
                    this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager!!.notify(0, builder!!.build())
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("isNotification", true)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
        } else {
            try {
                var contentIntent: PendingIntent? = null
                if (type.equals("articlePage", ignoreCase = true)) {
                    if (TextUtils.isEmpty(postId)) {
                        intent = Intent(this, DashboardActivity::class.java)
                        intent!!.putExtra("select", "0")
                        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                        )
                    } else {
                        intent = Intent(this, NewsDetailActivity::class.java)
                        intent!!.putExtra("postID", postId)
                        intent!!.putExtra("categoryID", categoryIds)
                        intent!!.putExtra(Constants.POSITION,0)
                        intent!!.putExtra(
                            Constants.NEWS_DETAIL_TYPE,
                            Constants.NEWS_FROM_NOTIFIATION
                        )
                        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        contentIntent = PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                        )
                    }
                } else if (type.equals("subscription", ignoreCase = true)) {
//                    intent = Intent(this, NotificationHandlerActivity::class.java)
//                    intent!!.putExtra("type", type)
//                    intent!!.putExtra("subscriptionId", subscriptionId)
//                    intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                    contentIntent = PendingIntent.getActivity(
//                        this,
//                        0,
//                        intent,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                    )
                } else if (type.equals("thirdParty", ignoreCase = true)) {
                    try {
                        if (!TextUtils.isEmpty(url)) {
                            if (!url.startsWith("http://") && !url.startsWith("https://")) url =
                                "http://$url"


                            // pending implicit intent to view url
                            val resultIntent = Intent(Intent.ACTION_VIEW)
                            resultIntent.data = Uri.parse(url)
                            contentIntent = PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                            )
                        }
                    } catch (e: java.lang.Exception) {
                        Toast.makeText(
                            this,
                            "Browser not found! Open URL Manually.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    intent = Intent(this, DashboardActivity::class.java)
                    intent!!.putExtra("select", "0")
                    intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    contentIntent = PendingIntent.getActivity(
                        this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                }
                val manager = getSystemService(AUDIO_SERVICE) as AudioManager
                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0)
                val uri =
                    Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/" + R.raw.notification_sound)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (Build.VERSION.SDK_INT < 26) {
                            return
                        }
                        val attributes = AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build()
                        val notificationManager =
                            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                        val channel = NotificationChannel(
                            "default",
                            "Channel name",
                            NotificationManager.IMPORTANCE_DEFAULT
                        )
                        channel.description = "Channel description"
                        channel.setSound(uri, attributes)
                        notificationManager.createNotificationChannel(channel)
                    }
                    builder = NotificationCompat.Builder(this, "default")
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setColor(resources.getColor(R.color.colorPrimary))
                        .setColor(Color.TRANSPARENT)
                        .setContentText(message)
                        .setSound(uri)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                } else {
                    builder = NotificationCompat.Builder(this)
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText(message)
                        .setSound(uri)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                    val notificationUtils = NotificationUtils(applicationContext)
                    notificationUtils.playNotificationSound()
                }
                builder!!.setContentIntent(contentIntent)
                mNotificationManager =
                    this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager!!.notify(0, builder!!.build())
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            val pushNotification = Intent(Config.PUSH_NOTIFICATION)
            pushNotification.putExtra("isNotification", true)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
        }
    }

}