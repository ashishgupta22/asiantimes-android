package com.asiantimes.api

import com.asiantimes.digital_addition.model.*
import com.asiantimes.model.*
import com.asiantimes.model.CommonModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*
import rx.Observable

interface ApiInterface {
    companion object {
        const val UPDATE_DB_VERSION = "updateDBVersion"
        const val GET_ALL_CATEGORIES = "getAllcategories"
        const val USER_REGISTER = "userRegister"
        const val LOGIN = "login"
        const val SOCIAL_LOGIN = "socialLogin"
        const val FORGOT_PASSWORD = "forgotPassword"
        const val UPDATE_PASSWORD = "updatePassword"
        const val GET_TOP_STORIES = "getTopStories"
        const val GET_POST_LIST_BY_ID = "getPostListByCateID"
        const val GET_BOOKMARK = "getBookmark"
        const val SAVE_BOOKMARK = "saveBookmark"
        const val GET_SEARCH_RESULT = "getSearchResult"
        const val API_URL_UPDATE_PROFILE: String = "updateProfile"
        const val API_URL_VIDEO: String = "getVideoList"
        const val API_URL_VIDEO_CAT: String = "getCatVideolist"
        const val API_URL_COMMENTLIST: String = "getcommentlist"
        const val API_URL_COMMENTINSERT: String = "insertPostComment"
        const val API_URL_GET_CONTACT: String = "getcontactlist"
        const val API_URL_SUBMIT_CONTACT: String = "submitContact";
        const val API_URL_PHOTO_SUB_CATEGORY: String = "getPhotoVideo";
        const val API_URL_ABOUT_US: String = "getaboutus";
        const val API_URL_PREMIUM_SUBSCRIPTION: String = "premiumSubscription"
        const val API_URL_GET_SHIPPING_PRICE: String = "getShippingPrice"
        /*
        * DIGITAL API URL
        * */
        const val GET_DIGITAL_CATEGORY: String =
            "getATDigitalEditionCategory"
        const val GET_DIGITAL_EDITION_LIST: String =
            "getATDigitalEditionList"
        const val GET_DIGITAL_EDITION_POST_LIST: String =
            "getDigitalEditonPostList"
        const val GET_DIGITAL_EDITION_GET_BOOKMARK: String =
            "at_articleLIstByUser"
        const val GET_DIGITAL_EDITION_ADD_BOOKMARK: String =
            "at_saveBookmarkPost"
        const val GET_DIGITAL_EDITION_SEARCH_RESULT: String =
            "at_getSearchResult"
        const val DIGITAL_EDITION_BUY_SINGLE_PRODUCT: String =
            "buySingleProduct"
        const val DIGITAL_EDITION_CHECK_SUBSCRIPTION: String =
            "checkSubscriptionPlan"
        const val SUBMIT_FEEDBACK: String =
            "submitFeedback"
        const val API_URL_PREMIUM_POST: String =
            "getPremimumArr"
        const val API_URL_GET_SPECIAL_PLAN: String = "getSpecialSubscription"
        const val API_URL_SUBSCRIPTION_PLAN_DETAILS: String = "getSubscriptionList"
        const val API_URL_ORDER_DETAILS: String = "submitDigitalPackage"
        const val API_URL_SUBSCRIPTION_PLAN = "getSubscriptionPlans"
    }

    @FormUrlEncoded
    @POST(UPDATE_DB_VERSION)
    suspend fun updateDBVERSION(
        @Field("deviceType") deviceType: String?,
        @Field("deviceToken") deviceToken: String?,
        @Field("versionType") versionType: String?,
    ): Response<CommonModel?>?

    @GET(GET_ALL_CATEGORIES)
    suspend fun getAllCategories(): Response<GetAllCategoriesModel?>?

    @FormUrlEncoded
    @POST(USER_REGISTER)
    suspend fun userRegister(
        @Field("name") name: String?,
        @Field("phone_number") phone_number: String?,
        @Field("email_address") email_address: String?,
        @Field("password") password: String?
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(LOGIN)
    suspend fun login(
        @Field("emailaddress") eamil: String?,
        @Field("password") password: String?
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(LOGIN)
    suspend fun loginStr(
        @Field("emailaddress") eamil: String?,
        @Field("password") password: String?
    ): Response<String?>?

    @FormUrlEncoded
    @POST(SOCIAL_LOGIN)
    suspend fun socialLogin(
        @Field("email_address") email_address: String?,
        @Field("name") name: String?,
        @Field("phone_number") phone_number: String?,
    ): Response<CommonModel?>?

    @Multipart
    @POST(API_URL_UPDATE_PROFILE)
    suspend fun updateProfile(
        @Part("user_Id") userId: RequestBody?,
        @Part("userName") userName: RequestBody?,
        @Part("mobileNumber") mobileNumber: RequestBody?,
        @Part image: MultipartBody.Part?
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    suspend fun forgotPassword(
        @Field("emailaddress") email_address: String?
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(UPDATE_PASSWORD)
    suspend fun updatePassword(
        @Field("user_id") userId: String?,
        @Field("token") token: String?,
        @Field("newpassword") newPassword: String?
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(GET_SEARCH_RESULT)
    suspend fun getSearchResult(
        @Field("userId") userId: String?,
        @Field("nextPage") nextPage: Int?,
        @Field("searchWord") term_id: String?,
    ): Response<GetTopStoriesModel?>?

    @FormUrlEncoded
    @POST(API_URL_VIDEO)
    suspend fun getVideoResult(
        @Field("userId") userId: String,
    ): Response<GetVideoListModel?>?

    @FormUrlEncoded
    @POST(API_URL_VIDEO_CAT)
    suspend fun getVideoCatResult(
        @Field("catId") userId: String,
        @Field("nextPage") nextPage: String,
    ): Response<GetVideoListModel?>?

    @FormUrlEncoded
    @POST(GET_POST_LIST_BY_ID)
    suspend fun getPostListByCateID(
        @Field("userId") userId: String?,
        @Field("nextPage") nextPage: Int?,
        @Field("categoryId") term_id: String?,
    ): Response<GetTopStoriesModel?>?

    @FormUrlEncoded
    @POST(GET_TOP_STORIES)
    suspend fun getTopStories(
        @Field("userId") userId: String
    ): Response<GetTopStoriesModel?>?

    @FormUrlEncoded
    @POST(GET_BOOKMARK)
    suspend fun getBookmark(
        @Field("user_id") userId: String?,
        @Field("nextPage") nextPage: Int?,
    ): Response<GetTopStoriesModel?>?

    @FormUrlEncoded
    @POST(SAVE_BOOKMARK)
    suspend fun saveBookmark(
        @Field("user_id") userId: String?,
        @Field("postID") postId: String?,
        @Field("isRemoved") isRemoved: Int?,
    ): Response<CommonModel?>?

    @FormUrlEncoded
    @POST(API_URL_COMMENTLIST)
    suspend fun commentList(
            @Field("userid") userId: String?,
        @Field("postId") postId: String?,
        @Field("nextPage") nextPage: String?,
    ): Response<ResponseResult?>?

    @FormUrlEncoded
    @GET(API_URL_ABOUT_US)
    suspend fun aboutUs(
    ): Response<ResponseResult?>?

    @FormUrlEncoded
    @POST(API_URL_COMMENTINSERT)
    suspend fun commentInsert(
        @Field("userid") userId: String?,
        @Field("postId") postId: String?,
        @Field("comment") comment: String?
    ): Response<ResponseResult?>?

    @FormUrlEncoded
    @POST(API_URL_PHOTO_SUB_CATEGORY)
    suspend fun getMorePhotos(
        @Field("categoryId") categoryId: String?,
    ): Response<ResponseResult?>?

    //    @FormUrlEncoded
    @GET(API_URL_GET_CONTACT)
    suspend fun contactList(): Response<ResponseResultNew?>?

    @FormUrlEncoded
    @POST(API_URL_SUBMIT_CONTACT)
    suspend fun submitContact(
        @Field("emailAddress") emailAddress: String?,
        @Field("first_name") first_name: String?,
        @Field("sur_name") sur_name: String?,
        @Field("phone_number") phone_number: String?,
        @Field("address") address: String?,
        @Field("complain_detail") complain_detail: String?
    ): Response<ResponseResultNew?>?

    @Multipart
    @POST("SIGN_IN")
    suspend fun updateProfile(
        @Part("name") name: RequestBody,
        @Part("dob") dob: RequestBody,
        @Part("betCatId") betCatId: RequestBody,
        @Part("paypalEmail") paypalemail: RequestBody?,
        @Part("facebookLink") facebookLink: RequestBody?,
        @Part("linkedinLink") linkedinLink: RequestBody?,
        @Part("twitterLink") twitterLink: RequestBody?,
        @Part("userId") userId: RequestBody?,
        @Part image: MultipartBody.Part
    ): Response<CommonModel>

    /*
     * DIGITAL API
     * */
    @GET(GET_DIGITAL_CATEGORY)
    suspend fun getDigitalEditionCategory(): Response<EditionCategoryModel?>?

    @POST(GET_DIGITAL_EDITION_LIST)
    suspend fun getATDigitalEditionList(@Body requestWebAPIBean: RequestBean?): Response<EditionListModel?>?

    @POST(GET_DIGITAL_EDITION_POST_LIST)
    suspend fun getATDigitalEditionPostList(@Body requestWebAPIBean: RequestBean?): Response<EditionPostListModel?>?


    @Headers("Content-Type: application/json")
    @POST(API_URL_SUBSCRIPTION_PLAN_DETAILS)
    suspend fun getSubScriptionPlanDetails(@Body requestBean: RequestBean?): Response<ResponseResult?>?

    @POST(GET_DIGITAL_EDITION_GET_BOOKMARK)
    suspend fun getATDigitalEditionBookmarks(@Body requestWebAPIBean: RequestBean?): Response<BookmarksModel?>?

    @POST(GET_DIGITAL_EDITION_SEARCH_RESULT)
    suspend fun getSearchResult(@Body requestWebAPIBean: RequestBean?): Response<SearchModel?>?

    @POST(GET_DIGITAL_EDITION_ADD_BOOKMARK)
    suspend fun addBookmark(@Body requestWebAPIBean: RequestBean?): Response<CommonModel?>?

    @POST(DIGITAL_EDITION_BUY_SINGLE_PRODUCT)
    suspend fun buySingleProduct(@Body requestWebAPIBean: RequestBean?): Response<BuySingleProductModel?>?

    @Headers("Content-Type: application/json")
    @POST(DIGITAL_EDITION_CHECK_SUBSCRIPTION)
    suspend fun checkSubscriptionPlan(@Body requestWebAPIBean: RequestBean?): Response<CheckSubscriptionModel?>?


    @Multipart
    @POST(SUBMIT_FEEDBACK)
    suspend fun submitFeedback(
        @Part("comment") comment: RequestBody?,
        @Part("emailAddress") emailAddress: RequestBody?,
        @Part image: MultipartBody.Part?
    ): Response<CommonModel?>?

    @Headers("Content-Type: application/json")
    @POST(API_URL_PREMIUM_POST)
    fun getPremiumPostRequest(@Body requestBean: RequestBean?): Observable<ResponseResult?>?


    @Headers("Content-Type: application/json")
    @POST(API_URL_PREMIUM_POST)
    fun getPremiumPostRequestNew(@Body requestBean: RequestBean?): Response<ResponseResult?>?


    @Headers("Content-Type: application/json")
    @POST(API_URL_GET_SPECIAL_PLAN)
    fun getSpecialSubscription(@Body requestBean: RequestBean?): Response<ResponseResult?>?

    @Headers("Content-Type: application/json")
    @POST(API_URL_ORDER_DETAILS)
    fun addTransaction(@Body requestBean: RequestBean?): Response<ResponseResult?>?

    @Headers("Content-Type: application/json")
    @POST(API_URL_SUBSCRIPTION_PLAN)
    fun getSubScriptionPlan(@Body requestBean: RequestBean?): Response<ResponseResult?>?

    @Headers("Content-Type: application/json")
    @POST(API_URL_PREMIUM_SUBSCRIPTION)
    fun setSubmitPayment(@Body requestBean: RequestBean?): Response<ResponseResult?>?


    @Headers("Content-Type: application/json")
    @POST(API_URL_GET_SHIPPING_PRICE)
    fun getShippingPrice(@Body requestBean: RequestBean?): Response<ResponseResult?>?



}