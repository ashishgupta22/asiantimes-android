package com.asiantimes.api

import androidx.lifecycle.MutableLiveData
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Constants.ANDROID
import com.asiantimes.base.Utils
import com.asiantimes.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

object ApiRepository {

    fun updateDbVersion(): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.updateDBVERSION(
                    ANDROID,
                    Utils.getDeviceId(),
                    ANDROID
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }

    fun getAllCategories(): MutableLiveData<GetAllCategoriesModel> {
        val mutableLiveData = MutableLiveData<GetAllCategoriesModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getAllCategories()
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetAllCategoriesModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetAllCategoriesModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun register(
        name: String?,
        email: String?,
        number: String?,
        password: String?
    ): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.userRegister(name, number, email, password)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun login(email: String?, password: String?): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val hashMap = HashMap<String, String>()
                hashMap.put("emailaddress", email!!)
                val put = hashMap.put("password", password!!)
                val response = AppConfig.msApiInterface!!.login(email, password)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun loginStr(email: String?, password: String?): MutableLiveData<String> {
        val mutableLiveData = MutableLiveData<String>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val hashMap = HashMap<String, String>()
                hashMap.put("emailaddress", email!!)
                val put = hashMap.put("password", password!!)
                val response = AppConfig.msApiInterface!!.loginStr(email, password)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
//                    val modal = CommonModel()
//                    modal.errorCode = 100
//                    modal.msg =
                    mutableLiveData.postValue(response.message())
                }
            } catch (e: Exception) {
                e.printStackTrace()
                mutableLiveData.postValue(e.message)
            }
        }

        return mutableLiveData
    }

    fun updateProfile(
        userId: RequestBody?,
        userName: RequestBody?,
        mobile: RequestBody?,
        img: MultipartBody.Part?
    ): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response =
                    AppConfig.msApiInterface!!.updateProfile(userId, userName, mobile, img)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun socialLogin(email: String?, name: String?, number: String?): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.socialLogin(email, name, number)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun forgotPassword(email: String?): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.forgotPassword(email)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun updatePassword(
        userId: String?,
        otp: String?,
        password: String?
    ): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.updatePassword(userId, otp, password)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getSearchResult(searchText: String, page: Int): MutableLiveData<GetTopStoriesModel> {
        val mutableLiveData = MutableLiveData<GetTopStoriesModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getSearchResult(
                    AppPreferences.getUserId(),
                    page,
                    searchText
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetTopStoriesModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetTopStoriesModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getVideoResult(): MutableLiveData<GetVideoListModel> {
        val mutableLiveData = MutableLiveData<GetVideoListModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppPreferences.getUserId()?.let {
                    AppConfig.msApiInterface!!.getVideoResult(
                        it
                    )
                }
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetVideoListModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetVideoListModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getVideoCatResult(catId: String, nextpage: String): MutableLiveData<GetVideoListModel> {
        val mutableLiveData = MutableLiveData<GetVideoListModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getVideoCatResult(catId, nextpage)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetVideoListModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetVideoListModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getBookmark(page: Int): MutableLiveData<GetTopStoriesModel> {
        val mutableLiveData = MutableLiveData<GetTopStoriesModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response =
                    AppConfig.msApiInterface!!.getBookmark(AppPreferences.getUserId()!!, page)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetTopStoriesModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetTopStoriesModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun saveBookmark(postId: String, isRemoved: Int): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.saveBookmark(
                    AppPreferences.getUserId()!!,
                    postId,
                    isRemoved
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun commentList(postId: String,nextPage:String): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.commentList(
                    AppPreferences.getUserId()!!,
                    postId,nextPage
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun aboutUs(): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.aboutUs()
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }


    fun commentReply(postId: String, comment: String): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.commentInsert(
                    AppPreferences.getUserId()!!,
                    postId, comment
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun contactList(): MutableLiveData<ResponseResultNew> {
        val mutableLiveData = MutableLiveData<ResponseResultNew>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.contactList()
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResultNew(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResultNew(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun submitContact(
        emailAddress: String,
        first_name: String,
        sur_name: String,

        phone_number: String,
        address: String,
        complain_detail: String
    ): MutableLiveData<ResponseResultNew> {
        val mutableLiveData = MutableLiveData<ResponseResultNew>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.submitContact(
                    emailAddress, first_name, sur_name, phone_number, address, complain_detail
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResultNew(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResultNew(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }


    fun getPostListByCateID(catId: String, page: Int): MutableLiveData<GetTopStoriesModel> {
        val mutableLiveData = MutableLiveData<GetTopStoriesModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getPostListByCateID(
                    AppPreferences.getUserId(),
                    page,
                    catId
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetTopStoriesModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetTopStoriesModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getTopStories(): MutableLiveData<GetTopStoriesModel> {
        val mutableLiveData = MutableLiveData<GetTopStoriesModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response =
                    AppConfig.msApiInterface!!.getTopStories(AppPreferences.getUserId()!!)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = GetTopStoriesModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = GetTopStoriesModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun viewMorePhotos(cateID: String): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getMorePhotos(
                    cateID
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
}