package com.asiantimes.ui.cart

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.RestrictionsManager.RESULT_ERROR
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import com.google.android.gms.wallet.*
import com.google.android.gms.wallet.Wallet.WalletOptions
import com.asiantimes.R
import com.asiantimes.base.*
import com.asiantimes.base.Utils.isEmailValid
import com.asiantimes.base.Utils.showToast
import com.asiantimes.database.AppDatabase
import com.asiantimes.model.*
import com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.widget.TextViewRegular
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.mukesh.countrypicker.listeners.OnCountryPickerListener
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.stripe.android.view.CardMultilineWidget
import java.util.*

class PaymentActivity() : BaseActivity() {
    var rl_actionbar: RelativeLayout? = null
    var stripe: Stripe? = null
    var linearHeaderProgress: LinearLayout? = null
    var linearSubscribeForm: LinearLayout? = null
    private var name_payment: String? = null
    private var email_payment: String? = null
    private var mobile_payment: String? = null
    private var address_first_payment: String? = null
    private var address_second_payment: String? = null
    private var city_payment: String? = null
    private var state_payment: String? = null
    private var country_payment: String? = null
    private var postal_code_payment: String? = null
    private var company_name_payment: String? = null
    private var lastNamePayment: String? = null
    private var name_deliver_payment: String? = null
    private var address_first_deliver_payment: String? = null
    private var address_second_deliver_payment: String? = null
    private var city_deliver_payment: String? = null
    private var country_deliver_payment: String? = null
    private var postal_code_deliver_payment: String? = null
    private var email_deliver_payment: String? = null
    private var mobile_deliver_payment: String? = null
    private var company_name_deliver_payment: String? = null
    private var lastNameDeliverPayment: String? = null
    var totalAmount = 0
    var dbHelper: AppDatabase? = null
    var subscriptionId: String? = null
    var subscriptionType: String? = null
    var subscriptionName: String? = null
    var formShow: String? = null
    var packageType: String? = null
    var text_card_payment: TextViewRegular? = null
    private var editNamePayment: EditText? = null
    private var editLastNamePayment: EditText? = null
    private var editEmailPayment: EditText? = null
    private var editMobilePayment: EditText? = null
    private var editCompanyNamePayment: EditText? = null
    private var editAddressFirstPayment: EditText? = null
    private var editAddressSecondPayment: EditText? = null
    private var editCityPayment: EditText? = null
    private var editStatePayment: EditText? = null
    private var editCountryPayment: EditText? = null
    private var editPostalCodePayment: EditText? = null
    private var checkboxDeliverPayment: CheckBox? = null
    private var linearDeliverAddress: LinearLayout? = null
    private var editNameDeliverPayment: EditText? = null
    private var editLastNameDeliverPayment: EditText? = null
    private var editEmailDeliverPayment: EditText? = null
    private var editMobileDeliverPayment: EditText? = null
    private var editCompanyNameDeliverPayment: EditText? = null
    private var editAddressFirstDeliverPayment: EditText? = null
    private var editAddressSecondDeliverPayment: EditText? = null
    private var editCityDeliverPayment: EditText? = null
    private var editCountryDeliverPayment: EditText? = null
    private var editPostalCodeDeliverPayment: EditText? = null
    private var editNoteOrderPayment: EditText? = null
    private var editAmountPayment: EditText? = null
    private var cardInputWidget: CardMultilineWidget? = null
    private var buttonStripePay: RippleButton? = null
    private var button_google_pay: RippleButton? = null
    private var paymentsClient: PaymentsClient? = null
    var userData: CommonModel.UserDetail? = null
    val viewModel: SubscriptionHomeViewModel by viewModels()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) window.statusBarColor =
            resources.getColor(R.color.colorButton)
        setContentView(R.layout.activity_payment)
        rl_actionbar = findViewById(R.id.rl_actionbar)
        initView()
        /// init google payment with stripe
        paymentsClient = Wallet.getPaymentsClient(
            this,
            WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                .build()
        )
        //
        //isReadyToPay();
        dbHelper = AppDatabase.getDatabase(this@PaymentActivity)
        userData = AppPreferences.getUserDetails()!!
        rl_actionbar!!.setBackgroundColor(resources.getColor(R.color.colorButton))
        linearHeaderProgress = findViewById(R.id.linlaHeaderProgress)
        linearSubscribeForm = findViewById(R.id.linear_subscribe_form)
        stripe = Stripe(this)
        stripe!!.setDefaultPublishableKey(resources.getString(R.string.stripeSecretKey))
        totalAmount = Math.round(intent.extras!!.getFloat("payment") * 100)
        val totalAmountShow = (totalAmount / 100)
        editEmailPayment!!.setText(userData!!.emailAddress)
        editAmountPayment!!.setText("Total amount: £ $totalAmountShow")
        subscriptionId = intent.extras!!.getString("subscriptionId")
        subscriptionType = intent.extras!!.getString("subscriptionType")
        formShow = intent.extras!!.getString("formShow")
        subscriptionName = intent.extras!!.getString("subscriptionName")
        packageType = intent.extras!!.getString("packageType")


        //get country name
        editCountryPayment!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val builder = CountryPicker.Builder().with(this@PaymentActivity)
                    .listener(OnCountryPickerListener { country ->
                        val country_name = country.name
                        editCountryPayment!!.setText(country.name)
                        /*if (!country_name.equalsIgnoreCase("United Kingdom")) {
                                            if (!TextUtils.isEmpty(getIntent().getExtras().getString("subscriptionSpecial"))) {
                                                if (!getIntent().getExtras().getString("subscriptionSpecial").equalsIgnoreCase("subscriptionSpecial")) {
                                            getShippingPrice(subscriptionType, subscriptionName, false);
                                                }
                                            }
                                        }*/if (!checkboxDeliverPayment!!.isChecked) {
                        if ((packageType == "uk")) {
                            if (country_name.equals("United Kingdom", ignoreCase = true)) {
                                editAmountPayment!!.setText(
                                    "Total amount: £ " + (Math.round(
                                        intent.extras!!.getFloat("payment") * 100
                                    )) / 100
                                )
                                totalAmount = (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            } else {
                                getShippingPrice(
                                    subscriptionId,
                                    subscriptionType,
                                    subscriptionName,
                                    false
                                )
                            }
                        } else if ((packageType == "international") && country_name.equals(
                                "United Kingdom",
                                ignoreCase = true
                            )
                        ) {
                            getShippingPrice(
                                subscriptionId,
                                subscriptionType,
                                subscriptionName,
                                true
                            )
                        } else {
                            editAmountPayment!!.setText(
                                "Total amount: £ " + (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            )
                            totalAmount = (Math.round(
                                intent.extras!!.getFloat("payment") * 100
                            )) / 100
                        }
                    }
                    })
                val picker = builder.build()
                picker.showDialog(this@PaymentActivity)
            }
        })
        editCountryDeliverPayment!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val builder = CountryPicker.Builder().with(this@PaymentActivity)
                    .listener(object : OnCountryPickerListener {
                        override fun onSelectCountry(country: Country) {
                            val country_name = country.name
                            editCountryDeliverPayment!!.setText(country.name)
                            /*if (!country_name.equalsIgnoreCase("United Kingdom")) {
                                            if (!TextUtils.isEmpty(getIntent().getExtras().getString("subscriptionSpecial"))) {
                                                if (!getIntent().getExtras().getString("subscriptionSpecial").equalsIgnoreCase("subscriptionSpecial")) {
                                            getShippingPrice(subscriptionType, subscriptionName, false);
                                                }
                                            }
                                        }*/if ((packageType == "uk")) {
                                if (country_name.equals("United Kingdom", ignoreCase = true)) {
                                    editAmountPayment!!.setText(
                                        "Total amount: £ " + (Math.round(
                                            intent.extras!!.getFloat("payment") * 100
                                        )) / 100
                                    )
                                    totalAmount = (Math.round(
                                        intent.extras!!.getFloat("payment") * 100
                                    )) / 100
                                } else {
                                    getShippingPrice(
                                        subscriptionId,
                                        subscriptionType,
                                        subscriptionName,
                                        false
                                    )
                                }
                            } else if ((packageType == "international") && country_name.equals(
                                    "United Kingdom",
                                    ignoreCase = true
                                )
                            ) {
                                getShippingPrice(
                                    subscriptionId,
                                    subscriptionType,
                                    subscriptionName,
                                    true
                                )
                            } else {
                                editAmountPayment!!.setText(
                                    "Total amount: £ " + (Math.round(
                                        intent.extras!!.getFloat("payment") * 100
                                    )) / 100
                                )
                                totalAmount = (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            }
                        }
                    })
                val picker = builder.build()
                picker.showDialog(this@PaymentActivity)
            }
        })
        if (formShow.equals(
                "0",
                ignoreCase = true
            )
        ) linearSubscribeForm!!.setVisibility(View.GONE) else if (formShow.equals(
                "2",
                ignoreCase = true
            )
        ) linearSubscribeForm!!.setVisibility(
            View.GONE
        ) else linearSubscribeForm!!.setVisibility(View.VISIBLE)
        linearDeliverAddress!!.visibility = View.GONE
        editNoteOrderPayment!!.visibility = View.VISIBLE
        checkboxDeliverPayment!!.setOnCheckedChangeListener(object :
            CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
                if (isChecked) linearDeliverAddress!!.visibility = View.VISIBLE else {
                    if ((packageType == "uk")) {
                        if (TextUtils.isEmpty(editCountryPayment!!.text.toString())) {
                            editAmountPayment!!.setText(
                                "Total amount: £ " + (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            )
                            totalAmount = (Math.round(
                                intent.extras!!.getFloat("payment") * 100
                            )) / 100
                        } else if (editCountryPayment!!.text.toString()
                                .equals("United Kingdom", ignoreCase = true)
                        ) {
                            editAmountPayment!!.setText(
                                "Total amount: £ " + (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            )
                            totalAmount = (Math.round(
                                intent.extras!!.getFloat("payment") * 100
                            )) / 100
                        } else {
                            getShippingPrice(
                                subscriptionId,
                                subscriptionType,
                                subscriptionName,
                                false
                            )
                        }
                    } else if ((packageType == "international")) {
                        if (TextUtils.isEmpty(editCountryPayment!!.text.toString())) {
                            editAmountPayment!!.setText(
                                "Total amount: £ " + (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            )
                            totalAmount = (Math.round(
                                intent.extras!!.getFloat("payment") * 100
                            )) / 100
                        } else if (editCountryPayment!!.text.toString()
                                .equals("United Kingdom", ignoreCase = true)
                        ) {
                            getShippingPrice(
                                subscriptionId,
                                subscriptionType,
                                subscriptionName,
                                true
                            )
                        } else {
                            editAmountPayment!!.setText(
                                "Total amount: £ " + (Math.round(
                                    intent.extras!!.getFloat("payment") * 100
                                )) / 100
                            )
                            totalAmount = (Math.round(
                                intent.extras!!.getFloat("payment") * 100
                            )) / 100
                        }
                    }
                    editCountryDeliverPayment!!.setText("")
                    linearDeliverAddress!!.visibility = View.GONE
                }
            }
        })
        if (checkboxDeliverPayment!!.isChecked) linearDeliverAddress!!.visibility =
            View.VISIBLE else linearDeliverAddress!!.visibility = View.GONE
        button_google_pay!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                name_payment = editNamePayment!!.text.toString().trim { it <= ' ' }
                lastNamePayment = editLastNamePayment!!.text.toString().trim { it <= ' ' }
                company_name_payment = editCompanyNamePayment!!.text.toString().trim { it <= ' ' }
                address_first_payment = editAddressFirstPayment!!.text.toString().trim { it <= ' ' }
                address_second_payment =
                    editAddressSecondPayment!!.text.toString().trim { it <= ' ' }
                city_payment = editCityPayment!!.text.toString().trim { it <= ' ' }
                state_payment = editStatePayment!!.text.toString().trim { it <= ' ' }
                country_payment = editCountryPayment!!.text.toString().trim { it <= ' ' }
                postal_code_payment = editPostalCodePayment!!.text.toString().trim { it <= ' ' }
                email_payment = editEmailPayment!!.text.toString().trim { it <= ' ' }
                mobile_payment = editMobilePayment!!.text.toString().trim { it <= ' ' }
                name_deliver_payment = editNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                address_first_deliver_payment =
                    editAddressFirstDeliverPayment!!.text.toString().trim { it <= ' ' }
                address_second_deliver_payment =
                    editAddressSecondDeliverPayment!!.text.toString().trim { it <= ' ' }
                city_deliver_payment = editCityDeliverPayment!!.text.toString().trim { it <= ' ' }
                country_deliver_payment =
                    editCountryDeliverPayment!!.text.toString().trim { it <= ' ' }
                postal_code_deliver_payment =
                    editPostalCodeDeliverPayment!!.text.toString().trim { it <= ' ' }
                email_deliver_payment = editEmailDeliverPayment!!.text.toString().trim { it <= ' ' }
                mobile_deliver_payment =
                    editMobileDeliverPayment!!.text.toString().trim { it <= ' ' }
                company_name_deliver_payment =
                    editCompanyNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                lastNameDeliverPayment =
                    editLastNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                if (name_payment!!.isEmpty() || name_payment!!.trim { it <= ' ' }.length < 3) {
                    showToast(getString(R.string.validated_first_name))
                    editNamePayment!!.requestFocus()
                } else if (lastNamePayment!!.isEmpty() || lastNamePayment!!.trim { it <= ' ' }.length < 3) {
                    showToast(getString(R.string.validated_last_name))
                    editLastNamePayment!!.requestFocus()
                } else if (email_payment!!.isEmpty() || !isEmailValid(email_payment)) {
                    showToast(getString(R.string.validated_email))
                    editEmailPayment!!.requestFocus()
                } else if (mobile_payment!!.isEmpty() || mobile_payment!!.length < 8) {
                    showToast(getString(R.string.validated_mobile))
                    editMobilePayment!!.requestFocus()
                } else if (company_name_payment!!.isEmpty() || company_name_payment!!.length < 3) {
                    showToast(getString(R.string.validated_company_name))
                    editCompanyNamePayment!!.requestFocus()
                } else if (address_first_payment!!.isEmpty() || address_first_payment!!.trim { it <= ' ' }.length < 3) {
                    showToast(
                        getString(R.string.validated_address_first_line)
                    )
                    editAddressFirstPayment!!.requestFocus()
                } else if (city_payment!!.isEmpty() || city_payment!!.trim { it <= ' ' }.length < 3) {
                    showToast(getString(R.string.validated_city))
                    editCityPayment!!.requestFocus()
                } else if (country_payment!!.isEmpty() || country_payment!!.trim { it <= ' ' }.length < 3) {
                    showToast(getString(R.string.validated_country))
                    editCountryPayment!!.requestFocus()
                } else if (postal_code_payment!!.isEmpty() || postal_code_payment!!.trim { it <= ' ' }.length < 3) {
                    showToast(getString(R.string.validated_post_code))
                    editPostalCodePayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && name_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_first_name))
                    editNameDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && lastNameDeliverPayment!!.isEmpty()) {
                    showToast(getString(R.string.validated_last_name))
                    editLastNameDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && email_deliver_payment!!.isEmpty() && !isEmailValid(
                        email_deliver_payment
                    )
                ) {
                    showToast(getString(R.string.validated_email))
                    editEmailDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && mobile_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_mobile))
                    editMobileDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && company_name_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_company_name))
                    editCompanyNameDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && address_first_deliver_payment!!.isEmpty()) {
                    showToast(
                        getString(R.string.validated_address_first_line)
                    )
                    editAddressFirstDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && city_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_city))
                    editCityDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && country_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_country))
                    editCountryDeliverPayment!!.requestFocus()
                } else if (checkboxDeliverPayment!!.isChecked && postal_code_deliver_payment!!.isEmpty()) {
                    showToast(getString(R.string.validated_post_code))
                    editPostalCodeDeliverPayment!!.requestFocus()
                } else {
                    val request: PaymentDataRequest = createPaymentDataRequest()
                    if (request != null) {
                        AutoResolveHelper.resolveTask(
                            paymentsClient!!.loadPaymentData(request),
                            this@PaymentActivity,
                            LOAD_PAYMENT_DATA_REQUEST_CODE
                        )
                        // LOAD_PAYMENT_DATA_REQUEST_CODE is a constant integer of your choice,
                        // similar to what you would use in startActivityForResult
                    }
                }
            }
        })
        buttonStripePay!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val view = currentFocus
                Utils.hideSoftKeyBoard(this@PaymentActivity, view)
                if (formShow.equals("0", ignoreCase = true)) {
                    var cardToSave = cardInputWidget!!.card
                    linearHeaderProgress!!.setVisibility(View.VISIBLE)
                    if (cardToSave == null) {
                        showToast(getString(R.string.invalid_card_details))
                        linearHeaderProgress!!.setVisibility(View.GONE)
                        return
                    }
                    if (cardToSave.validateCard()) {
                        cardToSave = Card(
                            cardToSave.number,
                            cardToSave.expMonth,
                            cardToSave.expYear,
                            cardToSave.cvc
                        )
                        cardToSave.name = userData!!.userName
                        cardToSave.addressLine1 = ""
                        cardToSave.addressLine2 = ""
                        cardToSave.addressZip = ""
                        cardToSave.addressCity = ""
                        cardToSave.addressState = ""
                        cardToSave.addressCountry = ""
                        cardToSave.currency = "gbp"
                        stripe!!.createToken(
                            cardToSave,
                            object : TokenCallback {
                                override fun onSuccess(token: Token) {
//                                        Log.v("token", "---" + token.getId());
                                    setPremiumSubscription(
                                        subscriptionId,
                                        (totalAmount / 100).toString(),
                                        token.id,
                                        subscriptionType
                                    )
                                    //          setNavigationCategory();          Get all category
                                }

                                override fun onError(error: Exception) {
                                    linearHeaderProgress!!.setVisibility(View.GONE)
                                    Toast.makeText(
                                        this@PaymentActivity,
                                        error.localizedMessage,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        )
                    } else {
                        showToast(getString(R.string.invalid_card_details))
                    }
                } else if (formShow.equals("2", ignoreCase = true)) {
                    var cardToSave = cardInputWidget!!.card
                    linearHeaderProgress!!.setVisibility(View.VISIBLE)
                    if (cardToSave == null) {
                        showToast(getString(R.string.invalid_card_details))
                        linearHeaderProgress!!.setVisibility(View.GONE)
                        return
                    }
                    if (cardToSave.validateCard()) {
                        cardToSave = Card(
                            cardToSave.number,
                            cardToSave.expMonth,
                            cardToSave.expYear,
                            cardToSave.cvc
                        )
                        cardToSave.name = userData!!.userName
                        cardToSave.addressLine1 = ""
                        cardToSave.addressLine2 = ""
                        cardToSave.addressZip = ""
                        cardToSave.addressCity = ""
                        cardToSave.addressState = ""
                        cardToSave.addressCountry = ""
                        cardToSave.currency = "gbp"
                        stripe!!.createToken(
                            cardToSave,
                            object : TokenCallback {
                                override fun onSuccess(token: Token) {
//                                        Log.v("token", "---" + token.getId());
                                    setPremiumSubscription(
                                        subscriptionId,
                                        (totalAmount / 100).toString() + totalAmount,
                                        token.id,
                                        subscriptionType
                                    )
                                    //          setNavigationCategory();          Get all category
                                }

                                override fun onError(error: Exception) {
                                    linearHeaderProgress!!.setVisibility(View.GONE)
                                    Toast.makeText(
                                        this@PaymentActivity,
                                        error.localizedMessage,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        )
                    } else {
                        showToast(getString(R.string.invalid_card_details))
                    }
                } else {
                    name_payment = editNamePayment!!.text.toString().trim { it <= ' ' }
                    lastNamePayment = editLastNamePayment!!.text.toString().trim { it <= ' ' }
                    company_name_payment =
                        editCompanyNamePayment!!.text.toString().trim { it <= ' ' }
                    address_first_payment =
                        editAddressFirstPayment!!.text.toString().trim { it <= ' ' }
                    address_second_payment =
                        editAddressSecondPayment!!.text.toString().trim { it <= ' ' }
                    city_payment = editCityPayment!!.text.toString().trim { it <= ' ' }
                    state_payment = editStatePayment!!.text.toString().trim { it <= ' ' }
                    country_payment = editCountryPayment!!.text.toString().trim { it <= ' ' }
                    postal_code_payment = editPostalCodePayment!!.text.toString().trim { it <= ' ' }
                    email_payment = editEmailPayment!!.text.toString().trim { it <= ' ' }
                    mobile_payment = editMobilePayment!!.text.toString().trim { it <= ' ' }
                    name_deliver_payment =
                        editNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                    address_first_deliver_payment =
                        editAddressFirstDeliverPayment!!.text.toString().trim { it <= ' ' }
                    address_second_deliver_payment =
                        editAddressSecondDeliverPayment!!.text.toString().trim { it <= ' ' }
                    city_deliver_payment =
                        editCityDeliverPayment!!.text.toString().trim { it <= ' ' }
                    country_deliver_payment =
                        editCountryDeliverPayment!!.text.toString().trim { it <= ' ' }
                    postal_code_deliver_payment =
                        editPostalCodeDeliverPayment!!.text.toString().trim { it <= ' ' }
                    email_deliver_payment =
                        editEmailDeliverPayment!!.text.toString().trim { it <= ' ' }
                    mobile_deliver_payment =
                        editMobileDeliverPayment!!.text.toString().trim { it <= ' ' }
                    company_name_deliver_payment =
                        editCompanyNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                    lastNameDeliverPayment =
                        editLastNameDeliverPayment!!.text.toString().trim { it <= ' ' }
                    if (name_payment!!.isEmpty() || name_payment!!.trim { it <= ' ' }.length < 3) {
                        showToast(getString(R.string.validated_first_name))
                        editNamePayment!!.requestFocus()
                    } else if (lastNamePayment!!.isEmpty() || lastNamePayment!!.trim { it <= ' ' }.length < 3) {
                        showToast(getString(R.string.validated_last_name))
                        editLastNamePayment!!.requestFocus()
                    } else if (email_payment!!.isEmpty() || !isEmailValid(email_payment)) {
                        showToast(getString(R.string.validated_email))
                        editEmailPayment!!.requestFocus()
                    } else if (mobile_payment!!.isEmpty() || mobile_payment!!.length < 8) {
                        showToast(getString(R.string.validated_mobile))
                        editMobilePayment!!.requestFocus()
                    } else if (company_name_payment!!.isEmpty() || company_name_payment!!.length < 3) {
                        showToast(getString(R.string.validated_company_name))
                        editCompanyNamePayment!!.requestFocus()
                    } else if (address_first_payment!!.isEmpty() || address_first_payment!!.trim { it <= ' ' }.length < 3) {
                        showToast(
                            getString(R.string.validated_address_first_line)
                        )
                        editAddressFirstPayment!!.requestFocus()
                    } else if (city_payment!!.isEmpty() || city_payment!!.trim { it <= ' ' }.length < 3) {
                        showToast(getString(R.string.validated_city))
                        editCityPayment!!.requestFocus()
                    } else if (country_payment!!.isEmpty() || country_payment!!.trim { it <= ' ' }.length < 3) {
                        showToast(getString(R.string.validated_country))
                        editCountryPayment!!.requestFocus()
                    } else if (postal_code_payment!!.isEmpty() || postal_code_payment!!.trim { it <= ' ' }.length < 3) {
                        showToast(getString(R.string.validated_post_code))
                        editPostalCodePayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && name_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_first_name))
                        editNameDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && lastNameDeliverPayment!!.isEmpty()) {
                        showToast(getString(R.string.validated_last_name))
                        editLastNameDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && email_deliver_payment!!.isEmpty() && !isEmailValid(
                            email_deliver_payment
                        )
                    ) {
                        showToast(getString(R.string.validated_email))
                        editEmailDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && mobile_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_mobile))
                        editMobileDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && company_name_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_company_name))
                        editCompanyNameDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && address_first_deliver_payment!!.isEmpty()) {
                        showToast(
                            getString(R.string.validated_address_first_line)
                        )
                        editAddressFirstDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && city_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_city))
                        editCityDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && country_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_country))
                        editCountryDeliverPayment!!.requestFocus()
                    } else if (checkboxDeliverPayment!!.isChecked && postal_code_deliver_payment!!.isEmpty()) {
                        showToast(getString(R.string.validated_post_code))
                        editPostalCodeDeliverPayment!!.requestFocus()
                    } else {
                        //   linearHeaderProgress.setVisibility(View.VISIBLE);
                        AppConfig.showProgress(this@PaymentActivity)
                        var cardToSave = cardInputWidget!!.card
                        if (cardToSave == null) {
                            showToast(
                                getString(R.string.invalid_card_details),
                            )
                            linearHeaderProgress!!.setVisibility(View.GONE)
                            AppConfig.hideProgress()
                            return
                        }
                        if (cardToSave.validateCard()) {
                            cardToSave = Card(
                                cardToSave.number,
                                cardToSave.expMonth,
                                cardToSave.expYear,
                                cardToSave.cvc
                            )
                            cardToSave.name = name_payment
                            cardToSave.addressLine1 = address_first_payment
                            cardToSave.addressLine2 = address_second_payment
                            cardToSave.addressZip = postal_code_payment
                            cardToSave.addressCity = city_payment
                            cardToSave.addressState = state_payment
                            cardToSave.addressCountry = country_payment
                            cardToSave.currency = "gbp"
                            stripe!!.createToken(
                                cardToSave,
                                object : TokenCallback {
                                    override fun onSuccess(token: Token) {
//                                            Log.v("token", "---" + token.getId());
                                        val isCheckBoxCheck: String
                                        if (checkboxDeliverPayment!!.isChecked) isCheckBoxCheck =
                                            "1" else isCheckBoxCheck = "0"
                                        payNowPaymentAPI(token.id,
                                            "gbp",
                                            name_payment,
                                            lastNamePayment,
                                            company_name_payment,
                                            email_payment,
                                            mobile_payment,
                                            address_first_payment,
                                            address_second_payment,
                                            city_payment,
                                            country_payment,
                                            postal_code_payment,
                                            subscriptionId,
                                            isCheckBoxCheck,
                                            name_deliver_payment,
                                            lastNameDeliverPayment,
                                            company_name_deliver_payment,
                                            email_deliver_payment,
                                            mobile_deliver_payment,
                                            address_first_deliver_payment,
                                            address_second_deliver_payment,
                                            city_deliver_payment,
                                            country_deliver_payment,
                                            postal_code_deliver_payment,
                                            editNoteOrderPayment!!.text.toString()
                                                .trim { it <= ' ' })
                                        //          setNavigationCategory();          Get all category
                                    }

                                    override fun onError(error: Exception) {
                                        linearHeaderProgress!!.setVisibility(View.GONE)
                                        AppConfig.hideProgress()
                                        Toast.makeText(
                                            this@PaymentActivity,
                                            error.localizedMessage,
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            )
                        } else {
                            linearHeaderProgress!!.setVisibility(View.GONE)
                            AppConfig.hideProgress()
                            showToast(
                                getString(R.string.invalid_card_details)
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        title = getString(R.string.payment_title)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        when (intent.extras!!.getString("nextActivity")) {
            "MainActivity" -> redirectToMainActivity()
            "Details" -> finish()
            "Subscription" -> finish()
            "NewsSection" -> finish()
            "CommentActivity" -> finish()
            "CartActivity" -> finish()
            else -> redirectToMainActivity()
        }
    }

    fun payNowPaymentAPI(
        tokenId: String?,
        currency: String?,
        name: String?,
        lastName: String?,
        companyName: String?,
        email: String?,
        mobile: String?,
        addressLineOne: String?,
        addressLineTow: String?,
        city: String?,
        country: String?,
        postalCode: String?,
        subscriptionId: String?,
        isCheckboxChecked: String,
        nameDeliver: String?,
        lastNameDeliver: String?,
        companyNameDeliver: String?,
        emailDeliver: String?,
        mobileDeliver: String?,
        addressLineOneDeliver: String?,
        addressLineTowDeliver: String?,
        cityDeliver: String?,
        countryDeliver: String?,
        postalCodeDeliver: String?,
        orderNoteDeliver: String?
    ) {
        try {
            val billingAddress = BillingAddress(
                name, lastName, companyName, email, mobile, addressLineOne,
                addressLineTow, city, country, postalCode
            )
            val shippingAddress: ShippingAddress
            if (isCheckboxChecked.equals("1", ignoreCase = true)) shippingAddress = ShippingAddress(
                nameDeliver,
                lastNameDeliver,
                companyNameDeliver,
                emailDeliver,
                mobileDeliver,
                addressLineOneDeliver,
                addressLineTowDeliver,
                cityDeliver,
                countryDeliver,
                postalCodeDeliver
            ) else shippingAddress = ShippingAddress(
                name, lastName, companyName, email, mobile, addressLineOne,
                addressLineTow, city, country, postalCode
            )
            val requestBean = RequestBean()
            requestBean.setSubscriptionPayment(
                AppPreferences.getUserId(),
                tokenId,
                "" + totalAmount,
                currency,
                subscriptionId,
                billingAddress,
                shippingAddress,
                orderNoteDeliver
            )

            viewModel.setSubmitPayment(requestBean).observe(this, {
                val responseResult = it
                if (responseResult.getErrorCode() === 0) {
                    showToast(responseResult.getMessage())
                    dbHelper!!.getCartManangeDao().removeCartItemALL()
                    premiumPostArray
                } else linearHeaderProgress!!.visibility = View.GONE
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    //save user membership
    private val premiumPostArray: Unit
        private get() {
            try {
                val requestBean = RequestBean()
                requestBean.getTopStory(AppPreferences.getUserId())

                viewModel.getPremiumPostRequestNew(requestBean).observe(this, {
                    val responseResult = it
                    if (responseResult.getErrorCode() === 0) {
                        //save user membership
                        AppPreferences.setPremiumArr(
                            responseResult.goldMembership,
                            responseResult.digitalPowerList,
                            responseResult.digitalRichList,
                            responseResult.digitalFreeContent,
                            responseResult.premiumPowerList,
                            responseResult.premiumRichList
                        )
                        redirectToMainActivity()
                    }
                })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    private fun initView() {
        editNamePayment = findViewById(R.id.edit_name_payment)
        editLastNamePayment = findViewById(R.id.edit_lastname_payment)
        editEmailPayment = findViewById(R.id.edit_email_payment)
        editMobilePayment = findViewById(R.id.edit_mobile_payment)
        editCompanyNamePayment = findViewById(R.id.edit_company_name_payment)
        editAddressFirstPayment = findViewById(R.id.edit_address_first_payment)
        editAddressSecondPayment = findViewById(R.id.edit_address_second_payment)
        editCityPayment = findViewById(R.id.edit_city_payment)
        editStatePayment = findViewById(R.id.edit_state_payment)
        editCountryPayment = findViewById(R.id.edit_country_payment)
        editPostalCodePayment = findViewById(R.id.edit_postal_code_payment)
        checkboxDeliverPayment = findViewById(R.id.checkbox_deliver_payment)
        linearDeliverAddress = findViewById(R.id.linear_deliver_address)
        editNameDeliverPayment = findViewById(R.id.edit_name_deliver_payment)
        editLastNameDeliverPayment = findViewById(R.id.edit_lastname_deliver_payment)
        editEmailDeliverPayment = findViewById(R.id.edit_email_deliver_payment)
        editMobileDeliverPayment = findViewById(R.id.edit_mobile_deliver_payment)
        editCompanyNameDeliverPayment = findViewById(R.id.edit_company_name_deliver_payment)
        editAddressFirstDeliverPayment = findViewById(R.id.edit_address_first_deliver_payment)
        editAddressSecondDeliverPayment = findViewById(R.id.edit_address_second_deliver_payment)
        editCityDeliverPayment = findViewById(R.id.edit_city_deliver_payment)
        editCountryDeliverPayment = findViewById(R.id.edit_country_deliver_payment)
        editPostalCodeDeliverPayment = findViewById(R.id.edit_postal_code_deliver_payment)
        editNoteOrderPayment = findViewById(R.id.edit_note_order_payment)
        editAmountPayment = findViewById(R.id.edit_amount_payment)
        cardInputWidget = findViewById(R.id.card_input_widget)
        buttonStripePay = findViewById(R.id.button_stripe_pay)
        button_google_pay = findViewById(R.id.button_google_pay)
        text_card_payment = findViewById(R.id.text_card_payment)
    }

    private fun setPremiumSubscription(
        subscriptionId: String?,
        subscriptionPrice: String,
        tokenId: String,
        subscriptionTitle: String?
    ) {
        if (linearHeaderProgress != null && linearHeaderProgress!!.isShown) linearHeaderProgress!!.visibility =
            View.GONE


        AppConfig.showProgress(this)
        val requestBean = RequestBean()
        requestBean.setPremiumSubscription(
            subscriptionId,
            subscriptionPrice,
            subscriptionTitle,
            tokenId,
            AppPreferences.getUserId()
        )
        viewModel.setSubmitPayment(requestBean).observe(this, {
            val responseResult = it
            AppConfig.hideProgress()
            if (responseResult.getErrorCode() === 0) {
                AppPreferences.setPremiumArr(
                    responseResult.goldMembership,
                    responseResult.digitalPowerList,
                    responseResult.digitalRichList,
                    responseResult.digitalFreeContent,
                    responseResult.premiumPowerList,
                    responseResult.premiumRichList
                )
                if (formShow.equals(
                        "2",
                        ignoreCase = true
                    )
                ) redirectToMainActivity() else finish()
            }
        })
    }

    private fun redirectToMainActivity() {
        val intent = Intent(this@PaymentActivity, DashboardActivity::class.java)
        intent.putExtra("select", "0")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    // get shipping details
    fun getShippingPrice(
        subscriptionId: String?,
        subscription_plan: String?,
        subscriptionName: String?,
        isUk: Boolean
    ) {
        AppConfig.showProgress(this)
        val requestBean = RequestBean()
        requestBean.setShippingPrice(subscriptionId, subscriptionName, subscription_plan, isUk)

        viewModel.getShippingPrice(requestBean).observe(this, {
            val responseResult = it
            if (responseResult.getErrorCode() == 0) {
                if (responseResult.getSubscriptionList()!!.size > 0) {
                    val adb = AlertDialog.Builder(this@PaymentActivity)
                    adb.setTitle("Rate Change")
                    adb.setMessage(responseResult.getMessage())
                    //adb.setIcon(android.R.drawable.ic_dialog_alert);
                    adb.setPositiveButton("OK", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface, which: Int) {
                            totalAmount = Math.round(
                                Objects.requireNonNull(
                                    responseResult.getSubscriptionList()!![0]!!.subscriptionPrice * 100
                                )
                                    .toDouble()
                            ).toInt()
                            val totalAmountShow = (totalAmount / 100)
                            editAmountPayment!!.setText("Total amount: £ $totalAmountShow")
                        }
                    })
                    adb.setNegativeButton(
                        "Cancel",
                        object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface, which: Int) {
                                totalAmount = Math.round(
                                    Objects.requireNonNull(
                                        responseResult.getSubscriptionList()!![0]!!.subscriptionPrice * 100
                                    )
                                        .toDouble()
                                ).toInt()
                                val totalAmountShow = (totalAmount / 100)
                                editAmountPayment!!.setText("Total amount: £ $totalAmountShow")
                            }
                        })
                    adb.show()
                }
            } else {
                showToast(responseResult.getMessage())
                editCountryPayment!!.setText("")
                editCountryPayment!!.requestFocus()
            }
        })
    }

    private fun createTokenizationParameters(): PaymentMethodTokenizationParameters {
        return PaymentMethodTokenizationParameters.newBuilder()
            .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
            .addParameter("gateway", "stripe")
            .addParameter("stripe:publishableKey", resources.getString(R.string.stripeSecretKey))
            .addParameter("stripe:version", "2018-11-08")
            .build()
    }

    private fun createPaymentDataRequest(): PaymentDataRequest {
        val totalAmountShow = (totalAmount / 100)
        val request = PaymentDataRequest.newBuilder()
            .setTransactionInfo(
                TransactionInfo.newBuilder()
                    .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                    .setTotalPrice(totalAmountShow.toString())
                    .setCurrencyCode("gbp")
                    .build()
            )
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
            .setCardRequirements(
                CardRequirements.newBuilder()
                    .addAllowedCardNetworks(
                        Arrays.asList(
                            WalletConstants.CARD_NETWORK_AMEX,
                            WalletConstants.CARD_NETWORK_DISCOVER,
                            WalletConstants.CARD_NETWORK_VISA,
                            WalletConstants.CARD_NETWORK_MASTERCARD
                        )
                    )
                    .build()
            )
        request.setPaymentMethodTokenizationParameters(createTokenizationParameters())
        return request.build()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            LOAD_PAYMENT_DATA_REQUEST_CODE -> when (resultCode) {
                RESULT_OK -> {
                    val paymentData = PaymentData.getFromIntent((data)!!)
                    // You can get some data on the user's card, such as the brand and last 4 digits
                    val info = paymentData!!.cardInfo
                    // You can also pull the user address from the PaymentData object.
                    val address = paymentData.shippingAddress

                    // This is the raw JSON string version of your Stripe token.
                    val rawToken = paymentData.paymentMethodToken!!.token

                    // Now that you have a Stripe token object, charge that by using the id
                    val stripeToken = Token.fromString(rawToken)
                    if (stripeToken != null) {
                        setPremiumSubscription(
                            subscriptionId,
                            (totalAmount / 100).toString() + totalAmount,
                            stripeToken.id,
                            subscriptionType
                        )
                        val isCheckBoxCheck: String
                        if (checkboxDeliverPayment!!.isChecked) isCheckBoxCheck =
                            "1" else isCheckBoxCheck = "0"
                        payNowPaymentAPI(stripeToken.id,
                            "gbp",
                            name_payment,
                            lastNamePayment,
                            company_name_payment,
                            email_payment,
                            mobile_payment,
                            address_first_payment,
                            address_second_payment,
                            city_payment,
                            country_payment,
                            postal_code_payment,
                            subscriptionId,
                            isCheckBoxCheck,
                            name_deliver_payment,
                            lastNameDeliverPayment,
                            company_name_deliver_payment,
                            email_deliver_payment,
                            mobile_deliver_payment,
                            address_first_deliver_payment,
                            address_second_deliver_payment,
                            city_deliver_payment,
                            country_deliver_payment,
                            postal_code_deliver_payment,
                            editNoteOrderPayment!!.text.toString().trim { it <= ' ' })
                        //          setNavigationCategory();          Get all category

                        // This chargeToken function is a call to your own server, which should then connect
                        //                            // to Stripe's API to finish the charge.
                        //                          //  chargeToken(stripeToken.getId());
                    }
                }
                RESULT_CANCELED -> {
                }
                RESULT_ERROR -> {

                }
            }
            else -> {
            }
        }
    }

    private fun redirectToHome() {
        val loginIntent = Intent(this@PaymentActivity, DashboardActivity::class.java)
        loginIntent.putExtra("select", "0")
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(loginIntent)
    }

    private val buildVersionCode: Int
        private get() {
            try {
                val pInfo = packageManager.getPackageInfo(packageName, 0)
                return pInfo.versionCode
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return -1
        }

//    private fun storeDataIntoTable(responsePacket: ResponseResult) {
//        val verCode = buildVersionCode
//        if (AppPreferences.getInstance().getAppVersionCode() < verCode) {
//            AppPreferences.getInstance().setAppVersionCode(verCode)
//            AppPreferences.getInstance().setIsDBClear("t")
//            dbHelper.dropDataBase(true)
//            dbHelper = EasternEyeDBHelper(this@PaymentActivity)
//        } else dbHelper.dropDataBase(false)
//        dbHelper.clearMainCategoryTable()
//        dbHelper.insertEasternEyeMainCategory("01", "Home", "#d12f2c", "", "News")
//        for (i in responsePacket.postList!!.indices) {
//            if (responsePacket.postList!![i].cat.length > 2) {
//                if (!responsePacket.postList!![i].categoryType.equals(
//                        "Subscribe",
//                        ignoreCase = true
//                    ) && !responsePacket.postList!![i].categoryType.equals(
//                        "contactus",
//                        ignoreCase = true
//                    )
//                ) {
//                    dbHelper.insertEasternEyeMainCategory(
//                        responsePacket.postList!![i].catId,
//                        responsePacket.postList!![i].cat,
//                        responsePacket.postList!![i].catColor.trim { it <= ' ' },
//                        responsePacket.postList!![i].catUrl.trim { it <= ' ' },
//                        responsePacket.postList!![i].categoryType
//                    )
//                }
//            }
//        }
//    }

    companion object {
        private val LOAD_PAYMENT_DATA_REQUEST_CODE = 1023
    }
}