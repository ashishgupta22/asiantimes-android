package com.asiantimes.ui.cart

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.Observer
import com.baoyz.swipemenulistview.SwipeMenu
import com.baoyz.swipemenulistview.SwipeMenuCreator
import com.baoyz.swipemenulistview.SwipeMenuItem
import com.baoyz.swipemenulistview.SwipeMenuListView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.snackbar.Snackbar
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.showProgress
import com.asiantimes.base.AppPreferences.getLoginStatus
import com.asiantimes.base.AppPreferences.getUserId
import com.asiantimes.base.AppPreferences.setPremiumArr
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.RippleButton
import com.asiantimes.base.Utils
import com.asiantimes.base.Utils.isOnline
import com.asiantimes.database.AppDatabase
import com.asiantimes.model.CartItem
import com.asiantimes.model.PremiumPlanList
import com.asiantimes.model.RequestBean
import com.asiantimes.model.ResponseResult
import com.asiantimes.subscription.viewmodel.SubscriptionViewModel
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.widget.TextViewRegular
import java.util.*

class CartActivity() : BaseActivity(),
    GoogleApiClient.OnConnectionFailedListener {
    private var cartItemList: ArrayList<CartItem>? = null
    private var subscriptionPrice: ArrayList<Float>? = null
    private var subscriptionCount: ArrayList<Float>? = null
    private var cartAdapter: CartAdapter? = null
    var listView: SwipeMenuListView? = null
    var layoutInflater: View? = null
    var rl_actionbar: RelativeLayout? = null
    private val LOGIN_REGISTER = 0
    private val premiumPlanLists: List<PremiumPlanList> = ArrayList()
    var dbHelper: AppDatabase? = null
    var totalPrice = 0.0f
    var subscriptionId: String? = null
    private var textSubtotalPriceCart: TextViewRegular? = null
    private var footerText: TextViewRegular? = null
    private var textTotalPriceCart: TextViewRegular? = null
    private var buttonProceedCart: RippleButton? = null
    var viewModel = SubscriptionViewModel()
    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) window.statusBarColor =
            resources.getColor(R.color.colorButton)
        setContentView(R.layout.activity_cart)
        initialisationView()
    }

    @SuppressLint("InflateParams")
    private fun initialisationView() {
        dbHelper = AppDatabase.getDatabase(this)
        cartItemList = ArrayList()
        subscriptionPrice = ArrayList()
        subscriptionCount = ArrayList()
        rl_actionbar = findViewById(R.id.rl_actionbar)
        rl_actionbar!!.setBackgroundColor(resources.getColor(R.color.colorButton))
        if (Build.VERSION.SDK_INT >= 21) window.statusBarColor =
            resources.getColor(R.color.colorButton)
        listView = findViewById(R.id.listView)
        setCartItemList()
        cartAdapter = CartAdapter(this@CartActivity, cartItemList!!, this)
        layoutInflater =
            (Objects.requireNonNull(this.getSystemService(LAYOUT_INFLATER_SERVICE)) as LayoutInflater).inflate(
                R.layout.cart_footor_layout,
                null,
                false
            )
        initView(layoutInflater)
        listView!!.addFooterView(layoutInflater)
        listView!!.setAdapter(cartAdapter)
        setTotalPrice(totalPrice, totalPrice)
        setViewEvent()
    }

    private fun setViewEvent() {
        val creator: SwipeMenuCreator = object : SwipeMenuCreator {
            override fun create(menu: SwipeMenu) {
                when (menu.viewType) {
                    0 -> createMenu1(menu)
                    1 -> createMenu2(menu)
                    2 -> createMenu3(menu)
                }
            }

            private fun createMenu1(menu: SwipeMenu) {
                val item1 = SwipeMenuItem(
                    applicationContext
                )
                item1.width = dp2px(90)
                item1.setIcon(R.drawable.delete)
                menu.addMenuItem(item1)
            }

            private fun createMenu2(menu: SwipeMenu) {
                val item1 = SwipeMenuItem(
                    applicationContext
                )
                item1.width = dp2px(90)
                item1.setIcon(R.drawable.delete)
                menu.addMenuItem(item1)
            }

            private fun createMenu3(menu: SwipeMenu) {
                val item1 = SwipeMenuItem(
                    applicationContext
                )
                item1.background = ColorDrawable(
                    Color.rgb(
                        0x30, 0xB1,
                        0xF5
                    )
                )
                item1.width = dp2px(90)
                item1.setIcon(R.drawable.close)
                menu.addMenuItem(item1)
                val item2 = SwipeMenuItem(
                    applicationContext
                )
                item2.background = ColorDrawable(
                    Color.rgb(
                        0xC9, 0xC9,
                        0xCE
                    )
                )
                item2.width = dp2px(90)
                item2.setIcon(R.drawable.close)
                menu.addMenuItem(item2)
            }
        }
        listView!!.setMenuCreator(creator)
        listView!!.setOnMenuItemClickListener(SwipeMenuListView.OnMenuItemClickListener { position, menu, index ->
            val item = cartItemList!![position]
            when (index) {
                0 -> {
                    val isRemove =
                        dbHelper!!.getCartManangeDao().removeCartItem(item.subscriptionID)
                    if (isRemove > 0) {
                        cartItemList!!.removeAt(position)
                        if (cartItemList!!.size > 0) {
                            cartAdapter!!.notifyDataSetChanged()
                            subscriptionPrice!!.clear()
                            subscriptionCount!!.clear()
                            var i = 0
                            while (i < cartItemList!!.size) {
                                subscriptionPrice!!.add(
                                    java.lang.Float.valueOf(
                                        cartItemList!![i].subscriptionPrice.replace(
                                            "£",
                                            ""
                                        ).trim { it <= ' ' })
                                )
                                subscriptionCount!!.add(
                                    cartItemList!![i].subscriptionCount.toFloat()
                                )
                                i++
                            }
                            if (subscriptionPrice!!.size > 0) {
                                totalPrice = 0.0f
                                var j = 0
                                while (j < subscriptionPrice!!.size) {
                                    totalPrice =
                                        totalPrice + (subscriptionPrice!![j] * subscriptionCount!![j])
                                    j++
                                }
                            }
                        } else finish()
                        setTotalPrice(totalPrice, totalPrice)
                    } else Snackbar.make(
                        findViewById(android.R.id.content),
                        "Please try again.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                1 -> {
                    cartItemList!!.removeAt(position)
                    cartAdapter!!.notifyDataSetChanged()
                }
            }
            false
        })
        buttonProceedCart!!.setOnClickListener({ v: View? ->
            if (isOnline(this@CartActivity)) {
                if (getLoginStatus()) {
                    val paymentIntent: Intent =
                        Intent(this@CartActivity, PaymentActivity::class.java)
                    totalPrice = textTotalPriceCart!!.getText().toString().replace("£", "")
                        .trim { it <= ' ' }
                        .toFloat()
                    subscriptionId = ""
                    for (i in cartItemList!!.indices) {
                        subscriptionId =
                            subscriptionId + "" + cartItemList!!.get(i).getSubscriptionID() + ","
                    }
                    paymentIntent.putExtra("payment", totalPrice)
                    paymentIntent.putExtra(
                        "subscriptionId",
                        subscriptionId!!.substring(0, subscriptionId!!.length - 1)
                    )
                    paymentIntent.putExtra("subscriptionType", "subscription")
                    paymentIntent.putExtra("formShow", "1")
                    paymentIntent.putExtra("nextActivity", "CartActivity")
                    startActivity(paymentIntent)
                    finish()
                } else startActivityForResult(
                    Intent(this, LogInActivity::class.java),
                    LOGIN_REGISTER
                )
            } else Snackbar.make(
                findViewById(android.R.id.content),
                getResources().getString(R.string.no_network),
                Snackbar.LENGTH_LONG
            ).show()
        })
    }

    fun setCartItemList() {
        cartItemList!!.clear()
        cartItemList!!.addAll(dbHelper!!.getCartManangeDao().getAllData())
        try {
            if (cartItemList!!.size > 0) {
                for (i in cartItemList!!.indices) {
                    subscriptionPrice!!.add(
                        java.lang.Float.valueOf(
                            cartItemList!![i].subscriptionPrice.replace(
                                "£",
                                ""
                            ).trim { it <= ' ' })
                    )
                    subscriptionCount!!.add(cartItemList!![i].subscriptionCount.toFloat())
                }
                if (subscriptionPrice!!.size > 0) {
                    for (j in subscriptionPrice!!.indices) {
                        totalPrice = totalPrice + (subscriptionPrice!![j] * subscriptionCount!![j])
                    }
                }
            }
        } catch (ex: NumberFormatException) {
            ex.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        totalPrice = 0.0f
        title = getString(R.string.cart_title)
        setCartItemList()
    }

    private fun dp2px(dp: Int): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            resources.displayMetrics
        ).toInt()
    }

    fun updateCart(subscriptionPrice: Float, subscriptionCountOld: Int, subscriptionCountNew: Int) {
        totalPrice =
            (totalPrice - (subscriptionPrice * subscriptionCountOld) + (subscriptionPrice * subscriptionCountNew))
        setTotalPrice(totalPrice, totalPrice)
    }

    @SuppressLint("DefaultLocale", "SetTextI18n")
    fun setTotalPrice(totalPrice: Float, subTotalPrice: Float) {
        var totalPrice = totalPrice
        var subTotalPrice = subTotalPrice
        totalPrice = String.format("%.02f", totalPrice).toFloat()
        subTotalPrice = String.format("%.02f", subTotalPrice).toFloat()
        textSubtotalPriceCart!!.text = "£$subTotalPrice"
        textTotalPriceCart!!.text = "£$totalPrice"
    }

    private fun setPreferencesData(responseResult: ResponseResult) {
//        if (responseResult.getErrorCode() == 0) {
//            AppPreferences.getInstance().setUserId(responseResult.getUserDetail().getUserId());
//            AppPreferences.getInstance().setUserMobile(responseResult.getUserDetail().getMobileNumber());
//            AppPreferences.getInstance().setUserEmail(responseResult.getUserDetail().getEmailAddress());
//            AppPreferences.getInstance().setUserName(responseResult.getUserDetail().getUserName());
////            AppPreferences.getInstance().setAPIHEADER(responseResult.getUserDetail().getSpecailvaltimeignore());
//            AppPreferences.getInstance().setImageURl(responseResult.getUserDetail().getImage());
//            AppPreferences.getInstance().setIsPremium(responseResult.getUserDetail().getIsPremium());
//            if (selectedPremiumPlanPrice != null && !selectedPremiumPlanPrice.equalsIgnoreCase("0"))
//                redirectToPaymentActivity();
//            else
//                getPremiumPostArray();
//        } else
//            Utils.INSTANCE.showToast(responseResult.getMessage());
    }

    //save user membership
    private val premiumPostArray: Unit
        private get() {
            try {
                showProgress(this)
                val requestBean = RequestBean()
                requestBean.getTopStory(getUserId())
                viewModel.getPremiumPostRequestNew(requestBean)
                    .observe(this, object : Observer<ResponseResult> {
                        override fun onChanged(responseResult: ResponseResult) {
                            if (responseResult.getErrorCode() === 0) {
                                //save user membership
                                setPremiumArr(
                                    responseResult.goldMembership,
                                    responseResult.digitalPowerList,
                                    responseResult.digitalRichList,
                                    responseResult.digitalFreeContent,
                                    responseResult.premiumPowerList,
                                    responseResult.premiumRichList
                                )
                            }
                        }
                    })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    private fun redirectToPaymentActivity() {
        val paymentIntent = Intent(this@CartActivity, PaymentActivity::class.java)
        paymentIntent.putExtra("payment", java.lang.Float.valueOf(selectedPremiumPlanPrice))
        paymentIntent.putExtra("subscriptionId", selectedPremiumPlanId)
        paymentIntent.putExtra("subscriptionType", selectedPremiumPlanTitle)
        paymentIntent.putExtra("formShow", "2")
        paymentIntent.putExtra("nextActivity", "CartActivity")
        startActivity(paymentIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOGIN_REGISTER) {
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    private fun initView(layoutInflater: View?) {
        textSubtotalPriceCart = layoutInflater!!.findViewById(R.id.text_subtotal_price_cart)
        textTotalPriceCart = layoutInflater.findViewById(R.id.text_total_price_cart)
        buttonProceedCart = layoutInflater.findViewById(R.id.button_proceed_cart)
        footerText = layoutInflater.findViewById(R.id.text_copy_right_cart)
        footerText!!.setText(Utils.COPYRIGHT_TXT)
    }

    companion object {
        private val selectedPremiumPlanId: String? = null
        private val selectedPremiumPlanPrice: String? = null
        private val selectedPremiumPlanTitle: String? = null
    }
}