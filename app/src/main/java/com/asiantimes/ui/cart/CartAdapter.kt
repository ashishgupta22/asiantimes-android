package com.asiantimes.ui.cart

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.ImageView
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.loadImage
import com.asiantimes.database.AppDatabase
import com.asiantimes.model.CartItem
import com.asiantimes.widget.TextViewRegular

/**
 * Created by Abhijeet-PC on 06-Jan-18.
 */
class CartAdapter(
    private val mContext: Context,
    private val mInfoList: ArrayList<CartItem>,
    activity: CartActivity
) : BaseAdapter() {
    private var itemCount = 0
    private val dbHelper: AppDatabase
    private val cartActivity: CartActivity
    override fun getCount(): Int {
        return mInfoList.size
    }

    override fun getItem(position: Int): CartItem {
        return mInfoList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        // menu type count
        return 3
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.cart_item_row, null)
            ViewHolder(convertView)
        }
        val holder = convertView.tag as ViewHolder
        val item = getItem(position)
        loadImage(holder.iv_icon, item.subscriptionImage, R.drawable.dummy)
        holder.tv_item_title_cart.text = item.subscriptionTitle
        if (!item.subscriptionSubtitle.isEmpty()) {
            holder.tv_edition_item_cart.text = "( " + item.subscriptionSubtitle + " )"
        }
        holder.tv_price_item_cart.text = "£" + item.subscriptionPrice
        holder.tv_item_count_cart_adpter.text = "" + item.subscriptionCount
        holder.ib_item_less_cart_adpter.setOnClickListener {
            itemCount = item.subscriptionCount
            if (1 < itemCount) {
                itemCount = itemCount - 1
                holder.tv_item_count_cart_adpter.text = "" + itemCount
                item.subscriptionCount = itemCount
                dbHelper.getCartManangeDao().updateEasternEyeCart(item.subscriptionID, item.subscriptionCount)
                notifyDataSetChanged()
                cartActivity.updateCart(
                    java.lang.Float.valueOf(item.subscriptionPrice),
                    itemCount + 1,
                    item.subscriptionCount
                )
            } else {
                if (mInfoList.size != 0) {
                    mInfoList.removeAt(position)
                    notifyDataSetChanged()
                    item.subscriptionCount = 0
                    dbHelper.getCartManangeDao().removeCartItem(item.subscriptionID)
                    cartActivity.updateCart(
                        java.lang.Float.valueOf(item.subscriptionPrice),
                        itemCount,
                        0
                    )
                }
            }
        }
        holder.ib_item_more_cart_adpter.setOnClickListener {
            itemCount = holder.tv_item_count_cart_adpter.text.toString().toInt()
            itemCount = itemCount + 1
            holder.tv_item_count_cart_adpter.text = "" + itemCount
            item.subscriptionCount = item.subscriptionCount + 1
            dbHelper.getCartManangeDao().updateEasternEyeCart(item.subscriptionID, item.subscriptionCount)
            cartActivity.updateCart(
                java.lang.Float.valueOf(item.subscriptionPrice),
                itemCount - 1,
                item.subscriptionCount
            )
        }
        return convertView
    }

    internal inner class ViewHolder(view: View) {
        var iv_icon: ImageView
        var tv_item_title_cart: TextViewRegular
        var tv_edition_item_cart: TextViewRegular
        var tv_item_count_cart_adpter: TextViewRegular
        var tv_price_item_cart: TextViewRegular
        var ib_item_less_cart_adpter: ImageButton
        var ib_item_more_cart_adpter: ImageButton

        init {
            tv_item_title_cart = view.findViewById(R.id.tv_item_title_cart)
            tv_edition_item_cart = view.findViewById(R.id.tv_edition_item_cart)
            tv_item_count_cart_adpter = view.findViewById(R.id.tv_item_count_cart_adpter)
            tv_price_item_cart = view.findViewById(R.id.tv_price_item_cart)
            iv_icon = view.findViewById(R.id.iv_icon_item_cart)
            ib_item_less_cart_adpter = view.findViewById(R.id.ib_item_less_cart_adpter)
            ib_item_more_cart_adpter = view.findViewById(R.id.ib_item_more_cart_adpter)
            view.tag = this
        }
    }

    init {
        dbHelper = AppDatabase.getDatabase(mContext)
        cartActivity = activity
    }
}