package com.asiantimes.ui.photo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.asiantimes.R;
import com.asiantimes.model.PhotoGallery;

import java.util.ArrayList;

public class PhotoGalleryActivity extends AppCompatActivity {

    private ArrayList<PhotoGallery> photoGalleries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery);

        ViewPager pager = findViewById(R.id.viewpager);

        PhotoGalleryAdapter photoGalleryAdapter = new PhotoGalleryAdapter(getSupportFragmentManager(), photoGalleries, pager);
        pager.setAdapter(photoGalleryAdapter);
        photoGalleryAdapter.notifyDataSetChanged();
        ArrayList<String> postImage = getIntent().getStringArrayListExtra("postImage");
        ArrayList<String> postTitle = getIntent().getStringArrayListExtra("postTitle");
        ArrayList<String> postContent = getIntent().getStringArrayListExtra("postContent");
        ArrayList<String> shareUrl = getIntent().getStringArrayListExtra("shareUrl");
        int position = 0;
        for (int i = 0; i < postImage.size(); i++) {
            PhotoGallery photoGallery = new PhotoGallery();
            photoGallery.setPostImage(postImage.get(i));
            photoGallery.setPostTitle(postTitle.get(i));
            photoGallery.setPostContent(postContent.get(i));
            photoGallery.setShareUrl(shareUrl.get(i));
            photoGalleries.add(photoGallery);
        }
        photoGalleryAdapter.notifyDataSetChanged();
        pager.setCurrentItem(getIntent().getIntExtra("position", 0));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
