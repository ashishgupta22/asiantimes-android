package com.asiantimes.ui.photo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asiantimes.R;
import com.asiantimes.model.PhotoCategory;

import java.util.ArrayList;

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.MyViewHolder> {

    private ArrayList<PhotoCategory> stringList;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text_title;
        public RecyclerView rv_photoList;

        public MyViewHolder(View view) {
            super(view);

            tv_text_title = view.findViewById(R.id.tv_text_title);
            rv_photoList = view.findViewById(R.id.rv_photoList);
        }
    }


    public PhotoListAdapter(Context context, ArrayList<PhotoCategory> photoCategories) {
        this.stringList = photoCategories;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int position) {
        try {
            PhotoCategory photoCategory = stringList.get(position);
            myViewHolder.tv_text_title.setText(photoCategory.getSubCategory().replace("amp;", ""));
            if (photoCategory.getSubCatColor() != null)
                myViewHolder.tv_text_title.setBackgroundColor(Color.parseColor(photoCategory.getSubCatColor()));
            myViewHolder.rv_photoList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            HorizontalRVAdapter horizontalAdapter = new HorizontalRVAdapter(context, photoCategory.getSubPostList());
            myViewHolder.rv_photoList.setAdapter(horizontalAdapter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }
}