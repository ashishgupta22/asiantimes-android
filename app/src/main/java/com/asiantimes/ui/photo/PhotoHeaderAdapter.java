package com.asiantimes.ui.photo;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.asiantimes.R;
import com.asiantimes.base.AppConfig;
import com.asiantimes.base.Utils;
import com.asiantimes.model.MainPostList;

import java.util.ArrayList;

/**
 * Created by inte on 06-Mar-18.
 */

public class PhotoHeaderAdapter extends RecyclerView.Adapter<PhotoHeaderAdapter.MyViewHolder> {

    private ArrayList<MainPostList> mainPostLists;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text_title;
        private ImageView im_slider;
        LinearLayout ll_first_ads_adapter;

        public MyViewHolder(View view) {
            super(view);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            int deviceWidth = displayMetrics.widthPixels;
            int deviceheight = displayMetrics.heightPixels;
            ll_first_ads_adapter = view.findViewById(R.id.ll_first_ads_adapter);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((70 * deviceWidth / 100), (35 * deviceheight / 100));
            params.leftMargin = 10;
            ll_first_ads_adapter.setLayoutParams(params);
            tv_text_title = view.findViewById(R.id.tv_title_photo_adapter);
            im_slider = view.findViewById(R.id.im_photo_slider);


        }
    }


    public PhotoHeaderAdapter(Context context, ArrayList<MainPostList> cropFeatureList) {
        this.mainPostLists = cropFeatureList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final MainPostList subPostList = mainPostLists.get(position);
        holder.tv_text_title.setText(Utils.INSTANCE.htmlToSppanned(subPostList.getPostTitle()));
        holder.tv_text_title.setVisibility(View.VISIBLE);

        AppConfig.Companion.loadImage(holder.im_slider, subPostList.getPostImage(), R.drawable.dummy);

        holder.im_slider.setOnClickListener(v -> {
            if (Utils.INSTANCE.isOnline(context)) {
                if (subPostList.getPhotoGallery().size() > 0) {
                    final ArrayList<String> postImage = new ArrayList<>();
                    final ArrayList<String> postTitle = new ArrayList<>();
                    final ArrayList<String> postContent = new ArrayList<>();
                    final ArrayList<String> shareUrl = new ArrayList<>();
                    for (int i = 0; i < subPostList.getPhotoGallery().size(); i++) {
                        postImage.add(subPostList.getPhotoGallery().get(i).getPostImage());
                        postTitle.add(subPostList.getPhotoGallery().get(i).getPostTitle());
                        postContent.add(subPostList.getPhotoGallery().get(i).getPostContent());
                        shareUrl.add(subPostList.getPhotoGallery().get(i).getShareUrl());
                    }
                    Intent intent = new Intent(context, PhotoGalleryActivity.class);
                    intent.putExtra("postID", subPostList.getPostId());
                    intent.putExtra("position", position);
                    intent.putStringArrayListExtra("postImage", postImage);
                    intent.putStringArrayListExtra("postTitle", postTitle);
                    intent.putStringArrayListExtra("postContent", postContent);
                    intent.putStringArrayListExtra("shareUrl", shareUrl);
                    context.startActivity(intent);
                }
            } else {
                Utils.INSTANCE.showToast(context.getResources().getString(R.string.no_network));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainPostLists.size();
    }
}