package com.asiantimes.ui.photo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.asiantimes.R;
import com.asiantimes.base.AppConfig;
import com.asiantimes.base.Utils;
import com.asiantimes.model.ViewMorePhoto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inte on 08-Mar-18.
 */

public class ViewMorePhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ViewMorePhoto> moviesList;
    private Context mContext;
    @SuppressLint("StaticFieldLeak")
    public static MyViewHolder myViewHolder;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title_photo_more;
        ImageView im_photo_more;
        LinearLayout ll_first_ads_adapter, ll_main_view_more;

        public MyViewHolder(View view) {
            super(view);
            tv_title_photo_more = view.findViewById(R.id.tv_title_photo_more);
            im_photo_more = view.findViewById(R.id.im_photo_more);
            ll_first_ads_adapter = view.findViewById(R.id.ll_first_ads_adapter);
            ll_main_view_more = view.findViewById(R.id.ll_main_view_more);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        FooterViewHolder(View view) {
            super(view);
            footerText = view.findViewById(R.id.footer_text);
            footerText.setText(mContext.getResources().getString(R.string.copy_right));
        }
    }


    public ViewMorePhotoAdapter(Context context, List<ViewMorePhoto> moviesList) {
        this.moviesList = moviesList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_more_photo_items, parent, false);
            myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }  else return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == moviesList.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyViewHolder) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            int deviceWidth = displayMetrics.widthPixels;
            int deviceHeight = displayMetrics.heightPixels;
            if (position < moviesList.size()) {
                myViewHolder = (MyViewHolder) viewHolder;
                final ViewMorePhoto viewMorePhoto = moviesList.get(position);
                myViewHolder.tv_title_photo_more.setText(Utils.INSTANCE.htmlToSppanned(viewMorePhoto.getTitle()));

                myViewHolder.ll_main_view_more.setVisibility(View.VISIBLE);
                if (position == 0) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((deviceWidth), (45 * deviceHeight / 100));
                    myViewHolder.ll_first_ads_adapter.setLayoutParams(params);
                } else {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((deviceWidth / 2), (deviceWidth / 2));
                    myViewHolder.ll_first_ads_adapter.setLayoutParams(params);
                }

                AppConfig.Companion.loadImage(myViewHolder.im_photo_more, viewMorePhoto.getImage(), R.drawable.dummy);

                myViewHolder.im_photo_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.INSTANCE.isOnline(mContext)) {
                            if (viewMorePhoto.getPhotoGallery().size() > 0) {
                                final ArrayList<String> postImage = new ArrayList<>();
                                final ArrayList<String> postTitle = new ArrayList<>();
                                final ArrayList<String> postContent = new ArrayList<>();
                                final ArrayList<String> shareUrl = new ArrayList<>();
                                for (int i = 0; i < viewMorePhoto.getPhotoGallery().size(); i++) {
                                    postImage.add(viewMorePhoto.getPhotoGallery().get(i).getPostImage());
                                    postTitle.add(viewMorePhoto.getPhotoGallery().get(i).getPostTitle());
                                    postContent.add(viewMorePhoto.getPhotoGallery().get(i).getPostContent());
                                    shareUrl.add(viewMorePhoto.getPhotoGallery().get(i).getShareUrl());
                                }
                                Intent intent = new Intent(mContext, PhotoGalleryActivity.class);
                                intent.putExtra("postID", viewMorePhoto.getPostId());
                                intent.putExtra("position", position);
                                intent.putStringArrayListExtra("postImage", postImage);
                                intent.putStringArrayListExtra("postTitle", postTitle);
                                intent.putStringArrayListExtra("postContent", postContent);
                                intent.putStringArrayListExtra("shareUrl", shareUrl);
                                mContext.startActivity(intent);
                            }
                        } else {
                            Utils.INSTANCE.showToast(mContext.getResources().getString(R.string.no_network));
                        }
                    }
                });

            } else {

                myViewHolder.ll_main_view_more.setVisibility(View.GONE);
                myViewHolder.tv_title_photo_more.setVisibility(View.GONE);
                myViewHolder.im_photo_more.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((deviceWidth), ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = 10;
                TextView footerView = new TextView(mContext);
                footerView.setTextColor(mContext.getResources().getColor(R.color.colorsub));
                footerView.setTextSize(16);
                footerView.setGravity(Gravity.CENTER_HORIZONTAL);
                myViewHolder.ll_first_ads_adapter.setLayoutParams(params);
                myViewHolder.ll_first_ads_adapter.addView(footerView);
            }

        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size() + 1;
    }
}