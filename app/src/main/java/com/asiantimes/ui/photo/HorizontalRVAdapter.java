package com.asiantimes.ui.photo;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.asiantimes.R;
import com.asiantimes.base.AppConfig;
import com.asiantimes.base.Utils;
import com.asiantimes.model.SubPostList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inte on 06-Mar-18.
 */

public class HorizontalRVAdapter extends RecyclerView.Adapter<HorizontalRVAdapter.MyViewHolder> {

    private List<SubPostList> stringList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text_title;
        private ImageView im_slider;
        LinearLayout ll_first_ads_adapter, ll_bottom_horizontal;

        public MyViewHolder(View view) {
            super(view);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowmanager = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
            int deviceWidth = displayMetrics.widthPixels;
            int deviceheight = displayMetrics.heightPixels;
            ll_first_ads_adapter = view.findViewById(R.id.ll_first_ads_adapter);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((47 * deviceWidth / 100), (25 * deviceheight / 100));
            params.topMargin = 8;
            params.leftMargin = 10;
            ll_first_ads_adapter.setLayoutParams(params);
            tv_text_title = view.findViewById(R.id.tv_title_photo_adapter);
            im_slider = view.findViewById(R.id.im_photo_slider);
            ll_bottom_horizontal = view.findViewById(R.id.ll_bottom_horizontal);

        }
    }


    HorizontalRVAdapter(Context context, List<SubPostList> cropFeatureList) {
        this.stringList = cropFeatureList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        if (position < 9) {
            final SubPostList subPostList = stringList.get(position);

            holder.tv_text_title.setText(Utils.INSTANCE.htmlToSppanned(subPostList.getPostTitle()));

            AppConfig.Companion.loadImage(holder.im_slider, subPostList.getPostImage(), R.drawable.dummy);

            holder.ll_bottom_horizontal.setVisibility(View.VISIBLE);
            holder.im_slider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.INSTANCE.isOnline(context)) {
                        if (stringList != null && stringList.size() > 0) {
                            final ArrayList<String> postImage = new ArrayList<>();
                            final ArrayList<String> postTitle = new ArrayList<>();
                            final ArrayList<String> postContent = new ArrayList<>();
                            final ArrayList<String> shareUrl = new ArrayList<>();
                            for (int i = 0; i < stringList.size(); i++) {
                                postImage.add(stringList.get(i).getPostImage());
                                postTitle.add(stringList.get(i).getPostTitle());
                                postContent.add(stringList.get(i).getPostContent());
                                shareUrl.add(stringList.get(i).getShareUrl());
                            }
                            Intent intent = new Intent(context, PhotoGalleryActivity.class);
                            intent.putExtra("postID", subPostList.getPostId());
                            intent.putExtra("position", position);
                            intent.putStringArrayListExtra("postImage", postImage);
                            intent.putStringArrayListExtra("postTitle", postTitle);
                            intent.putStringArrayListExtra("postContent", postContent);
                            intent.putStringArrayListExtra("shareUrl", shareUrl);
                            context.startActivity(intent);
                        }
                    } else {
                        Utils.INSTANCE.showToast(context.getResources().getString(R.string.no_network));
                    }
                }
            });
        } else {
            holder.ll_bottom_horizontal.setVisibility(View.GONE);
            holder.tv_text_title.setVisibility(View.GONE);
            holder.im_slider.setImageDrawable(context.getResources().getDrawable(R.drawable.view_more));
            holder.im_slider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.INSTANCE.isOnline(context)) {
                        Intent intent = new Intent(context, ViewMorePhotosActivity.class);
                        intent.putExtra("categoryID", stringList.get(0).getCatId());
                        intent.putExtra("categoryName", stringList.get(0).getSubCategory());
                        context.startActivity(intent);
                    } else {
                        Utils.INSTANCE.showToast(context.getResources().getString(R.string.no_network));
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }
}