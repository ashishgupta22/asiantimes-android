package com.asiantimes.ui.photo

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.hideProgress
import com.asiantimes.base.AppConfig.Companion.showProgress
import com.asiantimes.base.Utils.isOnline
import com.asiantimes.model.MainPostList
import com.asiantimes.model.PhotoCategory
import com.asiantimes.model.ResponseResult
import com.asiantimes.model.ViewMorePhoto
import java.util.*

/**
 * Created by inte on 01-Mar-18.
 */
class PhotoSectionFragment : Fragment() {
    var mLayoutManager: LinearLayoutManager? = null
    var layoutManager: GridLayoutManager? = null
    var rv_photo_section: RecyclerView? = null
    var rv_photo_header: RecyclerView? = null
    private var photoListAdapter: PhotoListAdapter? = null
    private var viewMorePhotoAdapter: ViewMorePhotoAdapter? = null
    private val photoCategoryArrayList = ArrayList<PhotoCategory>()
    private val viewMorePhotoArrayList = ArrayList<ViewMorePhoto>()
    private val mainPostLists = ArrayList<MainPostList?>()
    var ll_error_news_message: LinearLayout? = null
    var ll_gif_news_layout: RelativeLayout? = null
    var tv_retry_news: TextView? = null
    var state: Parcelable? = null
    private val viewModel = PhotosViewModel()
    fun getInstance(type: String?, categoryId: String?): PhotoSectionFragment {
        val newsListFragment = PhotoSectionFragment()
        val bundle = Bundle()
        bundle.putString("categoryId", categoryId)
        bundle.putString("type", type)
        newsListFragment.arguments = bundle
        return newsListFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        val view = inflater.inflate(R.layout.frag_photo_section, container, false)
        initializationView(view)
        return view
    }

    fun initializationView(view: View) {
        rv_photo_section = view.findViewById(R.id.rv_photo_section)
        rv_photo_header = view.findViewById(R.id.rv_photo_header)
        ll_error_news_message = view.findViewById(R.id.ll_error_news_message)
        ll_gif_news_layout = view.findViewById(R.id.ll_gif_news_layout)
        tv_retry_news = view.findViewById(R.id.tv_retry_news)
        rv_photo_section!!.setVisibility(View.GONE)
        ll_gif_news_layout!!.setVisibility(View.VISIBLE)
        ll_error_news_message!!.setVisibility(View.GONE)
        tv_retry_news!!.setVisibility(View.GONE)

//        footerText.setText(Utils.COPYRIGHT_TXT);

        //recyclerview performance
        rv_photo_section!!.setHasFixedSize(true)
        rv_photo_section!!.setItemViewCacheSize(20)
        rv_photo_section!!.setDrawingCacheEnabled(true)
        rv_photo_section!!.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH)
        setAdapter()
        tv_retry_news!!.setOnClickListener(View.OnClickListener { v: View? ->
            rv_photo_section!!.setVisibility(View.GONE)
            ll_gif_news_layout!!.setVisibility(View.VISIBLE)
            ll_error_news_message!!.setVisibility(View.GONE)
            tv_retry_news!!.setVisibility(View.GONE)
            setAdapter()
        })
    }

    private fun setAdapter() {
        if (requireArguments().getString("type") == "home") {
            if (isOnline(requireActivity())) setPhotoArticles(requireArguments().getString("categoryId"))
            mLayoutManager = LinearLayoutManager(context)
            rv_photo_section!!.layoutManager = mLayoutManager
            photoListAdapter = PhotoListAdapter(context, photoCategoryArrayList)
            rv_photo_section!!.adapter = photoListAdapter
            rv_photo_header!!.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            val photoHeaderAdapter = PhotoHeaderAdapter(context, mainPostLists)
            rv_photo_header!!.adapter = photoHeaderAdapter
        } else {
            setPhotoArticlesInMenu(requireArguments()!!.getString("categoryId"))
            val numberOfColumns = 2
            layoutManager = GridLayoutManager(activity, numberOfColumns)
            rv_photo_section!!.layoutManager = layoutManager
            layoutManager!!.spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position < viewMorePhotoArrayList.size) if (position == 0) 2 else 1 else 2
                }
            }
            viewMorePhotoAdapter = ViewMorePhotoAdapter(context, viewMorePhotoArrayList)
            rv_photo_section!!.adapter = viewMorePhotoAdapter
        }
    }

    fun setPhotoArticles(categoryID: String?) {
        try {
            showProgress(requireContext()!!)
            viewModel.getResult("41")
            viewModel.liveData.observe(viewLifecycleOwner, { responseResult ->
                hideProgress()
                setPhotoArticlesData(responseResult)
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun setPhotoArticlesInMenu(categoryID: String?) {
//        try {
//            RequestBean requestBean = new RequestBean();
//            requestBean.getPhotoVideoSectionNews(categoryID);
//            Observable<ResponseResult> resultObservable = ApplicationConfig.apiInterface.getPhotoMoreSectionNews(requestBean);
//            resultObservable.subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .unsubscribeOn(Schedulers.io())
//                    .subscribe(new Observer<ResponseResult>() {
//                        @Override
//                        public void onCompleted() {}
//
//                        @Override
//                        public void onError(Throwable e) {
//                            setPhotoVideoError();
//                        }
//
//                        @Override
//                        public void onNext(ResponseResult responseResult) {
//                            setPhotoMoreData(responseResult);
//                        }
//                    });
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    private fun setPhotoArticlesData(responseResult: ResponseResult) {
        rv_photo_section!!.visibility = View.VISIBLE
        ll_gif_news_layout!!.visibility = View.GONE
        ll_error_news_message!!.visibility = View.GONE
        tv_retry_news!!.visibility = View.GONE
        if (responseResult.getErrorCode() == 0) {
            photoCategoryArrayList.clear()
            for (i in responseResult.postList!!.indices) {
                val photoCategory = PhotoCategory(
                    responseResult.postList!![i].subCatColor,
                    responseResult.postList!![i].subCategory,
                    responseResult.postList!![i].catId,
                    responseResult.postList!![i].subPostList
                )
                photoCategoryArrayList.add(photoCategory)
            }
            mainPostLists.clear()
            if (responseResult.getMainPostList() != null && responseResult.getMainPostList()!!.size > 0) mainPostLists.addAll(
                responseResult.getMainPostList()!!
            )
            photoListAdapter!!.notifyDataSetChanged()
        }else setPhotoVideoError()
    }

    private fun setPhotoMoreData(responsePacket: ResponseResult) {
        rv_photo_section!!.visibility = View.VISIBLE
        ll_gif_news_layout!!.visibility = View.GONE
        ll_error_news_message!!.visibility = View.GONE
        tv_retry_news!!.visibility = View.GONE
        for (i in responsePacket.postList!!.indices) {
            val movie = ViewMorePhoto(
                responsePacket.postList!![i].postTitle,
                responsePacket.postList!![i].postImage, responsePacket.postList!![i].postId,
                responsePacket.postList!![i].photoGallery
            )
            viewMorePhotoArrayList.add(movie)
        }
        viewMorePhotoAdapter!!.notifyDataSetChanged()
    }

    private fun setPhotoVideoError() {
        rv_photo_section!!.visibility = View.GONE
        ll_gif_news_layout!!.visibility = View.VISIBLE
        ll_error_news_message!!.visibility = View.VISIBLE
        tv_retry_news!!.visibility = View.VISIBLE
    }

    override fun onPause() {
        super.onPause()
        state = if (requireArguments().getString("type") == "home") {
            mLayoutManager!!.onSaveInstanceState()
        } else {
            layoutManager!!.onSaveInstanceState()
        }
    }

    override fun onResume() {
        super.onResume()
        if (state != null) {
            if (requireArguments().getString("type") == "home") {
                mLayoutManager!!.onRestoreInstanceState(state)
            } else {
                layoutManager!!.onRestoreInstanceState(state)
            }
        }
    }
}