package com.asiantimes.ui.photo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.ResponseResult

class PhotosViewModel : ViewModel() {
    var liveData = MutableLiveData<ResponseResult>()

    fun getResult(searchText: String) {
        liveData = ApiRepository.viewMorePhotos(searchText)
    }
}