package com.asiantimes.ui.photo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.asiantimes.R;
import com.asiantimes.base.AppConfig;
import com.asiantimes.model.PhotoGallery;

import java.util.ArrayList;

public class PhotoGalleryFragment extends Fragment {

    private ArrayList<PhotoGallery> photoGalleries = new ArrayList<>();
    int position;
    ViewPager viewPager;
    PhotoGalleryMoreAdapter photoGalleryMoreAdapter;

    ImageView iv_share, iv_close, iv_photo;
    TextView tv_title;
    TextView tv_content;
    TextView tv_curr_count;
    TextView tv_total_count;

    public PhotoGalleryFragment newInstance(ArrayList<PhotoGallery> photoGalleries, int position, ViewPager pagers) {
        PhotoGalleryFragment photoGalleryFragment = new PhotoGalleryFragment();
        this.photoGalleries = photoGalleries;
        this.position = position;
        this.viewPager = pagers;
        photoGalleryMoreAdapter = new PhotoGalleryMoreAdapter();
        setPhotoGalleryList(this.photoGalleries);
        return photoGalleryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_gallery, container, false);


        iv_share = rootView.findViewById(R.id.iv_share_photo_adapter);
        iv_close = rootView.findViewById(R.id.iv_close_photo_adapter);
        iv_photo = rootView.findViewById(R.id.iv_photo_photo_adapter);
        tv_title = rootView.findViewById(R.id.tv_title_photo_adapter);
        tv_content = rootView.findViewById(R.id.tv_content_photo_adapter);
        tv_curr_count = rootView.findViewById(R.id.tv_curr_count_photo_adapter);
        tv_total_count = rootView.findViewById(R.id.tv_total_count_photo_adapter);

        setValues();
        return rootView;
    }

    public void setPhotoGalleryList(ArrayList<PhotoGallery> photoGalleryList) {
        photoGalleryMoreAdapter.setData(photoGalleryList, getActivity());
        photoGalleryMoreAdapter.notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    public void setValues() {
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        AppConfig.Companion.loadImage(iv_photo, photoGalleries.get(position).getPostImage(), R.drawable.dummy);

        tv_title.setText(photoGalleries.get(position).getPostTitle());
        tv_content.setText(photoGalleries.get(position).getPostContent());
        if (photoGalleries.size() == 1 && photoGalleries.get(position).getPostTitle().equalsIgnoreCase("")) {
            iv_share.setVisibility(View.GONE);
        } else {
            iv_share.setVisibility(View.VISIBLE);
            tv_curr_count.setText("" + (position + 1) + "/");
            tv_total_count.setText("" + photoGalleries.size());
        }
        if (photoGalleries.get(position).getShareUrl() != null && !photoGalleries.get(position).getShareUrl().isEmpty()) {
            iv_share.setVisibility(View.VISIBLE);
        } else iv_share.setVisibility(View.GONE);

        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, tv_title.getText().toString());
                share.putExtra(Intent.EXTRA_TEXT, photoGalleries.get(position).getShareUrl());
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
    }

}
