package com.asiantimes.ui.photo;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;


import com.asiantimes.R;
import com.asiantimes.base.BaseActivity;
import com.asiantimes.model.ViewMorePhoto;

import java.util.ArrayList;
import java.util.Objects;

public class ViewMorePhotosActivity extends BaseActivity {
    private RecyclerView rvViewPhoto;
    private ArrayList<ViewMorePhoto> viewMorePhotoArrayList;
    private ViewMorePhotoAdapter viewMorePhotoAdapter;
    private LinearLayout ll_error_news_message;
    private RelativeLayout ll_gif_news_layout;
    private TextView tv_retry_news;
    private PhotosViewModel viewModel = new PhotosViewModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_more_photos);

        String title = "Photos";
        TextView textViewTitle = findViewById(R.id.textViewTitle);
        textViewTitle.setText(title);
        ImageView imageViewBack = findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = new PhotoSectionFragment();
        Bundle bundle = new Bundle();
        bundle.putString("categoryId", getIntent().getExtras().getString("categoryID"));
        bundle.putString("type", "home");
        newFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_container, newFragment);
        fragmentTransaction.commit();

//        viewMorePhotoArrayList = new ArrayList<>();
//        rvViewPhoto = findViewById(R.id.rvViewPhoto);
//        ll_error_news_message = findViewById(R.id.ll_error_news_message);
//        ll_gif_news_layout = findViewById(R.id.ll_gif_news_layout);
//        tv_retry_news = findViewById(R.id.tv_retry_news);
//        rvViewPhoto.setVisibility(View.GONE);
//        ll_gif_news_layout.setVisibility(View.VISIBLE);
//        ll_error_news_message.setVisibility(View.GONE);
//        tv_retry_news.setVisibility(View.GONE);
//
//        tv_retry_news.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rvViewPhoto.setVisibility(View.GONE);
//                ll_gif_news_layout.setVisibility(View.VISIBLE);
//                ll_error_news_message.setVisibility(View.GONE);
//                tv_retry_news.setVisibility(View.GONE);
//                setPhotoArticles(Objects.requireNonNull(getIntent().getExtras()).getString("categoryID"));
//            }
//        });
//
//        int numberOfColumns = 2;
//        GridLayoutManager layoutManager = new GridLayoutManager(this, numberOfColumns);
//        rvViewPhoto.setLayoutManager(layoutManager);
//        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                if (position < viewMorePhotoArrayList.size())
//                    return position == 0 ? 2 : 1;
//                else
//                    return 2;
//            }
//        });
//
//        viewMorePhotoAdapter = new ViewMorePhotoAdapter(this, viewMorePhotoArrayList);
//        rvViewPhoto.setAdapter(viewMorePhotoAdapter);
//        setPhotoArticles(Objects.requireNonNull(getIntent().getExtras()).getString("categoryID"));
    }

    @Override
    protected void onResume() {
        super.onResume();
            setTitle(Objects.requireNonNull(getIntent().getExtras()).getString("categoryName"));
    }

    public void setPhotoArticles(String categoryID) {
//        try {
//            AppConfig.Companion.showProgress(this);
//            viewModel.getResult(categoryID);
//            viewModel.getLiveData().observe(this, new androidx.lifecycle.Observer<ResponseResult>() {
//                @Override
//                public void onChanged(ResponseResultNew responseResultNew) {
//                    AppConfig.Companion.hideProgress();
//                    setPhotoMoreData(responseResultNew);
//                }
//            });
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    private void setPhotoMoreData() {
//        rvViewPhoto.setVisibility(View.VISIBLE);
//        ll_gif_news_layout.setVisibility(View.GONE);
//        ll_error_news_message.setVisibility(View.GONE);
//        tv_retry_news.setVisibility(View.GONE);
//        for (int i = 0; i < responsePacket.getPostList().size(); i++) {
//            ViewMorePhoto movie = new ViewMorePhoto(responsePacket.getPostList().get(i).getPostTitle(),
//                    responsePacket.getPostList().get(i).getPostImage(), responsePacket.getPostList().get(i).getPostId(),
//                    responsePacket.getPostList().get(i).getPhotoGallery());
//            viewMorePhotoArrayList.add(movie);
//        }
//        viewMorePhotoAdapter.notifyDataSetChanged();
    }

    private void setPhotoMoreError() {
        rvViewPhoto.setVisibility(View.GONE);
        ll_gif_news_layout.setVisibility(View.VISIBLE);
        ll_error_news_message.setVisibility(View.VISIBLE);
        tv_retry_news.setVisibility(View.VISIBLE);
    }
}
