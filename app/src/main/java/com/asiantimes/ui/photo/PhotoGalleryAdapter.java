package com.asiantimes.ui.photo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.asiantimes.model.PhotoGallery;

import java.util.ArrayList;

/**
 * Created by Abhijeet-PC on 12-Feb-18.
 */

public class PhotoGalleryAdapter extends FragmentPagerAdapter {

    private ArrayList<PhotoGallery> photoGalleries;
    private ViewPager viewPager;

    public PhotoGalleryAdapter(FragmentManager fm, ArrayList<PhotoGallery> photoGalleries, ViewPager pager) {
        super(fm);
        this.photoGalleries = photoGalleries;
        this.viewPager = pager;
    }

    @Override
    public Fragment getItem(int position) {
        PhotoGalleryFragment photoGalleryFragment = new PhotoGalleryFragment();
        photoGalleryFragment.newInstance(photoGalleries, position, this.viewPager);
        return photoGalleryFragment;
    }

    @Override
    public int getCount() {
        return photoGalleries.size();
    }
}
