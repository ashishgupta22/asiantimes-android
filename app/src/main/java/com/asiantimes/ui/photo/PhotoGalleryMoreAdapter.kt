package com.asiantimes.ui.photo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.Utils.htmlToSppanned
import com.asiantimes.model.PhotoGallery
import com.asiantimes.ui.photo.PhotoGalleryMoreAdapter.MyViewHolder
import java.util.*

class PhotoGalleryMoreAdapter : RecyclerView.Adapter<MyViewHolder>() {
    private var photoGalleriesMore: ArrayList<PhotoGallery>? = null
    var mContext: Context? = null

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_details_title_adatper: TextView

        init {
            tv_details_title_adatper = view.findViewById(R.id.tv_details_title_adatper)
//            tv_details_title_adatper.setTextSize((14 * 1.0));
        }
    }

    fun setData(data: ArrayList<PhotoGallery>, context: Context?) {
        if (photoGalleriesMore !== data) {
            photoGalleriesMore = data
            notifyDataSetChanged()
            mContext = context
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val mContext = parent.context
        val itemView = LayoutInflater.from(mContext)
            .inflate(R.layout.cat_details_list_adapter, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val photoGallery = photoGalleriesMore!![position]
        holder.tv_details_title_adatper.text = htmlToSppanned(photoGallery.postTitle)
    }

    override fun getItemCount(): Int {
        return photoGalleriesMore!!.size
    }
}