package com.asiantimes.ui.sign_up

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel

class SignUpViewModel : ViewModel() {
    var liveData = MutableLiveData<CommonModel>()

    fun register(name: String?, email: String?, number: String?, password: String?) {
        liveData = ApiRepository.register(name,email,number,password)
    }
}