package com.asiantimes.ui.sign_up

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import androidx.core.util.PatternsCompat
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils
import com.asiantimes.databinding.ActivitySignUpActivityBinding
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInActivity

class SignUpActivityActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivitySignUpActivityBinding? = null
    private val viewModel: SignUpViewModel by viewModels()
    private var name: String? = null
    private var email: String? = null
    private var number: String? = null
    private var password: String? = null
    private var confirmPassword: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_activity)
        bindingView!!.cardViewLogIn.setOnClickListener(this)
        bindingView!!.buttonCreateAccount.setOnClickListener(this)

        bindingView!!.edtPass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                bindingView!!.textInputLayoutPassword.isEndIconVisible = true;
            }

            override fun afterTextChanged(s: Editable) {}
        })
        bindingView!!.edtConpass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = true;
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.cardViewLogIn -> {
                startActivity(Intent(this, LogInActivity::class.java))
                finish()
            }
            bindingView!!.buttonCreateAccount -> {
                name = bindingView!!.textInputLayoutName.editText!!.text.toString().trim()
                email = bindingView!!.textInputLayoutEmail.editText!!.text.toString().trim()
                number = bindingView!!.textInputLayoutNumber.editText!!.text.toString().trim()
                password = bindingView!!.textInputLayoutPassword.editText!!.text.toString().trim()
                confirmPassword =
                    bindingView!!.textInputLayoutConfirmPassword.editText!!.text.toString().trim()
                when {
                    name!!.isEmpty() -> {
                        bindingView!!.textInputLayoutName.editText!!.error =
                            getString(R.string.name_error)
                        bindingView!!.textInputLayoutName.editText!!.requestFocus()
                    }
                    email!!.isEmpty() -> {
                        bindingView!!.textInputLayoutEmail.editText!!.error =
                            getString(R.string.email_error)
                        bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                    }
                    !PatternsCompat.EMAIL_ADDRESS.matcher(email.toString()).matches() -> {
                        bindingView!!.textInputLayoutEmail.editText!!.error =
                            getString(R.string.email_valid)
                        bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                    }
                    password!!.isEmpty() -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()
                    }
                    password!!.length < 6 -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error_length)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()
                    }
                    confirmPassword!!.isEmpty() -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_error)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }
                    confirmPassword!!.length < 6 -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_error_length)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }
                    password != confirmPassword -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_match_error)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }

                    isOnline() -> {
                        AppConfig.showProgress(this)
                        registerAPI()
                        registerObserver()
                    }
                    else -> {
                        Utils.showToast(getString(R.string.internet_error))
                    }
                }
            }
        }
    }

    private fun registerAPI() {
        viewModel.register(name, email, number, password)
    }

    private fun registerObserver() {
        viewModel.liveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            if (responseData?.errorCode == 0) {
                AppPreferences.setLoginStatus(true)
//                AppDatabase.getDatabase(this).userDataDao().delete()
//                AppDatabase.getDatabase(this).userDataDao().insert(responseData.userDetail!!)
                AppPreferences.setUserId(responseData.userDetail!!.userId!!)
                AppPreferences.setUserDeatils(responseData.userDetail!!)
                val intent = Intent(this, DashboardActivity::class.java)
                intent.putExtra("call_from", "main")
                startActivity(intent)
                finish()
            } else {
                AppConfig.hideProgress()
                Utils.showToast(responseData.msg)
            }
        })
    }
}