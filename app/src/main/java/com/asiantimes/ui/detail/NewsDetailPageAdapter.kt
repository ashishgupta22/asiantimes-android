package com.asiantimes.ui.detail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.asiantimes.model.GetTopStoriesModel

class NewsDetailPageAdapter(private val activytType:String,
    manager: FragmentManager?, private val dataList: List<GetTopStoriesModel.PostList>?
) : FragmentStatePagerAdapter(manager!!) {

    override fun getItem(position: Int): Fragment {
        return NewsDetailFragment(activytType,dataList!!,position)
    }

    override fun getCount(): Int {
        return dataList!!.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

}