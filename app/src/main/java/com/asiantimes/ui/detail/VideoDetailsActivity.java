package com.asiantimes.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.asiantimes.R;
import com.asiantimes.base.Utils;
import com.asiantimes.player.ExoVideoPlayer;
import com.asiantimes.widget.YouTubeConfig;

import java.util.Objects;

public class VideoDetailsActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, YouTubePlayer.PlaybackEventListener {

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private ProgressBar videoProgress;
    private PlayerView nativePlayer;
    private ExoVideoPlayer videoPlayer;
    private long currentTime = 0;
    private String videoUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);

        if (getIntent().getExtras().getString("youtubeKey").contains("youtu")) {
            youTubeView = findViewById(R.id.youtube_view);
            youTubeView.initialize(YouTubeConfig.YOUTUBE_API_KEY, this);
        }else {
            videoUrl = getIntent().getExtras().getString("youtubeKey");
            setVideoExoPlay();
        }
        ImageView iv_categoryBack = findViewById(R.id.imageViewBack);
        TextView     textViewTitle = findViewById(R.id.textViewTitle);
        textViewTitle.setText("Video");
        iv_categoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        AppCompatTextView tv_title_video_details = findViewById(R.id.tv_title_video_details);
        AppCompatTextView tv_discription_video_details = findViewById(R.id.tv_discription_video_details);
        tv_title_video_details.setText(Objects.requireNonNull(getIntent().getExtras()).getString("title"));
        tv_discription_video_details.setText(getIntent().getExtras().getString("details"));
    }

    private void setVideoExoPlay() {
        videoProgress = findViewById(R.id.videoProgress);
        videoProgress.setVisibility(View.VISIBLE);
        nativePlayer = findViewById(R.id.nativePlayer);
        nativePlayer.setVisibility(View.VISIBLE);
        videoPlayer = new ExoVideoPlayer(this, nativePlayer, true, true);
        videoPlayer.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        videoPlayer.setPlayerControl(new ExoVideoPlayer.PlayerControl() {
            @Override
            public void start() {
//                if(videoProgress.getVisibility() == View.VISIBLE){
//                    videoPlayer.seekTo(currentTime);
//                    findViewById(R.id.image).setVisibility(View.GONE);
//                }
                videoProgress.setVisibility(View.GONE);
            }

            @Override
            public void complete() {
                videoPlayer.seekTo(0);
                videoPlayer.pauseVideo();
            }

            @Override
            public void buffering() {

            }

            @Override
            public void pause() {

            }

            @Override
            public void resume() {

            }

            @Override
            public void videoSize(int height, int width) {

            }
        });

        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.INSTANCE.isValidString(videoUrl)) {
                    videoPlayer.setPlayer(videoUrl);
                }
            }
        },1000);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            if (!Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).getString("youtubeKey")).equalsIgnoreCase("No")) {
                player.cueVideo(getIntent().getExtras().getString("youtubeKey").replace("https://youtu.be/", "")); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
                setAutoPlayYouTubeVideo(player);
            }
//            if (AppPreferences.getAllowVideo().equalsIgnoreCase(getResources().getString(R.string.wifi_only))) {
//                ConnectivityManager connectivitymanager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//                NetworkInfo[] networkInfo = new NetworkInfo[0];
//                if (connectivitymanager != null)
//                    networkInfo = connectivitymanager.getAllNetworkInfo();
//
//                for (NetworkInfo netInfo : networkInfo) {
//                    if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {
//                        if (netInfo.isConnected())
//                            setAutoPlayYouTubeVideo(player);
//                    }
//                }
//            } else if (AppPreferences.getInstance().getAllowVideo().equals(getResources().getString(R.string.wifi_mobile))) {
//                ConnectivityManager connectivitymanager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//                NetworkInfo[] networkInfo = new NetworkInfo[0];
//                if (connectivitymanager != null)
//                    networkInfo = connectivitymanager.getAllNetworkInfo();
//
//                for (NetworkInfo netInfo : networkInfo) {
//                    if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {
//                        if (netInfo.isConnected())
//                            setAutoPlayYouTubeVideo(player);
//                    }
//
//                    if (netInfo.getTypeName().equalsIgnoreCase("MOBILE")) {
//                        if (netInfo.isConnected())
//                            setAutoPlayYouTubeVideo(player);
//                    }
//                }
//            }
        }
    }

    private void setAutoPlayYouTubeVideo(final YouTubePlayer player) {
        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    player.play();
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError())
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Retry initialization if user performed a recovery action
        if (requestCode == RECOVERY_REQUEST)
            getYouTubePlayerProvider().initialize(YouTubeConfig.YOUTUBE_API_KEY, this);
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }


    @Override
    public void onPlaying() {
    }

    @Override
    public void onPaused() {
    }

    @Override
    public void onStopped() {
    }

    @Override
    public void onBuffering(boolean b) {
    }

    @Override
    public void onSeekTo(int i) {
    }

}
