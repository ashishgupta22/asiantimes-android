package com.asiantimes.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel

class NewsDetailsViewModel : ViewModel() {
    var commonLiveData = MutableLiveData<CommonModel>()

    fun saveBookmark(postId: String, isRemoved: Int) {
        commonLiveData = ApiRepository.saveBookmark(postId, isRemoved)
    }
}