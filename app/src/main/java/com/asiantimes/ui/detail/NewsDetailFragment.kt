package com.asiantimes.ui.detail

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.asiantimes.R
import com.asiantimes.base.*
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.FragmentNewsDetailBinding
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.ui.comment.CommentsActivity
import com.asiantimes.ui.login.LogInActivity


class NewsDetailFragment(
    private var activytType: String = "",
    private val newsDetailData: List<GetTopStoriesModel.PostList>,
    private val position: Int
) : BaseFragment(),
    View.OnClickListener {
    private var bindingView: FragmentNewsDetailBinding? = null
    private val viewModel: NewsDetailsViewModel by viewModels()
    private var appDatabase: AppDatabase? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_news_detail, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        appDatabase = AppDatabase.getDatabase(requireContext())
        bindingView!!.imageViewBack.setOnClickListener(this)
        bindingView!!.imageViewComment.setOnClickListener(this)
        bindingView!!.imageViewBookmark.setOnClickListener(this)
        bindingView!!.imageViewShare.setOnClickListener(this)
        bindingView!!.newsDetailData = newsDetailData[position]
        activytType = requireActivity().intent.getStringExtra(Constants.NEWS_DETAIL_TYPE).toString()
        bindBookMark()
        bindPremium()
        bindDetails()
        setFontSetText(AppPreferences.getFONT_SCALE()!!.toFloat())
    }

    fun setFontSetText(fontValue: Float) {
        bindingView!!.textViewNewsTitle.setTextSize(20 * fontValue)
        bindingView!!.textViewNewsCat.setTextSize(16 * fontValue)
    }

    private fun bindDetails() {
        val commentCouont: Int? = newsDetailData[position]!!.numberComments
        bindingView!!.tvNumberComments.setText(commentCouont.toString())
        for (i in newsDetailData[position].postDetail!!.indices) {
            when (newsDetailData[position]!!.postDetail?.get(i)!!.types) {
                "text" -> {
                    val contentParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    if (newsDetailData[position]!!.postDetail?.get(i)!!.types!!.contains("<iframe")) {
                        val htmlAds: String =
                            newsDetailData[position]!!.postDetail?.get(i)!!.key.toString()
                        val webAds = activity?.let { WebView(it) }
                        webAds!!.layoutParams = contentParams
                        webAds!!.settings.allowFileAccess = true
                        webAds!!.settings.javaScriptEnabled = true
                        webAds!!.settings.useWideViewPort = true
                        webAds!!.settings.loadWithOverviewMode = true
                        webAds!!.settings.builtInZoomControls = false
                        webAds!!.loadDataWithBaseURL(null, htmlAds, "text/html", "utf-8", null)
                        webAds!!.visibility = View.VISIBLE
                        bindingView!!.llContentDetailsFragment!!.addView(webAds)
                    } else {
                        setDetailData(newsDetailData[position]!!.postDetail?.get(i), contentParams)
                    }
                }
                "image" -> {
                    val contentParamsImage = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    contentParamsImage.topMargin = 10
                    contentParamsImage.gravity = Gravity.CENTER_HORIZONTAL
                    val imageView = ImageView(activity)
                    imageView.layoutParams = contentParamsImage
                    imageView.adjustViewBounds = true
                    imageView.scaleType = ImageView.ScaleType.FIT_XY
                    AppConfig.loadImage(
                        imageView,
                        newsDetailData[position]!!.postDetail?.get(i)!!.key,
                        R.drawable.dummy
                    )
                    bindingView!!.llContentDetailsFragment!!.addView(imageView)
                }
            }
        }
    }

    private fun setDetailData(
        newsDetailData: GetTopStoriesModel.PostDetail?,
        contentParams: LinearLayout.LayoutParams
    ) {
        val textView = TextView(activity)
        val font = Typeface.createFromAsset(requireActivity().getAssets(), "fonts/montserrat_regular.ttf")
        if (font != null){
            textView.setTypeface(font)
        }
        textView.setTextSize(18 * AppPreferences.getFONT_SCALE()!!.toFloat())
        textView.layoutParams = contentParams
        textView.setText(Utils.htmlToSppanned(newsDetailData!!.key))
        bindingView!!.llContentDetailsFragment.addView(textView)
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.imageViewBack -> {
                (context as Activity).finish()
            }
            bindingView!!.imageViewComment -> {
                val intent = Intent(activity, CommentsActivity::class.java)
                intent.putExtra("postId", newsDetailData[position].postId)
                startActivity(intent)
            }
            bindingView!!.imageViewBookmark -> {
                newsDetailData[position].postId?.let {
                    newsDetailData[position].isBookmark?.let { it1 ->
                        saveBookmarkAPI(
                            it,
                            it1
                        )
                    }
                }
                saveBookmarkObserver(newsDetailData[position])
            }
            bindingView!!.imageViewShare -> {
                shareUrl(newsDetailData[position].postUrl, newsDetailData[position].postTitle)
            }
        }
    }

    private fun saveBookmarkAPI(postId: String, isBookmark: Boolean) {
        if (checkLoginStatus()) {
            val isRemoved = if (!isBookmark!!) 0 else 1
            AppConfig.showProgress(requireContext())
            viewModel.saveBookmark(postId, isRemoved)
        }
    }

    private fun saveBookmarkObserver(data: GetTopStoriesModel.PostList) {
        viewModel.commonLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            newsDetailData[position].isBookmark = data.isBookmark
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                data.isBookmark = !data.isBookmark!!
                bindBookMark()
                if (requireActivity().intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.CAT_LIST_NEWS_HOME_TYPE) {
                    requireActivity().intent!!.getStringExtra(Constants.NEWS_POST_CATID)
                        ?.let { it1 ->
                            appDatabase!!.getCateNewsList().udpateWithPostCatID(
                                it1,
                                Constants.CAT_LIST_NEWS_HOME_TYPE, newsDetailData
                            )
                        }

                } else
                    appDatabase!!.getCateNewsList().inesrtUpdate(activytType!!, newsDetailData)
            }
        })
    }

    private fun bindBookMark() {
        if (newsDetailData[position]!!.isBookmark!!)
            bindingView!!.imageViewBookmark.setImageResource(R.drawable.ic_bookmark_fill_22)
        else
            bindingView!!.imageViewBookmark.setImageResource(R.drawable.ic_bookmark_22)
    }

    private fun bindPremium() {
        if (newsDetailData[position]!!.isPremium!!) {
            bindingView!!.tvIspremiumDetails.visibility = View.VISIBLE
        } else
            bindingView!!.tvIspremiumDetails.visibility = View.GONE
    }

    private fun checkLoginStatus(): Boolean {
        if (!AppPreferences.getLoginStatus()) {
            startActivity(Intent(activity, LogInActivity::class.java))
            activity?.finish()
            return false
        } else
            return true
    }

}