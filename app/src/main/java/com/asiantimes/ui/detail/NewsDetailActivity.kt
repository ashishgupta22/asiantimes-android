package com.asiantimes.ui.detail

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Constants
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivityNewsDetailBinding
import com.asiantimes.model.GetTopStoriesModel


class NewsDetailActivity : BaseActivity() {
    private var bindingView: ActivityNewsDetailBinding? = null
    private var dataList: List<GetTopStoriesModel.PostList>? = ArrayList()
    private var appDatabase: AppDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_news_detail)
        appDatabase = AppDatabase.getDatabase(this)
        val position = intent.getIntExtra(Constants.POSITION, 0)
        if (intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.BRAKING_NEWS_CATEGOTY_TYPE) {
            dataList = AppDatabase.getDatabase(this).getCateNewsList()
                .getTopStoriesList_new(Constants.BRAKING_NEWS_CATEGOTY_TYPE).postList!!
//            dataList = AppDatabase.getDatabase(this).getCateNewsList().getTopStoriesList()
        } else if (intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.SAVE_CATEGOTY_TYPE) {
            appDatabase!!.getCateNewsList()
                .getTopStoriesList_new(Constants.SAVE_CATEGOTY_TYPE).postList?.let {
                    dataList = it
                }
        } else if (intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.CAT_LIST_NEWS_CATEGOTY_TYPE || intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.SEARCH_NEWS_CATEGOTY_TYPE ) {
            appDatabase!!.getCateNewsList()
                .getTopStoriesList_new(intent.getStringExtra(Constants.NEWS_DETAIL_TYPE).toString()).postList?.let {
                    dataList = it

                }
        }else if (intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.TRADING_NEWS_CATEGOTY_TYPE) {
            appDatabase!!.getCateNewsList()
                .getTopStoriesList_new(Constants.TRADING_NEWS_CATEGOTY_TYPE).postList?.let {
                    dataList = it

                }
        } else if (intent.getStringExtra(Constants.NEWS_DETAIL_TYPE) == Constants.CAT_LIST_NEWS_HOME_TYPE) {
            dataList =
                intent!!.getStringExtra(Constants.NEWS_POST_CATID)?.let {
                    appDatabase!!.getCateNewsList().getTopStoriesList_newAllPST_ID(
                        Constants.CAT_LIST_NEWS_HOME_TYPE,
                        it
                    ).get(0).postList
                }
        }
        val newsDetailPagerAdapter = NewsDetailPageAdapter(
            intent.getStringExtra("post_title").toString(),
            supportFragmentManager,
            dataList
        )
        bindingView!!.viewPagerNewsDetail.adapter = newsDetailPagerAdapter
        bindingView!!.viewPagerNewsDetail.currentItem = position

    }


}