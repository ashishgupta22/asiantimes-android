package com.asiantimes.ui.forgot_password

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel

class ForgotPasswordViewModel : ViewModel() {
    var liveData = MutableLiveData<CommonModel>()

    fun forgotPassword(email: String?) {
        liveData = ApiRepository.forgotPassword(email)
    }

    fun updatePassword(userId: String?, otp: String?, password: String?) {
        liveData = ApiRepository.updatePassword(userId,otp,password)
    }
}