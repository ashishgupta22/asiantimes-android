package com.asiantimes.ui.forgot_password

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import androidx.core.util.PatternsCompat
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils
import com.asiantimes.databinding.ActivityForgotPasswordBinding
import kotlinx.android.synthetic.main.toolbar_forms_screen.*

class ForgotPasswordActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityForgotPasswordBinding? = null
    private val viewModel: ForgotPasswordViewModel by viewModels()
    private var email: String? = null
    private var otp: String? = null
    private var password: String? = null
    private var confirmPassword: String? = null
    private var userId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        text_view_title_toolbar.text = getString(R.string.forgot_password)
        image_view_back_toolbar.setOnClickListener(this)
        bindingView!!.textViewSendOtp.setOnClickListener(this)
        bindingView!!.buttonContinue.setOnClickListener(this)

        bindingView!!.edtPass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                bindingView!!.textInputLayoutPassword.isEndIconVisible = true;
            }

            override fun afterTextChanged(s: Editable) {}
        })
        bindingView!!.edtConpass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = true;
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onClick(view: View?) {
        when (view) {
            image_view_back_toolbar -> {
                finish()
            }
            bindingView!!.textViewSendOtp -> {
                email = bindingView!!.textInputLayoutEmail.editText!!.text.toString().trim()
                if (email!!.isEmpty()) {
                    bindingView!!.textInputLayoutEmail.editText!!.error =
                        getString(R.string.email_error)
                    bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                } else if (!PatternsCompat.EMAIL_ADDRESS.matcher(email.toString()).matches()) {
                    bindingView!!.textInputLayoutEmail.editText!!.error =
                        getString(R.string.email_valid)
                    bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                } else if (!isOnline()) {
                    Utils.showToast(getString(R.string.internet_error))
                } else {
                    forgotPasswordAPI()
                    forgotPasswordObserver()
                }
            }

            bindingView!!.buttonContinue -> {
                otp = bindingView!!.textInputLayoutOtp.editText!!.text.toString().trim()
                password = bindingView!!.textInputLayoutPassword.editText!!.text.toString().trim()
                confirmPassword =
                    bindingView!!.textInputLayoutConfirmPassword.editText!!.text.toString().trim()
                when {
                    otp!!.isEmpty() -> {
                        bindingView!!.textInputLayoutOtp.editText!!.error =
                            getString(R.string.otp_error)
                        bindingView!!.textInputLayoutOtp.editText!!.requestFocus()
                    }
                    password!!.isEmpty() -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()
                    }
                    confirmPassword!!.isEmpty() -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_error)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }
                    password!!.length < 6 -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error_length)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()
                    }
                    confirmPassword!!.length < 6 -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_error_length)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }
                    password != confirmPassword -> {
                        bindingView!!.textInputLayoutConfirmPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.error =
                            getString(R.string.confirm_password_match_error)
                        bindingView!!.textInputLayoutConfirmPassword.editText!!.requestFocus()
                    }

                    !isOnline() -> {
                        Utils.showToast(getString(R.string.internet_error))
                    }
                    else -> {
                        updatePasswordAPI()
                        updatePasswordObserver()
                    }
                }
            }

        }

    }

    private fun forgotPasswordAPI() {
        AppConfig.showProgress(this@ForgotPasswordActivity)
        viewModel.forgotPassword(email)
    }

    private fun updatePasswordAPI() {
        AppConfig.showProgress(this@ForgotPasswordActivity)
        viewModel.updatePassword(userId, otp, password)
    }

    private fun forgotPasswordObserver() {
        viewModel.liveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                userId = responseData.userId
                bindingView!!.layoutOtpPassword.visibility = View.VISIBLE
            } else
                bindingView!!.layoutOtpPassword.visibility = View.GONE
        })
    }

    private fun updatePasswordObserver() {
        viewModel.liveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                finish()
            }
        })
    }
}