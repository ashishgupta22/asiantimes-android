package com.asiantimes.ui.profile

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivityProfileBinding
import com.asiantimes.ui.login.LogInActivity
import kotlinx.android.synthetic.main.toolbar_forms_screen.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


class ProfileActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityProfileBinding? = null
    private var appDatabase: AppDatabase? = null
    var imageBody: MultipartBody.Part? = null
    private val viewModel: ProfileViewModel by viewModels()
    private val MY_CAMERA_REQUEST_CODE = 100

    var CAMERA_PERMISSION = Manifest.permission.CAMERA
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        text_view_title_toolbar.text = getString(R.string.profile)
        image_view_back_toolbar.setOnClickListener(this)
        bindingView!!.buttonContinue.setOnClickListener(this)
        bindingView!!.imgEdit.setOnClickListener(this)
        appDatabase = AppDatabase.getDatabase(this)
        bindingView!!.userData = AppPreferences.getUserDetails()
    }

    override fun onClick(view: View?) {
        when (view) {
            image_view_back_toolbar -> {
                finish()
            }

            bindingView!!.buttonContinue -> {
                val name = bindingView!!.textInputLayoutName.editText!!.text.toString().trim()
                val number =
                    bindingView!!.textInputLayoutPhoneNumber.editText!!.text.toString().trim()
                val email =
                    bindingView!!.textInputLayoutEmail.editText!!.text.toString().trim()
                when {
                    name.isEmpty() -> {
                        bindingView!!.textInputLayoutName.editText!!.error =
                            getString(R.string.name_error)
                        bindingView!!.textInputLayoutName.editText!!.requestFocus()
                    }
                    email.isEmpty() -> {
                        bindingView!!.textInputLayoutEmail.editText!!.error =
                            getString(R.string.email_error)
                        bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                    }
                    number.isEmpty() -> {
                        bindingView!!.textInputLayoutPhoneNumber.editText!!.error =
                            getString(R.string.email_error)
                        bindingView!!.textInputLayoutPhoneNumber.editText!!.requestFocus()
                    }

                    !isOnline() -> {
                        Utils.showToast(getString(R.string.internet_error))
                    }
                    else -> {
                        // api call

                        if (checkLoginStatus()) {
                            updateProfileAPI()
                        }
                    }
                }
            }
            bindingView!!.imgEdit -> {
                try {

                    val dialog = Dialog(this@ProfileActivity)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setCancelable(true)
                    dialog.setContentView(R.layout.layout_custom_choose_img)
                    val txtCamera = dialog.findViewById(R.id.txtCamera) as TextView
                    val yesBtn = dialog.findViewById(R.id.txtGallary) as TextView
                    val imgClose = dialog.findViewById(R.id.imgClose) as ImageView
                    yesBtn.setOnClickListener {
                        dialog.dismiss()
                        var pickPhoto = Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        );
                        startActivityForResult(pickPhoto, 1);

                    }
                    txtCamera.setOnClickListener {
                        dialog.dismiss()

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED
                            ) {
                                requestPermissions(
                                    arrayOf<String>(Manifest.permission.CAMERA),
                                    MY_CAMERA_REQUEST_CODE
                                );
                            } else {
                                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                startActivityForResult(
                                    takePicture,
                                    0
                                ) //ze // ro can be replaced with any action code (called requestCode)
                            }
                        } else {
                            val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                            startActivityForResult(
                                takePicture,
                                0
                            ) //ze // ro can be replaced with any action code (called requestCode)
                        }
                    }
                    dialog.show()
                    imgClose.setOnClickListener({
                        dialog.dismiss()
                    })
//                    PickImageDialog.build(PickSetup())
//                        .setOnPickResult { r ->
//                            if (r.getPickType() === EPickType.GALLERY) {
////                                    selectedImage = getRealPathFromURI_API19(UserProfileActivity.this, r.getUri());
//                                val fileName = Calendar.getInstance().time.toString()
//                                val file: File = persistImage(r.getBitmap(), fileName)
//                                val requestBody =
//                                    RequestBody.create(parse.parse("multipart/form-data"), file)
//                                imageBody =
//                                    MultipartBody.Part.createFormData(
//                                        "image",
//                                        file.name,
//                                        requestBody
//                                    )
//                            } else {
//                                selectedImage = r.getPath()
//                                val file: File = File(selectedImage)
//                                val requestBody =
//                                    RequestBody.create(parse.parse("multipart/form-data"), file)
//                                imageBody =
//                                    MultipartBody.Part.createFormData(
//                                        "image",
//                                        file.name,
//                                        requestBody
//                                    )
//                            }
//                            bindingView!!.imageViewProfile.setImageBitmap(r.getBitmap())
//                        }
//                        .setOnPickCancel {}.show(this)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                }
            }


        }

    }

    fun updateProfileAPI() {
        AppConfig.showProgress(this@ProfileActivity)
        if (imageBody == null) {
            val requestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "")
            imageBody = MultipartBody.Part.createFormData("image", "", requestBody)
        }
        val userId: RequestBody =
            RequestBody.create(
                contentType = "text/plain".toMediaTypeOrNull(),
                AppPreferences.getUserId().toString()
            )
        val name: RequestBody =
            RequestBody.create(
                contentType = "text/plain".toMediaTypeOrNull(),
                bindingView!!.textInputLayoutName.editText!!.text.toString().trim()
            )
        val number: RequestBody =
            RequestBody.create(
                contentType = "text/plain".toMediaTypeOrNull(),
                bindingView!!.textInputLayoutPhoneNumber.editText!!.text.toString().trim()
            )
        AppConfig.showProgress(this@ProfileActivity)
        viewModel.updateProfile(userId, name, number, imageBody)
        observLiveData()
    }

    private fun observLiveData() {
        viewModel.profileLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            if (responseData?.errorCode == 0) {
                bindingView!!.userData!!.userName = responseData.userDetail!!.userName
                bindingView!!.userData!!.mobileNumber = responseData.userDetail!!.mobileNumber
                bindingView!!.userData!!.image = responseData.userDetail!!.image
//                AppDatabase.getDatabase(this).userDataDao().delete()
//                AppDatabase.getDatabase(this).userDataDao().insert(bindingView!!.userData!!)
                AppPreferences.setUserDeatils(bindingView!!.userData!!)
                Utils.showToast(responseData.msg)
            } else Utils.showToast(responseData.msg)
        })
    }

    private fun checkLoginStatus(): Boolean {
        if (!AppPreferences.getLoginStatus()) {
            startActivity(Intent(this, LogInActivity::class.java))
            this?.finish()
            return false
        } else
            return true
    }

    private fun persistImage(bitmap: Bitmap, name: String): File? {
        val filesDir = filesDir
        val imageFile = File(filesDir, name)
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
            os.flush()
            os.close()
        } catch (e: java.lang.Exception) {
            Log.e("Exception", "Error writing bitmap", e)
        }
        return imageFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            0 -> if (resultCode == RESULT_OK) {
                val selectedImage: Uri? = data!!.data
                var bitmap = data.getExtras()!!.get("data") as Bitmap
                val bytes = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
                bindingView!!.imageViewProfile.setImageBitmap(bitmap)

//                                    selectedImage = getRealPathFromURI_API19(UserProfileActivity.this, r.getUri());
                val fileName = Calendar.getInstance().time.toString()
                try {
                    val file: File? = persistImage(bitmap, fileName + ".jpg")
                    val requestBody = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        file!!
                    )
                    imageBody = MultipartBody.Part.createFormData("image", file.name, requestBody)

                } catch (ex: Exception) {

                }
            }
            1 -> if (resultCode == RESULT_OK) {
                val selectedImage: Uri? = data!!.data
                bindingView!!.imageViewProfile.setImageURI(selectedImage)
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)

                val fileName = Calendar.getInstance().time.toString()
                try {
                    val file: File? = persistImage(bitmap, fileName + ".jpg")
                    val requestBody = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        file!!
                    )
                    imageBody = MultipartBody.Part.createFormData("image", file.name, requestBody)

                } catch (ex: Exception) {

                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_REQUEST_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(
                    takePicture,
                    0
                )
            } else {

                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_LONG).show();

            }
        }
    }
}