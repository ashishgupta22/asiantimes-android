package com.asiantimes.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ProfileViewModel : ViewModel() {
    var profileLiveData = MutableLiveData<CommonModel>()

    fun updateProfile(
        userId: RequestBody?,
        userName: RequestBody?,
        mobile: RequestBody?,
        img: MultipartBody.Part?
    ) {
        profileLiveData = ApiRepository.updateProfile(userId, userName, mobile, img)
    }
}