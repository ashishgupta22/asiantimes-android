package com.asiantimes.ui.category_news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterCategoriesNewsChildBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class CategoriesNewsListAdapter(private val onClickListener: OnClickPositionViewListener) : RecyclerView.Adapter<CategoriesNewsListAdapter.ViewHolder>() {
    private var catNewsList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_categories_news_child, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return catNewsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(catNewsList[position],onClickListener,position)
    }

    class ViewHolder(private val bindingView: AdapterCategoriesNewsChildBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            catNewsData: GetTopStoriesModel.PostList,
            onClickListener: OnClickPositionViewListener,
            position: Int
        ) {
            bindingView.setVariable(BR.catNewsData,catNewsData)
            bindingView.setVariable(BR.onClickListener,onClickListener)
            bindingView.setVariable(BR.position,position)
        }
    }

    fun setData(catNewsList: ArrayList<GetTopStoriesModel.PostList>) {
        this.catNewsList = catNewsList
        notifyDataSetChanged()
    }

    fun setData(catNewsList: ArrayList<GetTopStoriesModel.PostList>,position: Int) {
        this.catNewsList = catNewsList
        notifyItemChanged(position)
    }
}