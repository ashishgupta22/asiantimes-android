package com.asiantimes.ui.category_news

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.*
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivityCategoryNewsListBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.ui.detail.NewsDetailActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.search.SearchActivity

class CategoryNewsListActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityCategoryNewsListBinding? = null
    private var categoriesNewsListAdapter: CategoriesNewsListAdapter? = null
    private var catNewsList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()
    private val viewModel: CategoriesNewsViewModel by viewModels()
    private var catId: String? = null
    private var isPagination: Boolean? = null
    private var page = 1
    private var pastVisibleItems = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var appDatabase: AppDatabase? = null
    private var isFirst: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_category_news_list)
        appDatabase = AppDatabase.getDatabase(this)
        catId = intent.getStringExtra(Constants.CATEGORY_ID)
        bindingView!!.catName = intent.getStringExtra(Constants.CATEGORY_NAME)
        bindingView!!.imageViewBack.setOnClickListener(this)
        bindingView!!.imageViewSearch.setOnClickListener(this)
        setAdapter()
        isPagination = false
        page = 1
        if (isOnline()) {
            bindingView!!.imageViewNoInternet.visibility = View.GONE
            getPostListByCateIDAPI()
            getPostListByCateIDObserver()
        } else {
            bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
//            if (appDatabase!!.getCateNewsList()
//                    .getTopStoriesList_new(Constants.CAT_LIST_NEWS_CATEGOTY_TYPE).postList!!.size > 0
//            ) {
//                catNewsList.clear()
//                catNewsList.addAll(appDatabase!!.getCateNewsList()
//                    .getTopStoriesList_new(Constants.CAT_LIST_NEWS_CATEGOTY_TYPE).postList!!)
//                categoriesNewsListAdapter!!.setData(catNewsList)
//            }
        }
        val layoutManager = LinearLayoutManager(this)
        bindingView!!.recycleViewNewsList.layoutManager = layoutManager
        bindingView!!.recycleViewNewsList.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        page += 1
                        isPagination = true
                        if (isOnline()) {
                            getPostListByCateIDAPI()
                            getPostListByCateIDObserver()
                        } else {
                            Utils.showToast(getString(R.string.internet_error))
                        }
                    }
                }
            }
        })
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.imageViewBack -> {
                finish()
            }
            bindingView!!.imageViewSearch -> {
                val intent = Intent(this, SearchActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        appDatabase = AppDatabase.getDatabase(this)
        if (isFirst) {
            if (categoriesNewsListAdapter != null) {
                appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.CAT_LIST_NEWS_CATEGOTY_TYPE).postList?.let {
                        catNewsList = it as ArrayList<GetTopStoriesModel.PostList>

                    }
                if (!catNewsList.isNullOrEmpty()) {
                    categoriesNewsListAdapter!!.setData(catNewsList)
                }
            }
        } else isFirst = true
    }

    private fun setAdapter() {
        categoriesNewsListAdapter = CategoriesNewsListAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                val data = `object` as GetTopStoriesModel.PostList
                if (view == 0) {
                    val intent =
                        Intent(this@CategoryNewsListActivity, NewsDetailActivity::class.java)
                    intent.putExtra(
                        Constants.NEWS_DETAIL_TYPE,
                        Constants.CAT_LIST_NEWS_CATEGOTY_TYPE
                    )
                    intent.putExtra(Constants.POSITION, position)
                    startActivity(intent)
                } else if (view == 1) {
                    //bookmark
                    if (AppPreferences.getLoginStatus()) {
                        val isRemoved = if (!data.isBookmark!!) 0 else 1
                        saveBookmarkAPI(data.postId!!, isRemoved)
                        saveBookmarkObserver(data)
                    } else {
                        startActivity(
                            Intent(
                                this@CategoryNewsListActivity,
                                LogInActivity::class.java
                            )
                        )
                        finish()
                    }
                } else if (view == 2) {
                    //share
                    shareUrl(data.postUrl, data.postTitle)
                }

            }
        })
        bindingView!!.recycleViewNewsList.adapter = categoriesNewsListAdapter
    }

    private fun getPostListByCateIDAPI() {
        if (isPagination!!) {
            bindingView!!.progressBar.visibility = View.VISIBLE
            bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
        } else {
            bindingView!!.recycleViewNewsList.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.startShimmerAnimation()
        }
        viewModel.getPostListByCateID(catId!!, page)
    }

    private fun getPostListByCateIDObserver() {
        viewModel.liveData.observe(this, {
            bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
            bindingView!!.progressBar.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            val responseData = it
            if (responseData?.errorCode == 0) {
                if (responseData.PostLista.isNullOrEmpty()) {
                    if (!isPagination!!) {
                        bindingView!!.recycleViewNewsList.visibility = View.GONE
                        bindingView!!.imageViewNoData.visibility = View.VISIBLE
                    }
                } else {
                    bindingView!!.imageViewNoData.visibility = View.GONE
                    bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
                    if (!isPagination!!) {
                        catNewsList.clear()
                        catNewsList.addAll(responseData.PostLista)
                        categoriesNewsListAdapter!!.setData(catNewsList)
                        appDatabase!!.getCateNewsList().inesrtUpdate(
                            Constants.CAT_LIST_NEWS_CATEGOTY_TYPE!!,
                            catNewsList!!
                        )
                    } else {
                        val position = catNewsList.size - 1
                        catNewsList.addAll(responseData.PostLista)
                        categoriesNewsListAdapter!!.setData(catNewsList, position)
                        appDatabase!!.getCateNewsList().inesrtUpdate(
                            Constants.CAT_LIST_NEWS_CATEGOTY_TYPE!!,
                            catNewsList!!
                        )
                    }
                }

            } else {
                if (!isPagination!!) {
                    bindingView!!.recycleViewNewsList.visibility = View.GONE
                    bindingView!!.imageViewNoData.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun setHomeNewBookmarkDatabase() {
        if (!catNewsList.isNullOrEmpty())
            appDatabase!!.getCateNewsList()
                .inesrtUpdate(Constants.CAT_LIST_NEWS_CATEGOTY_TYPE!!, catNewsList!!)
    }

    private fun saveBookmarkAPI(postId: String, isRemoved: Int) {
        AppConfig.showProgress(this)
        viewModel.saveBookmark(postId, isRemoved)
    }

    private fun saveBookmarkObserver(data: GetTopStoriesModel.PostList) {
        viewModel.commonLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                data.isBookmark = !data.isBookmark!!
                categoriesNewsListAdapter!!.setData(catNewsList)
                appDatabase!!.getCateNewsList()
                    .inesrtUpdateNewWithPostID(data)
                setHomeNewBookmarkDatabase()
            }
        })
    }
}