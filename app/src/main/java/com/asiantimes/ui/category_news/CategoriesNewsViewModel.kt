package com.asiantimes.ui.category_news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import com.asiantimes.model.GetTopStoriesModel

class CategoriesNewsViewModel : ViewModel() {
    var liveData = MutableLiveData<GetTopStoriesModel>()
    var commonLiveData = MutableLiveData<CommonModel>()

    fun getPostListByCateID(catId: String, page: Int) {
        liveData = ApiRepository.getPostListByCateID(catId,page)
    }

    fun saveBookmark(postId: String,isRemoved: Int) {
        commonLiveData = ApiRepository.saveBookmark(postId,isRemoved)
    }

}