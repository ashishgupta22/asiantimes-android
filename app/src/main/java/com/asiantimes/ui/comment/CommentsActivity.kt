package com.asiantimes.ui.comment

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.hideProgress
import com.asiantimes.base.AppConfig.Companion.showProgress
import com.asiantimes.base.AppPreferences.getLoginStatus
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils.showToast
import com.asiantimes.model.InsertCommentsModel
import com.asiantimes.model.ResponseResult
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.widget.GifImageView
import java.util.*

class CommentsActivity : BaseActivity() {
    private var rv_replyList: RecyclerView? = null
    private var commentsArrayList: ArrayList<InsertCommentsModel>? = null
    private var myCommentsArrayList: ArrayList<InsertCommentsModel>? = null
    private var commentAdapter: CommentAdapter? = null
    private var tv_no_data_comments: TextView? = null
    private var ll_gif_comments: RelativeLayout? = null
    private var gif_loader_comments: GifImageView? = null
    private var ll_error_message_comments: LinearLayout? = null
    private var nextPage: String? = "1"
    private var edt_reply_comment: EditText? = null
    private var title: TextView? = null
    var scrollView: NestedScrollView? = null
    var tabLayout: TabLayout? = null
    var ll_sendMsg: LinearLayout? = null
    private val viewModel = CommentViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            window.statusBarColor = resources.getColor(R.color.colorButton)
        }
        //        Objects.requireNonNull(getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.actionbar);
        setContentView(R.layout.activity_comments)
        title = findViewById(R.id.action_bar_title)
        val iv_menuBack = findViewById<ImageView>(R.id.iv_menuBack)
        tabLayout = findViewById(R.id.tabs) //tab layout
        title!!.setText(getString(R.string.commnets_title))
        iv_menuBack.setOnClickListener { v: View? -> onBackPressed() }

        /*Adding tabs in tab layout*/tabLayout!!.addTab(
            tabLayout!!.newTab().setText("All Comments"),
            true
        )
        tabLayout!!.addTab(tabLayout!!.newTab().setText("My Commented"))
        edt_reply_comment = findViewById(R.id.edt_reply_comment)
        ll_gif_comments = findViewById(R.id.ll_gif_comments)
        gif_loader_comments = findViewById(R.id.gif_loader_comments)
        ll_error_message_comments = findViewById(R.id.ll_error_message_comments)
        val ll_reply_comment = findViewById<LinearLayout>(R.id.ll_reply_comment)
        scrollView = findViewById(R.id.scrollView)

//        gif_loader_comments.setGifImageResource(R.drawable.app_logo);
        commentsArrayList = ArrayList()
        myCommentsArrayList = ArrayList()
        tv_no_data_comments = findViewById(R.id.tv_no_data_comments)
        val tv_retry_comments = findViewById<TextView>(R.id.tv_retry_comments)
        rv_replyList = findViewById(R.id.rv_replyList)
        ll_gif_comments!!.setVisibility(View.VISIBLE)
        gif_loader_comments!!.setVisibility(View.GONE)
        ll_error_message_comments!!.setVisibility(View.GONE)
        rv_replyList!!.setVisibility(View.GONE)
        val layoutParams = ll_gif_comments!!.getLayoutParams() as LinearLayout.LayoutParams
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL
        ll_gif_comments!!.setLayoutParams(layoutParams)
        ll_sendMsg = findViewById(R.id.ll_sendMsg)

        /*refreshing data for first selected tab*/commentAdapter = CommentAdapter(
            this@CommentsActivity,
            commentsArrayList,
            tabLayout!!.getSelectedTabPosition()
        )
        rv_replyList!!.setAdapter(commentAdapter)
        //   setCommentList(Objects.requireNonNull(getIntent().getExtras()).getString("postId"), nextPage);
        commentAdapter!!.notifyDataSetChanged()
        tv_retry_comments.setOnClickListener { v: View? ->
            ll_gif_comments!!.setVisibility(View.VISIBLE)
            gif_loader_comments!!.setVisibility(View.GONE)
            ll_error_message_comments!!.setVisibility(View.GONE)
            rv_replyList!!.setVisibility(View.GONE)
            scrollView!!.setVisibility(View.GONE)
            setCommentList(Objects.requireNonNull(intent.extras)!!.getString("postId"), nextPage)
            setCommentListObserve()
        }
        if (isOnline()) {
            setCommentList(Objects.requireNonNull(intent.extras)!!.getString("postId"), nextPage)
            setCommentListObserve()
        } else {
            ll_gif_comments!!.setVisibility(View.VISIBLE)
            gif_loader_comments!!.setVisibility(View.GONE)
            ll_error_message_comments!!.setVisibility(View.VISIBLE)
            tv_retry_comments.visibility = View.VISIBLE
            rv_replyList!!.setVisibility(View.GONE)
            scrollView!!.setVisibility(View.GONE)
        }
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        rv_replyList!!.setLayoutManager(mLayoutManager)
        rv_replyList!!.setItemAnimator(DefaultItemAnimator())
        ll_reply_comment.setOnClickListener { v: View? ->
            if (TextUtils.isEmpty(edt_reply_comment!!.getText().toString().trim { it <= ' ' })) Snackbar.make(
                findViewById(android.R.id.content),
                resources.getString(R.string.commentError),
                Snackbar.LENGTH_LONG
            ).show() else {
                if (!getLoginStatus()) startActivity(
                    Intent(
                        this@CommentsActivity,
                        LogInActivity::class.java
                    )
                ) else setCommentReply(intent.extras!!.getString("postId"), edt_reply_comment!!.getText().toString()
                )
            }
        }
        tabLayout!!.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val pos = tab.position
                when (pos) {
                    0 -> {
                        ll_sendMsg!!.setVisibility(View.VISIBLE)
                        if (commentsArrayList!!.size > 0) {
                            rv_replyList!!.setVisibility(View.VISIBLE)
                            scrollView!!.setVisibility(View.VISIBLE)
                            tv_no_data_comments!!.setVisibility(View.GONE)
                            commentAdapter =
                                CommentAdapter(this@CommentsActivity, commentsArrayList, pos)
                            rv_replyList!!.setAdapter(commentAdapter)
                            commentAdapter!!.notifyDataSetChanged()
                        } else {
                            rv_replyList!!.setVisibility(View.GONE)
                            scrollView!!.setVisibility(View.GONE)
                            tv_no_data_comments!!.setVisibility(View.VISIBLE)
                        }
                    }
                    1 -> {
                        ll_sendMsg!!.setVisibility(View.GONE)
//                        setCommentList(Objects.requireNonNull(intent.extras)!!.getString("postId"), nextPage)
//                        setCommentListObserve()
                        if (myCommentsArrayList!!.size > 0) {
                            rv_replyList!!.setVisibility(View.VISIBLE)
                            scrollView!!.setVisibility(View.VISIBLE)
                            tv_no_data_comments!!.setVisibility(View.GONE)
                            commentAdapter =
                                CommentAdapter(this@CommentsActivity, myCommentsArrayList, pos)
                            rv_replyList!!.setAdapter(commentAdapter)
                            commentAdapter!!.notifyDataSetChanged()
                        } else {
                            rv_replyList!!.setVisibility(View.GONE)
                            scrollView!!.setVisibility(View.GONE)
                            tv_no_data_comments!!.setVisibility(View.VISIBLE)
                        }
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun setCommentListObserve() {
        viewModel.liveDataComment.observe(this, { responseResult ->
            hideProgress()
            if (responseResult.getErrorCode() == 0) {
                setCommentsListData(responseResult)
            } else if (responseResult.getErrorCode() == 1) {
                setCommentsListData(responseResult)
            } else {
                ll_gif_comments!!.visibility = View.VISIBLE
                gif_loader_comments!!.visibility = View.GONE
                ll_error_message_comments!!.visibility = View.VISIBLE
                rv_replyList!!.visibility = View.GONE
                scrollView!!.visibility = View.GONE
            }
        })
    }

    fun setCommentList(postId: String?, nextPage: String?) {
        try {
            showProgress(this)
            commentsArrayList!!.clear()
            viewModel.getAllComments(postId!!, nextPage!!)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setCommentsListData(responseResult: ResponseResult) {
        commentsArrayList!!.clear()
        myCommentsArrayList!!.clear()
        ll_gif_comments!!.visibility = View.GONE
        gif_loader_comments!!.visibility = View.GONE
        ll_error_message_comments!!.visibility = View.GONE
        rv_replyList!!.visibility = View.VISIBLE
        scrollView!!.visibility = View.VISIBLE

        if (responseResult.errorCode == 0) {
            if (responseResult.postList!!.size > 0) {
//                nextPage = responseResult.getNextPage()
                //                Log.v("response", responseResult.getNextPage() + "-----" + responseResult.getPostList().size());
                for (i in responseResult.postList!!.indices) {
                    val comments = InsertCommentsModel(
                        responseResult.postList!![i].commentAuthor,
                        responseResult.postList!![i].commentDiscrption,
                        responseResult.postList!![i].commentDate,
                        responseResult.postList!![i].postcommentId,
                        responseResult.postList!![i].commentId,
                        responseResult.postList!![i].commentTitle,
                        responseResult.postList!![i].getshareUrl()
                    )
                    commentsArrayList!!.add(comments)
                }
                commentAdapter!!.notifyDataSetChanged()
            } else {
                rv_replyList!!.visibility = View.GONE
                scrollView!!.visibility = View.GONE
                tv_no_data_comments!!.visibility = View.VISIBLE
            }
            if (responseResult.inActivepostList!!.size > 0) {
//                nextPage = responseResult.getNextPage()
                //                Log.v("response", responseResult.getNextPage() + "-----" + responseResult.getInActivepostList().size());
                for (i in responseResult.inActivepostList!!.indices) {
                    val comments = InsertCommentsModel(
                        responseResult.inActivepostList!![i].commentAuthor,
                        responseResult.inActivepostList!![i].commentDiscrption,
                        responseResult.inActivepostList!![i].commentDate,
                        responseResult.inActivepostList!![i].postcommentId,
                        responseResult.inActivepostList!![i].commentId,
                        responseResult.inActivepostList!![i].commentTitle,
                        responseResult.inActivepostList!![i].getshareUrl()
                    )
                    myCommentsArrayList!!.add(comments)
                }
                commentAdapter!!.notifyDataSetChanged()
            }
        } else {
            rv_replyList!!.visibility = View.GONE
            scrollView!!.visibility = View.GONE
            tv_no_data_comments!!.visibility = View.VISIBLE
        }
    }

    fun setCommentReply(postId: String?, comment: String?) {
        try {
            showProgress(this)
            viewModel.getCommentReply(postId!!, comment!!)
            viewModel.liveDataInsert.observe(this, { responseResult ->
                hideProgress()
                if (responseResult.getErrorCode() == 0) {
                    setCommentList(Objects.requireNonNull(intent.extras)!!.getString("postId"), nextPage)
                    setCommentListObserve()
                    showToast(resources.getString(R.string.commentSuccessMessage))
                    edt_reply_comment!!.setText("")
                } else {
                    ll_gif_comments!!.visibility = View.VISIBLE
                    gif_loader_comments!!.visibility = View.GONE
                    ll_error_message_comments!!.visibility = View.VISIBLE
                    rv_replyList!!.visibility = View.GONE
                    scrollView!!.visibility = View.GONE
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        title!!.text = getString(R.string.commnets_title)
    }
}