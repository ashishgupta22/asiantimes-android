package com.asiantimes.ui.comment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.asiantimes.R;
import com.asiantimes.model.InsertCommentsModel;

import java.util.ArrayList;

/**
 * Created by inte on 05-Feb-18.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {

    private ArrayList<InsertCommentsModel> stringList;
    private Context context;
    private int tab_position;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_feature_title;
        TextView tv_comments_comments;
        TextView tv_time_comments;
        TextView tv_reply_comments;
        TextView tv_title_comments;
        ImageView iv_feature;
        LinearLayout ll_comments_rootview;
        View view_divider;

        public MyViewHolder(View view) {
            super(view);

            tv_feature_title = view.findViewById(R.id.tv_name_comments);
            tv_comments_comments = view.findViewById(R.id.tv_comments_comments);
            tv_time_comments = view.findViewById(R.id.tv_time_comments);
            tv_reply_comments = view.findViewById(R.id.tv_reply_comments);
            tv_title_comments = view.findViewById(R.id.tv_title_comments);

            iv_feature = view.findViewById(R.id.iv_share_comments);
            ll_comments_rootview = view.findViewById(R.id.ll_comments_rootview);
            view_divider = view.findViewById(R.id.view_divider);

        }
    }


    public CommentAdapter(Context context, ArrayList<InsertCommentsModel> cropFeatureList, int tab_position) {
        this.stringList = cropFeatureList;
        this.context = context;
        this.tab_position = tab_position;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        switch (tab_position){
            case 0:
                holder.ll_comments_rootview.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                holder.view_divider.setBackgroundColor(context.getResources().getColor(R.color.colorBackground));
                break;
            case 1:
                holder.ll_comments_rootview.setBackgroundColor(context.getResources().getColor(R.color.colorBackground));
                holder.view_divider.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                break;
        }

        final InsertCommentsModel comments = stringList.get(position);
        holder.tv_feature_title.setText(comments.getName());
        holder.tv_comments_comments.setText(comments.getComments());
        holder.tv_title_comments.setText(comments.getCommentTitle());
        holder.tv_time_comments.setText(comments.getTime());
        holder.iv_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, comments.getCommentTitle());
                share.putExtra(Intent.EXTRA_TEXT, comments.getShareUrl());
                context.startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
        if(comments.getShareUrl().equalsIgnoreCase("false")){
            holder.iv_feature.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }
}