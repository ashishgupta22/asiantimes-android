package com.asiantimes.ui.comment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.ResponseResult

class CommentViewModel : ViewModel() {

    var liveDataComment = MutableLiveData<ResponseResult>()
    var liveDataInsert = MutableLiveData<ResponseResult>()


    fun getAllComments(postId: String,nextPage:String) {
        liveDataComment = ApiRepository.commentList(postId,nextPage)
    }

    fun getCommentReply(postId: String, comment: String) {
        liveDataInsert = ApiRepository.commentReply(postId, comment)
    }
}