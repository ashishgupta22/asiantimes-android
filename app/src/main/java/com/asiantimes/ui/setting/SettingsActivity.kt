package com.asiantimes.ui.setting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.asiantimes.R
import com.asiantimes.base.AppPreferences.getAllowVideo
import com.asiantimes.base.AppPreferences.getBreakingNewsNotifications
import com.asiantimes.base.AppPreferences.getFONT_SCALE
import com.asiantimes.base.AppPreferences.getImageOffLine
import com.asiantimes.base.AppPreferences.getIsBackground
import com.asiantimes.base.AppPreferences.getIsMobileNetwork
import com.asiantimes.base.AppPreferences.setAllowVideo
import com.asiantimes.base.AppPreferences.setBreakingNewsNotifications
import com.asiantimes.base.AppPreferences.setFONT_SCALE
import com.asiantimes.base.AppPreferences.setIsBackground
import com.asiantimes.base.AppPreferences.setIsImageOffLine
import com.asiantimes.base.AppPreferences.setIsMobileNetwork
import com.asiantimes.base.BaseActivity
import com.asiantimes.interfaces.OnClickButtonListener
import com.asiantimes.ui.search.SearchActivity
import com.warkiz.widget.IndicatorSeekBar

class SettingsActivity : BaseActivity(), View.OnClickListener, OnClickButtonListener {
    private var rb_breaking_notification: Switch? = null
    private var rb_background_sync: Switch? = null
    private var rb_image_sync: Switch? = null
    private var rb_mobile_network_sync: Switch? = null
    var tv_textSize: TextView? = null
    var tv_copy_right_settings: TextView? = null
    var tv_waer_message_settings: TextView? = null
    var tv_wear_settings: TextView? = null
    var tv_widget_message_settings: TextView? = null
    var tv_widget_settings: TextView? = null
    var tv_text_size_huge_settings: TextView? = null
    var tv_text_size_large_settings: TextView? = null
    var tv_text_size_normal_settings: TextView? = null
    var tv_text_size_small_settings: TextView? = null
    var tv_text_size_settings: TextView? = null
    var tv_breaking_news_message_settings: TextView? = null
    var tv_breaking_news_settings: TextView? = null
    var tv_sync_mobile_settings: TextView? = null
    var tv_sync_mobile_message_settings: TextView? = null
    var tv_sync_images_message_settings: TextView? = null
    var tv_sync_images_settings: TextView? = null
    var tv_background_settings: TextView? = null
    var action_bar_title: TextView? = null
    var tv_background_message_settings: TextView? = null
    private var rb_never: RadioButton? = null
    private var rb_onwifi: RadioButton? = null
    private var rb_wifi_mobile: RadioButton? = null
    private var iv_huge_settings: ImageView? = null
    private var iv_large_settings: ImageView? = null
    private var iv_normal_settings: ImageView? = null
    private var iv_small_settings: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        initialisationView()
    }

    fun initialisationView() {
        val image_view_back = findViewById<ImageView>(R.id.image_view_back)
        val image_view_search = findViewById<ImageView>(R.id.image_view_search)
        image_view_back.setOnClickListener { v: View? -> finish() }
        image_view_search.setOnClickListener { v: View? ->
            startActivity(Intent(this, SearchActivity::class.java))
            finish()
        }
        val ll_image_sync = findViewById<LinearLayout>(R.id.ll_image_sync)
        val ll_text_size = findViewById<LinearLayout>(R.id.ll_text_size)
        val ll_breaking_news = findViewById<LinearLayout>(R.id.ll_breaking_news)
        val ll_background_sync = findViewById<LinearLayout>(R.id.ll_background_sync)
        val ll_small_settings = findViewById<LinearLayout>(R.id.ll_small_settings)
        val ll_normal_settings = findViewById<LinearLayout>(R.id.ll_normal_settings)
        val ll_large_settings = findViewById<LinearLayout>(R.id.ll_large_settings)
        val ll_huge_settings = findViewById<LinearLayout>(R.id.ll_huge_settings)
        val ll_mobile_network_sync = findViewById<LinearLayout>(R.id.ll_mobile_network_sync)
        rb_never = findViewById(R.id.rb_never)
        rb_onwifi = findViewById(R.id.rb_onwifi)
        rb_wifi_mobile = findViewById(R.id.rb_wifi_mobile)
        iv_huge_settings = findViewById(R.id.iv_huge_settings)
        iv_large_settings = findViewById(R.id.iv_large_settings)
        iv_normal_settings = findViewById(R.id.iv_normal_settings)
        iv_small_settings = findViewById(R.id.iv_small_settings)
        rb_mobile_network_sync = findViewById(R.id.rb_mobile_network_sync)
        rb_breaking_notification = findViewById(R.id.rb_breaking_notification)
        rb_background_sync = findViewById(R.id.rb_background_sync)
        rb_image_sync = findViewById(R.id.rb_image_sync)
        tv_textSize = findViewById(R.id.tv_textSize)
        tv_copy_right_settings = findViewById(R.id.tv_copy_right_settings)
        tv_copy_right_settings!!.setText(getString(R.string.copy_right))
        tv_waer_message_settings = findViewById(R.id.tv_waer_message_settings)
        tv_wear_settings = findViewById(R.id.tv_wear_settings)
        tv_widget_message_settings = findViewById(R.id.tv_widget_message_settings)
        tv_widget_settings = findViewById(R.id.tv_widget_settings)
        tv_text_size_huge_settings = findViewById(R.id.tv_text_size_huge_settings)
        tv_text_size_large_settings = findViewById(R.id.tv_text_size_large_settings)
        tv_text_size_normal_settings = findViewById(R.id.tv_text_size_normal_settings)
        tv_text_size_small_settings = findViewById(R.id.tv_text_size_small_settings)
        tv_text_size_settings = findViewById(R.id.tv_text_size_settings)
        tv_breaking_news_message_settings = findViewById(R.id.tv_breaking_news_message_settings)
        tv_breaking_news_settings = findViewById(R.id.tv_breaking_news_settings)
        tv_sync_mobile_message_settings = findViewById(R.id.tv_sync_mobile_message_settings)
        tv_sync_mobile_settings = findViewById(R.id.tv_sync_mobile_settings)
        tv_sync_images_message_settings = findViewById(R.id.tv_sync_images_message_settings)
        tv_sync_images_settings = findViewById(R.id.tv_sync_images_settings)
        tv_background_message_settings = findViewById(R.id.tv_background_message_settings)
        tv_background_settings = findViewById(R.id.tv_background_settings)

        ll_mobile_network_sync.setOnClickListener(this)
        ll_image_sync.setOnClickListener(this)
        ll_text_size.setOnClickListener(this)
        ll_breaking_news.setOnClickListener(this)
        ll_background_sync.setOnClickListener(this)
        rb_breaking_notification!!.setOnClickListener(this)
        rb_background_sync!!.setOnClickListener(this)
        rb_image_sync!!.setOnClickListener(this)
        rb_mobile_network_sync!!.setOnClickListener(this)
        rb_never!!.setOnClickListener(View.OnClickListener { v: View? ->
            rb_never!!.setChecked(true)
            rb_onwifi!!.setChecked(false)
            rb_wifi_mobile!!.setChecked(false)
            setAllowVideo(resources.getString(R.string.never))
        })
        rb_onwifi!!.setOnClickListener(View.OnClickListener { v: View? ->
            rb_never!!.setChecked(false)
            rb_onwifi!!.setChecked(true)
            rb_wifi_mobile!!.setChecked(false)
            setAllowVideo(resources.getString(R.string.wifi_only))
        })
        rb_wifi_mobile!!.setOnClickListener(View.OnClickListener { v: View? ->
            rb_never!!.setChecked(false)
            rb_onwifi!!.setChecked(false)
            rb_wifi_mobile!!.setChecked(true)
            setAllowVideo(resources.getString(R.string.wifi_mobile))
        })

//        rb_compact.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (!isChecked) {
//                AppPreferences.INSTANCE.setCompactLayout("f");
//                rb_compact.setChecked(false);
//            } else {
//                AppPreferences.getInstance().setCompactLayout("t");
//                rb_compact.setChecked(true);
//                rb_usecarousel.setChecked(false);
//                AppPreferences.getInstance().setIsCarouselsLayout("f");
//            }
//        });
//
//        rb_usecarousel.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (!isChecked) {
//                rb_usecarousel.setChecked(false);
//                AppPreferences.getInstance().setIsCarouselsLayout("f");
//            } else {
//                AppPreferences.getInstance().setIsCarouselsLayout("t");
//                rb_usecarousel.setChecked(true);
//                AppPreferences.getInstance().setCompactLayout("f");
//                rb_compact.setChecked(false);
//            }
//        });
        rb_breaking_notification!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (!isChecked) {
//                    setBreakingNewsNotification("f");
                setBreakingNewsNotifications("f")
                rb_breaking_notification!!.setChecked(false)
            } else {
//                    setBreakingNewsNotification("t");
                setBreakingNewsNotifications("t")
                rb_breaking_notification!!.setChecked(true)
            }
        })
        rb_background_sync!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (!isChecked) {
                setIsBackground("f")
                rb_background_sync!!.setChecked(false)
            } else {
                setIsBackground("t")
                rb_background_sync!!.setChecked(true)
            }
        })
        rb_image_sync!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (!isChecked) {
                setIsImageOffLine("f")
                rb_image_sync!!.setChecked(false)
            } else {
                setIsImageOffLine("t")
                rb_image_sync!!.setChecked(true)
            }
        })
        rb_mobile_network_sync!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (!isChecked) {
                setIsMobileNetwork("f")
                rb_mobile_network_sync!!.setChecked(false)
            } else {
                setIsMobileNetwork("t")
                rb_mobile_network_sync!!.setChecked(true)
            }
        })
        if (getIsBackground().equals("t", ignoreCase = true)) {
            rb_background_sync!!.setChecked(true)
        } else {
            rb_background_sync!!.setChecked(false)
        }
        if (getIsMobileNetwork().equals("t", ignoreCase = true)) {
            rb_mobile_network_sync!!.setChecked(true)
        } else {
            rb_mobile_network_sync!!.setChecked(false)
        }
        if (getImageOffLine().equals("t", ignoreCase = true)) {
            rb_image_sync!!.setChecked(true)
        } else {
            rb_image_sync!!.setChecked(false)
        }

//        if (AppPreferences.getInstance().getIsCarouselsLayout().equalsIgnoreCase("f")) {
//            rb_usecarousel.setChecked(false);
//        } else {
//            rb_usecarousel.setChecked(true);
//        }
        if (getAllowVideo().equals(resources.getString(R.string.never), ignoreCase = true)) {
            rb_never!!.setChecked(true)
            rb_onwifi!!.setChecked(false)
            rb_wifi_mobile!!.setChecked(false)
        } else if (getAllowVideo().equals(
                resources.getString(R.string.wifi_only),
                ignoreCase = true
            )
        ) {
            rb_never!!.setChecked(false)
            rb_onwifi!!.setChecked(true)
            rb_wifi_mobile!!.setChecked(false)
        } else if (getAllowVideo().equals(
                resources.getString(R.string.wifi_mobile),
                ignoreCase = true
            )
        ) {
            rb_never!!.setChecked(false)
            rb_onwifi!!.setChecked(false)
            rb_wifi_mobile!!.setChecked(true)
        }
        if (getFONT_SCALE().equals("0.85", ignoreCase = true)) {
            iv_huge_settings!!.setVisibility(View.GONE)
            iv_large_settings!!.setVisibility(View.GONE)
            iv_normal_settings!!.setVisibility(View.GONE)
            iv_small_settings!!.setVisibility(View.VISIBLE)
        } else if (getFONT_SCALE().equals("1.0", ignoreCase = true)) {
            iv_huge_settings!!.setVisibility(View.GONE)
            iv_large_settings!!.setVisibility(View.GONE)
            iv_normal_settings!!.setVisibility(View.VISIBLE)
            iv_small_settings!!.setVisibility(View.GONE)
        } else if (getFONT_SCALE().equals("1.15", ignoreCase = true)) {
            iv_huge_settings!!.setVisibility(View.GONE)
            iv_large_settings!!.setVisibility(View.VISIBLE)
            iv_normal_settings!!.setVisibility(View.GONE)
            iv_small_settings!!.setVisibility(View.GONE)
        } else if (getFONT_SCALE().equals("1.30", ignoreCase = true)) {
            iv_huge_settings!!.setVisibility(View.VISIBLE)
            iv_large_settings!!.setVisibility(View.GONE)
            iv_normal_settings!!.setVisibility(View.GONE)
            iv_small_settings!!.setVisibility(View.GONE)
        }
        setValues()
        ll_small_settings.setOnClickListener(this)
        ll_normal_settings.setOnClickListener(this)
        ll_large_settings.setOnClickListener(this)
        ll_huge_settings.setOnClickListener(this)
        val indicatorSeekBar = findViewById<IndicatorSeekBar>(R.id.isb_textsize_settings)
        if (getFONT_SCALE().equals(
                "0.85",
                ignoreCase = true
            )
        ) indicatorSeekBar.setProgress(0f) else if (getFONT_SCALE().equals(
                "1.0",
                ignoreCase = true
            )
        ) indicatorSeekBar.setProgress(33f) else if (getFONT_SCALE().equals(
                "1.15",
                ignoreCase = true
            )
        ) indicatorSeekBar.setProgress(67f) else if (getFONT_SCALE().equals(
                "1.30",
                ignoreCase = true
            )
        ) indicatorSeekBar.setProgress(100f)
        indicatorSeekBar.setOnSeekChangeListener(object : IndicatorSeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: IndicatorSeekBar,
                progress: Int,
                progressFloat: Float,
                fromUserTouch: Boolean
            ) {
                if (progress == 0) setFONT_SCALE("0.85") else if (progress == 33) setFONT_SCALE("1.0") else if (progress == 67) setFONT_SCALE(
                    "1.15"
                ) else if (progress == 100) setFONT_SCALE("1.30")
            }

            override fun onSectionChanged(
                seekBar: IndicatorSeekBar,
                thumbPosOnTick: Int,
                textBelowTick: String,
                fromUserTouch: Boolean
            ) {
                //only callback on discrete series SeekBar type.
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar, thumbPosOnTick: Int) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ll_text_size -> {
            }
            R.id.ll_small_settings -> {
                setFONT_SCALE("0.85")
                iv_huge_settings!!.visibility = View.GONE
                iv_large_settings!!.visibility = View.GONE
                iv_normal_settings!!.visibility = View.GONE
                iv_small_settings!!.visibility = View.VISIBLE
            }
            R.id.ll_normal_settings -> {
                setFONT_SCALE("1.0")
                iv_huge_settings!!.visibility = View.GONE
                iv_large_settings!!.visibility = View.GONE
                iv_normal_settings!!.visibility = View.VISIBLE
                iv_small_settings!!.visibility = View.GONE
            }
            R.id.ll_large_settings -> {
                setFONT_SCALE("1.15")
                iv_huge_settings!!.visibility = View.GONE
                iv_large_settings!!.visibility = View.VISIBLE
                iv_normal_settings!!.visibility = View.GONE
                iv_small_settings!!.visibility = View.GONE
            }
            R.id.ll_huge_settings -> {
                setFONT_SCALE("1.30")
                iv_huge_settings!!.visibility = View.VISIBLE
                iv_large_settings!!.visibility = View.GONE
                iv_normal_settings!!.visibility = View.GONE
                iv_small_settings!!.visibility = View.GONE
            }
            R.id.ll_breaking_news -> if (rb_breaking_notification!!.isChecked) {
                setBreakingNewsNotification("f")
                setBreakingNewsNotifications("f")
                rb_breaking_notification!!.isChecked = false
            } else {
                setBreakingNewsNotification("t")
                setBreakingNewsNotifications("t")
                rb_breaking_notification!!.isChecked = true
            }
            R.id.ll_background_sync -> if (rb_background_sync!!.isChecked) {
                setIsBackground("f")
                rb_background_sync!!.isChecked = false
            } else {
                setIsBackground("t")
                rb_background_sync!!.isChecked = true
            }
            R.id.rb_breaking_notification -> if (!rb_breaking_notification!!.isChecked) {
                setBreakingNewsNotifications("f")
                setBreakingNewsNotification("f")
                rb_breaking_notification!!.isChecked = false
            } else {
                setBreakingNewsNotification("t")
                setBreakingNewsNotifications("t")
                rb_breaking_notification!!.isChecked = true
            }
            R.id.rb_background_sync -> if (!rb_background_sync!!.isChecked) {
                setIsBackground("f")
                rb_background_sync!!.isChecked = false
            } else {
                setIsBackground("t")
                rb_background_sync!!.isChecked = true
            }
            R.id.ll_image_sync -> if (rb_image_sync!!.isChecked) {
                setIsImageOffLine("f")
                rb_image_sync!!.isChecked = false
            } else {
                setIsImageOffLine("t")
                rb_image_sync!!.isChecked = true
            }
            R.id.rb_image_sync -> if (!rb_image_sync!!.isChecked) {
                setIsImageOffLine("f")
                rb_image_sync!!.isChecked = false
            } else {
                setIsImageOffLine("t")
                rb_image_sync!!.isChecked = true
            }
            R.id.ll_mobile_network_sync -> if (rb_mobile_network_sync!!.isChecked) {
                setIsMobileNetwork("f")
                rb_mobile_network_sync!!.isChecked = false
            } else {
                setIsMobileNetwork("t")
                rb_mobile_network_sync!!.isChecked = true
            }
            R.id.rb_mobile_network_sync -> if (!rb_mobile_network_sync!!.isChecked) {
                setIsMobileNetwork("f")
                rb_mobile_network_sync!!.isChecked = false
            } else {
                setIsMobileNetwork("t")
                rb_mobile_network_sync!!.isChecked = true
            }
        }
    }

    fun setBreakingNewsNotification(isBreakingNotification: String?) {
//        try {
//
//            RequestBean requestBean = new RequestBean();
//            requestBean.breakingNewsNotification(AppPreferences.getInstance().getFcmToken(), isBreakingNotification, "ANDROID", AppPreferences.getInstance().getUserId());
//
//            Observable<ResponseResult> resultObservable = ApplicationConfig.apiInterface.breakingNews(
//                    requestBean);
//            resultObservable.subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .unsubscribeOn(Schedulers.io())
//                    .subscribe(new Observer<ResponseResult>() {
//                        @Override
//                        public void onCompleted() {
//
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onNext(ResponseResult responseResult) {
//                            AppPreferences.INSTANCE.setBreakingNewsNotifications("t");
//                            Utils.showToast(responseResult.getMessage());
//                        }
//                    });
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    fun setValues() {

//        if (AppPreferences.getInstance().getCompactLayout().equalsIgnoreCase("t"))
//            rb_compact.setChecked(true);
//        else
//            rb_compact.setChecked(false);
        if (getBreakingNewsNotifications().equals(
                "f",
                ignoreCase = true
            )
        ) rb_breaking_notification!!.isChecked = false else rb_breaking_notification!!.isChecked =
            true
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();
        setHeader(true);
        showBackButton(true);
        setCartButton(false);
        setShareButton(false);
        setTitle("Settings");
    }*/
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onButtonClick(`object`: Any) {
        setFONT_SCALE(`object`.toString())
        if (getFONT_SCALE().equals("0.85", ignoreCase = true)) tv_textSize!!.text =
            getString(R.string.small_size) else if (getFONT_SCALE().equals(
                "1.0",
                ignoreCase = true
            )
        ) tv_textSize!!.text = getString(R.string.normal_size) else if (getFONT_SCALE().equals(
                "1.15",
                ignoreCase = true
            )
        ) tv_textSize!!.text = getString(R.string.large_size) else if (getFONT_SCALE().equals(
                "1.30",
                ignoreCase = true
            )
        ) tv_textSize!!.text = getString(R.string.huge_size)
    }
}