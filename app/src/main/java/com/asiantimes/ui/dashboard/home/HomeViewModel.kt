package com.asiantimes.ui.dashboard.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import com.asiantimes.model.GetTopStoriesModel

class HomeViewModel : ViewModel() {
    var liveData = MutableLiveData<GetTopStoriesModel>()
    var commonLiveData = MutableLiveData<CommonModel>()

    fun getTopStories() {
        liveData = ApiRepository.getTopStories()
    }

    fun saveBookmark(postId: String,isRemoved: Int) {
        commonLiveData = ApiRepository.saveBookmark(postId,isRemoved)
    }
}