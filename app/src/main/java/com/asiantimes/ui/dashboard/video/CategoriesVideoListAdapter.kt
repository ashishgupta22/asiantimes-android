package com.asiantimes.ui.dashboard.video

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterCategoriesVideoChildBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetVideoListModel

class CategoriesVideoListAdapter(private val onClickListener: OnClickPositionViewListener) : RecyclerView.Adapter<CategoriesVideoListAdapter.ViewHolder>() {
    private var catNewsList: ArrayList<GetVideoListModel.NewsListBean> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_categories_video_child, parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return catNewsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(catNewsList[position],onClickListener,position)
    }

    class ViewHolder(private val bindingView: AdapterCategoriesVideoChildBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            catNewsData: GetVideoListModel.NewsListBean,
            onClickListener: OnClickPositionViewListener,
            position: Int
        ) {
            bindingView.setVariable(BR.catNewsData,catNewsData)
            bindingView.setVariable(BR.onClickListener,onClickListener)
            bindingView.setVariable(BR.position,position)
        }
    }

    fun setData(catNewsList: ArrayList<GetVideoListModel.NewsListBean>) {
        this.catNewsList = catNewsList
        notifyDataSetChanged()
    }

    fun setData(catNewsList: ArrayList<GetVideoListModel.NewsListBean>, position: Int) {
        this.catNewsList = catNewsList
        notifyItemChanged(position)
    }
}