package com.asiantimes.ui.dashboard.video

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.asiantimes.R
import com.asiantimes.base.BaseFragment
import com.asiantimes.base.Utils
import com.asiantimes.databinding.FragmentVideoBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetVideoListModel
import com.asiantimes.ui.detail.VideoDetailsActivity

class VideoFragment : BaseFragment() {
    private lateinit var catVideoList: java.util.ArrayList<GetVideoListModel.VideoSubCatListBean>
    private lateinit var trendingVideoList: java.util.ArrayList<GetVideoListModel.TrandListBean>
    private lateinit var newVideoList: java.util.ArrayList<GetVideoListModel.NewsListBean>
    private var bindingView: FragmentVideoBinding? = null
    private var newVideoAdapter: NewVideoAdapter? = null
    private var trendingVideoAdapter: TrendingVideoAdapter? = null
    private var categoriesVideoAdapter: CategoryVideoAdapterNew? = null
    private val viewModel: VideoViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        setAdapter()
        getTopStoriesAPI()
        getTopStoriesObserver()
        bindingView!!.layoutShimmer.visibility = View.VISIBLE
        bindingView!!.layoutShimmer.startShimmerAnimation()
        bindingView!!.clMain.visibility = View.GONE
    }

    private fun getTopStoriesAPI() {
        viewModel.getVideoList()
    }

    private fun getTopStoriesObserver() {
        viewModel.liveData.observe(viewLifecycleOwner, {
            val responseData = it
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.imageViewNoInternet.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            bindingView!!.clMain.visibility = View.VISIBLE
            if (responseData?.errorCode == 0) {
                newVideoList =
                    responseData.newsList!! as ArrayList<GetVideoListModel.NewsListBean>
                trendingVideoList =
                    responseData.trandList!! as ArrayList<GetVideoListModel.TrandListBean>
                catVideoList =
                    responseData.videoSubCatList!! as ArrayList<GetVideoListModel.VideoSubCatListBean>
                setAdapter()
            } else {
                bindingView!!.clMain.visibility = View.GONE
                bindingView!!.imageViewNoInternet.visibility = View.GONE
                bindingView!!.layoutShimmer.visibility = View.GONE
                bindingView!!.layoutShimmer.stopShimmerAnimation()
                bindingView!!.imageViewNoData.visibility = View.VISIBLE
            }
        })
    }

    private fun setAdapter() {
        //new adapter bind
        newVideoAdapter = NewVideoAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                val item = `object` as GetVideoListModel.NewsListBean
                item.videoUrl?.let {
                    item.postTitle?.let { it1 ->
                        item.postContent?.let { it2 ->
                            callVideoDetailsIntent(
                                it,
                                it1, it2
                            )
                        }
                    }
                }
            }

        })
        bindingView!!.recycleViewNewVideo.adapter = newVideoAdapter

        if (newVideoList.isNullOrEmpty())
            bindingView!!.recycleViewNewVideo.visibility = View.GONE
        else {
            bindingView!!.recycleViewNewVideo.visibility = View.VISIBLE
            newVideoAdapter!!.setData(newVideoList)
        }

        //  Trading Video bind
        trendingVideoAdapter = TrendingVideoAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                val item = `object` as GetVideoListModel.TrandListBean
                item.videoUrl?.let {
                    item.postTitle?.let { it1 ->
                        item.postContent?.let { it2 ->
                            callVideoDetailsIntent(
                                it,
                                it1, it2
                            )
                        }
                    }
                }
            }

        })
        bindingView!!.recycleViewTrendingVideo.adapter = trendingVideoAdapter
        if (trendingVideoList.isNullOrEmpty())
            bindingView!!.recycleViewTrendingVideo.visibility = View.GONE
        else {
            bindingView!!.recycleViewTrendingVideo.visibility = View.VISIBLE
            trendingVideoAdapter!!.setData(trendingVideoList)
        }

        // bind Category
        categoriesVideoAdapter = CategoryVideoAdapterNew(catVideoList)
//        {
//            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
//                val data = `object` as GetVideoListModel.VideoSubCatListBean
//                val intent = Intent(requireContext(), CategoryNewsListActivity::class.java)
//                intent.putExtra(Constants.CATEGORY_ID, data.catId.toString())
//                intent.putExtra(Constants.CATEGORY_NAME, data.subCategory)
//                startActivity(intent)
//            }
//        })


        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.canScrollVertically()
        bindingView!!.recycleViewCategoriesVideo.layoutManager = layoutManager
        bindingView!!.recycleViewCategoriesVideo.adapter = categoriesVideoAdapter
        if (catVideoList.isNullOrEmpty())
            bindingView!!.recycleViewCategoriesVideo.visibility = View.GONE
        else {
            bindingView!!.recycleViewCategoriesVideo.visibility = View.VISIBLE
            categoriesVideoAdapter!!.setData(catVideoList)
        }

    }

    private fun callVideoDetailsIntent(videoUrl: String, postTitle: String, postContent: String) {
        if (isOnline()) {
            if (videoUrl!!.length > 0) {
                val intent = Intent(activity, VideoDetailsActivity::class.java)
                intent.putExtra(
                    "youtubeKey",
                    videoUrl!!
                )
                intent.putExtra("title", postTitle)
                intent.putExtra("details", postContent)
                startActivity(intent)
            } else {
                val intent = Intent(activity, VideoDetailsActivity::class.java)
                intent.putExtra("youtubeKey", "No")
                intent.putExtra("title", postTitle)
                intent.putExtra("details", postContent)
                startActivity(intent)
            }
        } else {
            Utils.showToast(getString(R.string.internet_error))
        }
    }
}