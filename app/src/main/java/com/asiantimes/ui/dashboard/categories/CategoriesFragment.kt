package com.asiantimes.ui.dashboard.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.asiantimes.R
import com.asiantimes.base.BaseFragment
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.FragmentCategoriesBinding
import com.asiantimes.model.GetAllCategoriesModel
import com.asiantimes.ui.splash.SplashViewModel
import java.util.*
import kotlin.collections.ArrayList


class CategoriesFragment : BaseFragment(), View.OnClickListener {
    private var bindingView: FragmentCategoriesBinding? = null
    private var categoriesAdapter: CategoryAdapterNew? = null
    private val viewModel: SplashViewModel by viewModels()
    private var catList: List<GetAllCategoriesModel.Post> = ArrayList()
    private val filteredList: MutableList<GetAllCategoriesModel.Post> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindingView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        catList = getCateList()
        setAdapter()
        bindingView!!.imageViewRefresh.setOnClickListener(this)

        bindingView!!.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(searchWord: String?): Boolean {
                val s = searchWord!!.toLowerCase(Locale.ROOT)
                filteredList.clear()
                for (i in catList.indices) {
                    val c = catList[i].cat!!.toLowerCase(Locale.ROOT)
                    if (c.contains(s))
                        filteredList.add(catList[i])
                }
                categoriesAdapter!!.setData(filteredList)
                return false
            }
        })
    }

    override fun onClick(view: View?) {
        if (view == bindingView!!.imageViewRefresh) {
            if (isOnline()) {
                //Clear query
                bindingView!!.searchView.setQuery("", false);
                getAllCategoriesAPI()
                getAllCategoriesObserver()

            } else
                Utils.showToast(getString(R.string.internet_error))
        }
    }

    private fun setAdapter() {

        categoriesAdapter = CategoryAdapterNew(catList!!)
//              bindingView!!.recycleViewCategories.layoutManager = layoutManager
//        categoriesAdapter = CategoriesAdapter(object : OnClickListener {
//            override fun onClickListener(`object`: Any?) {
//                val data = `object` as GetAllCategoriesModel.Post
//                val intent = Intent(requireContext(), CategoryNewsListActivity::class.java)
//                intent.putExtra(Constants.CATEGORY_ID, data.catId)
//                intent.putExtra(Constants.CATEGORY_NAME, data.cat)
//                startActivity(intent)
//            }
//        })

        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.canScrollVertically()
        bindingView!!.recycleViewCategories.layoutManager = layoutManager
        bindingView!!.recycleViewCategories.adapter = categoriesAdapter

//        bindingView!!.recycleViewCategories.adapter = categoriesAdapter
//        categoriesAdapter!!.setData(catList)
    }

    private fun getAllCategoriesAPI() {
        bindingView!!.recycleViewCategories.visibility = View.GONE
        bindingView!!.layoutShimmer.visibility = View.VISIBLE
        bindingView!!.layoutShimmer.startShimmerAnimation()
        viewModel.getAllCategories()
    }

    private fun getAllCategoriesObserver() {
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        bindingView!!.recycleViewCategories.layoutManager = layoutManager

        viewModel.liveDataAllCategories.observe(this, {
            val responseData = it
            bindingView!!.recycleViewCategories.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            if (responseData?.errorCode == 0) {
                AppDatabase.getDatabase(requireContext()).getAllCategoryDao().delete()
                AppDatabase.getDatabase(requireContext()).getAllCategoryDao()
                    .insert(responseData.postList!!)
                catList = getCateList()
                setAdapter()
            } else {
                Utils.showToast(responseData.msg)
            }
        })
    }

    private fun getCateList(): List<GetAllCategoriesModel.Post> {
        var list: ArrayList<GetAllCategoriesModel.Post> = ArrayList()
        val lis = AppDatabase.getDatabase(requireContext()).getAllCategoryDao().getAllCategories()
        for (i in lis) {
            if (!i.cat!!.contains("Privacy") && !i.cat!!.contains("Terms") && !i.cat!!.contains("About")&& !i.cat!!.contains(
                    "Contact"
                )) {
                list.add(i)
            }
        }
        return list
    }
}