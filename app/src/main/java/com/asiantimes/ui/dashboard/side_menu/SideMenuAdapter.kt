package com.asiantimes.ui.dashboard.side_menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.databinding.AdapterSideMenuChildBinding
import com.asiantimes.databinding.AdapterSideMenuGroupBinding
import com.asiantimes.model.GetAllCategoriesModel


class SideMenuAdapter(
    private val context: Context,
    private val catList: List<GetAllCategoriesModel.Post>
) :
    BaseExpandableListAdapter() {


    override fun getGroup(groupPosition: Int): Any? {
        return catList[groupPosition]
    }

    override fun getGroupCount(): Int {
        return catList.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(
        groupPosition: Int, isExpanded: Boolean, view: View?, parent: ViewGroup?
    ): View? {
        val bindingView: AdapterSideMenuGroupBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.adapter_side_menu_group, parent, false
        )
        bindingView.categoriesData = getGroup(groupPosition) as GetAllCategoriesModel.Post?

        if (catList[groupPosition].subCatArr.isNullOrEmpty())
            bindingView.imageViewGroupIndicator.visibility = View.GONE

        if (isExpanded) {
            bindingView.imageViewGroupIndicator.setImageResource(R.drawable.ic_icon_material_keyboard_arrow_up)
            bindingView.layoutParent.setBackgroundColor(
                ContextCompat.getColor(context, R.color.colorGreyLight)
            )
        } else {
            bindingView.imageViewGroupIndicator.setImageResource(R.drawable.ic_icon_material_keyboard_arrow_down)
            bindingView.layoutParent.setBackgroundColor(
                ContextCompat.getColor(context, R.color.colorWhite)
            )
        }

        return bindingView.root
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        if (catList[groupPosition].subCatArr.isNullOrEmpty()) return 0
        return catList[groupPosition].subCatArr!!.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any? {
        return catList[groupPosition].subCatArr!![childPosition]
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }


    override fun getChildView(
        groupPosition: Int, childPosition: Int,
        isLastChild: Boolean, view: View?, parent: ViewGroup?
    ): View? {
        val bindingView: AdapterSideMenuChildBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.adapter_side_menu_child, parent, false
        )
        bindingView.subCategoriesData =
            getChild(groupPosition, childPosition) as GetAllCategoriesModel.Post.SubCatArr?

        if (childPosition == 0)
            bindingView.viewTopHorizontal.visibility = View.VISIBLE
        else
            bindingView.viewTopHorizontal.visibility = View.GONE

        if (isLastChild)
            bindingView.viewBottomVertical.visibility = View.INVISIBLE
        else
            bindingView.viewBottomVertical.visibility = View.VISIBLE

        return bindingView.root
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}