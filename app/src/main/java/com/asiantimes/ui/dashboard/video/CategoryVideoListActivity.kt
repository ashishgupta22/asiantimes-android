package com.asiantimes.ui.dashboard.video

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Constants
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivityCategoryVideoListBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.model.GetVideoListModel
import com.asiantimes.ui.detail.VideoDetailsActivity
import com.asiantimes.ui.search.SearchActivity

class CategoryVideoListActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityCategoryVideoListBinding? = null
    private var categoriesNewsListAdapter: CategoriesVideoListAdapter? = null
    private var catNewsList: ArrayList<GetVideoListModel.NewsListBean> = ArrayList()
    private val viewModel: VideoViewModel by viewModels()
    private var catId: String? = null
    private var isPagination: Boolean? = null
    private var page = 1
    private var pastVisibleItems = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var appDatabase: AppDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_category_video_list)
        appDatabase = AppDatabase.getDatabase(this)
        catId = intent.getStringExtra(Constants.CATEGORY_ID)
        bindingView!!.catName = intent.getStringExtra(Constants.CATEGORY_NAME)
        bindingView!!.imageViewBack.setOnClickListener(this)
        bindingView!!.imageViewSearch.setOnClickListener(this)
        setAdapter()
        isPagination = false
        page = 1
        if (isOnline()) {
            bindingView!!.imageViewNoInternet.visibility = View.GONE
            getPostListByCateIDAPI()
            getPostListByCateIDObserver()
        }
        val layoutManager = LinearLayoutManager(this)
        bindingView!!.recycleViewNewsList.layoutManager = layoutManager
        bindingView!!.recycleViewNewsList.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        page += 1
                        isPagination = true
                        if (isOnline()) {
                            getPostListByCateIDAPI()
                            getPostListByCateIDObserver()
                        } else {
                            Utils.showToast(getString(R.string.internet_error))
                        }
                    }
                }
            }
        })
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.imageViewBack -> {
                finish()
            }

            bindingView!!.imageViewSearch -> {
                val intent = Intent(this, SearchActivity::class.java)
                startActivity(intent)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        appDatabase = AppDatabase.getDatabase(this)
    }

    private fun setAdapter() {
        categoriesNewsListAdapter =
            CategoriesVideoListAdapter(object : OnClickPositionViewListener {
                override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                    val data = `object` as GetVideoListModel.NewsListBean
                    if (view == 0) {
                        if (!data.videoUrl.isNullOrEmpty()) {
                            val intent = Intent(
                                this@CategoryVideoListActivity,
                                VideoDetailsActivity::class.java
                            )
                            intent.putExtra(
                                "youtubeKey",
                                data.videoUrl!!
                            )
                            intent.putExtra("title", data.postTitle)
                            intent.putExtra("details", data.postContent)
                            startActivity(intent)
                        } else Utils.showToast("Video URL Not Available")
                    } else if (view == 1) {
                        //bookmark
//                        val isRemoved = if (data.isBookmark!!) 0 else 1
//                        saveBookmarkAPI(data.postId!!, isRemoved)
//                        saveBookmarkObserver(data)
                    } else if (view == 2) {
                        //share
//                        shareUrl(data.post, data.postTitle)
                    }

                }
            })
        bindingView!!.recycleViewNewsList.adapter = categoriesNewsListAdapter
    }

    private fun getPostListByCateIDAPI() {
        if (isPagination!!) {
            bindingView!!.progressBar.visibility = View.VISIBLE
            bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
        } else {
            bindingView!!.recycleViewNewsList.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.startShimmerAnimation()
        }
        viewModel.getVideoCatList(catId!!, page.toString())
    }

    private fun getPostListByCateIDObserver() {
        viewModel.liveData.observe(this, {
            bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
            bindingView!!.progressBar.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            val responseData = it
            if (responseData?.errorCode == 0) {
                if (responseData.catVideoList.isNullOrEmpty()) {
                    if (!isPagination!!) {
                        bindingView!!.recycleViewNewsList.visibility = View.GONE
                        bindingView!!.imageViewNoData.visibility = View.VISIBLE
                    }
                } else {
                    bindingView!!.imageViewNoData.visibility = View.GONE
                    bindingView!!.recycleViewNewsList.visibility = View.VISIBLE
                    if (!isPagination!!) {
                        catNewsList.clear()

                        catNewsList.addAll(responseData.catVideoList!! as ArrayList<GetVideoListModel.NewsListBean>)
                        categoriesNewsListAdapter!!.setData(catNewsList)
                    } else {
                        val position = catNewsList.size - 1
                        catNewsList.addAll(responseData.catVideoList!! as ArrayList<GetVideoListModel.NewsListBean>)
                        categoriesNewsListAdapter!!.setData(catNewsList, position)
                    }
                }

            } else {
                if (!isPagination!!) {
                    bindingView!!.recycleViewNewsList.visibility = View.GONE
                    bindingView!!.imageViewNoData.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun saveBookmarkAPI(postId: String, isRemoved: Int) {
        AppConfig.showProgress(this)
        viewModel.saveBookmark(postId, isRemoved)
    }

    private fun saveBookmarkObserver(data: GetTopStoriesModel.PostList) {
        viewModel.commonLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                data.isBookmark = !data.isBookmark!!
                categoriesNewsListAdapter!!.setData(catNewsList)
//                appDatabase!!.getCateNewsList().update(data)
            }
        })
    }
}

