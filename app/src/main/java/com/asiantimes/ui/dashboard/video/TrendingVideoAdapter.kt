package com.asiantimes.ui.dashboard.video

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterTrendingVideoBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetVideoListModel
import java.util.ArrayList

class TrendingVideoAdapter(private val OnClickListener: OnClickPositionViewListener) : RecyclerView.Adapter<TrendingVideoAdapter.ViewHolder>() {

    private lateinit var trendingVideoList: ArrayList<GetVideoListModel.TrandListBean>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_trending_video, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return trendingVideoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(OnClickListener, trendingVideoList!![position], position)
    }

    class ViewHolder(private val bindingView: AdapterTrendingVideoBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            OnClickListener: OnClickPositionViewListener,
            cateList: GetVideoListModel.TrandListBean,
            parentPosition: Int
        ) {
            bindingView.setVariable(BR.tradingList, cateList)
            bindingView.setVariable(BR.onClickListener, OnClickListener)
            bindingView.setVariable(BR.position, parentPosition)
        }
    }

    fun setData(trendingVideoList: ArrayList<GetVideoListModel.TrandListBean>) {
        this.trendingVideoList = trendingVideoList
        notifyDataSetChanged()
    }
}