package com.asiantimes.ui.dashboard

import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.google.android.material.snackbar.Snackbar
import com.asiantimes.R
import com.asiantimes.base.BaseActivity
import com.asiantimes.databinding.ActivityDashBoardBinding


class DashboardActivity : BaseActivity() {
    private var bindingView: ActivityDashBoardBinding? = null
    private var dashboardAdapter: DashboardAdapter? = null
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_dash_board)
        setupPager()
    }

    private fun setupPager() {
        dashboardAdapter = DashboardAdapter(supportFragmentManager)
        bindingView!!.viewPagerHome.adapter = dashboardAdapter
        bindingView!!.tabLayoutHome.setupWithViewPager(bindingView!!.viewPagerHome)
        bindingView!!.tabLayoutHome.getTabAt(0)!!.setIcon(R.drawable.ic_home)
        bindingView!!.tabLayoutHome.getTabAt(1)!!.setIcon(R.drawable.ic_categories)
        bindingView!!.tabLayoutHome.getTabAt(2)!!.setIcon(R.drawable.ic_video)
        bindingView!!.tabLayoutHome.getTabAt(3)!!.setIcon(R.drawable.ic_bookmark_png)
        bindingView!!.tabLayoutHome.getTabAt(4)!!.setIcon(R.drawable.ic_side_menu)
    }
    override fun onBackPressed() {
            if (doubleBackToExitPressedOnce) {
                finishAffinity()
                return
            }
            this.doubleBackToExitPressedOnce = true
            Snackbar.make(findViewById(android.R.id.content), "Press again to Exit...", Snackbar.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
    fun logout(pos:Int){
        bindingView!!.viewPagerHome.currentItem = pos
    }
}