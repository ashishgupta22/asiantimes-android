package com.asiantimes.ui.dashboard.video

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterNewVideoBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetVideoListModel

class NewVideoAdapter(private val OnClickListener: OnClickPositionViewListener) :
    RecyclerView.Adapter<NewVideoAdapter.ViewHolder>() {
    private var cateList: List<GetVideoListModel.NewsListBean>? = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_new_video, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return cateList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(OnClickListener, cateList!![position], position)
    }

    class ViewHolder(private val bindingView: AdapterNewVideoBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            OnClickListener: OnClickPositionViewListener,
            cateList: GetVideoListModel.NewsListBean,
            parentPosition: Int
        ) {
            bindingView.setVariable(BR.videoList, cateList)
            bindingView.setVariable(BR.onClickListener, OnClickListener)
            bindingView.setVariable(BR.position, parentPosition)
        }
    }

    fun setData(cateList: List<GetVideoListModel.NewsListBean>?) {
        this.cateList = cateList
        notifyDataSetChanged()
    }
}