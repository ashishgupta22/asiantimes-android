package com.asiantimes.ui.dashboard.categories

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterCategoriesBinding
import com.asiantimes.interfaces.OnClickListener
import com.asiantimes.model.GetAllCategoriesModel


class CategoriesAdapter(private val clickListener: OnClickListener) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {
    private var catList: List<GetAllCategoriesModel.Post>? = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_categories, parent, false
            )
        )
    }
    override fun getItemCount(): Int {
        return catList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position, clickListener, catList!![position])
    }

    class ViewHolder(private val bindingView: AdapterCategoriesBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            position: Int,
            clickListener: OnClickListener,
            categories: GetAllCategoriesModel.Post
        ) {
            bindingView.setVariable(BR.position, position)
            bindingView.setVariable(BR.clickListener, clickListener)
            bindingView.setVariable(BR.categoriesData, categories)
            bindingView.textViewTitle.text = "#"+categories.cat
            var colorCode: String? = categories.catColor
            if (colorCode.isNullOrEmpty()){
                colorCode = "#FF006F"
            }
            val gradientDrawable = GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                intArrayOf(Color.parseColor(colorCode),
                    Color.parseColor(colorCode))
            );
            gradientDrawable.cornerRadius = 55f;
            //Set Gradient
            bindingView.ccMain.setBackground(gradientDrawable);
        }
    }

    fun setData(catList: List<GetAllCategoriesModel.Post>) {
        this.catList = catList
        notifyDataSetChanged()
    }
}