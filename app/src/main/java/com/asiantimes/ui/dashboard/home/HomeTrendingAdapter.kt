package com.asiantimes.ui.dashboard.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterHomeTrendingBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class HomeTrendingAdapter(private val onClickListener: OnClickPositionViewListener) :
    RecyclerView.Adapter<HomeTrendingAdapter.ViewHolder>() {
    private var trendingNews: List<GetTopStoriesModel.PostList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_home_trending, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return trendingNews!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(trendingNews!![position], position, onClickListener)
    }

    class ViewHolder(private val bindingView: AdapterHomeTrendingBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(trendingNews: Any, position: Int, onClickListener: Any) {
            bindingView.setVariable(BR.trendingNews, trendingNews)
            bindingView.setVariable(BR.position, position)
            bindingView.setVariable(BR.onClickListener, onClickListener)
        }
    }

    fun setData(trendingNews: ArrayList<GetTopStoriesModel.PostList>) {
        this.trendingNews = trendingNews
        notifyDataSetChanged()
    }
}