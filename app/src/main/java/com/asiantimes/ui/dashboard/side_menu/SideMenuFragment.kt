package com.asiantimes.ui.dashboard.side_menu

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.BaseFragment
import com.asiantimes.base.Constants
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.FragmentSideMenuBinding
import com.asiantimes.digital_addition.ui.home.DigitalHomeActivity
import com.asiantimes.digital_addition.ui.setting.PolicyAndTermsActivity
import com.asiantimes.digital_addition.ui.splash.DigitalSplashActivity
import com.asiantimes.model.GetAllCategoriesModel
import com.asiantimes.ui.category_news.CategoryNewsListActivity
import com.asiantimes.ui.contact_us.ContactUsActivity
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.help.HelpActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.photo.ViewMorePhotosActivity
import com.asiantimes.ui.profile.ProfileActivity
import com.asiantimes.ui.setting.SettingsActivity
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.expandable_footer_view.view.*

class SideMenuFragment : BaseFragment() {
    private var bindingView: FragmentSideMenuBinding? = null
    private var sideMenuAdapter: SideMenuAdapter? = null
    private val prevExpandPosition = intArrayOf(-1)
    private var footerView: View? = null
    private var appDatabase: AppDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindingView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_side_menu, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        appDatabase = AppDatabase.getDatabase(requireContext())
        if(AppPreferences.getLoginStatus()) {
            bindingView!!.userData = AppPreferences.getUserDetails()
        }
        bindingView!!.imageViewProfile.setOnClickListener {
            if (AppPreferences.getLoginStatus()) {
                startActivity(Intent(requireContext(), ProfileActivity::class.java))
            }else{
                startActivity(Intent(activity, LogInActivity::class.java))
                activity?.finish()
            }
        }
        setupDrawer()
    }

    override fun onResume() {
        super.onResume()
        if(AppPreferences.getLoginStatus()) {
            appDatabase = AppDatabase.getDatabase(requireContext())
            bindingView!!.userData = AppPreferences.getUserDetails()
        }
    }

    @SuppressLint("InflateParams")
    private fun setupDrawer() {
        val catList = appDatabase!!.getAllCategoryDao().getAllCategories()
        footerView = layoutInflater.inflate(R.layout.expandable_footer_view, null)
        bindingView!!.expandableList.addFooterView(footerView)
        sideMenuAdapter = SideMenuAdapter(requireContext(), catList)
        bindingView!!.expandableList.setAdapter(sideMenuAdapter)

        bindingView!!.expandableList.setOnGroupExpandListener { groupPosition ->
            if (prevExpandPosition[0] >= 0 && prevExpandPosition[0] != groupPosition) {
                bindingView!!.expandableList.collapseGroup(prevExpandPosition[0])
            }
            prevExpandPosition[0] = groupPosition
        }
        bindingView!!.expandableList.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            catList[groupPosition].cat?.let {
                callCateListIntent(
                    catList[groupPosition],
                    catList[groupPosition].subCatArr?.get(childPosition)?.subCatId.toString(),
                    catList[groupPosition].subCatArr?.get(childPosition)?.subCat.toString(),
                    it
                )
            }
            true
        }
        bindingView!!.expandableList.setOnGroupClickListener { _, _, groupPosition, _ ->
            if (catList[groupPosition].subCatArr.isNullOrEmpty()) {
                catList[groupPosition].cat?.let {
                    catList[groupPosition].catId?.let { it1 ->
                        catList[groupPosition].cat?.let { it2 ->
                            callCateListIntent(
                                catList[groupPosition],
                                it1,
                                it, it2
                            )
                        }
                    }
                }
            }
            false
        }
        bindLogout()

    }

    private fun callCateListIntent(
        data: GetAllCategoriesModel.Post,
        catId: String,
        catName: String,
        mainCatName: String
    ) {
        if (mainCatName.toLowerCase() == "video") {
            (activity as DashboardActivity).logout(2)
        } else if (mainCatName.toLowerCase() == "photos") {
            val intent = Intent(context, ViewMorePhotosActivity::class.java)
            intent.putExtra("categoryID", catId)
            intent.putExtra("categoryName", catName)
            startActivity(intent)
        } else if (mainCatName.contains("About Us") || mainCatName.contains("Privacy Policy") || mainCatName.contains(
                "Terms & Conditions")
        ) {
            val intent = Intent(context, PolicyAndTermsActivity::class.java)
            intent.putExtra("TITLE", catName)
            intent.putExtra("URL", data.catUrl)
            startActivity(intent)
        } else if (mainCatName.contains("Contact")) {
            startActivity(Intent(activity, ContactUsActivity::class.java))
        } else {
            val intent = Intent(requireContext(), CategoryNewsListActivity::class.java)
            intent.putExtra(Constants.CATEGORY_ID, catId)
            intent.putExtra(Constants.CATEGORY_NAME, catName)
            startActivity(intent)
        }
    }

    private fun bindLogout() {
        if (!AppPreferences.getLoginStatus()) {
            footerView!!.text_view_group_title.text = "Login"
            footerView!!.image_view_icon.setImageResource(R.drawable.ic_login)
        } else {
            footerView!!.text_view_group_title.text = "Logout"
            footerView!!.image_view_icon.setImageResource(R.drawable.ic_logout)
        }
        footerView!!.text_help.setOnClickListener {
            startActivity(Intent(activity, HelpActivity::class.java))
        }
        footerView!!.layout_setting.setOnClickListener {
            startActivity(Intent(activity, SettingsActivity::class.java))
        }
        footerView!!.layout_sign_out.setOnClickListener {
            if (footerView!!.text_view_group_title.text.toString().toLowerCase()
                    .contentEquals("logout")
            ) {
                AlertDialog.Builder(activity)
                    .setIcon(activity?.let { it1 ->
                        ContextCompat.getDrawable(
                            it1,
                            R.mipmap.ic_launcher
                        )
                    })
                    .setTitle(resources.getString(R.string.app_name))
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Yes") { _: DialogInterface?, _: Int -> logout(footerView!!.text_view_group_title.text.toString()) }
                    .setNegativeButton("No", null)
                    .show()
            } else {
                logout(footerView!!.text_view_group_title.text.toString())
            }
        }
        footerView!!.layout_digital.setOnClickListener({
            if (Utils.isOnline(requireContext())) {
                if (AppPreferences.getLoginStatus()) {
                    startActivity(Intent(requireContext(), DigitalHomeActivity::class.java))
                } else {
                    startActivity(Intent(requireContext(), DigitalSplashActivity::class.java))
                }
            } else {
                Utils.showToast(getString(R.string.internet_error))
            }
        })
    }

    private fun logout(type: String) {
        if (isOnline()) {
            if (type.contentEquals("Logout")) {
                AppPreferences.setLoginStatus(false)
                AppPreferences.setUserId("")
                (activity as DashboardActivity).logout(0)
            } else {
                startActivity(Intent(activity, LogInActivity::class.java))
                activity?.finish()
            }
        } else {
            Utils.showToast(resources.getString(R.string.internet_error))
        }
    }
}