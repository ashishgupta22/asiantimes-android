package com.asiantimes.ui.dashboard.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterHomeNewsChildBinding
import com.asiantimes.interfaces.OnClickGroupPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class HomeNewsChildAdapter(
    private val catPostList: List<GetTopStoriesModel.PostList>?,
    private val onClickListener: OnClickGroupPositionViewListener,private val  groupPositon:Int
) : RecyclerView.Adapter<HomeNewsChildAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_home_news_child, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return catPostList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(groupPositon,position, onClickListener, catPostList!![position])
    }

    class ViewHolder(private val bindingView: AdapterHomeNewsChildBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(groupPositon:Int,
            position: Int,
            onClickListener: Any,
            catPostList: GetTopStoriesModel.PostList
        ) {
            val imageViewBookmark = bindingView.imageViewBookmark
            val layoutNews = bindingView.layoutNews
            val layoutTop = bindingView.layoutTop
            val view = bindingView.view

            if (position == 0) {
                view.visibility = View.GONE
                layoutNews.visibility = View.GONE
                layoutTop.visibility = View.VISIBLE
            } else {
                layoutTop.visibility = View.GONE
                view.visibility = View.VISIBLE
                layoutNews.visibility = View.VISIBLE
            }
            if (catPostList.isBookmark!!)
                imageViewBookmark.setImageResource(R.drawable.ic_bookmark_fill)
            else
                imageViewBookmark.setImageResource(R.drawable.ic_bookmark)
            bindingView.setVariable(BR.onClickListener, onClickListener)
            bindingView.setVariable(BR.catPostList, catPostList)
            bindingView.setVariable(BR.position, position)
            bindingView.setVariable(BR.groupPosition, groupPositon)
        }
    }

    fun setData() {
        notifyDataSetChanged()
    }
}