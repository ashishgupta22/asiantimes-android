package com.asiantimes.ui.dashboard.video

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterCategoriesVideoBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetVideoListModel
import java.util.ArrayList

class CategoriesVideoAdapter(private val OnClickListener: OnClickPositionViewListener) :
    RecyclerView.Adapter<CategoriesVideoAdapter.ViewHolder>() {

    private lateinit var catVideoList: ArrayList<GetVideoListModel.VideoSubCatListBean>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_categories_video, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return catVideoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(OnClickListener, catVideoList!![position], position)
    }

    class ViewHolder(private val bindingView: AdapterCategoriesVideoBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            OnClickListener: OnClickPositionViewListener,
            cateList: GetVideoListModel.VideoSubCatListBean,
            parentPosition: Int
        ) {
            bindingView.setVariable(BR.catList, cateList)
            bindingView.setVariable(BR.onClickListener, OnClickListener)
            bindingView.setVariable(BR.position, parentPosition)
            bindingView.textViewTrendingTitle.text = "#" + cateList.subCategory
        }
    }

    fun setData(catVideoList: ArrayList<GetVideoListModel.VideoSubCatListBean>) {
        this.catVideoList = catVideoList
        notifyDataSetChanged()
    }
}