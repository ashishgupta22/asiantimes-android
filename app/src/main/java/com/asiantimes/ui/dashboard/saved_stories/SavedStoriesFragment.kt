package com.asiantimes.ui.dashboard.saved_stories

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.*
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.FragmentSavedStoriesBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.ui.detail.NewsDetailActivity
import com.asiantimes.ui.login.LogInActivity

class SavedStoriesFragment : BaseFragment() {
    private var bindingView: FragmentSavedStoriesBinding? = null
    private var savedStoriesAdapter: SavedStoriesAdapter? = null
    private val viewModel: SavedArticleViewModel by viewModels()
    private var bookmarksList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()
    private var isPagination: Boolean? = null
    private var page = 1
    private var pastVisibleItems = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var appDatabase: AppDatabase? = null
    var isVisible: Boolean? = false
    var isFirstTime: Boolean? = false
    private var _hasLoadedOnce = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindingView =
            DataBindingUtil.inflate(inflater, R.layout.fragment_saved_stories, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (isOnline()) {
            appDatabase = activity?.let { AppDatabase.getDatabase(it) }
            bindingView!!.imageViewNoInternet.visibility = View.GONE
            isPagination = false
            page = 1
            if (AppPreferences.getLoginStatus()) {
                getBookmarkAPI()
                getBookmarkObserver()
            } else {
                bindingView!!.rlNotLogin.visibility = View.VISIBLE
                bindingView!!.buttonLogin.setOnClickListener({
                    startActivity(Intent(activity, LogInActivity::class.java))
                    activity?.finish()
                })
            }
        } else {
            bookmarksList.clear()
            appDatabase!!.getCateNewsList()
                .getTopStoriesList_new(Constants.SAVE_CATEGOTY_TYPE).postList?.let {
                    bookmarksList.addAll(
                        it
                    )
                }
            if (bookmarksList != null && bookmarksList.size > 0)
                savedStoriesAdapter!!.setData(bookmarksList)
            else
                bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
        }
        setAdapter()

        val layoutManager = LinearLayoutManager(requireContext())
        bindingView!!.recycleViewSavedStories.layoutManager = layoutManager
        bindingView!!.recycleViewSavedStories.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        page += 1
                        isPagination = true
                        if (isOnline()) {
                            getBookmarkAPI()
                            getBookmarkObserver()
                        } else {
                            Utils.showToast(getString(R.string.internet_error))
                        }
                    }
                }
            }
        })
    }

/*    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            isPagination = false
            page = 1
            try {
                if (isOnline()) {
                    bindingView!!.imageViewNoInternet.visibility = View.GONE
                    getBookmarkAPI()
                    getBookmarkObserver()
                } else {
                    bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
            }
        }
    }*/

    private fun setAdapter() {
        savedStoriesAdapter = SavedStoriesAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                val data = `object` as GetTopStoriesModel.PostList
                if (view == 0) {
                    val intent = Intent(requireContext(), NewsDetailActivity::class.java)
                    intent.putExtra(Constants.NEWS_DETAIL_TYPE, Constants.SAVE_CATEGOTY_TYPE)
                    intent.putExtra(Constants.POSITION, position)
                    startActivity(intent)
                } else if (view == 1) {
                    //bookmark click
                    if (isOnline()) {
                        saveBookmarkAPI(data.postId!!)
                        saveBookmarkObserver(position)
                    } else
                        Utils.showToast(getString(R.string.internet_error))
                } else if (view == 2) {
                    //share click
                    shareUrl(data.postUrl, data.postTitle)
                }
            }
        })
        bindingView!!.recycleViewSavedStories.adapter = savedStoriesAdapter
    }

    private fun getBookmarkAPI() {
        if (isPagination!!) {
            bindingView!!.progressBar.visibility = View.VISIBLE
            bindingView!!.recycleViewSavedStories.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
        } else {
            bindingView!!.recycleViewSavedStories.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.startShimmerAnimation()
        }
        viewModel.getBookmark(page)
    }

    private fun getBookmarkObserver() {
        try {
            viewModel.liveData.observe(viewLifecycleOwner, {
                bindingView!!.recycleViewSavedStories.visibility = View.VISIBLE
                bindingView!!.progressBar.visibility = View.GONE
                bindingView!!.layoutShimmer.visibility = View.GONE
                bindingView!!.layoutShimmer.stopShimmerAnimation()

                val responseData = it
                if (responseData?.errorCode == 0) {
                    if (responseData.postList.isNullOrEmpty()) {
                        if (!isPagination!!) {
                            bindingView!!.recycleViewSavedStories.visibility = View.GONE
                            bindingView!!.imageViewNoData.visibility = View.VISIBLE
                        }
                    } else {
                        bindingView!!.imageViewNoData.visibility = View.GONE
                        bindingView!!.recycleViewSavedStories.visibility = View.VISIBLE
                        if (isPagination!!) {
                            val position = bookmarksList.size - 1
                            bookmarksList.addAll(responseData.postList)
                            savedStoriesAdapter!!.setData(bookmarksList, position)
                            activity?.let { it1 -> AppDatabase.getDatabase(it1) }!!
                                .getCateNewsList()
                                .inesrtUpdate(
                                    Constants.SAVE_CATEGOTY_TYPE!!,
                                    bookmarksList!!
                                )
                        } else {
                            bookmarksList.clear()
                            bookmarksList.addAll(responseData.postList)
                            savedStoriesAdapter!!.setData(bookmarksList)
                            activity?.let { it1 -> AppDatabase.getDatabase(it1) }!!
                                .getCateNewsList()
                                .inesrtUpdate(
                                    Constants.SAVE_CATEGOTY_TYPE!!,
                                    bookmarksList!!
                                )
                        }
                    }

                } else {
                    if (!isPagination!!) {
                        bindingView!!.recycleViewSavedStories.visibility = View.GONE
                        bindingView!!.imageViewNoData.visibility = View.VISIBLE
                    }
                }
            })
        }catch (ex:Exception){

        }
    }

    private fun saveBookmarkAPI(postId: String) {
        AppConfig.showProgress(requireContext())
        viewModel.saveBookmark(postId, 1)
    }

    private fun saveBookmarkObserver(position: Int) {
        viewModel.commonLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                bookmarksList.removeAt(position)
                savedStoriesAdapter!!.setData(bookmarksList)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (AppPreferences.getLoginStatus()) {
            isFirstTime = true
            isPagination = false
            page = 1
            getBookmarkAPI()
            getBookmarkObserver()
        } else {
            bindingView!!.rlNotLogin.visibility = View.VISIBLE
            bindingView!!.buttonLogin.setOnClickListener({
                startActivity(Intent(activity, LogInActivity::class.java))
                activity?.finish()
            })
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        isVisible = isVisibleToUser
        if (isVisibleToUser && isFirstTime!!) {
            _hasLoadedOnce = true
            if (_hasLoadedOnce) {
                isPagination = false
                page = 1
                _hasLoadedOnce = false
                getBookmarkAPI()
                getBookmarkObserver()
            }
        }
    }
}