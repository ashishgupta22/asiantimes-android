package com.asiantimes.ui.dashboard

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.asiantimes.ui.dashboard.categories.CategoriesFragment
import com.asiantimes.ui.dashboard.home.HomeFragment
import com.asiantimes.ui.dashboard.saved_stories.SavedStoriesFragment
import com.asiantimes.ui.dashboard.side_menu.SideMenuFragment
import com.asiantimes.ui.dashboard.video.VideoFragment


class DashboardAdapter(fragmentManager: FragmentManager?) :
    FragmentPagerAdapter(fragmentManager!!) {

    override fun getCount(): Int {
        return 5
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> HomeFragment()
            1 -> CategoriesFragment()
            2 -> VideoFragment()
            3 -> SavedStoriesFragment()
            4 -> SideMenuFragment()
            else -> HomeFragment()
        }
    }
}

