package com.asiantimes.ui.dashboard.saved_stories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import com.asiantimes.model.GetTopStoriesModel

class SavedArticleViewModel : ViewModel() {
    var liveData = MutableLiveData<GetTopStoriesModel>()
    var commonLiveData = MutableLiveData<CommonModel>()

    fun getBookmark(page: Int) {
        liveData = ApiRepository.getBookmark(page)
    }

    fun saveBookmark(postId: String,isRemoved: Int) {
        commonLiveData = ApiRepository.saveBookmark(postId,isRemoved)
    }
}