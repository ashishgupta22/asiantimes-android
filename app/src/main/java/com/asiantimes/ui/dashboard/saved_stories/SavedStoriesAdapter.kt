package com.asiantimes.ui.dashboard.saved_stories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterSavedStoriesBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class SavedStoriesAdapter(private val onClickListener: OnClickPositionViewListener) : RecyclerView.Adapter<SavedStoriesAdapter.ViewHolder>() {
    var bookmarksList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_saved_stories, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return bookmarksList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(bookmarksList[position],onClickListener)
    }

    class ViewHolder(private val bindingView: AdapterSavedStoriesBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            bookmarkData: GetTopStoriesModel.PostList,
            onClickListener: OnClickPositionViewListener
        ) {
            bindingView.setVariable(BR.bookmarkData,bookmarkData)
            bindingView.setVariable(BR.onClickListener,onClickListener)
            bindingView.setVariable(BR.position,position)
        }
    }

    fun setData(bookmarksList: ArrayList<GetTopStoriesModel.PostList>) {
        this.bookmarksList = bookmarksList
        notifyDataSetChanged()
    }
    fun setData(bookmarksList: ArrayList<GetTopStoriesModel.PostList>,position: Int) {
        this.bookmarksList = bookmarksList
        notifyItemChanged(position)
    }
}