package com.asiantimes.ui.dashboard.categories

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.Constants
import com.asiantimes.model.GetAllCategoriesModel
import com.asiantimes.ui.category_news.CategoryNewsListActivity
import com.asiantimes.ui.photo.ViewMorePhotosActivity
import kotlinx.android.synthetic.main.adapter_news_category.view.*


class CategoryAdapterNew(private var childCatList: List<GetAllCategoriesModel.Post?>) :
    RecyclerView.Adapter<CategoryAdapterNew.ViewHolder>() {
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_news_category, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val output: String = childCatList[position]!!.cat.toString()
        holder.buttonChildCat.text = output
        AppConfig.loadImage(holder.imgCate, childCatList[position]!!.image, R.drawable.dummy);
        holder.ccMain.setOnClickListener({
            if (childCatList[position]!!.cat!!.toLowerCase() == "photos") {
                val intent = Intent(context, ViewMorePhotosActivity::class.java)
                intent.putExtra("categoryID", childCatList[position]!!.catId)
                intent.putExtra("categoryName", childCatList[position]!!.cat)
                context!!.startActivity(intent)
            } else {
                val intent = Intent(context, CategoryNewsListActivity::class.java)
                intent.putExtra(Constants.CATEGORY_ID, childCatList[position]!!.catId)
                intent.putExtra(Constants.CATEGORY_NAME, childCatList[position]!!.cat)
                context!!.startActivity(intent)
            }
        })
    }

    override fun getItemCount(): Int {
        return childCatList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val buttonChildCat: TextView = itemView.button_child_cat
        val ccMain: LinearLayout = itemView.cc_main
        val imgCate: ImageView = itemView.imgCate
    }

    fun setData(catList: List<GetAllCategoriesModel.Post>) {
        this.childCatList = catList
        notifyDataSetChanged()
    }
}