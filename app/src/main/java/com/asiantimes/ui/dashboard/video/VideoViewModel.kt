package com.asiantimes.ui.dashboard.video

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import com.asiantimes.model.GetVideoListModel

class VideoViewModel : ViewModel() {
    var liveData = MutableLiveData<GetVideoListModel>()

    var commonLiveData = MutableLiveData<CommonModel>()

    fun getVideoList() {
        liveData = ApiRepository.getVideoResult()
    }

    fun getVideoCatList(catID: String, paneNo: String) {
        liveData = ApiRepository.getVideoCatResult(catID, paneNo)
    }

    fun saveBookmark(postId: String, isRemoved: Int) {
        commonLiveData = ApiRepository.saveBookmark(postId, isRemoved)
    }
}