package com.asiantimes.ui.dashboard.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterHomeNewsParentBinding
import com.asiantimes.interfaces.OnClickGroupPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class HomeNewsParentAdapter(private val OnClickListener: OnClickGroupPositionViewListener) :
    RecyclerView.Adapter<HomeNewsParentAdapter.ViewHolder>() {
    private var cateList: List<GetTopStoriesModel.CategoryList>? = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_home_news_parent,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return cateList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(OnClickListener, cateList!![position], position)
    }

    class ViewHolder(private val bindingView: AdapterHomeNewsParentBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            OnClickListener: OnClickGroupPositionViewListener,
            cateList: GetTopStoriesModel.CategoryList,
            parentPosition: Int
        ) {
            bindingView.setVariable(BR.cateList, cateList)
            val recycleViewNewsChild = bindingView.recycleViewNewsChild
            recycleViewNewsChild.adapter =
                HomeNewsChildAdapter(cateList.postList, object : OnClickGroupPositionViewListener {
                    override fun onClickPositionViewListener(
                        `object`: Any?, gropu_pos: Int, position: Int, view: Int
                    ) {
                        if (view == 0) {
                            OnClickListener.onClickPositionViewListener(
                                parentPosition,
                                gropu_pos, position,
                                view
                            )
                        } else if (view == 1) {
                            // bookmark click
                            OnClickListener.onClickPositionViewListener(
                                parentPosition,
                                gropu_pos, position,
                                view
                            )
                        } else if (view == 2) {
                            // share click
                            OnClickListener.onClickPositionViewListener(
                                parentPosition,
                                gropu_pos, position,
                                view
                            )
                        }
                    }
                },parentPosition)
        }
    }

    fun setData(cateList: List<GetTopStoriesModel.CategoryList>?) {
        this.cateList = cateList
        notifyDataSetChanged()
    }

    fun setData(cateList: List<GetTopStoriesModel.CategoryList>?, position: Int) {
        this.cateList = cateList
        notifyItemChanged(position)
    }
}