package com.asiantimes.ui.dashboard.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterHomeBreakingNewsBinding
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTopStoriesModel

class HomeBreakingNewsAdapter(private val onClickListener: OnClickPositionViewListener) :
    RecyclerView.Adapter<HomeBreakingNewsAdapter.ViewHolder>() {
    private var breakingNews: List<GetTopStoriesModel.PostList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_home_breaking_news, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return breakingNews!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (breakingNews!![position].isBookmark!!)
            holder.imageViewBookmark.setImageResource(R.drawable.ic_bookmark_fill)
        else
            holder.imageViewBookmark.setImageResource(R.drawable.ic_bookmark)
        holder.bind(breakingNews!![position], position, onClickListener)
    }

    class ViewHolder(private val bindingView: AdapterHomeBreakingNewsBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        val imageViewBookmark: AppCompatImageView = bindingView.imageViewBookmark
        fun bind(breakingNews: Any, position: Int, onClickListener: Any) {
            bindingView.setVariable(BR.breakingNews, breakingNews)
            bindingView.setVariable(BR.position, position)
            bindingView.setVariable(BR.onClickListener, onClickListener)
        }
    }

    fun setData(breakingNews: List<GetTopStoriesModel.PostList>?) {
        this.breakingNews = breakingNews
        notifyDataSetChanged()
    }
    fun setData(breakingNews: List<GetTopStoriesModel.PostList>?,position: Int) {
        this.breakingNews = breakingNews
        notifyItemChanged(position)
    }
}