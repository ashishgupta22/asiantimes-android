package com.asiantimes.ui.dashboard.home

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.asiantimes.R
import com.asiantimes.base.*
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.FragmentHomeBinding
import com.asiantimes.interfaces.OnClickGroupPositionViewListener
import com.asiantimes.interfaces.OnClickPositionViewListener
import com.asiantimes.model.GetTableTypeModel
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.ui.detail.NewsDetailActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.profile.ProfileActivity
import com.asiantimes.ui.search.SearchActivity


class HomeFragment : BaseFragment(), View.OnClickListener {
    private var bindingView: FragmentHomeBinding? = null
    private val viewModel: HomeViewModel by viewModels()
    private var homeTrendingAdapter: HomeTrendingAdapter? = null
    private var homeBreakingNewsAdapter: HomeBreakingNewsAdapter? = null
    private var homeNewsAdapter: HomeNewsParentAdapter? = null
    private var isRefreshing = false
    private var appDatabase: AppDatabase? = null
    private var breakingNewsList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()
    private var trendingNewsList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()
    private var catNewsList: ArrayList<GetTopStoriesModel.CategoryList> = ArrayList()
    private var firstTime = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (isOnline()) {
            getTopStoriesAPI()
            getTopStoriesObserver()
            bindingView!!.layoutBreakingNews.visibility = View.GONE
            bindingView!!.layoutTrendingNews.visibility = View.GONE
            bindingView!!.recycleViewNews.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.VISIBLE
            bindingView!!.layoutShimmer.startShimmerAnimation()
        } else {
            bindingView!!.layoutBreakingNews.visibility = View.GONE
            bindingView!!.layoutTrendingNews.visibility = View.GONE
            bindingView!!.recycleViewNews.visibility = View.GONE
            bindingView!!.imageViewNoData.visibility = View.GONE
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            bindingView!!.layoutSwipeRefresh.isRefreshing = false
            bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
        }
        bindingView!!.layoutSwipeRefresh.setOnRefreshListener {
            isRefreshing = true
            if (isOnline()) {
                getTopStoriesAPI()
                getTopStoriesObserver()
            } else {
                bindingView!!.layoutSwipeRefresh.isRefreshing = false
                Utils.showToast(getString(R.string.internet_error))
            }
        }

        bindingView!!.imageViewSearch.setOnClickListener(this)
        bindingView!!.imageViewProfile.setOnClickListener(this)

        val snapHelper: SnapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(bindingView!!.recycleViewBreakingNews)
    }

    override fun onResume() {
        super.onResume()
        appDatabase = AppDatabase.getDatabase(requireContext())
        if (firstTime) {
            if (AppPreferences.getLoginStatus())
                bindingView!!.userData = AppPreferences.getUserDetails()
            if (appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.BRAKING_NEWS_CATEGOTY_TYPE) != null
            ) {
                breakingNewsList = appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.BRAKING_NEWS_CATEGOTY_TYPE).postList as ArrayList<GetTopStoriesModel.PostList>
            }
            if (appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.TRADING_NEWS_CATEGOTY_TYPE) != null
            ) {
                trendingNewsList = appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.TRADING_NEWS_CATEGOTY_TYPE).postList as ArrayList<GetTopStoriesModel.PostList>
            }
            if (!breakingNewsList.isNullOrEmpty() && !trendingNewsList.isNullOrEmpty()) {
                setAdapter()
            }
            setHomeNewDatabase();
        } else {
            firstTime = true
        }
        if (!AppPreferences.getLoginStatus()) {
            bindingView!!.imageViewProfile.visibility = GONE
        }
    }

    private fun setHomeNewDatabase() {
        val catPostList = appDatabase!!.getCateNewsList()
            .getTopStoriesList_newAll(Constants.CAT_LIST_NEWS_HOME_TYPE);
        if (!catPostList.isNullOrEmpty()
        ) {
            catNewsList.clear()
            for (i in catPostList) {
                var bean = GetTopStoriesModel.CategoryList()
                bean.CatName = i.cateName
                bean.postList = i.postList
                bean.CatID = i.postCatID
                catNewsList.add(bean)
            }
            homeNewsAdapter!!.setData(catNewsList)
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.imageViewSearch -> {
                startActivity(Intent(requireContext(), SearchActivity::class.java))
            }
            bindingView!!.imageViewProfile -> {
                startActivity(Intent(requireContext(), ProfileActivity::class.java))
            }
        }
    }

    private fun getTopStoriesAPI() {
        viewModel.getTopStories()
    }

    private fun getTopStoriesObserver() {
        viewModel.liveData.observe(viewLifecycleOwner, {
            val responseData = it
            bindingView!!.layoutSwipeRefresh.isRefreshing = false
            bindingView!!.layoutShimmer.visibility = View.GONE
            bindingView!!.imageViewNoInternet.visibility = View.GONE
            bindingView!!.layoutShimmer.stopShimmerAnimation()
            bindingView!!.layoutBreakingNews.visibility = View.VISIBLE
            bindingView!!.layoutTrendingNews.visibility = View.VISIBLE
            bindingView!!.recycleViewNews.visibility = View.VISIBLE
            if (responseData?.errorCode == 0) {
//                Insert Breaking news
                appDatabase!!.getCateNewsList().inesrtUpdate(
                    Constants.BRAKING_NEWS_CATEGOTY_TYPE!!,
                    responseData.breakingNews!!
                )
                breakingNewsList = appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.BRAKING_NEWS_CATEGOTY_TYPE).postList as ArrayList<GetTopStoriesModel.PostList>

//                Insert Trading news
                val tableType2 = GetTableTypeModel()
                tableType2.tableType = Constants.TRADING_NEWS_CATEGOTY_TYPE
                tableType2.postList = responseData.trendingNews!!
                appDatabase!!.getCateNewsList().inesrtUpdate(
                    Constants.TRADING_NEWS_CATEGOTY_TYPE!!,
                    responseData.trendingNews!!
                )
                trendingNewsList = appDatabase!!.getCateNewsList()
                    .getTopStoriesList_new(Constants.TRADING_NEWS_CATEGOTY_TYPE).postList as ArrayList<GetTopStoriesModel.PostList>

                //Insert CateList
//                appDatabase!!.getHomePageNewsDao().insertCate(responseData.cateList)
//                val categoryNews = appDatabase!!.getHomePageNewsDao().getCateGoryNews()
                catNewsList = responseData.cateList!! as ArrayList<GetTopStoriesModel.CategoryList>

                appDatabase!!.getCateNewsList().inesrtUpdateNew(
                    Constants.CAT_LIST_NEWS_HOME_TYPE!!,
                    catNewsList
                )
//                val tableType3 = GetTableTypeModel()
//                tableType3.tableType = Constants.BRAKING_NEWS_CATEGOTY_TYPE
//                tableType3.postList = responseData.cateList
//                appDatabase!!.getCateNewsList().insert_new(tableType3)

                setAdapter()
            } else {
                if (isRefreshing) {
                    Utils.showToast(responseData.msg)
                } else {
                    bindingView!!.layoutBreakingNews.visibility = View.GONE
                    bindingView!!.layoutTrendingNews.visibility = View.GONE
                    bindingView!!.recycleViewNews.visibility = View.GONE
                    bindingView!!.imageViewNoInternet.visibility = View.GONE
                    bindingView!!.layoutShimmer.visibility = View.GONE
                    bindingView!!.layoutShimmer.stopShimmerAnimation()
                    bindingView!!.layoutSwipeRefresh.isRefreshing = false
                    bindingView!!.imageViewNoData.visibility = View.VISIBLE
                }

            }
        })
    }

    private fun setAdapter() {
        // breaking news adapter
        homeBreakingNewsAdapter = HomeBreakingNewsAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                val data = `object` as GetTopStoriesModel.PostList
                if (view == 0) {
                    val intent = Intent(requireContext(), NewsDetailActivity::class.java)
                    intent.putParcelableArrayListExtra(Constants.NEWS_DETAIL_DATA, breakingNewsList)
                    intent.putExtra(Constants.POSITION, position)
                    intent.putExtra("post_title", data.postCatName)
                    intent.putExtra(
                        Constants.NEWS_DETAIL_TYPE,
                        Constants.BRAKING_NEWS_CATEGOTY_TYPE
                    )
                    startActivity(intent)
                } else if (view == 1) {
                    //bookmark click
                    val isRemoved = if (!data.isBookmark!!) 0 else 1
                    saveBookmarkAPI(data.postId!!, isRemoved)
                    saveBookmarkObserver(data, Constants.BRAKING_NEWS_CATEGOTY_TYPE, 0, position)
                } else if (view == 2) {
                    shareUrl(data.postUrl, data.postTitle)
                }
            }
        })
        bindingView!!.recycleViewBreakingNews.adapter = homeBreakingNewsAdapter
        if (breakingNewsList.isNullOrEmpty())
            bindingView!!.layoutBreakingNews.visibility = View.GONE
        else {
            bindingView!!.layoutBreakingNews.visibility = View.VISIBLE
            homeBreakingNewsAdapter!!.setData(breakingNewsList)
        }


        // top trending adapter
        homeTrendingAdapter = HomeTrendingAdapter(object : OnClickPositionViewListener {
            override fun onClickPositionViewListener(`object`: Any?, position: Int, view: Int) {
                if (view == 0) {
                    val data = `object` as GetTopStoriesModel.PostList
//                    val intent = Intent(requireContext(), NewsDetailActivity::class.java)
//                    intent.putParcelableArrayListExtra(Constants.NEWS_DETAIL_DATA, trendingNewsList)
//                    intent.putExtra(Constants.POSITION, position)
//                    intent.putExtra("post_title", data.postCatName)
//                    intent.putExtra(Constants.NEWS_DETAIL_TYPE, Constants.NEWS_DETAIL_COMMAN)
//                    startActivity(intent)

                    val intent = Intent(requireContext(), NewsDetailActivity::class.java)
                    intent.putExtra(
                        Constants.NEWS_DETAIL_TYPE,
                        Constants.TRADING_NEWS_CATEGOTY_TYPE
                    )
                    intent.putExtra(Constants.NEWS_POST_CATID, data.postCatID)
                    intent.putExtra("post_title", data.postCatName)
                    intent.putExtra(Constants.POSITION, position)
                    startActivity(intent)
                }
            }
        })
        bindingView!!.recycleViewTrending.adapter = homeTrendingAdapter
        if (trendingNewsList.isNullOrEmpty())
            bindingView!!.layoutTrendingNews.visibility = View.GONE
        else {
            bindingView!!.layoutTrendingNews.visibility = View.VISIBLE
            homeTrendingAdapter!!.setData(trendingNewsList)
        }

        // all cat post list adapter
        homeNewsAdapter = HomeNewsParentAdapter(object : OnClickGroupPositionViewListener {
            override fun onClickPositionViewListener(
                `object`: Any?,
                group_post: Int,
                position: Int,
                view: Int
            ) {
                val parentPosition: Int = `object` as Int
                val data: ArrayList<GetTopStoriesModel.PostList> =
                    catNewsList[parentPosition].postList as ArrayList<GetTopStoriesModel.PostList>
                if (view == 0) {
                    val intent = Intent(requireContext(), NewsDetailActivity::class.java)
                    intent.putExtra(Constants.NEWS_DETAIL_TYPE, Constants.CAT_LIST_NEWS_HOME_TYPE)
                    intent.putExtra(Constants.NEWS_POST_CATID, data[0].postCatID)
                    intent.putExtra("post_title", data[0].postCatName)
                    intent.putExtra(Constants.POSITION, position)
                    startActivity(intent)
                } else if (view == 1) {
                    // bookmark click
                    val isRemoved = if (!data[position].isBookmark!!) 0 else 1
                    saveBookmarkAPI(data[position].postId!!, isRemoved)
                    saveBookmarkObserver(
                        data[position],
                        Constants.CAT_LIST_NEWS_HOME_TYPE,
                        group_post, position
                    )
                } else if (view == 2) {
                    // share click
                    shareUrl(data[position].postUrl, data[position].postTitle)
                }
            }
        })
        bindingView!!.recycleViewNews.adapter = homeNewsAdapter

        if (catNewsList.isNullOrEmpty())
            bindingView!!.recycleViewNews.visibility = View.GONE
        else {
            bindingView!!.recycleViewNews.visibility = View.VISIBLE
            homeNewsAdapter!!.setData(catNewsList)
        }
    }


    private fun saveBookmarkAPI(postId: String, isRemoved: Int) {
        if (checkLoginStatus()) {
            AppConfig.showProgress(requireContext())
            viewModel.saveBookmark(postId, isRemoved)
        }
    }

    private fun checkLoginStatus(): Boolean {
        if (!AppPreferences.getLoginStatus()) {
            startActivity(Intent(activity, LogInActivity::class.java))
            activity?.finish()
            return false
        } else
            return true
    }

    private fun saveBookmarkObserver(
        data: GetTopStoriesModel.PostList,
        tableType: String,
        group_post: Int, position: Int
    ) {
        viewModel.commonLiveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            Utils.showToast(responseData.msg)
            if (responseData?.errorCode == 0) {
                data.isBookmark = !data.isBookmark!!

                if (tableType == Constants.BRAKING_NEWS_CATEGOTY_TYPE) {
                    homeBreakingNewsAdapter!!.setData(breakingNewsList, position)
                    appDatabase!!.getCateNewsList().inesrtUpdate(tableType!!, breakingNewsList)
                } else if (tableType == Constants.CAT_LIST_NEWS_HOME_TYPE) {
                    setHomeNewBookmarkDatabase(data)
                    homeNewsAdapter!!.setData(catNewsList, group_post)
                }
            }
        })
    }

    private fun setHomeNewBookmarkDatabase(data: GetTopStoriesModel.PostList) {
        if (!catNewsList.isNullOrEmpty())
            appDatabase!!.getCateNewsList()
                .inesrtUpdateNew(Constants.CAT_LIST_NEWS_HOME_TYPE!!, catNewsList)
    }
}