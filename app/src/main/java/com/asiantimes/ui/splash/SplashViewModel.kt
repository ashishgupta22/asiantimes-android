package com.asiantimes.ui.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel
import com.asiantimes.model.GetAllCategoriesModel

class SplashViewModel : ViewModel() {
    var liveDataUpdateDbVersion = MutableLiveData<CommonModel>()
    var liveDataAllCategories = MutableLiveData<GetAllCategoriesModel>()

    fun updateDbVersion() {
        liveDataUpdateDbVersion = ApiRepository.updateDbVersion()
    }
    fun getAllCategories() {
        liveDataAllCategories = ApiRepository.getAllCategories()
    }
}