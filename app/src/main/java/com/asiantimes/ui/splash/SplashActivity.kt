package com.asiantimes.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Constants.SPLASH_DELAY_TIME
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivitySplashBinding
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInViewModel
import com.asiantimes.ui.welcome.WelcomeActivityActivity


class SplashActivity : BaseActivity() {
    private var bindingView: ActivitySplashBinding? = null
    private val viewModel: SplashViewModel by viewModels()
    private val viewModel1: LogInViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        Firebase.messaging.isAutoInitEnabled = true
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            Log.d("FCM Token", token.toString())
        })
        if (isOnline()) {
            updateDbVersionAPI()
            updateDbVersionObserver()
        } else {
            val localCatList = AppDatabase.getDatabase(this).getAllCategoryDao().getAllCategories()
            if (!localCatList.isNullOrEmpty())
                Handler(Looper.getMainLooper()).postDelayed({
                    if (AppPreferences.getLoginStatus()) {
                        startActivity(Intent(this, DashboardActivity::class.java))
                        finish()
                    } else {
                        startActivity(Intent(this, WelcomeActivityActivity::class.java))
                        finish()
                    }
                }, SPLASH_DELAY_TIME)
            else {
                Utils.showToast(getString(R.string.internet_error))
                bindingView!!.buttonRefresh.visibility = View.VISIBLE
            }
        }

        bindingView!!.buttonRefresh.setOnClickListener {
            if (isOnline()) {
                updateDbVersionAPI()
                updateDbVersionObserver()
                bindingView!!.buttonRefresh.visibility = View.GONE
            } else {
                Utils.showToast(getString(R.string.internet_error))
            }
        }
    }


    private fun updateDbVersionAPI() {
        bindingView!!.progressBar.visibility = View.VISIBLE
        viewModel.updateDbVersion()
    }

    private fun updateDbVersionObserver() {
        viewModel.liveDataUpdateDbVersion.observe(this, {
            val responseData = it
            bindingView!!.progressBar.visibility = View.GONE
            if (responseData?.errorCode == 0) {
                AppPreferences.setString(
                    AppPreferences.HTTP_SPLALGOVAL, responseData.specailvaltimeignore
                )
                getAllCategoriesAPI()
                getAllCategoriesObserver()
            } else {
                bindingView!!.buttonRefresh.visibility = View.VISIBLE
            }
        })
    }

    private fun getAllCategoriesAPI() {
        bindingView!!.progressBar.visibility = View.VISIBLE
        viewModel.getAllCategories()
        
    }

    private fun getAllCategoriesObserver() {
        viewModel.liveDataAllCategories.observe(this, {
            val responseData = it
            bindingView!!.progressBar.visibility = View.GONE
            if (responseData?.errorCode == 0) {
                AppDatabase.getDatabase(this).getAllCategoryDao().delete()
                AppDatabase.getDatabase(this).getAllCategoryDao().insert(responseData.postList!!)
                if (AppPreferences.getLoginStatus()) {
                    startActivity(Intent(this, DashboardActivity::class.java))
                    finish()
                } else {
                    startActivity(Intent(this, WelcomeActivityActivity::class.java))
                    finish()
                }
            } else {
                bindingView!!.buttonRefresh.visibility = View.VISIBLE
            }
        })
    }
}