package com.asiantimes.ui.welcome

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.base.BaseActivity
import com.asiantimes.databinding.ActivityWelcomeActivityBinding
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.sign_up.SignUpActivityActivity

class WelcomeActivityActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityWelcomeActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_welcome_activity)
        bindingView!!.buttonContinue.setOnClickListener(this)
        bindingView!!.buttonLogin.setOnClickListener(this)
        bindingView!!.buttonSignUp.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.buttonContinue -> {
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            }
            bindingView!!.buttonLogin -> {
                startActivity(Intent(this, LogInActivity::class.java))
            }
            bindingView!!.buttonSignUp -> {
                startActivity(Intent(this, SignUpActivityActivity::class.java))
            }
        }
    }
}