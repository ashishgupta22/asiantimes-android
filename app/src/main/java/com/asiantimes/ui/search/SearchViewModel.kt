package com.asiantimes.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.GetTopStoriesModel

class SearchViewModel : ViewModel() {
    var liveData = MutableLiveData<GetTopStoriesModel>()

    fun getSearchResult(searchText: String, page: Int) {
        liveData = ApiRepository.getSearchResult(searchText,page)
    }
}