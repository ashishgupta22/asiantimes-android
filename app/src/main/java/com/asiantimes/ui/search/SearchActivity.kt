package com.asiantimes.ui.search

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Constants
import com.asiantimes.base.Utils
import com.asiantimes.database.AppDatabase
import com.asiantimes.databinding.ActivitySearchBinding
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.model.GetTopStoriesModel
import com.asiantimes.ui.detail.NewsDetailActivity
import kotlinx.android.synthetic.main.toolbar_forms_screen.*


class SearchActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivitySearchBinding? = null
    private var searchAdapter: SearchAdapter? = null
    private var searchDataList: ArrayList<GetTopStoriesModel.PostList> = ArrayList()
    private val viewModel: SearchViewModel by viewModels()
    private var searchText: String? = null
    private var isPagination: Boolean? = null
    private var page = 1
    private var pastVisibleItems = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_search)
        text_view_title_toolbar.text = getString(R.string.search)
        image_view_back_toolbar.setOnClickListener(this)
        bindingView!!.textViewSearch.setOnClickListener(this)
        setAdapter()

        val layoutManager = LinearLayoutManager(this)
        bindingView!!.recycleViewSearch.layoutManager = layoutManager
        bindingView!!.recycleViewSearch.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        page += 1
                        isPagination = true
                        if (isOnline()) {
                            getSearchResultAPI()
                            getSearchResultObserver()
                        } else {
                            Utils.showToast(getString(R.string.internet_error))
                        }
                    }
                }
            }
        })

        bindingView!!.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String?): Boolean {
                searchText = bindingView!!.searchView.query.toString().trim()
                if (searchText!!.length < 3) {
                    Utils.showToast(getString(R.string.searching_error))
                } else if (!isOnline()) {
                    Utils.showToast(getString(R.string.internet_error))
                } else {
                    bindingView!!.root.requestFocus()
                    isPagination = false
                    page = 1
                    if (isOnline()) {
                        bindingView!!.imageViewNoInternet.visibility = View.GONE
                        getSearchResultAPI()
                        getSearchResultObserver()
                    } else {
                        bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
                    }
                }
                return true
            }

            override fun onQueryTextChange(s: String?): Boolean {
                return false
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View?) {
        when (view) {
            image_view_back_toolbar -> {
                finish()
            }
            bindingView!!.textViewSearch -> {
                searchText = bindingView!!.searchView.query.toString().trim()
                if (searchText!!.length < 3) {
                    Utils.showToast(getString(R.string.searching_error))
                } else if (!isOnline()) {
                    Utils.showToast(getString(R.string.internet_error))
                } else {
                    bindingView!!.root.requestFocus()
                    isPagination = false
                    page = 1
                    if (isOnline()) {
                        bindingView!!.imageViewNoInternet.visibility = View.GONE
                        getSearchResultAPI()
                        getSearchResultObserver()
                    } else {
                        bindingView!!.imageViewNoInternet.visibility = View.VISIBLE
                    }
                }
            }
        }

    }

    private fun setAdapter() {
        searchAdapter = SearchAdapter(object : OnClickPositionListener {
            override fun onClickPositionListener(`object`: Any?, position: Int) {
                val intent =
                    Intent(this@SearchActivity, NewsDetailActivity::class.java)
                intent.putExtra(Constants.POSITION, position)
                intent.putExtra(Constants.NEWS_DETAIL_TYPE, Constants.SEARCH_NEWS_CATEGOTY_TYPE)
                startActivity(intent)
            }
        })
        bindingView!!.recycleViewSearch.adapter = searchAdapter
    }

    private fun getSearchResultAPI() {
        if (!isPagination!!)
            AppConfig.showProgress(this)
        else
            bindingView!!.progressBar.visibility = View.VISIBLE
        viewModel.getSearchResult(searchText!!, page)
    }

    private fun getSearchResultObserver() {
        viewModel.liveData.observe(this, {
            AppConfig.hideProgress()
            bindingView!!.progressBar.visibility = View.GONE
            val responseData = it
            if (responseData?.errorCode == 0) {
                if (responseData.searchDataList.isNullOrEmpty()) {
                    if (!isPagination!!) {
                        bindingView!!.recycleViewSearch.visibility = View.GONE
                        bindingView!!.imageViewNoData.visibility = View.VISIBLE
                    }
                } else {
                    bindingView!!.imageViewNoData.visibility = View.GONE
                    bindingView!!.recycleViewSearch.visibility = View.VISIBLE
                    if (!isPagination!!) {
                        searchDataList.clear()
                        searchDataList.addAll(responseData.searchDataList)
                        searchAdapter!!.setData(searchDataList)
                        AppDatabase.getDatabase(this)!!.getCateNewsList().inesrtUpdate(
                            Constants.SEARCH_NEWS_CATEGOTY_TYPE!!,
                            searchDataList!!
                        )
                    } else {
                        val position = searchDataList.size - 1
                        searchDataList.addAll(responseData.searchDataList)
                        searchAdapter!!.setData(searchDataList, position)
                        AppDatabase.getDatabase(this)!!.getCateNewsList().inesrtUpdate(
                            Constants.SEARCH_NEWS_CATEGOTY_TYPE!!,
                            searchDataList!!
                        )
                    }
                }
            } else {
                if (!isPagination!!) {
                    bindingView!!.recycleViewSearch.visibility = View.GONE
                    bindingView!!.imageViewNoData.visibility = View.VISIBLE
                }
            }
        })
    }
}