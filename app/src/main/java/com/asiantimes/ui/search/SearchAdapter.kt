package com.asiantimes.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterSearchBinding
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.model.GetTopStoriesModel

class SearchAdapter(private val onClickListener: OnClickPositionListener) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private var searchDataList: List<GetTopStoriesModel.PostList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.adapter_search, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return searchDataList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(searchDataList!![position],onClickListener,position)
    }

    class ViewHolder(private val bindingView: AdapterSearchBinding) :
        RecyclerView.ViewHolder(bindingView.root) {
        fun bind(
            searchData: GetTopStoriesModel.PostList,
            onClickListener: OnClickPositionListener,
            position: Int
        ) {
            bindingView.setVariable(BR.searchData,searchData)
            bindingView.setVariable(BR.onClickListener,onClickListener)
            bindingView.setVariable(BR.position,position)
        }

    }

    fun setData(searchDataList: List<GetTopStoriesModel.PostList>?) {
        this.searchDataList = searchDataList
        notifyDataSetChanged()
    }

    fun setData(searchDataList: List<GetTopStoriesModel.PostList>?,position: Int) {
        this.searchDataList = searchDataList
        notifyItemChanged(position)
    }
}