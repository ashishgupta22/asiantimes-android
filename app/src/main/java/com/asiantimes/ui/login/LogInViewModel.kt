package com.asiantimes.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.CommonModel

class LogInViewModel : ViewModel() {
    var liveData = MutableLiveData<CommonModel>()
    var liveDataStr = MutableLiveData<String>()

    fun login(email: String?,password: String?) {
        liveData = ApiRepository.login(email,password)
    }
    fun loginStrr(email: String?,password: String?) {
        liveDataStr = ApiRepository.loginStr(email,password)
    }

    fun socialLogin(email: String?,name: String?,number: String?) {
        liveData = ApiRepository.socialLogin(email,name,number)
    }
}