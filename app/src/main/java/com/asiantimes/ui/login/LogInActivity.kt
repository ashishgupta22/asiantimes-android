package com.asiantimes.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import androidx.core.util.PatternsCompat.EMAIL_ADDRESS
import androidx.databinding.DataBindingUtil
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils
import com.asiantimes.databinding.ActivityLogInBinding
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.forgot_password.ForgotPasswordActivity
import com.asiantimes.ui.sign_up.SignUpActivityActivity
import org.json.JSONException


class LogInActivity : BaseActivity(), View.OnClickListener {
    private var bindingView: ActivityLogInBinding? = null
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var firebaseAuth: FirebaseAuth
    private var callbackManager: CallbackManager? = null
    private val viewModel: LogInViewModel by viewModels()
    private var name: String? = null
    private var email: String? = null
    private var password: String? = null
    private var number: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_log_in)
        bindingView!!.textViewForgotPassword.setOnClickListener(this)
        bindingView!!.buttonContinue.setOnClickListener(this)
        bindingView!!.buttonGoogle.setOnClickListener(this)
        bindingView!!.buttonFacebook.setOnClickListener(this)
        bindingView!!.cardViewSignUp.setOnClickListener(this)
        bindingView!!.edtPass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                bindingView!!.textInputLayoutPassword.isEndIconVisible = true;
            }

            override fun afterTextChanged(s: Editable) {}
        })
        googleInit()
        facebookInit()
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingView!!.cardViewSignUp -> {
                startActivity(Intent(this, SignUpActivityActivity::class.java))
                finish()
            }
            bindingView!!.textViewForgotPassword -> {
                startActivity(Intent(this, ForgotPasswordActivity::class.java))
            }
            bindingView!!.buttonContinue -> {
                email = bindingView!!.textInputLayoutEmail.editText!!.text.toString().trim()
                password = bindingView!!.textInputLayoutPassword.editText!!.text.toString().trim()
                when {
                    email!!.isEmpty() -> {
                        bindingView!!.textInputLayoutEmail.editText!!.error =
                            getString(R.string.email_error)
                        bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                    }
                    !EMAIL_ADDRESS.matcher(email.toString()).matches() -> {
                        bindingView!!.textInputLayoutEmail.editText!!.error =
                            getString(R.string.email_valid)
                        bindingView!!.textInputLayoutEmail.editText!!.requestFocus()
                    }
                    password!!.isEmpty() -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()

                    }
                    password!!.length < 6 -> {
                        bindingView!!.textInputLayoutPassword.isEndIconVisible = false;
                        bindingView!!.textInputLayoutPassword.editText!!.error =
                            getString(R.string.password_error_length)
                        bindingView!!.textInputLayoutPassword.editText!!.requestFocus()

                    }

                    !isOnline() -> {
                        Utils.showToast(getString(R.string.internet_error))
                    }
                    else -> {
                        loginAPI()
                        loginObserver()
                    }
                }
            }

            bindingView!!.buttonGoogle -> {
                if (isOnline()) {
                    val signInIntent = googleSignInClient.signInIntent
                    startActivityForResult(signInIntent, 0)
                    overridePendingTransition(0, 0)
                } else
                    Utils.showToast(getString(R.string.internet_error))
            }

            bindingView!!.buttonFacebook -> {
                if (isOnline()) {
                    LoginManager.getInstance()
                        .logInWithReadPermissions(this, listOf("public_profile", "email"))
                } else
                    Utils.showToast(getString(R.string.internet_error))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            try {
                val result: GoogleSignInResult? =
                    Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                val account: GoogleSignInAccount? = result!!.signInAccount
                if (account != null) {
                    email = account.email
                    name = account.displayName
//                val socialId = account.id
                    socialLoginAPI()
                    loginObserver()
                }
                disconnectGoogle()
            } catch (e: ApiException) {
            }
        }
    }

    private fun googleInit() {
        firebaseAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun disconnectGoogle() {
        firebaseAuth.signOut()
        googleSignInClient.signOut().addOnCompleteListener { }
    }

    private fun facebookInit() {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { userData, _ ->
                        try {
//                            val socialId = userData.getString("id")
                            if (userData.has("email")) {

                                email = userData.getString("email")
                                name = userData.getString("name")
                                /*             val profilePhoto =
                                                 URL("https://graph.facebook.com/${socialId}/picture?type=large").toString()*/
                                socialLoginAPI()
                                loginObserver()
                            } else Utils.showToast("Email not linked with your facebook account");
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        disconnectFacebook()
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,link,email")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {}
                override fun onError(error: FacebookException) {}
            })
    }

    private fun disconnectFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return
        }
        GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me/permissions/", null, HttpMethod.DELETE
        ) { LoginManager.getInstance().logOut() }.executeAsync()
    }

    private fun loginAPI() {
        AppConfig.showProgress(this@LogInActivity)
        viewModel.login(email, password)
    }

    private fun socialLoginAPI() {
        AppConfig.showProgress(this@LogInActivity)
        viewModel.socialLogin(email, name, number)
    }

    private fun loginObserver() {
        viewModel.liveData.observe(this, {
            AppConfig.hideProgress()
            val responseData = it
            if (responseData?.errorCode == 0) {
                AppPreferences.setLoginStatus(true)
                AppPreferences.setUserId(responseData.userDetail!!.userId!!)
                AppPreferences.setUserDeatils(responseData.userDetail!!)
                val intent = Intent(this, DashboardActivity::class.java)
                intent.putExtra("call_from", "main")
                startActivity(intent)
                finish()
            } else {
                AppConfig.hideProgress()
                Utils.showToast(responseData.msg)
            }
        })
    }
}