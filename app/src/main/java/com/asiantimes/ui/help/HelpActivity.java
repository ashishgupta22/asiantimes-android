package com.asiantimes.ui.help;

import android.os.Build;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.asiantimes.R;


public class HelpActivity extends AppCompatActivity {
    RelativeLayout rl_actionbar;
    TextView tv_section_help,footer_text;
    TextView tv_section_message_help;
    TextView tv_reading_help;
    TextView tv_reading_message_help;
    TextView tv_article_help;
    TextView tv_article_message_help;
    TextView tv_menu_help;
    TextView tv_menu_message_help;
    TextView tv_saving_article_help;
    TextView tv_saving_article_message_help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21)
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorButton));
        setContentView(R.layout.activity_help);
        initialView();
    }

    public void initialView() {
//        rl_actionbar = findViewById(R.id.rl_actionbar);
//
//        rl_actionbar.setBackgroundColor(getResources().getColor(R.color.colorButton));

        tv_section_help = findViewById(R.id.tv_section_help);
        tv_section_message_help = findViewById(R.id.tv_section_message_help);
        tv_reading_help = findViewById(R.id.tv_reading_help);
        tv_reading_message_help = findViewById(R.id.tv_reading_message_help);
        tv_article_help = findViewById(R.id.tv_article_help);
        tv_article_message_help = findViewById(R.id.tv_article_message_help);
        tv_menu_help = findViewById(R.id.tv_menu_help);
        tv_menu_message_help = findViewById(R.id.tv_menu_message_help);
        tv_saving_article_help = findViewById(R.id.tv_saving_article_help);
        tv_saving_article_message_help = findViewById(R.id.tv_saving_article_message_help);

    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.help_title));
    }

    @Override
    public void onBackPressed() {
        HelpActivity.this.finish();
        super.onBackPressed();
    }

}
