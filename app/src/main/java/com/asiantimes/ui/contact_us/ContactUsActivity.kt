package com.asiantimes.ui.contact_us

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.FragmentManager
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.BaseActivity
import com.asiantimes.base.Utils
import com.asiantimes.model.ConatctU
import kotlinx.android.synthetic.main.activity_contact_us.*

class ContactUsActivity : BaseActivity() {
    var getConatctUs = mutableListOf<ConatctU>()
    private val viewModel: ContactUsViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        val iv_back: ImageView = findViewById(R.id.iv_menuBack)
       val action_bar_title:TextView = findViewById(R.id.action_bar_title)
        action_bar_title.setText("Contact us")
        iv_back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        if (isOnline()) {
            getContactDetails()
        } else {
            Utils.showToast(getString(R.string.internetConnectionFailed))
        }

    }

    class PagerAdapter(
        fragmentManager: FragmentManager,
        conatctUs: MutableList<ConatctU>,
        address: String
    ) : androidx.fragment.app.FragmentStatePagerAdapter(fragmentManager) {

        var listContact = conatctUs
        var addressData = address

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return ContactFragment.newInstance(listContact.get(position).details, addressData)
        }

        override fun getCount(): Int {
            return listContact.size
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return listContact.get(position).officeName
        }
    }

    fun getContactDetails() {

        AppConfig.showProgress(this)
        viewModel.contactList();
        viewModel.liveDataUpdateDbVersion.observe(this, {
            AppConfig.hideProgress()
            val responseResult = it
            if (responseResult.errorCode == 0) {
                if (responseResult.conatctUs != null && responseResult.conatctUs!!.size > 0) {
                    getConatctUs.clear()
                    getConatctUs.addAll(responseResult.conatctUs!!)
                    viewpager.adapter = responseResult.address?.let { it1 ->
                        PagerAdapter(
                            supportFragmentManager,
                            getConatctUs,
                            it1
                        )
                    }
                    tabLayout.setupWithViewPager(viewpager)
                }
            }
        })
    }

}
