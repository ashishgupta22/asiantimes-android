package com.asiantimes.ui.contact_us

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.Utils
import com.asiantimes.model.Detail
import com.asiantimes.model.PersonDetail
import kotlinx.android.synthetic.main.contact_personal_info_recycler_list.view.*
import kotlinx.android.synthetic.main.contact_recycler_list.view.*
import kotlinx.android.synthetic.main.fragment_contact_us_pager.*
import kotlinx.android.synthetic.main.fragment_contact_us_pager.view.*
import java.util.*

class ContactFragment : Fragment() {
    private val viewModel: ContactUsViewModel by viewModels()
    companion object {
        @JvmStatic
        fun newInstance(personDetail: MutableList<Detail>, address: String): ContactFragment {
            val contactFragment = ContactFragment()
            val args = Bundle()
            args.putParcelableArrayList("personDetail", personDetail as ArrayList<out Parcelable>)
            args.putString("address", address)
            contactFragment.arguments = args
            return contactFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //return super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.fragment_contact_us_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val personDetail = requireArguments().getParcelableArrayList<Detail>("personDetail")
        val personAddress = requireArguments().getString("address")

        view.recycleView.adapter = activity?.let { ContactRecyclerAdapter(it, personDetail) }
        if (!TextUtils.isEmpty(personAddress)) {
            htvAddress.visibility = View.VISIBLE
            htvAddress.setHtml(personAddress!!)
        }
        view.btnSubmit.setOnClickListener {
            val name = etxtFirstName.text.toString().trim()
            val surname = etxtSurname.text.toString().trim()
            val number = etxtNumber.text.toString().trim()
            val email = etxtEmail.text.toString().trim()
            val address = etxtAddress.text.toString().trim()
            val message = etxtMessage.text.toString().trim()
            val isAgreeChecked = checkBox.isChecked

            if (name.isEmpty()) {
                etxtFirstName.error = "This field is required"
                etxtFirstName.requestFocus()
            } else if (surname.isEmpty()) {
                etxtSurname.error = "This field is required"
                etxtSurname.requestFocus()
            } else if (email.isEmpty()) {
                etxtEmail.error = "This field is required"
                etxtEmail.requestFocus()
            } else if (!Utils.isEmailValid(email)) {
                etxtEmail.error = "Enter valid email"
                etxtEmail.requestFocus()
            } else if (number.isEmpty()) {
                etxtNumber.error = "This field is required"
                etxtNumber.requestFocus()
            } else if (number.length < 9) {
                etxtNumber.error = "Enter valid number"
                etxtNumber.requestFocus()
            } else if (address.isEmpty()) {
                etxtAddress.error = "This field is required"
                etxtAddress.requestFocus()
            } else if (message.isEmpty()) {
                etxtMessage.error = "This field is required"
                etxtMessage.requestFocus()
            } else if (!isAgreeChecked)
                Utils.showToast("Please accept the privacy checkbox.")
            else
                submitContactDetailts(name, surname, email, number, address, message)

        }

    }


    class ContactRecyclerAdapter(val context: Context, private val personDetail: ArrayList<Detail>?) : RecyclerView.Adapter<ContactRecyclerAdapter.Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            return Holder(LayoutInflater.from(context).inflate(R.layout.contact_recycler_list, parent, false))
        }

        override fun getItemCount(): Int {
            return personDetail!!.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            if (!TextUtils.isEmpty(personDetail!![position].designation)) {
                holder.txtPost.visibility = View.VISIBLE
                holder.txtPost.text = personDetail[position].designation
            }
            if (personDetail[position].personDetail != null && personDetail[position].personDetail.size > 0) {
                holder.recycleViewpPersonalInfoList.visibility = View.VISIBLE
                holder.recycleViewpPersonalInfoList.adapter = ContactInfoRecyclerAdapter(context, personDetail[position].personDetail)
            }
        }

        class Holder(view: View) : RecyclerView.ViewHolder(view) {
            var recycleViewpPersonalInfoList: RecyclerView = itemView.recycleViewpPersonalInfoList
            var txtPost: TextView = itemView.txtPost


        }
    }

    class ContactInfoRecyclerAdapter(val context: Context, private val personDetail: MutableList<PersonDetail>) : RecyclerView.Adapter<ContactInfoRecyclerAdapter.Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            return Holder(LayoutInflater.from(context).inflate(R.layout.contact_personal_info_recycler_list, parent, false))
        }

        override fun getItemCount(): Int {
            return personDetail.size
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {

            if (!TextUtils.isEmpty(personDetail[position].name)) {
                holder.txtName.visibility = View.VISIBLE
                holder.txtName.text = personDetail[position].name
            }
            if (!TextUtils.isEmpty(personDetail[position].mobileNumber)) {
                holder.txtNumber.visibility = View.VISIBLE
                holder.txtNumber.text = personDetail[position].mobileNumber
            }
            if (!TextUtils.isEmpty(personDetail[position].emailAddress)) {
                holder.txtEmail.visibility = View.VISIBLE
                holder.txtEmail.text = personDetail[position].emailAddress
            }

        }

        class Holder(view: View) : RecyclerView.ViewHolder(view) {
            var txtName: TextView = itemView.txtName
            var txtNumber: TextView = itemView.txtNumber
            var txtEmail: TextView = itemView.txtEmail
        }
    }


    private fun submitContactDetailts(first_name: String, sur_name: String, emailAddress: String, phone_number: String, address: String, complain_detail: String) {
        try {
            AppConfig.showProgress(requireContext())
            viewModel.contactSubmit(emailAddress, first_name, sur_name, phone_number, address, complain_detail)
            viewModel.liveDataUpdateDbVersion.observe(viewLifecycleOwner,{
                AppConfig.hideProgress()
                val responseData = it
                if (responseData!!.errorCode == 0) {
                    etxtFirstName.setText("")
                    etxtSurname.setText("")
                    etxtNumber.setText("")
                    etxtEmail.setText("")
                    etxtAddress.setText("")
                    etxtMessage.setText("")
                    checkBox.isChecked = false
                    Utils.showToast(responseData.message)
                }
            })

        } catch (jex: Exception) {
            jex.printStackTrace()
        }

    }

}
