package com.asiantimes.ui.contact_us

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.api.ApiRepository
import com.asiantimes.model.ResponseResultNew

class ContactUsViewModel : ViewModel() {
    var liveDataUpdateDbVersion = MutableLiveData<ResponseResultNew>()


    fun contactSubmit(
        emailAddress: String,
        first_name: String,
        sur_name: String,
        phone_number: String,
        address: String,
        complain_detail: String
    ) {
        liveDataUpdateDbVersion = ApiRepository.submitContact(
            emailAddress,
            first_name,
            sur_name,
            phone_number,
            address,
            complain_detail
        )
    }

    fun contactList() {
        liveDataUpdateDbVersion = ApiRepository.contactList()
    }
}