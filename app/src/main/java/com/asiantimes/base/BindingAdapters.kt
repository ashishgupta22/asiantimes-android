package com.asiantimes.base

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.asiantimes.R
import com.asiantimes.model.GetTopStoriesModel
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader

object BindingAdapters {
    private var imageLoader: ImageLoader? = null

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun imageUrl(view: ImageView, imageURL: String?) {
        imageLoader = ImageLoader.getInstance()
        val options: DisplayImageOptions = DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.img_place_holder)
            .showImageForEmptyUri(R.drawable.img_place_holder)
            .showImageOnFail(R.drawable.img_place_holder)
            .cacheInMemory(true)
            .considerExifParams(true)
            .build()
        imageLoader!!.displayImage(imageURL, view, options)
    }

    @JvmStatic
    @BindingAdapter("htmlStrip")
    fun htmlStrip(view: TextView, html: String) {
        view.text = HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
    }

    @JvmStatic
    @BindingAdapter("setMargin:position", "setMargin:spanCount")
    fun setMargin(view: View, position: Int, spanCount: Int) {
        val params: ViewGroup.MarginLayoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
        params.topMargin = 5
        if (position % spanCount == 0) {
            params.marginStart = 0
            view.layoutParams = params
        } else {
            params.marginStart = 5
            view.layoutParams = params
        }
    }
    @JvmStatic
    @BindingAdapter("htmlStripDetail")
    fun htmlStripDetail(view: TextView, html: List<GetTopStoriesModel.PostDetail>) {
        var title = "";
        if(html != null && !html.isEmpty()){
            if(html.get(0).types.equals("text")){
                title = html.get(0).key.toString();
            }
        }
        view.text = HtmlCompat.fromHtml(title!!, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
    }
    @JvmStatic
    @BindingAdapter("setBookmark")
    fun setBookmark(view: ImageView, isBookmark: Boolean) {
        if (isBookmark)
            view.setImageDrawable(
                ContextCompat.getDrawable(AppConfig.getInstance()!!, R.drawable.ic_bookmark_fill)
            )
        else
            view.setImageDrawable(
                ContextCompat.getDrawable(AppConfig.getInstance()!!, R.drawable.ic_bookmark)
            )
    }

    @JvmStatic
    @BindingAdapter("setBookmarkDetails")
    fun setBookmarkNewsDetails(view: ImageView, isBookmark: Boolean) {
        if (isBookmark)
            view.setImageDrawable(
                ContextCompat.getDrawable(AppConfig.getInstance()!!, R.drawable.ic_bookmark_fill_22)
            )
        else
            view.setImageDrawable(
                ContextCompat.getDrawable(AppConfig.getInstance()!!, R.drawable.ic_bookmark_22)
            )
    }
}