@file:Suppress("DEPRECATION")

package com.asiantimes.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.graphics.*
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern


object Utils {
    val COPYRIGHT_TXT: String? = ""
    var isToastShow = false
    var isDebugModeOn = false
    /*
        * Show Toast Message
        * */
    fun showToast(message: String?) {
        Toast.makeText(AppConfig.getInstance(), message!!, Toast.LENGTH_SHORT).show()
    }

    /*
    * show snack bar message
    * */
    fun showSnackBarAlert(view: View, msg: String) {
        val snackBar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
            .setAction("OK") { }
        snackBar.show()
    }

    /*
    * get device id
    * */
    @SuppressLint("HardwareIds")
    fun getDeviceId(): String? {
        return Settings.Secure.getString(
            AppConfig.getInstance()!!.contentResolver, Settings.Secure.ANDROID_ID
        )
    }

    /*
    * get device screen height
    * */
    fun getHeight(): Int {
        val displayMetrics = DisplayMetrics()
        val context = AppConfig.getInstance() as (Activity)
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    /*
    * get device screen width
    * */
    fun getWidth(): Int {
        val displayMetrics = DisplayMetrics()
        val context = AppConfig.getInstance() as (Activity)
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

    //Convert Html text to Spanned textview
    fun htmlToSppanned(s: String?): Spanned? {
        val result: Spanned
        if (!TextUtils.isEmpty(s)) {
            result = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(s, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(s)
            }
            return result
        }
        return null
    }
    fun isEmailValid(email: String?): Boolean {
        var isValid = false
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        if (matcher.matches()) {
            isValid = true
        }
        return isValid
    }
    fun isOnline(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        if (!(activeNetworkInfo != null && activeNetworkInfo.isConnected) && !isToastShow) {
            isToastShow = true
            //            Toast.makeText(mContext,"Please check network connection and try again!",Toast.LENGTH_SHORT).show();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    fun isLandscape(context: Context): Boolean {
        return context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }
    fun isValidString(str: String?): Boolean {
        return !TextUtils.isEmpty(str)
    }
    fun isActivityDestroyed(context: Activity?): Boolean {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (context == null || context.isDestroyed) return true
            }
            if (context == null || context.isFinishing) {
                return true
            }
        } catch (e: Throwable) {
            if (isDebugModeOn) {
                e.printStackTrace()
            }
            return true
        }
        return false
    }
    fun drawTextToBitmap(gContext: Context, gResId: Int, gText: String): Bitmap? {
        val resources = gContext.resources
        val scale = resources.displayMetrics.density
        var bitmap = BitmapFactory.decodeResource(resources, gResId)
        var bitmapConfig = bitmap.config
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = Bitmap.Config.ARGB_8888
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true)
        val canvas = Canvas(bitmap)
        // new antialised Paint
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        // text color - #3D3D3D
//        paint.setColor(Color.rgb(255, 00, 00));
        paint.color = Color.parseColor("#d12f2c")
        // text size in pixels
        paint.setTextSize((44 * scale))
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE)

        // draw text to the Canvas center
        val bounds = Rect()
        paint.getTextBounds(gText, 0, gText.length, bounds)
        val x = (bitmap.width - bounds.width()) / 2
        val y = (bitmap.height + bounds.height()) / 2
        canvas.drawText(gText, x.toFloat(), y.toFloat(), paint)
        return bitmap
    }
    fun hideSoftKeyBoard(mContext: Context, view: View?) {
        var view = view
        val imm = mContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(mContext)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}