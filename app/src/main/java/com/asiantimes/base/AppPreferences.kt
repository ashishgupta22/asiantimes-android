package com.asiantimes.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.asiantimes.model.CommonModel

object AppPreferences {
    private const val msPreferences = "com.asiantimes"
    private const val LOGIN_STATUS = "login_status"
    private const val USER_ID = "user_id"
    private const val USER_IMAGE = "user_image"
    const val HTTP_SPLALGOVAL = "http_splalgoval"
    private const val fcmToken = "fcmToken";
    private const val allowVideo = "allowVideo"
    private const val FONT_SCALE = "fontScale"
    private const val breakingNewsNotifications = "breaking_news_notifications"
    private const val breakingNewsSound = "breaking_news_sound"
    private const val backgroundSyncing = "background_syncing"
    private const val backgroundOnMobileNetwork = "background_on_mobile_network"

    private const val isCarouselsLayout = "isCarouselsLayout"
    private const val isImageOffLine = "isImageOffLine"
    private const val isMobileNetwork = "isMobileNetwork"
    private const val isBackground = "isBackground"
    private const val imageOffline = "image_offline"
    private const val ImageURl = "image"
    private const val digitalTextSize = "digital_text_size"
    private const val digitalTextLineSpacing = "digital_text_line_spacing"
    private const val IsPremium = "isPremium"
    private const val isUserDigitalSubscription = "isUserDigitalSubscription"
    private const val GoldMembership =
        "GoldMembership"
    private var digitalPowerList: kotlin.String? = "digitalPowerList"
    private var digitalRichList: kotlin.String? = "digitalRichList"
    private var digitalFreeContent: kotlin.String? = "digitalFreeContent"
    private var premiumPowerList: kotlin.String? = "premiumPowerList"
    private var premiumRichList: kotlin.String? = "premiumRichList"
    private var userDetails: kotlin.String? = "userDetails"
    private val pref: SharedPreferences =
        AppConfig.getInstance()!!.getSharedPreferences(msPreferences, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = pref.edit()

    fun setBoolean(preferenceKey: String, data: Boolean) {
        editor.putBoolean(preferenceKey, data).apply()
    }

    fun setFONT_SCALE(data: String) {
        editor.putString(FONT_SCALE, data).apply()
    }

    fun getFONT_SCALE(): String? {
        return pref.getString(FONT_SCALE, "1.0")
    }

    fun getBoolean(preferenceKey: String): Boolean {
        return pref.getBoolean(preferenceKey, false)
    }


    fun setString(preferenceKey: String, data: String?) {
        editor.putString(preferenceKey, data).apply()
    }


    fun getString(preferenceKey: String): String {
        return pref.getString(preferenceKey, "")!!
    }


    fun setInteger(preferenceKey: String, data: Int) {
        editor.putInt(preferenceKey, data).apply()
    }

    fun getInteger(preferenceKey: String): Int {
        return pref.getInt(preferenceKey, 0)
    }


    fun clearData() {
        pref.edit().clear().apply()
    }


    fun setLoginStatus(data: Boolean) {
        editor.putBoolean(LOGIN_STATUS, data).apply()
    }

    fun setFcmToken(data: String) {
        editor.putString(fcmToken, data).apply()
    }

    fun getFcmToken(): String? {
        return pref.getString(fcmToken, "")
    }

    fun getLoginStatus(): Boolean {
        return pref.getBoolean(LOGIN_STATUS, false)
    }


    fun setUserId(data: String) {
        editor.putString(USER_ID, data).apply()
    }

    fun getUserId(): String? {
        return pref.getString(USER_ID, "")
    }

    fun setAllowVideo(data: String) {
        editor.putString(allowVideo, data).apply()
    }

    fun getAllowVideo(): String? {
        return pref.getString(allowVideo, "")
    }

    fun setBreakingNewsNotifications(data: String) {
        editor.putString(breakingNewsNotifications, data).apply()
    }

    fun getBreakingNewsNotifications(): String? {
        return pref.getString(breakingNewsNotifications, "")
    }

    fun setIsBackground(data: String) {
        editor.putString(isBackground, data).apply()
    }

    fun getIsBackground(): String? {
        return pref.getString(isBackground, "")
    }

    fun setIsImageOffLine(data: String) {
        editor.putString(imageOffline, data).apply()
    }

    fun getImageOffLine(): String? {
        return pref.getString(imageOffline, "")
    }

    fun setIsMobileNetwork(data: String) {
        editor.putString(isMobileNetwork, data).apply()
    }

    fun getIsMobileNetwork(): String? {
        return pref.getString(isMobileNetwork, "")
    }


    // set premium arr
    @SuppressLint("ApplySharedPref")
    fun setPremiumArr(
        GoldMembership: String?,
        digitalPowerList: String?,
        digitalRichList: String?,
        digitalFreeContent: String?,
        premiumPowerList: String?,
        premiumRichList: String?
    ) {
        editor.putString(AppPreferences.GoldMembership, GoldMembership)
        editor.putString(AppPreferences.digitalPowerList, digitalPowerList)
        editor.putString(AppPreferences.digitalRichList, digitalRichList)
        editor.putString(AppPreferences.digitalFreeContent, digitalFreeContent)
        editor.putString(AppPreferences.premiumPowerList, premiumPowerList)
        editor.putString(AppPreferences.premiumRichList, premiumRichList)
        editor.apply()
    }

    @SuppressLint("ApplySharedPref")
    fun cleanGetPremiumArr() {
        editor.remove(GoldMembership)
        editor.remove(digitalPowerList)
        editor.remove(digitalRichList)
        editor.remove(digitalFreeContent)
        editor.remove(premiumPowerList)
        editor.remove(premiumRichList)
        editor.apply()
    }

    fun getImageURl(): String? {
        return pref.getString(ImageURl, "No")
    }

    @SuppressLint("ApplySharedPref")
    fun setImageURl(imageURl: String?) {
        editor.putString(this.ImageURl, imageURl)
        editor.apply()
    }

    fun getDigitalTextLineSpacing(): String? {
        return pref.getString(digitalTextLineSpacing, "0")
    }

    @SuppressLint("ApplySharedPref")
    fun setDigitalTextLineSpacing(textSpacing: String?) {
        editor.putString(this.digitalTextLineSpacing, textSpacing)
        editor.apply()
    }


    fun getDigitalTextSize(): String? {
        return pref.getString(digitalTextSize, "0")
    }

    @SuppressLint("ApplySharedPref")
    fun setDigitalTextSize(textSize: String?) {
        editor.putString(digitalTextSize, textSize)
        editor.apply()
    }

    fun getIsPremium(): String? {
        return pref.getString(IsPremium, "f")
    }

    @SuppressLint("ApplySharedPref")
    fun setIsPremium(isPremium: String?) {
        editor.putString(this.IsPremium, isPremium)
        editor.apply()
    }

    fun getUserDigitalSubscription(): String? {
        return pref.getString(isUserDigitalSubscription, "f")
    }

    fun setUserDigitalSubscription(isSubscribe: String?) {
        editor.putString(this.isUserDigitalSubscription, isSubscribe)
        editor.apply()
    }

    fun setUserDeatils(userdetails: CommonModel.UserDetail) {
        val gson = Gson()
        var userdetaisStr: String = gson.toJson(userdetails, CommonModel.UserDetail::class.java)
        editor.putString(this.userDetails, userdetaisStr)
        editor.apply()
    }


    fun getUserDetails(): CommonModel.UserDetail? {
        val gson = Gson()
        var userdetails: CommonModel.UserDetail
        userdetails = gson.fromJson(
            pref.getString(userDetails, ""),
            CommonModel.UserDetail::class.java
        )
        return userdetails
    }
}