package com.asiantimes.base

object Constants {

    var DEVICE_TYPE: String = "ANDROID"


    // app constants
    const val SPLASH_DELAY_TIME = 1000.toLong()
    const val ANDROID = "android"
    const val CATEGORY_ID = "category_id"
    const val CATEGORY_NAME = "category_name"
    const val NEWS_DETAIL_DATA = "news_detail_data"
    const val NEWS_DETAIL_TYPE = "news_detail_type"
    const val NEWS_POST_CATID = "news_postcatId"
    const val POSITION = "position"
    const val HTTP_AUTHENTICATION_KEY = "ciVxvtAsfWU:APA91bFd"
    const val HTTP_AUTH_KEY = "t~E/x^sB9m>7CG1==k}M3N4iSmB-UY1~Om6rE{yiMwk1r=tVv}]MPdMR4Du?^77"

    //NewsDetails Type
    const val NEWS_DETAIL_COMMAN = 1
    const val NEWS_FROM_NEWS_LIST = 2
    const val NEWS_FROM_NOTIFIATION = 3
    const val NEWS_PST_CAT_ID = 4

    // Local database constants
    const val APP_DATABASE_NAME = "asian_times"
    const val USER_DATA_TABLE = "user_data_table"
    const val GET_ALL_CATEGORIES_TABLE = "get_all_categories_table"
    const val HOME_PAGE_BREAKING_NEWS_TABLE = "home_page_breaking_news_table"
    const val HOME_PAGE_TRENDING_NEWS_TABLE = "home_page_trending_news_table"
    const val HOME_PAGE_NEWS_WITH_CAT_TABLE = "home_page_news_with_cat_table"
    const val HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE = "home_page_news_with_cat_table_type"
    const val CART_MANAGE_TABLE = "cart_manage_table"

    //tableType
    const val SAVE_CATEGOTY_TYPE = "savedCategory"
    const val BRAKING_NEWS_CATEGOTY_TYPE = "brakingNews"
    const val TRADING_NEWS_CATEGOTY_TYPE = "tradingNews"
    const val SEARCH_NEWS_CATEGOTY_TYPE = "searchNews"
    const val CAT_LIST_NEWS_CATEGOTY_TYPE = "catlist_News"
    const val CAT_LIST_NEWS_HOME_TYPE = "catlist_News_Home"
}