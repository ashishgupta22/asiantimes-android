package com.asiantimes.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.google.gson.GsonBuilder
import com.asiantimes.BuildConfig
import com.asiantimes.R
import com.asiantimes.api.ApiInterface
import com.asiantimes.widget.CustomProgressDialog
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class AppConfig : Application() {

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        msApiInterface = getRequestQueue().create(ApiInterface::class.java)
        msImageLoader = getImageLoader()
    }

    companion object {
        var msApiInterface: ApiInterface? = null
        private var context: Context? = null
        private var progressDialog: CustomProgressDialog? = null
        lateinit var msImageLoader: ImageLoader
        const val ACCESS_TOKEN = "51232b15cbd4336194aeed9020ba0b4b"
        fun getInstance(): Context? {
            return context
        }

        /**
         * Image Loader
         */
        public fun getImageLoader(): ImageLoader {
            msImageLoader = ImageLoader.getInstance()
            msImageLoader.init(ImageLoaderConfiguration.createDefault(context))
            return msImageLoader
        }

        /*
        * init retrofit
        * */
        private fun getRequestQueue(): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
//            httpClient.addInterceptor(interceptor)
            httpClient.addInterceptor { chain ->
                val httpSplalgoval = AppPreferences.getString(AppPreferences.HTTP_SPLALGOVAL)
                val original = chain.request()
                val request = original.newBuilder()
//                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .header("HTTP_AUTHENTICATIONKEY", Constants.HTTP_AUTHENTICATION_KEY)
                    .header("HTTP_AUTHKEY", Constants.HTTP_AUTH_KEY)
                    .header("HTTP_SPLALGOVAL", httpSplalgoval)//HTTP_SPLALGOVAL
                    .build()
                chain.proceed(request)
            }
            httpClient.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).build()
            val gson = GsonBuilder()
                .setLenient()
                .create()
            return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build()
        }

        /*
        * Show progress bar
        * */
        fun showProgress(context: Context) {
            val text = ""
            try {
                if (progressDialog != null) {
                    if (progressDialog?.isShowing!!) {
                        progressDialog?.dismiss()
                    }
                }
                progressDialog = CustomProgressDialog(
                    context,
                    R.drawable.ic_progress
                )
                if (text.isEmpty())
                    progressDialog?.show()
                else {
                    progressDialog?.setTitle(text)
                    progressDialog?.show()
                }

            } catch (ee: Exception) {
                ee.printStackTrace()
            }
        }

        /*
        * Hide progress bar
        * */
        fun hideProgress() {
            try {
                if (progressDialog != null) {
                    if (progressDialog?.isShowing!!) {
                        progressDialog?.dismiss()
                    }
                }
            } catch (ee: Exception) {
                ee.printStackTrace()
            }
        }


        /*
        * hide keyboard
        * */
        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager = activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!
                    .windowToken, 0
            )
        }

        fun loadImage(imageView: ImageView?, url: String?, defaultResId: Int) {
            try {
                val options: DisplayImageOptions
                options = DisplayImageOptions.Builder()
                    .showImageOnLoading(defaultResId)
                    .showImageForEmptyUri(defaultResId)
                    .showImageOnFail(defaultResId)
                    .cacheInMemory(true)
                    .cacheOnDisc(true)
                    .considerExifParams(true)
                    .build()
                getImageLoader()
                    .displayImage(url, imageView, options)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

    }
}