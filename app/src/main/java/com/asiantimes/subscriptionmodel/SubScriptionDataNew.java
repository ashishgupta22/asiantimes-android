package com.asiantimes.subscriptionmodel;

import java.util.ArrayList;

/**
 * Created by Abhijeet-PC on 12-Feb-18.
 */

public class SubScriptionDataNew {
    String subscriptionCat;
    ArrayList<SubscriptionDetails> subscriptionDetails;

    public SubScriptionDataNew(String subscriptionCat, ArrayList<SubscriptionDetails> subscriptionDetails) {
        this.subscriptionCat = subscriptionCat;
        this.subscriptionDetails = subscriptionDetails;
    }

    public String getSubscriptionCat() {
        return subscriptionCat;
    }

    public void setSubscriptionCat(String subscriptionCat) {
        this.subscriptionCat = subscriptionCat;
    }

    public ArrayList<SubscriptionDetails> getSubscriptionDetails() {
        return subscriptionDetails;
    }

    public void setSubscriptionDetails(ArrayList<SubscriptionDetails> subscriptionDetails) {
        this.subscriptionDetails = subscriptionDetails;
    }
}
