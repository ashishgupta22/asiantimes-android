package com.asiantimes.subscriptionmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubSubscriptionPlan {
    @SerializedName("premiumTitle")
    @Expose
    private String premiumTitle;
    @SerializedName("premiumPrice")
    @Expose
    private String premiumPrice;
    @SerializedName("premiumId")
    @Expose
    private String premiumId;

    public String getPremiumTitle() {
        return premiumTitle;
    }

    public void setPremiumTitle(String premiumTitle) {
        this.premiumTitle = premiumTitle;
    }

    public String getPremiumPrice() {
        return premiumPrice;
    }

    public void setPremiumPrice(String premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public String getPremiumId() {
        return premiumId;
    }

    public void setPremiumId(String premiumId) {
        this.premiumId = premiumId;
    }
}
