package com.asiantimes.subscriptionmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class SubscriptionPlanData implements Parcelable{
    @SerializedName("subscriptionId")
    @Expose
    private String subscriptionId;

    @SerializedName("subscriptionTitle")
    @Expose
    private String subscriptionTitle;

    @SerializedName("isChecked")
    @Expose
    private Boolean isChecked = false;

    @SerializedName("subscriptionPrice")
    @Expose
    private Integer subscriptionPrice;
    @SerializedName("subscriptionUnit")
    @Expose
    private String subscriptionUnit;
    @SerializedName("subscriptionYearlyPrice")
    @Expose
    private String subscriptionYearlyPrice;
    @SerializedName("subscriptionYearlyUnit")
    @Expose
    private String subscriptionYearlyUnit;
    @SerializedName("subscriptionImage")
    @Expose
    private String subscriptionImage;
    @SerializedName("subPlan")
    @Expose
    private String subPlan;
    @SerializedName("subscriptionPay")
    @Expose
    private String subscriptionPay;
    @SerializedName("subscriptionDescription")
    @Expose
    private String subscriptionDescription;
    @SerializedName("subPlanUnit")
    @Expose
    private List<SubPlanUnit> subPlanUnit = null;
    @SerializedName("subPostList")
    @Expose
    private List<SubscriptionDescription> subPostList = null;


    protected SubscriptionPlanData(Parcel in) {
        subscriptionId = in.readString();
        subscriptionTitle = in.readString();
        byte tmpIsChecked = in.readByte();
        isChecked = tmpIsChecked == 0 ? null : tmpIsChecked == 1;
        if (in.readByte() == 0) {
            subscriptionPrice = null;
        } else {
            subscriptionPrice = in.readInt();
        }
        subscriptionUnit = in.readString();
        subscriptionYearlyPrice = in.readString();
        subscriptionYearlyUnit = in.readString();
        subscriptionImage = in.readString();
        subPlan = in.readString();
        subscriptionPay = in.readString();
        subscriptionDescription = in.readString();
        subPlanUnit = in.createTypedArrayList(SubPlanUnit.CREATOR);
    }

    public static final Creator<SubscriptionPlanData> CREATOR = new Creator<SubscriptionPlanData>() {
        @Override
        public SubscriptionPlanData createFromParcel(Parcel in) {
            return new SubscriptionPlanData(in);
        }

        @Override
        public SubscriptionPlanData[] newArray(int size) {
            return new SubscriptionPlanData[size];
        }
    };

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public Integer getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(Integer subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getSubscriptionUnit() {
        return subscriptionUnit;
    }

    public void setSubscriptionUnit(String subscriptionUnit) {
        this.subscriptionUnit = subscriptionUnit;
    }

    public String getSubscriptionYearlyPrice() {
        return subscriptionYearlyPrice;
    }

    public void setSubscriptionYearlyPrice(String subscriptionYearlyPrice) {
        this.subscriptionYearlyPrice = subscriptionYearlyPrice;
    }

    public Boolean getisChecked() {
        return isChecked;
    }

    public void setisChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getSubscriptionYearlyUnit() {
        return subscriptionYearlyUnit;
    }

    public void setSubscriptionYearlyUnit(String subscriptionYearlyUnit) {
        this.subscriptionYearlyUnit = subscriptionYearlyUnit;
    }

    public String getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(String subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

    public String getSubPlan() {
        return subPlan;
    }

    public void setSubPlan(String subPlan) {
        this.subPlan = subPlan;
    }

    public String getSubscriptionPay() {
        return subscriptionPay;
    }

    public void setSubscriptionPay(String subscriptionPay) {
        this.subscriptionPay = subscriptionPay;
    }

    public String getSubscriptionDescription() {
        return subscriptionDescription;
    }

    public void setSubscriptionDescription(String subscriptionDescription) {
        this.subscriptionDescription = subscriptionDescription;
    }

    public List<SubscriptionDescription> getSubPostList() {
        return subPostList;
    }

    public void setSubPostList(List<SubscriptionDescription> subPostList) {
        this.subPostList = subPostList;
    }

    public List<SubPlanUnit> getSubPlanUnit() {
        return subPlanUnit;
    }

    public void setSubPlanUnit(List<SubPlanUnit> subPlanUnit) {
        this.subPlanUnit = subPlanUnit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subscriptionId);
        parcel.writeString(subscriptionTitle);
        parcel.writeByte((byte) (isChecked == null ? 0 : isChecked ? 1 : 2));
        if (subscriptionPrice == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(subscriptionPrice);
        }
        parcel.writeString(subscriptionUnit);
        parcel.writeString(subscriptionYearlyPrice);
        parcel.writeString(subscriptionYearlyUnit);
        parcel.writeString(subscriptionImage);
        parcel.writeString(subPlan);
        parcel.writeString(subscriptionPay);
        parcel.writeString(subscriptionDescription);
        parcel.writeTypedList(subPlanUnit);
    }
}
