package com.asiantimes.subscriptionmodel;



public  class SubscriptionInAppPlanData {

    private String subscriptionId;
    private String subscriptionTitle;
    private String subscriptionPrice;
    private String subscriptionDescription;
    private String subscriptionPeriod;


    public SubscriptionInAppPlanData(String subscriptionId, String subscriptionTitle, String subscriptionPrice, String subscriptionDescription) {
        this.subscriptionId = subscriptionId;
        this.subscriptionTitle = subscriptionTitle;
        this.subscriptionPrice = subscriptionPrice;
        this.subscriptionDescription = subscriptionDescription;
    }
    public SubscriptionInAppPlanData(String subscriptionId, String subscriptionTitle, String subscriptionPrice, String subscriptionDescription, String subscriptionPeriod) {
        this.subscriptionId = subscriptionId;
        this.subscriptionTitle = subscriptionTitle;
        this.subscriptionPrice = subscriptionPrice;
        this.subscriptionDescription = subscriptionDescription;
        this.subscriptionPeriod = subscriptionPeriod;
    }
    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getSubscriptionDescription() {
        return subscriptionDescription;
    }

    public void setSubscriptionDescription(String subscriptionDescription) {
        this.subscriptionDescription = subscriptionDescription;
    }

    public String getSubscriptionPeriod() {
        return subscriptionPeriod;
    }

    public void setSubscriptionPeriod(String subscriptionPeriod) {
        this.subscriptionPeriod = subscriptionPeriod;
    }
}
