package com.asiantimes.subscriptionmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class SubscriptionDescription {
    @SerializedName("subscriptionDescription")
    @Expose
    private String subscriptionDescription;

    public String getSubscriptionDescription() {
        return subscriptionDescription;
    }

    public void setSubscriptionDescription(String subscriptionDescription) {
        this.subscriptionDescription = subscriptionDescription;
    }
}
