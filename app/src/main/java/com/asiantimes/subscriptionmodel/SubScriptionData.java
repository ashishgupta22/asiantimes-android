package com.asiantimes.subscriptionmodel;

/**
 * Created by Abhijeet-PC on 12-Feb-18.
 */

public class SubScriptionData {
    String imageUrl, subscriptionId, title, subTitle, price, subscribe, subscriptionDate;

    public SubScriptionData(String imageUrl, String subscriptionId, String title, String subTitle,
                            String price, String subscribe, String subscriptionDate) {
        this.imageUrl = imageUrl;
        this.subscriptionId = subscriptionId;
        this.title = title;
        this.subTitle = subTitle;
        this.price = price;
        this.subscribe = subscribe;
        this.subscriptionDate = subscriptionDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubrTitle() {
        return subTitle;
    }

    public void setSubrTitle(String subrTitle) {
        this.subTitle = subrTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    public String getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(String subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
