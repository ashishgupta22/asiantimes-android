package com.asiantimes.subscriptionmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubPlanUnit implements Parcelable {

    @SerializedName("subscriptionUnit")
    @Expose
    private String subscriptionUnit;
    @SerializedName("subscriptionUnitPrice")
    @Expose
    private String subscriptionUnitPrice;
    @SerializedName("subscriptionUnitKey")
    @Expose
    private String subscriptionUnitKey;

    private Boolean isChecked = false;


    protected SubPlanUnit(Parcel in) {
        subscriptionUnit = in.readString();
        subscriptionUnitPrice = in.readString();
        subscriptionUnitKey = in.readString();
        byte tmpIsChecked = in.readByte();
        isChecked = tmpIsChecked == 0 ? null : tmpIsChecked == 1;
    }

    public static final Creator<SubPlanUnit> CREATOR = new Creator<SubPlanUnit>() {
        @Override
        public SubPlanUnit createFromParcel(Parcel in) {
            return new SubPlanUnit(in);
        }

        @Override
        public SubPlanUnit[] newArray(int size) {
            return new SubPlanUnit[size];
        }
    };

    public String getSubscriptionUnit() {
        return subscriptionUnit;
    }

    public void setSubscriptionUnit(String subscriptionUnit) {
        this.subscriptionUnit = subscriptionUnit;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getSubscriptionUnitPrice() {
        return subscriptionUnitPrice;
    }

    public void setSubscriptionUnitPrice(String subscriptionUnitPrice) {
        this.subscriptionUnitPrice = subscriptionUnitPrice;
    }

    public String getSubscriptionUnitKey() {
        return subscriptionUnitKey;
    }

    public void setSubscriptionUnitKey(String subscriptionUnitKey) {
        this.subscriptionUnitKey = subscriptionUnitKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subscriptionUnit);
        parcel.writeString(subscriptionUnitPrice);
        parcel.writeString(subscriptionUnitKey);
        parcel.writeByte((byte) (isChecked == null ? 0 : isChecked ? 1 : 2));
    }
}