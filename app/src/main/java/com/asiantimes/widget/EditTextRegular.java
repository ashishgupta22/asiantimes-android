package com.asiantimes.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * Created by inte on 09-Jul-18.
 */

public class EditTextRegular extends AppCompatEditText {
    public EditTextRegular(Context context) {
        super(context);
    init();
    }

    public EditTextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();}

    public EditTextRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();}

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/utopia_regular.otf");
        setTypeface(tf);

    }
}
