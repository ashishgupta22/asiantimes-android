package com.asiantimes.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by inte on 09-Jul-18.
 */

public class TextViewRegularBold extends AppCompatTextView {
    public TextViewRegularBold(Context context) {
        super(context);
        init();
    }

    public TextViewRegularBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewRegularBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/futura_bold.ttf");
        setTypeface(tf, Typeface.BOLD);
    }
}
