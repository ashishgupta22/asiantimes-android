package com.asiantimes.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.base.Constants

class CommonModel {


    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: String? = null

    @SerializedName("versionCode")
    @Expose
    var versionCode: String? = null

    @SerializedName("specailvaltimeignore")
    @Expose
    var specailvaltimeignore: String? = null

    @SerializedName("DBversion")
    @Expose
    var DBversion: Boolean? = null

    @SerializedName("message")
    @Expose
    var childItem: ArrayList<String>? = null

    @SerializedName("userDetail")
    @Expose
    val userDetail: UserDetail? = null

    @Entity(tableName = Constants.USER_DATA_TABLE)

    class UserDetail {

        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("userName")
        @Expose
        var userName: String? = null

        @SerializedName("userId")
        @Expose
        var userId: String? = null

        @SerializedName("emailAddress")
        @Expose
        var emailAddress: String? = null

        @SerializedName("isSubscribe")
        @Expose
        var isSubscribe: Boolean? = null

        @SerializedName("mobileNumber")
        @Expose
        var mobileNumber: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("isExpired")
        @Expose
        var isExpired: Boolean? = null
    }
}