package com.asiantimes.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijeet-PC on 15-Feb-18.
 */

public class PostList {
    @SerializedName("postId")
    @Expose
    private String postId;

    @SerializedName("isPremium")
    @Expose
    private String isPremium;

    @SerializedName("premiumType")
    @Expose
    private String premiumType;

    @SerializedName("postCatColor")
    @Expose
    private String postCatColor;

    @SerializedName("postTitle")
    @Expose
    private String postTitle;

    @SerializedName("have_child")
    @Expose
    private String haveChild;

    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("postCatName")
    @Expose
    private String postCatName;

    @SerializedName("isBookmark")
    @Expose
    private String isBookmark;

    @SerializedName("shareUrl")
    @Expose
    private String shareUrl;

    @SerializedName("postCatID")
    @Expose
    private String postCatID;

    @SerializedName("postContent")
    @Expose
    private String postContent;
    @SerializedName("productList")
    @Expose
    private List<ProductList> productList = null;

    @SerializedName("child_cate_list")
    @Expose
    private List<ChildCatModel> childCateList = null;
    @SerializedName("select_cate_list")
    @Expose
    private List<PostList> select_cate_list = null;

    @Expose
    private String childCategory;
    @SerializedName("child_category")

    @Expose
    private String selectedChildCategory;
    @SerializedName("selected_child_category")

    @Expose
    private String subCatColor;
    @SerializedName("subCategory")
    @Expose
    private String subCategory;
    @SerializedName("subPostList")
    @Expose
    private List<SubPostList> subPostList = null;
    @SerializedName("cat")
    @Expose
    private String cat;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("catColor")
    @Expose
    private String catColor;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("cat_url")
    @Expose
    private String catUrl;
    @SerializedName("sub_cat_arr")
    @Expose
    private List<SubCategory> subCatArr = null;
    @SerializedName("subscriptionCat")
    @Expose
    private String subscriptionCat;
    @SerializedName("subscriptionDetails")
    @Expose
    private List<SubscriptionDetails> subscriptionDetails = null;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("categoryType")
    @Expose
    private String categoryType;
    private String headingTitle;

    private boolean adsLoad=true;


    private ArrayList<PhotoGallery> photoGallery;
    private String id;
    private String name;

    private String subscriptionId;
    private String subscriptionTitle;
    private String subscriptionDate;
    private String subscriptionImage;
    private String subscriptionPrice;
    private String subscriptionSubTitle;
    private String premium;
    private String isSetPremium;

    private String postcommentId;
    private String commentId;
    private String commentAuthor;
    private String commentTitle;
    private String commentDiscrption;
    private String commentDate;
    private String types;
    private String videoUrl;

    private Bitmap catImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatColor() {
        return catColor;
    }

    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }

    public String getimage() {
        return image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getHaveChild() {
        return haveChild;
    }

    public String getpostCatID() {
        return postCatID;
    }

    public void setpostCatID(String postCatID) {
        this.postCatID = postCatID;
    }

    public String getshareUrl() {
        return shareUrl;
    }

    public void setshareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public void setHaveChild(String haveChild) {
        this.haveChild = haveChild;
    }

    public String getPostCatName() {
        return postCatName;
    }

    public void setPostCatName(String postCatName) {
        this.postCatName = postCatName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostCatColor() {
        return postCatColor;
    }

    public void setPostCatColor(String postCatColor) {
        this.postCatColor = postCatColor;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatId() {
        return catId;
    }

    public String getCatUrl() {
        return catUrl;
    }

    public void setCatUrl(String catUrl) {
        this.catUrl = catUrl;
    }

    public List<SubCategory> getSubCatArr() {
        return subCatArr;
    }

    public void setSubCatArr(List<SubCategory> subCatArr) {
        this.subCatArr = subCatArr;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public String getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(String subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public String getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(String subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getSubscriptionSubTitle() {
        return subscriptionSubTitle;
    }

    public void setSubscriptionSubTitle(String subscriptionSubTitle) {
        this.subscriptionSubTitle = subscriptionSubTitle;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public Bitmap getCatImage() {
        return catImage;
    }

    public void setCatImage(Bitmap catImage) {
        this.catImage = catImage;
    }

    public String getPostcommentId() {
        return postcommentId;
    }

    public void setPostcommentId(String postcommentId) {
        this.postcommentId = postcommentId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentDiscrption() {
        return commentDiscrption;
    }

    public void setCommentDiscrption(String commentDiscrption) {
        this.commentDiscrption = commentDiscrption;
    }

    public String getCommentTitle() {
        return commentTitle;
    }

    public void setCommentTitle(String commentTitle) {
        this.commentTitle = commentTitle;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }


    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(String isPremium) {
        this.isPremium = isPremium;
    }

    public String getSubCatColor() {
        return subCatColor;
    }

    public void setSubCatColor(String subCatColor) {
        this.subCatColor = subCatColor;
    }

    public String getchildCategory() {
        return childCategory;
    }

    public void setchildCategory(String childCategory) {
        this.childCategory = childCategory;
    }

    public String getSelectedCategory() {
        return selectedChildCategory;
    }

    public void setSelectedCategory(String childCategory) {
        this.selectedChildCategory = selectedChildCategory;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public List<SubPostList> getSubPostList() {
        return subPostList;
    }

    public void setSubPostList(List<SubPostList> subPostList) {
        this.subPostList = subPostList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public ArrayList<PhotoGallery> getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(ArrayList<PhotoGallery> photoGallery) {
        this.photoGallery = photoGallery;
    }


    public String getIsBookmark() {
        return isBookmark;
    }

    public void setIsBookmark(String isBookmark) {
        this.isBookmark = isBookmark;
    }

    public List<ProductList> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductList> productList) {
        this.productList = productList;
    }

    public List<ChildCatModel> getChildCateList() {
        return childCateList;
    }

    public void setChildCateList(List<ChildCatModel> childCateList) {
        this.childCateList = childCateList;
    }

    public List<PostList> getSelectedCateList() {
        return select_cate_list;
    }

    public void setSelectedCateList(List<PostList> select_cate_list) {
        this.select_cate_list = select_cate_list;
    }

    public List<SubscriptionDetails> getSubscriptionDetails() {
        return subscriptionDetails;
    }

    public void setSubscriptionDetails(List<SubscriptionDetails> subscriptionDetails) {
        this.subscriptionDetails = subscriptionDetails;
    }

    public String getSubscriptionCat() {
        return subscriptionCat;
    }

    public void setSubscriptionCat(String subscriptionCat) {
        this.subscriptionCat = subscriptionCat;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getPremiumType() {
        return premiumType;
    }

    public void setPremiumType(String premiumType) {
        this.premiumType = premiumType;
    }

    public String getIsSetPremium() {
        return isSetPremium;
    }

    public void setIsSetPremium(String isSetPremium) {
        this.isSetPremium = isSetPremium;
    }

    public boolean isAdsLoad() {
        return adsLoad;
    }

    public void setAdsLoad(boolean adsLoad) {
        this.adsLoad = adsLoad;
    }

    public String getHeadingTitle() {
        return headingTitle;
    }

    public void setHeadingTitle(String headingTitle) {
        this.headingTitle = headingTitle;
    }
}
