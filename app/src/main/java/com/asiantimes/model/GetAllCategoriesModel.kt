package com.asiantimes.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.base.Constants.GET_ALL_CATEGORIES_TABLE

class GetAllCategoriesModel {

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("postList")
    @Expose
     val postList: List<Post>? = null

    @SerializedName("state")
    @Expose
     val state: String? = null

    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @Entity(tableName = GET_ALL_CATEGORIES_TABLE)
    class Post {
        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("cat")
        @Expose
        var cat: String? = null

        @SerializedName("cat_id")
        @Expose
        var catId: String? = null

        @SerializedName("catColor")
        @Expose
        var catColor: String? = null

        @SerializedName("cat_url")
        @Expose
        var catUrl: String? = null

        @SerializedName("categoryType")
        @Expose
        var categoryType: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("sub_cat_arr")
        @Expose
        var subCatArr: List<SubCatArr>? = null

        class SubCatArr {
            @SerializedName("sub_cat")
            @Expose
            var subCat: String? = null

            @SerializedName("sub_cat_id")
            @Expose
            var subCatId: Int? = null

            @SerializedName("subCatColor")
            @Expose
            var subCatColor: String? = null
        }
    }
}