package com.asiantimes.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonDetail implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("email_address")
    @Expose
    private String emailAddress;

    protected PersonDetail(Parcel in) {
        name = in.readString();
        mobileNumber = in.readString();
        emailAddress = in.readString();
    }

    public static final Creator<PersonDetail> CREATOR = new Creator<PersonDetail>() {
        @Override
        public PersonDetail createFromParcel(Parcel in) {
            return new PersonDetail(in);
        }

        @Override
        public PersonDetail[] newArray(int size) {
            return new PersonDetail[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(mobileNumber);
        dest.writeString(emailAddress);
    }
}
