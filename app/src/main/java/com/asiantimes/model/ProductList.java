package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet-PC on 30-Mar-18.
 */

public class ProductList {

    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("types")
    @Expose
    private String types;
    @SerializedName("numberComments")
    @Expose
    private String numberComments;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("shareUrl")
    @Expose
    private String shareUrl;
    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("postCatName")
    @Expose
    private String postCatName;
    @SerializedName("postImageCaption")
    @Expose
    private String postImageCaption;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("imageCaption")
    @Expose
    private String imageCaption;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getNumberComments() {
        return numberComments;
    }

    public void setNumberComments(String numberComments) {
        this.numberComments = numberComments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostCatName() {
        return postCatName;
    }

    public void setPostCatName(String postCatName) {
        this.postCatName = postCatName;
    }

    public String getPostImageCaption() {
        return postImageCaption;
    }

    public void setPostImageCaption(String postImageCaption) {
        this.postImageCaption = postImageCaption;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageCaption() {
        return imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

}
