package com.asiantimes.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestBean {

    public RequestBean() {

    }

    @SerializedName("versionType")
    @Expose
    private String versionType;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("isBreakingNew")
    @Expose
    private String isBreakingNew;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("parentCate")
    @Expose
    private String parentCate;

    @SerializedName("cat_id")
    @Expose
    private String cat_id;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;

    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("sur_name")
    @Expose
    private String sur_name;
    @SerializedName("phone_number")
    @Expose
    private String phone_number;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("complain_detail")
    @Expose
    private String complain_detail;
    @SerializedName("company_name")
    @Expose
    private String company_name;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("loginType")
    @Expose
    private String loginType;
    @SerializedName("fbUserId")
    @Expose
    private String fbUserId;

    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    @SerializedName("nextPage")
    @Expose
    private String nextPage;

    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("tokenId")
    @Expose
    private String tokenId;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("subscriptionId")
    @Expose
    private String subscriptionId;
    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private ShippingAddress shippingAddress;
    @SerializedName("order_Description")
    @Expose
    private String orderDescription;
    @SerializedName("isRemoved")
    @Expose
    private String isRemoved;
    @SerializedName("editionId")
    @Expose
    private String editionId;
    @SerializedName("searchWord")
    @Expose
    private String searchWord;
    @SerializedName("subscriptionPrice")
    @Expose
    private String subscriptionPrice;
    @SerializedName("subscriptionTitle")
    @Expose
    private String subscriptionTitle;
    @SerializedName("subscriptionID")
    @Expose
    private String subscriptionID;
    @SerializedName("subscriptionName")
    @Expose
    private String subscriptionName;
    @SerializedName("isUk")
    @Expose
    private boolean isUk;
    @SerializedName("subscriptionPlan")
    @Expose
    private String subscriptionPlan;
    @SerializedName("isFullList")
    @Expose
    private Boolean isFullList;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("productId")
    @Expose
    private String productId;

    @SerializedName("detailsTopAds")
    @Expose
    private String detailsTopAds;
    @SerializedName("detailsBottomAds")
    @Expose
    private String detailsBottomAds;
    @SerializedName("homeListAds")
    @Expose
    private String homeListAds;
    @SerializedName("homeBottomAds")
    @Expose
    private String homeBottomAds;
    @SerializedName("swipeRolloutAds")
    @Expose
    private String swipeRolloutAds;

    @SerializedName("reach_way")
    @Expose
    private String reach_way;
    @SerializedName("service")
    @Expose
    private String service;


    /*********************************************
     * *
     * ********************************************/
    public void set_cat_id(String cat_id, String nextPage, boolean isFullList) {
        this.cat_id = cat_id;
        this.nextPage = nextPage;
        this.isFullList = isFullList;
    }

    /*********************************************
     * check version of database
     * *
     * ********************************************/
    public void updateDBVersion(String versionType) {
        this.versionType = versionType;
    }

    /*********************************************
     breaking news API Parameter
     * *
     * ********************************************/
    public void breakingNewsNotification(String deviceId, String isBreakingNew, String deviceType, String userId) {
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.isBreakingNew = isBreakingNew;
        this.userId = userId;
    }

    /*********************************************
     * fotget api
     * *
     * ********************************************/
    public void forgotPassword(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /*********************************************
     *  register with mail or google plus or facebook
     * *
     * ********************************************/
    public void registerFBGooglePlus(String name, String mobileNumber, String emailAddress, String password,
                                     String deviceType, String loginType, String fbUserId, String deviceId) {
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.password = password;
        this.deviceType = deviceType;
        this.loginType = loginType;
        this.fbUserId = fbUserId;
        this.deviceId = deviceId;
    }

    /*********************************************
     * login via email and password
     * *
     * ********************************************/

    public void login(String emailAddress, String password, String deviceType, String deviceId) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.deviceType = deviceType;
        this.deviceId = deviceId;
    }

    /*********************************************
     * top story get
     * *
     * ********************************************/

    public void getTopStory(String userId) {
        this.userId = userId;
    }

    public void getPremiumData(String userId, String nextPage) {
        this.userId = userId;
        this.nextPage = nextPage;
    }


    /*********************************************
     this is oldest part to fetching single post at notificaiton time
     * *
     * ********************************************/
    //signle category Post (News section)
    public void signleCategoryPost(String categoryId, String nextPage, String userId) {
        this.categoryId = categoryId;
        this.nextPage = nextPage;
        this.userId = userId;
    }

    /*********************************************
     Get Single post when notification received from server side that is open in article page
     * *
     * ********************************************/

    public void getSingleCategory(String userId, String postId) {
        this.userId = userId;
        this.postId = postId;

    }

    /*********************************************
     //photo and video section
     * *
     * ********************************************/
    public void getPhotoVideoSectionNews(String categoryId) {
        this.categoryId = categoryId;
    }

    /*********************************************
     //get comments list
     **********************************************/

    public void getCommentsList(String userId, String postId, String nextPage) {
        this.userId = userId;
        this.postId = postId;
        this.nextPage = nextPage;
    }

    /*********************************************
     //get comments reply
     **********************************************/

    public void getCommentsReply(String postId, String userId, String comment) {
        this.postId = postId;
        this.userId = userId;
        this.comment = comment;
    }

    /*********************************************
     //subscription
     **********************************************/
    public void setSubscriptionPayment(String userId, String tokenId, String totalAmount, String currency,
                                       String subscriptionId, BillingAddress billingAddress,
                                       ShippingAddress shippingAddress, String orderDescription) {
        this.userId = userId;
        this.tokenId = tokenId;
        this.totalAmount = totalAmount;
        this.currency = currency;
        this.subscriptionId = subscriptionId;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.orderDescription = orderDescription;
    }

    /*********************************************
     //set Saved Article or bookmark articles
     **********************************************/
    public void setSavedArticles(String postId, String userId, String isRemoved) {
        this.postId = postId;
        this.userId = userId;
        this.isRemoved = isRemoved;
    }


    /*
     * get post list by sub category request
     * */
    public void getPostListBySubCategory(String categoryId, String userId, String parentCate) {
        this.categoryId = categoryId;
        this.userId = userId;
        this.parentCate = parentCate;
    }

    /*********************************************
     search article
     **********************************************/
    public void searchArticles(String userId, String searchWord) {
        this.userId = userId;
        this.searchWord = searchWord;
    }

    /*********************************************
     premium subscription
     **********************************************/
    public void setPremiumSubscription(String subscriptionId, String subscriptionPrice, String subscriptionTitle, String tokenId, String userId) {
        this.subscriptionId = subscriptionId;
        this.subscriptionPrice = subscriptionPrice;
        this.subscriptionTitle = subscriptionTitle;
        this.tokenId = tokenId;
        this.userId = userId;
    }

    /*********************************************
     get subscription plan list
     **********************************************/

    public void setSubscription(String userId) {
        this.userId = userId;
    }

    /*********************************************
     **********************************************/
    public void setSubscriptionDetails(String subscriptionName, boolean isUk,String deviceType,String type) {
        this.subscriptionName = subscriptionName;
        this.isUk = isUk;
        this.deviceType = deviceType;
        this.type = type;
    }

    /*********************************************
     **********************************************/
    public void setShippingPrice(String subscriptionId,String subscriptionTitle, String subscriptionPlan, boolean isUk) {
        this.subscriptionId = subscriptionId;
        this.subscriptionTitle = subscriptionTitle;
        this.subscriptionPlan = subscriptionPlan;
        this.isUk = isUk;
    }

    /*********************************************
     **********************************************/
    public void setSpecialSubscrition(String subscriptionID,String deviceType) {
        this.subscriptionID = subscriptionID;
        this.deviceType = deviceType;

    }

    /*********************************************
     **********************************************/
    public void setOrderPlaced(String transactionId, String productId, String deviceType, String userId) {
        this.transactionId = transactionId;
        this.productId = productId;
        this.deviceType = deviceType;
        this.userId = userId;
    }

    /*********************************************
     **********************************************/

    public void setUserData(String userId, String deviceType) {
        this.deviceType = deviceType;
        this.userId = userId;
    }

    /*********************************************
     check condition where ads will load or not
     **********************************************/
    public void setAdsLoad(String userId, String detailsTopAds, String detailsBottomAds, String homeListAds, String homeBottomAds, String swipeRolloutAds) {
        this.userId = userId;
        this.detailsTopAds = detailsTopAds;
        this.detailsBottomAds = detailsBottomAds;
        this.homeListAds = homeListAds;
        this.homeBottomAds = homeBottomAds;
        this.swipeRolloutAds = swipeRolloutAds;
    }


    public void getVersionType(String versionType) {
        this.versionType = versionType;
    }

    public void getDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void getDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setContactRequest(String emailAddress, String first_name, String sur_name, String phone_number, String address, String complain_detail) {
        this.emailAddress = emailAddress;
        this.first_name = first_name;
        this.sur_name = sur_name;
        this.phone_number = phone_number;
        this.address = address;
        this.complain_detail = complain_detail;
    }

    public void setDigitalEditionBookmarks(String userId) {
        this.userId = userId;
    }

    public void setDigitalEditionPostList(String userId, String productId) {
        this.userId = userId;
        this.productId = productId;
    }

    public void setDigitalEditionSearchResult(String searchWord, String userId, String productId) {
        this.searchWord = searchWord;
        this.userId = userId;
        this.productId = productId;
    }
    public void setDigitalEditionAddBookmark(String postId, String userId, String isRemoved, String editionId) {
        this.postId = postId;
        this.userId = userId;
        this.editionId = editionId;
        this.isRemoved = isRemoved;
    }

    public void setDigitalEditionBuySingleProduct(String subscriptionId, String productId, String deviceType, String userId, String email) {
        this.subscriptionId = subscriptionId;
        this.productId = productId;
        this.deviceType = deviceType;
        this.userId = userId;
        this.email = email;
    }
    public void setDigitalEditionList(String userId, String deviceToken, String deviceType) {
        this.userId = userId;
        this.deviceToken = deviceToken;
        this.deviceType = deviceType;
    }
}
