package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet-PC on 16-Feb-18.
 */

public class SubCategory {

    @SerializedName("sub_cat")
    @Expose
    private String subCat;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("subCatColor")
    @Expose
    private String subCatColor;

    String mainCatId;
    int id;

    public String getMainCatId() {
        return mainCatId;
    }

    public void setMainCatId(String mainCatId) {
        this.mainCatId = mainCatId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubCat() {
        return subCat;
    }

    public void setSubCat(String subCat) {
        this.subCat = subCat;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubCatColor() {
        return subCatColor;
    }

    public void setSubCatColor(String subCatColor) {
        this.subCatColor = subCatColor;
    }
}
