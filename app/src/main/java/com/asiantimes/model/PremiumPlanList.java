package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.asiantimes.subscriptionmodel.SubSubscriptionPlan;

import java.util.List;

/**
 * Created on 07-Mar-18.
 */

public class PremiumPlanList {

    @SerializedName("premiumId")
    @Expose
    private String premiumId;
    @SerializedName("premiumTitle")
    @Expose
    private String premiumTitle;
    @SerializedName("premiumDescription")
    @Expose
    private String premiumDescription;
    @SerializedName("premiumPrice")
    @Expose
    private String premiumPrice;
    @SerializedName("premiumDays")
    @Expose
    private String premiumDays;
    @SerializedName("subSubscriptionPlan")
    @Expose
    private List<SubSubscriptionPlan> subSubscriptionPlan = null;

    public String getPremiumId() {
        return premiumId;
    }

    public void setPremiumId(String premiumId) {
        this.premiumId = premiumId;
    }

    public String getPremiumTitle() {
        return premiumTitle;
    }

    public void setPremiumTitle(String premiumTitle) {
        this.premiumTitle = premiumTitle;
    }

    public String getPremiumPrice() {
        return premiumPrice;
    }

    public void setPremiumPrice(String premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public String getPremiumDays() {
        return premiumDays;
    }

    public void setPremiumDays(String premiumDays) {
        this.premiumDays = premiumDays;
    }

    public String getPremiumDescription() {
        return premiumDescription;
    }

    public void setPremiumDescription(String premiumDescription) {
        this.premiumDescription = premiumDescription;
    }

    public List<SubSubscriptionPlan> getSubSubscriptionPlan() {
        return subSubscriptionPlan;
    }

    public void setSubSubscriptionPlan(List<SubSubscriptionPlan> subSubscriptionPlan) {
        this.subSubscriptionPlan = subSubscriptionPlan;
    }
}
