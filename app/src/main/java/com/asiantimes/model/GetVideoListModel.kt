package com.asiantimes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetVideoListModel {

    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("newsList")
    @Expose
    var newsList: List<NewsListBean?>? = null

    @SerializedName("catVideoList")
    @Expose
    var catVideoList: List<NewsListBean?>? = null

    @SerializedName("trandList")
    @Expose
    var trandList: List<TrandListBean?>? = null

    @SerializedName("videoSubCatList")
    @Expose
    var videoSubCatList: List<VideoSubCatListBean?>? = null

    @SerializedName("state")
    @Expose
    var state: String? = null


    class NewsListBean : Serializable {
        @SerializedName("CatColor")
        @Expose
        var catColor: String? = null

        @SerializedName("CatName")
        @Expose
        var catName: String? = null

        @SerializedName("catId")
        @Expose
        var catId: Int? = null

        @SerializedName("videoUrl")
        @Expose
        var videoUrl: String? = null

        @SerializedName("time")
        @Expose
        var time: String? = null

        @SerializedName("isPremium")
        @Expose
        var isPremium: String? = null

        @SerializedName("postContent")
        @Expose
        var postContent: String? = null

        @SerializedName("postId")
        @Expose
        var postId: String? = null

        @SerializedName("postDate")
        @Expose
        var postDate: String? = null

        @SerializedName("postTitle")
        @Expose
        var postTitle: String? = null

        @SerializedName("postImage")
        @Expose
        var postImage: String? = null
    }

    class TrandListBean : Serializable {
        @SerializedName("CatColor")
        @Expose
        var catColor: String? = null

        @SerializedName("CatName")
        @Expose
        var catName: String? = null

        @SerializedName("catId")
        @Expose
        var catId: Int? = null

        @SerializedName("videoUrl")
        @Expose
        var videoUrl: String? = null

        @SerializedName("time")
        @Expose
        var time: String? = null

        @SerializedName("isPremium")
        @Expose
        var isPremium: String? = null

        @SerializedName("postContent")
        @Expose
        var postContent: String? = null

        @SerializedName("postId")
        @Expose
        var postId: String? = null

        @SerializedName("postDate")
        @Expose
        var postDate: String? = null

        @SerializedName("postTitle")
        @Expose
        var postTitle: String? = null

        @SerializedName("postImage")
        @Expose
        var postImage: String? = null
    }

    class VideoSubCatListBean : Serializable {
        @SerializedName("subCatColor")
        @Expose
        var subCatColor: String? = null

        @SerializedName("subCategory")
        @Expose
        var subCategory: String? = null

        @SerializedName("catId")
        @Expose
        var catId: Int? = null

        @SerializedName("catImage")
        @Expose
        var catImage: String? = null
    }

}