package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoGallery {

    @SerializedName("shareUrl")
    @Expose
    private String shareUrl;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("postTitle")
    @Expose
    private String postTitle;
    @SerializedName("postContent")
    @Expose
    private String postContent;

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }
}
