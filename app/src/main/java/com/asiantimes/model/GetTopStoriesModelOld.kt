package com.asiantimes.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.base.Constants
import java.io.Serializable


class GetTopStoriesModelOld : Serializable {

    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("breakingnews")

    @Expose
    val breakingNews: List<PostList>? = null

    @SerializedName("postList")
    @Expose
    val postList: List<PostList>? = null

    @SerializedName("PostList")
    @Expose
    val PostLista: List<PostList>? = null

    @SerializedName("headlinestories")
    @Expose
    val trendingNews: List<PostList>? = null

    @SerializedName("CateList")
    @Expose
    val cateList: List<CategoryList>? = null


    @SerializedName("searchDataList")
    @Expose
    val searchDataList: List<PostList>? = null


    @Entity(tableName = Constants.HOME_PAGE_BREAKING_NEWS_TABLE)
    class PostList() : Serializable {
        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("msg")
        @Expose
        var msg: String? = null

        @SerializedName("errorCode")
        @Expose
        var errorCode: Int? = null

        @SerializedName("postId")
        @Expose
        var postId: String? = null

        @SerializedName("postUrl")
        @Expose
        var postUrl: String? = null

        @SerializedName("isPremium")
        @Expose
        var isPremium: Boolean? = null

        @SerializedName("postTitle")
        @Expose
        var postTitle: String? = null

        @SerializedName("postDate")
        @Expose
        var postDate: String? = null

        @SerializedName("postImage")
        @Expose
        var postImage: String? = null

        @SerializedName("postCatName")
        @Expose
        var postCatName: String? = null

        @SerializedName("isBookmark")
        @Expose
        var isBookmark: Boolean? = null

        @SerializedName("post_content")
        @Expose
        var postDetail: List<PostDetail>? = null

    }

    @Entity(tableName = Constants.HOME_PAGE_NEWS_WITH_CAT_TABLE)
    class CategoryList() {
        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("CatID")
        @Expose
        var CatID: String? = null

        @SerializedName("CatName")
        @Expose
        var CatName: String? = null

        @SerializedName("CatColor")
        @Expose
        var CatColor: String? = null

        @SerializedName("postList")
        @Expose
        var postList: List<PostList>? = null

    }

    class PostDetail() : Parcelable {
        @SerializedName("key")
        @Expose
        var key: String? = null

        @SerializedName("types")
        @Expose
        var types: String? = null

        constructor(parcel: Parcel) : this() {
            key = parcel.readString()
            types = parcel.readString()
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(key)
            parcel.writeString(types)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<PostDetail> {
            override fun createFromParcel(parcel: Parcel): PostDetail {
                return PostDetail(parcel)
            }

            override fun newArray(size: Int): Array<PostDetail?> {
                return arrayOfNulls(size)
            }
        }
    }
}