package com.asiantimes.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.asiantimes.base.Constants;

/**
 * Created by DMK-PC on 16-Jul-21.
 */
@Entity(tableName = Constants.CART_MANAGE_TABLE)
public class CartItem {

    @PrimaryKey(autoGenerate = true)
    public int autoGenerateId  = 0;

    String subscriptionID, subscriptionTitle, subscriptionDate, subscriptionSubtitle, subscriptionPrice,
            subscriptionImage;
    int subscriptionCount, id;

    public String getSubscriptionID() {
        return subscriptionID;
    }

    public void setSubscriptionID(String subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubscriptionCount() {
        return subscriptionCount;
    }

    public void setSubscriptionCount(int subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    public String getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(String subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public String getSubscriptionSubtitle() {
        return subscriptionSubtitle;
    }

    public void setSubscriptionSubtitle(String subscriptionSubtitle) {
        this.subscriptionSubtitle = subscriptionSubtitle;
    }

    public String getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(String subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }
}
