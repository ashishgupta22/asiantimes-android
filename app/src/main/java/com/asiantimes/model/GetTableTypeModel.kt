package com.asiantimes.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.base.Constants

@Entity(tableName = Constants.HOME_PAGE_NEWS_WITH_CAT_TABLE_TYPE)
class GetTableTypeModel() {

    @PrimaryKey(autoGenerate = true)
    var autoGenerateId: Int = 0

    @SerializedName("postList")
    @Expose
    var postList: List<GetTopStoriesModel.PostList>? = null

    @ColumnInfo(name = "tableType")
    var tableType: String? = null

    @ColumnInfo(name = "cateName")
    var cateName: String? = null

    @ColumnInfo(name = "postCatID")
    var postCatID: String? = null
}