package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionDetails {

    @SerializedName("subscriptionSubTitle")
    @Expose
    private String subscriptionSubTitle;
    @SerializedName("subscriptionId")
    @Expose
    private String subscriptionId;
    @SerializedName("subscriptionTitle")
    @Expose
    private String subscriptionTitle;
    @SerializedName("subscriptionDate")
    @Expose
    private String subscriptionDate;
    @SerializedName("subscriptionImage")
    @Expose
    private String subscriptionImage;
    @SerializedName("subscriptionPrice")
    @Expose
    private String subscriptionPrice;

    boolean isChecked;

    public String getSubscriptionSubTitle() {
        return subscriptionSubTitle;
    }

    public void setSubscriptionSubTitle(String subscriptionSubTitle) {
        this.subscriptionSubTitle = subscriptionSubTitle;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public String getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(String subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public String getSubscriptionImage() {
        return subscriptionImage;
    }

    public void setSubscriptionImage(String subscriptionImage) {
        this.subscriptionImage = subscriptionImage;
    }

    public String getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public void setSubscriptionPrice(String subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
