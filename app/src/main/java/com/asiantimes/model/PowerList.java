package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PowerList {
    @SerializedName("postId")
    @Expose
    private Integer postId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("rankPerson")
    @Expose
    private String rankPerson;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("sector")
    @Expose
    private List<String> sector = null;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("postContent")
    @Expose
    private String postContent;
    @SerializedName("rankingYear")
    @Expose
    private String rankingYear;
    @SerializedName("quote")
    @Expose
    private String quote;
    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("postImage")
    @Expose
    private String postImage;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRankPerson() {
        return rankPerson;
    }

    public void setRankPerson(String rankPerson) {
        this.rankPerson = rankPerson;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getSector() {
        return sector;
    }

    public void setSector(List<String> sector) {
        this.sector = sector;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getRankingYear() {
        return rankingYear;
    }

    public void setRankingYear(String rankingYear) {
        this.rankingYear = rankingYear;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }


}
