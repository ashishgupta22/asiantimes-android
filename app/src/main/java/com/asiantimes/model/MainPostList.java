package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Abhijeet-PC on 06-Mar-18.
 */

public class MainPostList implements Serializable {
    @SerializedName("postCatColor")
    @Expose
    private String postCatColor;
    @SerializedName("isPremium")
    @Expose
    private String isPremium;
    @SerializedName("postId")
    @Expose
    private Integer postId;
    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("postTitle")
    @Expose
    private String postTitle;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("photoGallery")
    @Expose
    private List<PhotoGallery> photoGallery = null;

    public String getPostCatColor() {
        return postCatColor;
    }

    public void setPostCatColor(String postCatColor) {
        this.postCatColor = postCatColor;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(String isPremium) {
        this.isPremium = isPremium;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public List<PhotoGallery> getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(List<PhotoGallery> photoGallery) {
        this.photoGallery = photoGallery;
    }
}
