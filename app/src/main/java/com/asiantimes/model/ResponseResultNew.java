package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijeet-PC on 15-Dec-17.
 */

public class ResponseResultNew {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("code")
    @Expose
    private Integer code;


    @SerializedName("contactUs")
    @Expose
    private List<ConatctU> conatctUs = null;
    @SerializedName("address")
    @Expose
    private String address;

    public ResponseResultNew(String status, int errorCode) {
        this.status = status;
        this.errorCode = errorCode;
    }

    private String message;
    private String nextPage;
    private String versionCode;

    ArrayList<PhotoGallery> photoGallery;


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public int getCode() {
        return this.code;
    }


    public ArrayList<PhotoGallery> getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(ArrayList<PhotoGallery> photoGallery) {
        this.photoGallery = photoGallery;
    }




    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }
    public List<ConatctU> getConatctUs() {
        return conatctUs;
    }

    public void setConatctUs(List<ConatctU> conatctUs) {
        this.conatctUs = conatctUs;
    }

    public String getAddress() {
        return address;
    }


}
