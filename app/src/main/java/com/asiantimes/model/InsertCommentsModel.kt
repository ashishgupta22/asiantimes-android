package com.asiantimes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Abhijeet-PC on 05-Feb-18.
 */
class InsertCommentsModel(
) {
    constructor(
        name: String?, comments: String?, time: String?, postcommentId: String?, commentId: String?,
        commentTitle: String?, shareUrl: String?
    ) : this() {
        this.name = name
        this.time = time
        this.comments = comments
        this.postcommentId = postcommentId
        this.commentId = commentId
        this.commentTitle = commentTitle
        this.shareUrl = shareUrl
    }

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("comments")
    @Expose
    private var comments: String? = null

    @SerializedName("time")
    @Expose
    private var time: String? = null

    @SerializedName("postcommentId")
    @Expose
    private var postcommentId: String? = null

    @SerializedName("commentId")
    @Expose
    private var commentId: String? = null

    @SerializedName("commentTitle")
    @Expose
    private var commentTitle: String? = null

    @SerializedName("shareUrl")
    @Expose
    private var shareUrl: String? = null


    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getComments(): String? {
        return comments
    }

    fun setComments(comments: String?) {
        this.comments = comments
    }

    fun getTime(): String? {
        return time
    }

    fun setTime(time: String?) {
        this.time = time
    }

    fun getPostcommentId(): String? {
        return postcommentId
    }

    fun setPostcommentId(postcommentId: String?) {
        this.postcommentId = postcommentId
    }

    fun getCommentTitle(): String? {
        return commentTitle
    }

    fun setCommentTitle(commentTitle: String?) {
        this.commentTitle = commentTitle
    }

    fun getCommentId(): String? {
        return commentId
    }

    fun setCommentId(commentId: String?) {
        this.commentId = commentId
    }

    fun getShareUrl(): String? {
        return shareUrl
    }

    fun setShareUrl(shareUrl: String?) {
        this.shareUrl = shareUrl
    }

}