package com.asiantimes.model;

import java.util.ArrayList;

/**
 * Created by inte on 08-Mar-18.
 */

public class ViewMorePhoto {
    String title, image, postId;
    ArrayList<PhotoGallery> photoGallery;

    public ViewMorePhoto(String title, String image, String postId, ArrayList<PhotoGallery> photoGallery) {
        this.title = title;
        this.image = image;
        this.postId = postId;
        this.photoGallery = photoGallery;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public ArrayList<PhotoGallery> getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(ArrayList<PhotoGallery> photoGallery) {
        this.photoGallery = photoGallery;
    }
}
