package com.asiantimes.model;

import java.io.Serializable;

public class PlanListSerialize implements Serializable {

    private String planName;
    private String id;

    public PlanListSerialize(String planName, String id) {
        this.planName = planName;
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

