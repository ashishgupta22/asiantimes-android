package com.asiantimes.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inte on 06-Mar-18.
 */

public class PhotoCategory {
    String subCatColor, subCategory, catId;
    List<SubPostList> subPostList;

    public PhotoCategory(String subCatColor, String subCategory, String catId, List<SubPostList> subPostList) {
        this.subCatColor = subCatColor;
        this.subCategory = subCategory;
        this.subPostList = subPostList;
        this.catId = catId;
    }

    public String getSubCatColor() {
        return subCatColor;
    }

    public void setSubCatColor(String subCatColor) {
        this.subCatColor = subCatColor;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public List<SubPostList> getSubPostList() {
        return subPostList;
    }

    public void setSubPostList(ArrayList<SubPostList> subPostList) {
        this.subPostList = subPostList;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }
}
