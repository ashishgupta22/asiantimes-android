package com.asiantimes.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConatctU implements Parcelable {

    @SerializedName("office_name")
    @Expose
    private String officeName;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    protected ConatctU(Parcel in) {
        officeName = in.readString();
    }

    public static final Creator<ConatctU> CREATOR = new Creator<ConatctU>() {
        @Override
        public ConatctU createFromParcel(Parcel in) {
            return new ConatctU(in);
        }

        @Override
        public ConatctU[] newArray(int size) {
            return new ConatctU[size];
        }
    };

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(officeName);
    }
}
