package com.asiantimes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Detail implements Parcelable {
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("person_details")
    @Expose
    private List<PersonDetail> personDetail = null;

    protected Detail(Parcel in) {
        designation = in.readString();
        personDetail = in.createTypedArrayList(PersonDetail.CREATOR);
    }

    public static final Creator<Detail> CREATOR = new Creator<Detail>() {
        @Override
        public Detail createFromParcel(Parcel in) {
            return new Detail(in);
        }

        @Override
        public Detail[] newArray(int size) {
            return new Detail[size];
        }
    };

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public List<PersonDetail> getPersonDetail() {
        return personDetail;
    }

    public void setPersonDetail(List<PersonDetail> personDetail) {
        this.personDetail = personDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(designation);
        parcel.writeTypedList(personDetail);
    }
}
