package com.asiantimes.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.subscriptionmodel.SubscriptionPlanData
import java.util.*

/**
 * Created by Abhijeet-PC on 15-Dec-17.
 */
class ResponseResult : Parcelable {
    @SerializedName("status")
    @Expose
    var status: String?

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int?  = 0

    @SerializedName("code")
    @Expose
    var code: Int? = null

    @SerializedName("postList")
    @Expose
    var postList: List<PostList>? = null

    @SerializedName("catpostlist")
    @Expose
    private var catpostlist: List<PostList>? = null

    @SerializedName("inActivepostList")
    @Expose
    var inActivepostList: List<PostList>? = null

    @SerializedName("powerList")
    @Expose
    var powerList: List<PowerList> = ArrayList()

    @SerializedName("mainPostList")
    @Expose
    private var mainPostList: List<MainPostList>? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    //    public List<SubscriptionPlanData> getSubscriptionList() {
    //        return subscriptionList;
    //    }
    //
    //    public void setSubscriptionList(List<SubscriptionPlanData> subscriptionList) {
    //        this.subscriptionList = subscriptionList;
    //    }

    @SerializedName("GoldMembership")
    @Expose
    var goldMembership: String? = null

    @SerializedName("digitalPowerList")
    @Expose
    var digitalPowerList: String? = null

    @SerializedName("digitalRichList")
    @Expose
    var digitalRichList: String? = null

    @SerializedName("subscriptionList")
    @Expose
    private var subscriptionList: List<SubscriptionPlanData>? = null

    @SerializedName("digitalFreeContent")
    @Expose
    var digitalFreeContent: String? = null

    @SerializedName("premiumPowerList")
    @Expose
    var premiumPowerList: String? = null

    @SerializedName("premiumRichList")
    @Expose
    var premiumRichList: String? = null

    @SerializedName("userDetail")
    @Expose
    var userDetail: CommonModel.UserDetail? = null

    fun getSubscriptionList(): List<SubscriptionPlanData?>? {
        return subscriptionList
    }

    fun setSubscriptionList(subscriptionList: List<SubscriptionPlanData?>?) {
        this.subscriptionList = subscriptionList as List<SubscriptionPlanData>?
    }
    //
    //    public List<WidgetCategoryList> getWidgetCategoryList() {
    //        return widgetCategoryList;
    //    }
    //
    //    public void setWidgetCategoryList(ArrayList<WidgetCategoryList> widgetCategoryList) {
    //        this.widgetCategoryList = widgetCategoryList;
    //    }
    //
    //    @SerializedName("headlinestories")
    //    @Expose
    //    private List<HeadLinestories> headlinestories = null;
    //
    //    @SerializedName("breakingnews")
    //    @Expose
    //    private BreakingNews breakingnews = null;
    //    @SerializedName("widgetCategoryList")
    //    @Expose
    //    private List<WidgetCategoryList> widgetCategoryList = null;
    //    @SerializedName("isRemoved")
    //    @Expose
    //    private String isRemoved;
    //    @SerializedName("premiumPlanList")
    //    @Expose
    //    private List<PremiumPlanList> premiumPlanList = null;
    //    @SerializedName("premiumId")
    //    @Expose
    //    private List<String> premiumId = null;

    @SerializedName("DBversion")
    @Expose
    var dBVersion: String? = null

    @SerializedName("aboutus")
    @Expose
    var aboutus: String? = null

    @SerializedName("content")
    @Expose
    var content: String? = null

    //    public void setWidgetCategoryList(List<WidgetCategoryList> widgetCategoryList) {
    //        this.widgetCategoryList = widgetCategoryList;
    //    }
    //
    //    public BreakingNews getbreakingnews() {
    //        return breakingnews;
    //    }
    //
    //    public void setbreakingnews(BreakingNews breakingnews) {
    //        this.breakingnews = breakingnews;
    //    }
    //    public List<String> getPremiumId() {
    //        return premiumId;
    //    }
    //
    //    public void setPremiumId(List<String> premiumId) {
    //        this.premiumId = premiumId;
    //    }
    @SerializedName("planList")
    @Expose
    private var planList: List<PlanList>? = null

    //
    //    @SerializedName("subscriptionList")
    //    @Expose
    //    private List<SubscriptionPlanData> subscriptionList = null;
    // ads getter
    @SerializedName("detailsBottomAds")
    @Expose
    private var detailsBottomAds: String? = null

    @SerializedName("detailsTopAds")
    @Expose
    private var detailsTopAds: String? = null

    @SerializedName("homeBottomAds")
    @Expose
    private var homeBottomAds: String? = null

    @SerializedName("swipeRolloutAds")
    @Expose
    private var swipeRolloutAds: String? = null

    @SerializedName("homeListAds")
    @Expose
    private var homeListAds: String? = null

    @SerializedName("specailvaltimeignore")
    @Expose
    private var specailvaltimeignore: String? = null

    @SerializedName("conatctUs")
    @Expose
    var conatctUs: List<ConatctU>? = null

    @SerializedName("address")
    @Expose
    val address: String? = null

    constructor(status: String?, errorCode: Int) {
        this.status = status
        this.errorCode = errorCode
    }

    private var message: String? = null
    private var nextPage: String? = null
    private var versionCode: String? = null


    protected constructor(`in`: Parcel) {
        status = `in`.readString()
        errorCode = if (`in`.readByte().toInt() == 0) {
            null
        } else {
            `in`.readInt()
        }
        code = if (`in`.readByte().toInt() == 0) {
            null
        } else {
            `in`.readInt()
        }
        state = `in`.readString()
        goldMembership = `in`.readString()
        digitalPowerList = `in`.readString()
        digitalRichList = `in`.readString()
        digitalFreeContent = `in`.readString()
        premiumPowerList = `in`.readString()
        premiumRichList = `in`.readString()
        //        isRemoved = in.readString();
//        premiumId = in.createStringArrayList();
        dBVersion = `in`.readString()
        aboutus = `in`.readString()
        content = `in`.readString()
        detailsBottomAds = `in`.readString()
        detailsTopAds = `in`.readString()
        homeBottomAds = `in`.readString()
        swipeRolloutAds = `in`.readString()
        homeListAds = `in`.readString()
        message = `in`.readString()
        nextPage = `in`.readString()
        versionCode = `in`.readString()
    }


    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMessage(): String? {
        return message
    }

    fun setErrorCode(errorCode: Int) {
        this.errorCode = errorCode
    }

    fun setCode(code: Int) {
        this.code = code
    }

    fun getErrorCode(): Int {
        return errorCode!!
    }

    fun getCode(): Int {
        return code!!
    }

    @JvmName("setErrorCode1")
    fun setErrorCode(errorCode: Int?) {
        this.errorCode = errorCode
    }

    fun getCatPostList(): List<PostList>? {
        return catpostlist
    }

    fun setCatPostList(catpostlist: List<PostList>?) {
        this.catpostlist = catpostlist
    }

    fun getNextPage(): String? {
        return nextPage
    }

    fun setNextPage(nextPage: String?) {
        this.nextPage = nextPage
    }

    fun getMainPostList(): List<MainPostList?>? {
        return mainPostList
    }

    fun setMainPostList(mainPostList: List<MainPostList>) {
        this.mainPostList = mainPostList
    }
    //    public List<MainPostList> getMainPostList() {
    //        return mainPostList;
    //    }
    //
    //    public void setMainPostList(List<MainPostList> mainPostList) {
    //        this.mainPostList = mainPostList;
    //    }
    //
    //    public List<PremiumPlanList> getPremiumPlanList() {
    //        return premiumPlanList;
    //    }
    //
    //    public void setPremiumPlanList(List<PremiumPlanList> premiumPlanList) {
    //        this.premiumPlanList = premiumPlanList;
    //    }
//    fun getPhotoGallery(): ArrayList<PhotoGallery>? {
//        return photoGallery
//    }
//
//    fun setPhotoGallery(photoGallery: ArrayList<PhotoGallery>?) {
//        this.photoGallery = photoGallery
//    }

    fun getVersionCode(): String? {
        return versionCode
    }

    fun setVersionCode(versionCode: String?) {
        this.versionCode = versionCode
    }

//    fun setPlanList(planList: List<PlanList>?) {
//        field = planList
//    }

//    @JvmName("getConatctUs1")
//    fun getConatctUs(): List<ConatctU>? {
//        return conatctUs
//    }
//
//    @JvmName("setConatctUs1")
//    fun setConatctUs(conatctUs: List<ConatctU>?) {
//        this.conatctUs = conatctUs
//    }

    @JvmName("getAddress1")
    fun getAddress(): String? {
        return address
    }

    fun getDetailsTopAds(): String? {
        return detailsTopAds
    }

    fun setDetailsTopAds(detailsTopAds: String?) {
        this.detailsTopAds = detailsTopAds
    }

    fun getHomeBottomAds(): String? {
        return homeBottomAds
    }

    fun setHomeBottomAds(homeBottomAds: String?) {
        this.homeBottomAds = homeBottomAds
    }

    fun getSwipeRolloutAds(): String? {
        return swipeRolloutAds
    }

    fun setSwipeRolloutAds(swipeRolloutAds: String?) {
        this.swipeRolloutAds = swipeRolloutAds
    }

    fun getHomeListAds(): String? {
        return homeListAds
    }

    fun setHomeListAds(homeListAds: String?) {
        this.homeListAds = homeListAds
    }

    fun getDetailsBottomAds(): String? {
        return detailsBottomAds
    }

    fun setDetailsBottomAds(detailsBottomAds: String?) {
        this.detailsBottomAds = detailsBottomAds
    }

    fun getSpecailvaltimeignore(): String? {
        return specailvaltimeignore
    }

    fun setSpecailvaltimeignore(specailvaltimeignore: String?) {
        this.specailvaltimeignore = specailvaltimeignore
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(status)
        if (errorCode == null) {
            dest.writeByte(0.toByte())
        } else {
            dest.writeByte(1.toByte())
            dest.writeInt(errorCode!!)
        }
        if (code == null) {
            dest.writeByte(0.toByte())
        } else {
            dest.writeByte(1.toByte())
            dest.writeInt(code!!)
        }
        dest.writeString(state)
        dest.writeString(goldMembership)
        dest.writeString(digitalPowerList)
        dest.writeString(digitalRichList)
        dest.writeString(digitalFreeContent)
        dest.writeString(premiumPowerList)
        dest.writeString(premiumRichList)
        //        dest.writeString(isRemoved);
//        dest.writeStringList(premiumId);
        dest.writeString(dBVersion)
        dest.writeString(aboutus)
        dest.writeString(content)
        dest.writeString(detailsBottomAds)
        dest.writeString(detailsTopAds)
        dest.writeString(homeBottomAds)
        dest.writeString(swipeRolloutAds)
        dest.writeString(homeListAds)
        dest.writeString(message)
        dest.writeString(nextPage)
        dest.writeString(versionCode)
    }

    //    companion object {
//        val CREATOR: Parcelable.Creator<ResponseResult> =
//            object : Parcelable.Creator<ResponseResult?> {
//                override fun createFromParcel(`in`: Parcel): ResponseResult? {
//                    return ResponseResult(`in`)
//                }
//
//                override fun newArray(size: Int): Array<ResponseResult?> {
//                    return arrayOfNulls(size)
//                }
//            }
//    }
    companion object CREATOR : Parcelable.Creator<ResponseResult> {
        override fun createFromParcel(parcel: Parcel): ResponseResult {
            return ResponseResult(parcel)
        }

        override fun newArray(size: Int): Array<ResponseResult?> {
            return arrayOfNulls(size)
        }
    }

    fun getPlanList(): List<PlanList?>? {
        return planList
    }

    fun setPlanList(planList: List<PlanList?>?) {
        this.planList = planList as List<PlanList>?
    }

}