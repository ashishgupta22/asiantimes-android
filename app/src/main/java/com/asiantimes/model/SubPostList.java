package com.asiantimes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhijeet-PC on 06-Mar-18.
 */

public class SubPostList {

    @SerializedName("isBookmark")
    @Expose
    private String isBookmark;
    @SerializedName("productList")
    @Expose
    private List<ProductList> productList = null;
    @SerializedName("videoUrl")
    @Expose
    private String videoUrl;
    @SerializedName("subCategory")
    @Expose
    private String subCategory;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("isPremium")
    @Expose
    private String isPremium;
    @SerializedName("premiumType")
    @Expose
    private String premiumType;
    @SerializedName("postContent")
    @Expose
    private String postContent;

    private String postDescription;

    @SerializedName("postCatColor")
    @Expose
    private String postCatColor;
    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("postTitle")
    @Expose
    private String postTitle;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("types")
    @Expose
    private String types;

    @SerializedName("photoGallery")
    @Expose
    private List<PhotoGallery> photoGallery = null;

    @SerializedName("shareUrl")
    @Expose
    private String shareUrl;


    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    String productItemList;

    public String getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(String isPremium) {
        this.isPremium = isPremium;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostCatColor() {
        return postCatColor;
    }

    public void setPostCatColor(String postCatColor) {
        this.postCatColor = postCatColor;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public List<PhotoGallery> getPhotoGallery() {
        return photoGallery;
    }

    public void setPhotoGallery(List<PhotoGallery> photoGallery) {
        this.photoGallery = photoGallery;
    }

    public String getProductItemList() {
        return productItemList;
    }

    public void setProductItemList(String productItemList) {
        this.productItemList = productItemList;
    }

    public String getIsBookmark() {
        return isBookmark;
    }

    public void setIsBookmark(String isBookmark) {
        this.isBookmark = isBookmark;
    }

    public List<ProductList> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductList> productList) {
        this.productList = productList;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getPremiumType() {
        return premiumType;
    }

    public void setPremiumType(String premiumType) {
        this.premiumType = premiumType;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getShareUrl() {
        return shareUrl;
    }
}
