package com.asiantimes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginModel {


    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("status")
    @Expose
    var status: String? = null


    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("userDetail")
    @Expose
    val userDetail: UserDetail? = null


    class UserDetail {

        @SerializedName("userName")
        @Expose
        var userName: String? = null

        @SerializedName("userId")
        @Expose
        var userId: String? = null

        @SerializedName("emailAddress")
        @Expose
        var emailAddress: String? = null

        @SerializedName("isSubscribe")
        @Expose
        var isSubscribe: Boolean? = null

        @SerializedName("mobileNumber")
        @Expose
        var mobileNumber: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null
    }
}