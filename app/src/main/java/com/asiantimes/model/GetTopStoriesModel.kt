package com.asiantimes.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.asiantimes.base.Constants
import java.io.Serializable


class GetTopStoriesModel : Serializable {

    @PrimaryKey(autoGenerate = true)
    var autoGenerateId: Int = 0

    @SerializedName("msg")
    @Expose
    var msg: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("breakingnews")
    @Expose
    val breakingNews: List<PostList>? = null

    @SerializedName("postList")
    @Expose
    val postList: List<PostList>? = null

    @SerializedName("PostList")
    @Expose
    val PostLista: List<PostList>? = null

    @SerializedName("headlinestories")
    @Expose
    val trendingNews: List<PostList>? = null

    @SerializedName("CateList")
    @Expose
    val cateList: List<CategoryList>? = null


    @SerializedName("searchDataList")
    @Expose
    val searchDataList: List<PostList>? = null


    @Entity(tableName = Constants.HOME_PAGE_BREAKING_NEWS_TABLE)
    class PostList() : Parcelable {
        //
        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("msg")
        @Expose
        var msg: String? = null

        @SerializedName("errorCode")
        @Expose
        var errorCode: Int? = null
        @SerializedName("numberComments")
        @Expose
        var numberComments: Int? = 0

        @ColumnInfo(name = "postId")
        @SerializedName("postId")
        @Expose
        var postId: String? = null

        @SerializedName("postCatID")
        @Expose
        var postCatID: String? = null

        @SerializedName("postCatColor")
        @Expose
        var postCatColor: String? = null

        @SerializedName("postUrl")
        @Expose
        var postUrl: String? = null

        @SerializedName("isPremium")
        @Expose
        var isPremium: Boolean? = false

        @SerializedName("postTitle")
        @Expose
        var postTitle: String? = null

        @SerializedName("postDate")
        @Expose
        var postDate: String? = null

        @SerializedName("postImage")
        @Expose
        var postImage: String? = null

        @SerializedName("postCatName")
        @Expose
        var postCatName: String? = null

        @ColumnInfo(name = "isBookmark")
        @SerializedName("isBookmark")
        @Expose
        var isBookmark: Boolean? = false

        @SerializedName("post_content")
        @Expose
        var postDetail: List<PostDetail>? = null

        constructor(parcel: Parcel) : this() {
            autoGenerateId = parcel.readInt()
            msg = parcel.readString()
            errorCode = parcel.readValue(Int::class.java.classLoader) as? Int
            numberComments = parcel.readValue(Int::class.java.classLoader) as? Int
            postId = parcel.readString()
            postCatColor = parcel.readString()
            postCatID = parcel.readString()
            postUrl = parcel.readString()
            isPremium = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
            postTitle = parcel.readString()
            postDate = parcel.readString()
            postImage = parcel.readString()
            postCatName = parcel.readString()
            isBookmark = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
            postDetail = parcel.createTypedArrayList(PostDetail)
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(autoGenerateId)
            parcel.writeString(msg)
            parcel.writeValue(errorCode)
            parcel.writeValue(numberComments)
            parcel.writeString(postId)
            parcel.writeString(postCatColor)
            parcel.writeString(postCatID)
            parcel.writeString(postUrl)
            parcel.writeValue(isPremium)
            parcel.writeString(postTitle)
            parcel.writeString(postDate)
            parcel.writeString(postImage)
            parcel.writeString(postCatName)
            parcel.writeValue(isBookmark)
            parcel.writeTypedList(postDetail)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<PostList> {
            override fun createFromParcel(parcel: Parcel): PostList {
                return PostList(parcel)
            }

            override fun newArray(size: Int): Array<PostList?> {
                return arrayOfNulls(size)
            }
        }
    }

    @Entity(tableName = Constants.HOME_PAGE_NEWS_WITH_CAT_TABLE)
    class CategoryList() : Parcelable {
        @PrimaryKey(autoGenerate = true)
        var autoGenerateId: Int = 0

        @SerializedName("CatID")
        @Expose
        var CatID: String? = null

        @SerializedName("CatName")
        @Expose
        var CatName: String? = null

        @SerializedName("CatColor")
        @Expose
        var CatColor: String? = null

        @SerializedName("postList")
        @Expose
        var postList: List<PostList>? = null

        constructor(parcel: Parcel) : this() {
            autoGenerateId = parcel.readInt()
            CatID = parcel.readString()
            CatName = parcel.readString()
            CatColor = parcel.readString()
            postList = parcel.createTypedArrayList(PostList)
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(autoGenerateId)
            parcel.writeString(CatID)
            parcel.writeString(CatName)
            parcel.writeString(CatColor)
            parcel.writeTypedList(postList)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<CategoryList> {
            override fun createFromParcel(parcel: Parcel): CategoryList {
                return CategoryList(parcel)
            }

            override fun newArray(size: Int): Array<CategoryList?> {
                return arrayOfNulls(size)
            }
        }

    }

    class PostDetail() : Parcelable {
        @SerializedName("key")
        @Expose
        var key: String? = null

        @SerializedName("types")
        @Expose
        var types: String? = null

        constructor(parcel: Parcel) : this() {
            key = parcel.readString()
            types = parcel.readString()
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(key)
            parcel.writeString(types)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<PostDetail> {
            override fun createFromParcel(parcel: Parcel): PostDetail {
                return PostDetail(parcel)
            }

            override fun newArray(size: Int): Array<PostDetail?> {
                return arrayOfNulls(size)
            }
        }
    }
}