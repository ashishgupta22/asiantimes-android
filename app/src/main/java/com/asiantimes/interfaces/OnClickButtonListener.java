package com.asiantimes.interfaces;

/**
 * Created by DMK-PC on 07-Jul-2021.
 */

public interface OnClickButtonListener {
    void onButtonClick(Object object);
}
