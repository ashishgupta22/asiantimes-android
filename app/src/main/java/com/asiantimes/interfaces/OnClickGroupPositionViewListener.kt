package com.asiantimes.interfaces

interface OnClickGroupPositionViewListener {
    fun onClickPositionViewListener(`object`: Any?, group_position: Int, position: Int, view: Int)
}
