package com.asiantimes.subscription

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.hideProgress
import com.asiantimes.base.AppConfig.Companion.showProgress
import com.asiantimes.base.AppPreferences.getUserId
import com.asiantimes.base.Utils.showToast
import com.asiantimes.model.PlanListSerialize
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.viewmodel.SubscriptionHomeViewModel
import com.asiantimes.widget.TextViewRegular
import im.delight.android.location.SimpleLocation
import java.io.IOException
import java.io.Serializable
import java.util.*

class SubscriptionHomePageActivity : AppCompatActivity(), View.OnClickListener {
    var progress_bar: ProgressBar? = null
    private var yes_continue: TextViewRegular? = null
    private var visit_uk_site: TextViewRegular? = null
    private var action_bar_title: TextViewRegular? = null
    private var iv_menuBack: ImageView? = null
    private val planListSerializes: MutableList<PlanListSerialize> = ArrayList()
    private var location: SimpleLocation? = null
    var lattitude: String? = null
    var longitude: String? = null
    var address: String? = null
    var country: String? = null
    var hidesubscriptionpage: RelativeLayout? = null
    private var callFrom: String? = null
    var viewModel = SubscriptionHomeViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_home)
        callFrom = intent.getStringExtra("CALL_FROM")
        initializationView()
        if (checkAndRequestPermissions()) {
            Handler().postDelayed({
                location = SimpleLocation(this@SubscriptionHomePageActivity)
                if (!location!!.hasLocationEnabled()) {
                    // ask the user to enable location access
                    buildAlertMessageNoGps()
                } else {
                    //   country = prefs.getString("country", null);
                    getLocation()
                }
            }, 2500)
        }
    }

    fun initializationView() {
        yes_continue = findViewById(R.id.yes_continue)
        visit_uk_site = findViewById(R.id.visit_uk_site)
        action_bar_title = findViewById(R.id.action_bar_title)
        action_bar_title!!.setText("Subscription")
        hidesubscriptionpage = findViewById(R.id.hidesubscriptionpage)
        iv_menuBack = findViewById(R.id.iv_menuBack)
        progress_bar = findViewById(R.id.progress_bar)
        yes_continue!!.setOnClickListener(this)
        visit_uk_site!!.setOnClickListener(this)
        iv_menuBack!!.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.yes_continue -> setSubscriptionList(false)
            R.id.visit_uk_site -> setSubscriptionList(true)
            R.id.iv_menuBack -> onBackPressed()
        }
    }

    fun setSubscriptionList(isuk: Boolean) {
        showProgress(this)
        val requestBean = RequestBean()
        requestBean.setSubscription(getUserId())
        viewModel.getSubScriptionPlan(requestBean).observe(this, { responseResult ->
            hideProgress()
            planListSerializes.clear()
            if (responseResult.getErrorCode() === 0) {
                for (i in responseResult.getPlanList()!!.indices) {
                    val planListSerialize = PlanListSerialize(
                        responseResult.getPlanList()!![i]!!.planName,
                        responseResult.getPlanList()!![i]!!.id
                    )
                    planListSerializes.add(planListSerialize)
                }
                val subscription_intent =
                    Intent(this@SubscriptionHomePageActivity, Subscription_Plan_Page::class.java)
                subscription_intent.putExtra(
                    "subscription_plan",
                    planListSerializes as Serializable
                )
                subscription_intent.putExtra("isuk", isuk)
                subscription_intent.putExtra("callFrom", callFrom)
                startActivity(subscription_intent)
                finish()
            } else showToast(getString(R.string.internetConnectionFailed))
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        getLocation()
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PERMISSION_REQUEST_CODE) {
            when (requestCode) {
                PERMISSION_REQUEST_CODE -> {
                    if (checkAndRequestPermissions()) {
                        Handler().postDelayed({
                            location = SimpleLocation(this@SubscriptionHomePageActivity)
                            if (!location!!.hasLocationEnabled()) {
                                // ask the user to enable location access
                                buildAlertMessageNoGps()
                            } else {
                                //   country = prefs.getString("country", null);
                                getLocation()
                            }
                        }, 2500)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // make the device update its location
        if (location != null) {
            location!!.beginUpdates()
        }
    }

    override fun onPause() {
        // stop location updates (saves battery)
        if (location != null) {
            location!!.endUpdates()
        }
        super.onPause()
    }

    protected fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("You have to turn on your GPS connection.")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                startActivityForResult(
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                    PERMISSION_REQUEST_CODE
                )
            }
        try {
            val alert = builder.create()
            alert.show()
        } catch (e: Exception) {
        }
    }

    private fun getLocation() {
        progress_bar!!.isIndeterminate = true
        if (location != null) {
            val latti = location!!.latitude
            val longi = location!!.longitude
            if (latti != 0.0 && longi != 0.0) {
                lattitude = latti.toString()
                longitude = longi.toString()
                val geocoder: Geocoder
                var addresses: List<Address>? = ArrayList()
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        latti,
                        longi,
                        1
                    ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null && addresses.size > 0) {
                        address =
                            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        country = addresses[0].countryName
                        val editor = getSharedPreferences("getlocation", MODE_PRIVATE).edit()
                        editor.putString("lat", lattitude)
                        editor.putString("lng", longitude)
                        editor.putString("country", country)
                        editor.putString("address", address)
                        editor.apply()
                    } else {
                        progress_bar!!.visibility = View.GONE
                        //     Toast.makeText(this, "We are unable to get your current location.Please select manually!", Toast.LENGTH_SHORT).show();
                        hidesubscriptionpage!!.visibility = View.VISIBLE
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                val prefs = getSharedPreferences("getlocation", MODE_PRIVATE)
                val country = prefs.getString("country", null)
                if (!TextUtils.isEmpty(country)) {
                    if (country!!.toLowerCase().contains("uk") || country.equals(
                            "United Kingdom",
                            ignoreCase = true
                        )
                    ) {
                        setSubscriptionList(true)
                    } else {
                        progress_bar!!.visibility = View.GONE
                        hidesubscriptionpage!!.visibility = View.VISIBLE
                    }
                }
            } else {
                progress_bar!!.visibility = View.GONE
                hidesubscriptionpage!!.visibility = View.VISIBLE
            }
        } else {
            progress_bar!!.visibility = View.GONE
            hidesubscriptionpage!!.visibility = View.VISIBLE
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val FineLocPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val CoarseLocPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (FineLocPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (CoarseLocPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                PERMISSION_REQUEST_CODE
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        // Log.d(TAG, "Permission callback called-------");
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                val perms: MutableMap<String, Int> = HashMap()
                // Initialize the map with both permissions
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_COARSE_LOCATION] =
                    PackageManager.PERMISSION_GRANTED
                /*perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);*/
                // Fill with actual results from user
                if (grantResults.size > 0) {
                    var i = 0
                    while (i < permissions.size) {
                        perms[permissions[i]] = grantResults[i]
                        i++
                    }
                    // Check for both permissions
                    if (perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.ACCESS_COARSE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    ) {
                        // --------------process the normal flow-------------------------
                        Handler().postDelayed({
                            location = SimpleLocation(this@SubscriptionHomePageActivity)
                            if (!location!!.hasLocationEnabled()) {
                                // ask the user to enable location access
                                buildAlertMessageNoGps()
                            } else {
                                //   country = prefs.getString("country", null);
                                getLocation()
                            }
                        }, 2500)


                        //-----------------else any one or both the permissions are not granted-------------------------------------------------
                    } else {
                        // Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) || ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            )
                        ) {
                            showDialogOK(
                                "External Storage access permissions required for this app"
                            ) { dialog, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                    DialogInterface.BUTTON_NEGATIVE -> getLocation()
                                }
                            }
                        } else {
                            Toast.makeText(
                                this,
                                "Go to settings and enable permissions",
                                Toast.LENGTH_LONG
                            )
                                .show()
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, PERMISSION_REQUEST_CODE)
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }
    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("ok", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }

    companion object {
        private const val PERMISSION_REQUEST_CODE = 1
    }
}