package com.asianmedia.easterneye.adapter.subscription

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.subscriptionmodel.SubscriptionPlanData

import kotlinx.android.synthetic.main.adapter_subscriptions.view.*

class SubscriptionListAdapter(private val onClickPositionListener: OnClickPositionListener) : RecyclerView.Adapter<SubscriptionListAdapter.ViewHolder>() {
    private var context: Context? = null
    private var subscriptionPlanData: ArrayList<SubscriptionPlanData?> = ArrayList()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.adapter_subscriptions, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (subscriptionPlanData!![position]!!.getisChecked() != null && subscriptionPlanData!![position]!!.getisChecked()) {
            holder.textViewSubscriptionPlan.background = ContextCompat.getDrawable(context!!, R.drawable.bg_black_round_corner)
            holder.textViewSubscriptionPlan.setTextColor(context!!.getColor(R.color.colorWhite))
        } else {
            holder.textViewSubscriptionPlan.background = ContextCompat.getDrawable(context!!, R.drawable.border_black_round_corner)
            holder.textViewSubscriptionPlan.setTextColor(context!!.getColor(R.color.colorBlack))
        }


        holder.textViewSubscriptionPlan.text = subscriptionPlanData!![position]!!.subscriptionTitle
        holder.textViewSubscriptionPlan.setOnClickListener {
            for (i in subscriptionPlanData!!.indices) {
                subscriptionPlanData!![i]!!.setisChecked(false)
            }

            subscriptionPlanData!![position]!!.setisChecked(true)
            notifyDataSetChanged()
            onClickPositionListener.onClickPositionListener(subscriptionPlanData!![position], position)
        }
    }

    override fun getItemCount(): Int {
        return subscriptionPlanData!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewSubscriptionPlan: TextView = itemView.textViewSubscriptionPlan
    }

    fun setData(subscriptionPlanData: ArrayList<SubscriptionPlanData?>) {
        this.subscriptionPlanData = subscriptionPlanData
        notifyDataSetChanged()
    }

}