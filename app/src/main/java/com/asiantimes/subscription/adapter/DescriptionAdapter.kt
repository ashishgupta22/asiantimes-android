package com.asianmedia.easterneye.adapter.subscription

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.subscriptionmodel.SubscriptionDescription
import kotlinx.android.synthetic.main.adapter_subscription_dscription.view.*


class DescriptionAdapter : RecyclerView.Adapter<DescriptionAdapter.ViewHolder>() {
    private var context: Context? = null
    private var list: MutableList<SubscriptionDescription>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.adapter_subscription_dscription, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewDescription.text = list!![position].subscriptionDescription
        if (position == 0)
            holder.viewVerticalTop.visibility = View.INVISIBLE

        if (list!!.size == (position + 1))
            holder.viewVerticalBottom.visibility = View.GONE
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewDescription: TextView = itemView.textViewDescription
        val viewVerticalTop: View = itemView.viewVerticalTop
        val viewVerticalBottom: View = itemView.viewVerticalBottom
    }

    fun setData(list: MutableList<SubscriptionDescription>) {
        this.list = list
        notifyDataSetChanged()
    }

}