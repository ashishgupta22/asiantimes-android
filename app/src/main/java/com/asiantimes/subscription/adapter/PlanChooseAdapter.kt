package com.asianmedia.easterneye.adapter.subscription

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.subscriptionmodel.SubPlanUnit
import kotlinx.android.synthetic.main.adapter_plan_choose.view.*


class PlanChooseAdapter(private val onClickPositionListener: OnClickPositionListener) : RecyclerView.Adapter<PlanChooseAdapter.ViewHolder>() {
    private var context: Context? = null
    private var subPlan: List<SubPlanUnit>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.adapter_plan_choose, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val price = subPlan!![position].subscriptionUnitPrice + " per " + subPlan!![position].subscriptionUnit
        holder.radioButtonPlan.text = price
        holder.radioButtonPlan.isChecked = subPlan!![position].isChecked != null && subPlan!![position].isChecked == true

        holder.radioButtonPlan.setOnCheckedChangeListener { _, b ->
            for (i in subPlan!!.indices) {
                subPlan!![i].isChecked = false
            }
            subPlan!![position].isChecked = true

            onClickPositionListener.onClickPositionListener(subPlan!![position], position)
        }
    }

    override fun getItemCount(): Int {
        return subPlan!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioButtonPlan: RadioButton = itemView.radioButtonPlan
    }

    fun setData(subPlan: List<SubPlanUnit>) {
        this.subPlan = subPlan
        notifyDataSetChanged()
    }

}