package com.asiantimes.subscription

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.TransactionDetails
import com.asianmedia.easterneye.adapter.subscription.DescriptionAdapter
import com.asianmedia.easterneye.adapter.subscription.PlanChooseAdapter
import com.asianmedia.easterneye.adapter.subscription.SubscriptionListAdapter
import com.asiantimes.R
import com.asiantimes.base.AppConfig.Companion.loadImage
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.AppPreferences.getLoginStatus
import com.asiantimes.base.Utils.isOnline
import com.asiantimes.base.Utils.showToast
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.viewmodel.SubscriptionViewModel
import com.asiantimes.subscriptionmodel.SubPlanUnit
import com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData
import com.asiantimes.subscriptionmodel.SubscriptionPlanData
import com.asiantimes.ui.cart.PaymentActivity
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInActivity
import kotlin.collections.ArrayList

class SubscriptionListFragment : Fragment() {
    // Google pay start
    private var pos: Int? = null
    private var id: Int? = 0
    private var packageType: String? = null
    private var title: String? = null
    private var callFrom: String? = null
    private var isuk = false
    private var recycleViewChoosePlans: RecyclerView? = null
    private var recycleViewDescription: RecyclerView? = null
    private val subscriptionPlanData: ArrayList<SubscriptionPlanData?>? = ArrayList()
    var subscriptionPlan: SubscriptionPlanData? = null
    private var progress_bar: ProgressBar? = null
    var subsCriptionListAdapter: SubscriptionListAdapter? = null
    var descriptionAdapter: DescriptionAdapter? = null
    var planChooseAdapter: PlanChooseAdapter? = null
    var imageViewSubscription: ImageView? = null
    var textViewDescription: TextView? = null
    var buttonViewBuy: Button? = null
    private var sub_pay = false
    var subPlanUnitList: List<SubPlanUnit>? = null
    var selectedPlanData: SubscriptionPlanData? = null
    var selectedSubPlanUnit: SubPlanUnit? = null
    var bp: BillingProcessor? = null
    private val subscriptionAppPlanData: MutableList<SubscriptionInAppPlanData> = ArrayList()
    var recycleViewSubscription: RecyclerView? = null
    val viewModel: SubscriptionViewModel by viewModels()

    // newInstance constructor for creating fragment with arguments
    fun newInstance(
        id: Int,
        title: String?,
        isuk: Boolean,
        callFrom: String?
    ): SubscriptionListFragment {
        val fragmentFirst = SubscriptionListFragment()
        val args = Bundle()
        args.putInt("id", id)
        args.putBoolean("isuk", isuk)
        args.putString("callFrom", callFrom)
        args.putString("title", title)
        fragmentFirst.arguments = args
        return fragmentFirst
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        id = if (arguments != null) requireArguments().getInt("id") else 0
        isuk = requireArguments().getBoolean("isuk")
        callFrom = requireArguments().getString("callFrom")
        if (callFrom == null) {
            callFrom = "news"
        }
        packageType = if (isuk) {
            "uk"
        } else {
            "international"
        }
        title = requireArguments().getString("title")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.subscription_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycleViewSubscription = view.findViewById(R.id.recycleViewSubscription)
        recycleViewChoosePlans = view.findViewById(R.id.recycleViewChoosePlans)
        recycleViewDescription = view.findViewById(R.id.recycleViewDescription)
        imageViewSubscription = view.findViewById(R.id.imageViewSubscription)
        textViewDescription = view.findViewById(R.id.textViewDescription)
        buttonViewBuy = view.findViewById(R.id.buttonViewBuy)
        progress_bar = view.findViewById(R.id.progress_bar)
        if (title.equals("subscriptionSpecial", ignoreCase = true)) {
            specialSubscriptionAPI
        } else {
            getSubscriptionPlanDetailsAPI(title, isuk)
        }
        subsCriptionListAdapter = SubscriptionListAdapter(object : OnClickPositionListener {
            override fun onClickPositionListener(`object`: Any?, position: Int) {
                val data = `object` as SubscriptionPlanData
                selectedPlanData = data as SubscriptionPlanData?
                setSelectedSubscriptionData()
            }
        })
        recycleViewSubscription!!.setAdapter(subsCriptionListAdapter)
        buttonViewBuy!!.setOnClickListener(View.OnClickListener { v: View? ->
            if (selectedSubPlanUnit != null) {
                if (sub_pay) {
                    if (isOnline(requireContext())) {
                        if (getLoginStatus()) {
                            if (subPlanUnitList != null && subPlanUnitList!!.size > 1) {
                                redirectToStripe(selectedPlanData)
                            } else {
                                val paymentIntent = Intent(context, PaymentActivity::class.java)
                                val totalPrice =
                                    selectedPlanData!!.subPlanUnit[0].subscriptionUnitPrice.replace(
                                        "[^\\d]".toRegex(),
                                        ""
                                    ).toFloat()
                                paymentIntent.putExtra("payment", totalPrice)
                                paymentIntent.putExtra(
                                    "subscriptionId",
                                    selectedPlanData!!.subscriptionId
                                )
                                paymentIntent.putExtra(
                                    "subscriptionName",
                                    selectedPlanData!!.subscriptionTitle
                                )
                                paymentIntent.putExtra("subscriptionType", title)
                                paymentIntent.putExtra("formShow", "1")
                                paymentIntent.putExtra("nextActivity", "Subscription")
                                paymentIntent.putExtra(
                                    "subscriptionSpecial",
                                    title.equals("subscriptionSpecial", ignoreCase = true)
                                )
                                paymentIntent.putExtra("packageType", packageType)
                                startActivity(paymentIntent)
                            }
                        } else {
                            startActivity(Intent(context, LogInActivity::class.java))
                        }
                    } else {
                        showToast(resources.getString(R.string.no_network))
                    }
                } else {
                    if (selectedPlanData!!.subPlanUnit != null && selectedPlanData!!.subPlanUnit.size > 0) {
                        subscription
                    }
                }
            } else {
                showToast("Please choose your plan")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!bp!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        if (bp != null) bp!!.release()
    }

    private val subscription: Unit
        private get() {
            try {
                if (getLoginStatus()) {
                    if (bp != null) bp!!.subscribe(
                        activity,
                        subscriptionAppPlanData[pos!!].subscriptionId
                    ) else billingProcess()
                } else {
                    startActivity(Intent(context, LogInActivity::class.java))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    private fun setSelectedSubscriptionData() {
        textViewDescription!!.text = selectedPlanData!!.subscriptionDescription
        loadImage(imageViewSubscription, selectedPlanData!!.subscriptionImage, R.drawable.dummy)

        planChooseAdapter = PlanChooseAdapter(object : OnClickPositionListener {
            override fun onClickPositionListener(`object`: Any?, position: Int) {
                val data = `object` as SubPlanUnit
                selectedSubPlanUnit = data as SubPlanUnit?
                pos = position
                recycleViewChoosePlans!!.post { planChooseAdapter!!.setData(subPlanUnitList!!) }
            }
        })
        recycleViewChoosePlans!!.adapter = planChooseAdapter
        subPlanUnitList = selectedPlanData!!.subPlanUnit
        recycleViewChoosePlans!!.post {
            planChooseAdapter!!.setData(subPlanUnitList!!)
            billingProcess()
        }
        selectedSubPlanUnit = subPlanUnitList!!.get(0)
        pos = 0
        descriptionAdapter = DescriptionAdapter()
        recycleViewDescription!!.adapter = descriptionAdapter
        descriptionAdapter!!.setData(selectedPlanData!!.subPostList)
        sub_pay = selectedPlanData!!.subscriptionPay.equals("t", ignoreCase = true)
    }

    private fun billingProcess() {
        try {
            if (!BillingProcessor.isIabServiceAvailable(requireContext())) {
                Toast.makeText(
                    context,
                    "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16",
                    Toast.LENGTH_LONG
                ).show()
            }
            bp = BillingProcessor(context, LICENSE_KEY, MERCHANT_ID, object : IBillingHandler {
                override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                    //purchaseToken;
                    if (details != null) {
                        addTransactionAPI(
                            details.purchaseInfo.purchaseData.purchaseToken,
                            details.purchaseInfo.purchaseData.productId,
                            "Android"
                        )
                    }
                }

                override fun onBillingError(errorCode: Int, error: Throwable?) {
                    Toast.makeText(context, "Something went wrong !", Toast.LENGTH_SHORT).show()
                }

                override fun onBillingInitialized() {
                    for (planUnit in subPlanUnitList!!) {
                        val listingDetails =
                            bp!!.getSubscriptionListingDetails(planUnit.subscriptionUnitKey)
                        if (listingDetails != null && !TextUtils.isEmpty(listingDetails.priceText)) {
                            subscriptionAppPlanData.add(
                                SubscriptionInAppPlanData(
                                    listingDetails.productId,
                                    listingDetails.title,
                                    listingDetails.priceText,
                                    listingDetails.description,
                                    planUnit.subscriptionUnit
                                )
                            )
                        }
                    }

//                recyclerViewPlanList.setAdapter(new SubscriptionInAppPurchasesListDialog.SubPlanUnitAdapter(subscriptionPlanData));
                    progress_bar!!.visibility = View.GONE
                }

                override fun onPurchaseHistoryRestored() {}
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun redirectToStripe(subscriptionPlanData: SubscriptionPlanData?) {
        subscriptionPlan = subscriptionPlanData
        redirectToSubscriptionList(
            selectedSubPlanUnit!!.subscriptionUnitPrice.replace(
                "[^\\d]".toRegex(),
                ""
            )
        )
    }

    private fun redirectToMainActivity() {
        val intent = Intent(context, DashboardActivity::class.java)
        intent.putExtra("select", "0")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        //        finish();
    }

    private fun redirectToSubscriptionList(subscriptionPrice: String) {
        val paymentIntent = Intent(activity, PaymentActivity::class.java)
        val totalPrice = subscriptionPrice.toFloat()
        paymentIntent.putExtra("payment", totalPrice)
        paymentIntent.putExtra("subscriptionId", subscriptionPlan!!.subscriptionId)
        paymentIntent.putExtra("subscriptionName", subscriptionPlan!!.subscriptionTitle)
        paymentIntent.putExtra("subscriptionType", title)
        paymentIntent.putExtra("formShow", "1")
        paymentIntent.putExtra("nextActivity", "Subscription")
        paymentIntent.putExtra(
            "subscriptionSpecial",
            title.equals("subscriptionSpecial", ignoreCase = true)
        )
        paymentIntent.putExtra("packageType", packageType)
        startActivity(paymentIntent)
    }

    //                                mAdapter.notifyDataSetChanged();
    val specialSubscriptionAPI: Unit
        get() {
            progress_bar!!.isIndeterminate = true
            progress_bar!!.visibility = View.VISIBLE
            subscriptionPlanData!!.clear()
            val requestBean = RequestBean()
            requestBean.setSpecialSubscrition(id.toString(), "ANDROID")

            viewModel.getSpecialSubscription(requestBean)
                .observe(this, androidx.lifecycle.Observer {
                    progress_bar!!.visibility = View.GONE
                    progress_bar!!.visibility = View.GONE
                    val responseResult = it
                    if (responseResult.getErrorCode() === 0) {
                        if (responseResult.getSubscriptionList()!!.size > 0) {
                            subscriptionPlanData.addAll(responseResult.getSubscriptionList()!!)
                            //                                mAdapter.notifyDataSetChanged();
                            if (subscriptionPlanData != null) {
                                subscriptionPlanData[0]!!.setisChecked(true)
                                selectedPlanData = subscriptionPlanData[0]
                                setSelectedSubscriptionData()
                            }
                            if (subscriptionPlanData.size > 0 && subscriptionPlanData[0]!!
                                    .subscriptionId != "Free"
                            ) {
                                recycleViewSubscription!!.visibility = View.VISIBLE
                                subsCriptionListAdapter!!.setData(
                                    subscriptionPlanData!!
                                )
                            } else {
                                recycleViewSubscription!!.visibility = View.GONE
                            }
                        }
                    } else showToast(getString(R.string.internetConnectionFailed))
                })
        }

    fun getSubscriptionPlanDetailsAPI(subscriptionName: String?, isUk: Boolean) {
        progress_bar!!.isIndeterminate = true
        progress_bar!!.visibility = View.VISIBLE
        /* final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();*/
        subscriptionPlanData!!.clear()
        val requestBean = RequestBean()
        requestBean.setSubscriptionDetails(subscriptionName, isUk, "ANDROID", callFrom)

        viewModel.getSubScriptionPlanDetails(requestBean).observe(viewLifecycleOwner, {
            val responseResult = it
            progress_bar!!.visibility = View.GONE
            if (responseResult.getErrorCode() === 0) {
                if (responseResult.getSubscriptionList()!!.size > 0) {
                    subscriptionPlanData.addAll(responseResult.getSubscriptionList()!!)
                    if (subscriptionPlanData != null) {
                        subscriptionPlanData[0]!!.setisChecked(true)
                        selectedPlanData = subscriptionPlanData[0]
                        setSelectedSubscriptionData()
                    }
                    if (subscriptionPlanData.size > 0 && subscriptionPlanData[0]!!.subscriptionTitle != "FREE") {
                        recycleViewSubscription!!.visibility = View.VISIBLE
                        subsCriptionListAdapter!!.setData(
                            subscriptionPlanData!!
                        )
                    } else {
                        recycleViewSubscription!!.visibility = View.GONE
                    }
                }
            } else showToast(getString(R.string.internetConnectionFailed))
        })
    }

    fun addTransactionAPI(transactionId: String?, productId: String?, deviceType: String?) {
        progress_bar!!.visibility = View.VISIBLE
        val requestBean = RequestBean()
        requestBean.setOrderPlaced(
            transactionId,
            productId,
            deviceType,
            AppPreferences.getUserId()
        )

        viewModel.addTransaction(requestBean).observe(this,{
            progress_bar!!.visibility = View.GONE
            val responseResult = it
            try {
                if (responseResult.getErrorCode() === 0) {
                    showToast("" + responseResult.getMessage())
                } else showToast(getString(R.string.internetConnectionFailed))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

    }

    //save user membership
//    private val premiumPostArrayAPI: Unit
//        private get() {
//            try {
//                val requestBean = RequestBean()
//                requestBean.getTopStory(AppPreferences.getInstance().getUserId())
//                val resultObservable: Observable<ResponseResult> =
//                    ApplicationConfig.apiInterface.getPremiumPostRequest(
//                        requestBean
//                    )
//                resultObservable.subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .unsubscribeOn(Schedulers.io())
//                    .subscribe(object : Observer<ResponseResult> {
//                        override fun onCompleted() {}
//                        override fun onError(e: Throwable) {}
//                        override fun onNext(responseResult: ResponseResult) {
//                            if (responseResult.getErrorCode() === 0) {
//                                //save user membership
//                                AppPreferences.getInstance().setPremiumArr(
//                                    responseResult.goldMembership,
//                                    responseResult.digitalPowerList,
//                                    responseResult.digitalRichList,
//                                    responseResult.digitalFreeContent,
//                                    responseResult.premiumPowerList,
//                                    responseResult.premiumRichList
//                                )
//                                redirectToMainActivity()
//                            }
//                        }
//                    })
//            } catch (ex: Exception) {
//                ex.printStackTrace()
//            }
//        }

    companion object {
        private const val LICENSE_KEY =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB" // PUT YOUR MERCHANT KEY HERE;

        // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
        // if filled library will provide protection against Freedom alike Play Market simulators
        private const val MERCHANT_ID = "15638088474552299429"
    }
}