package com.asiantimes.subscription;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.asiantimes.R;
import com.asiantimes.model.PlanListSerialize;

import java.util.ArrayList;
import java.util.List;

public class Subscription_Plan_Page extends AppCompatActivity implements View.OnClickListener {
    private List<PlanListSerialize> planListSerializes = new ArrayList<>();
    public boolean isuk;
    public String callFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription__plan__page);

        Intent i = getIntent();
        planListSerializes.clear();


        planListSerializes = (List<PlanListSerialize>) i.getSerializableExtra("subscription_plan");
        isuk = i.getExtras().getBoolean("isuk");
        callFrom = i.getExtras().getString("callFrom");

        ImageView imageViewBack = findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(this);

        TabLayout tabs_dashboard = findViewById(R.id.tabs);
        ViewPager viewPager = findViewById(R.id.viewpager);
        tabs_dashboard.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#d12f2c"));
        setupViewPager(viewPager);
        tabs_dashboard.setupWithViewPager(viewPager);

    }

    static class ViewPagerAdapters extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapters(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        //this is called when notifyDataSetChanged() is called
        @Override
        public int getItemPosition(Object object) {
            // refresh all fragments when data set changed
            return PagerAdapter.POSITION_NONE;
        }


        public void clear() {

            mFragmentList.clear();
            mFragmentTitleList.clear();

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        try {
            ViewPagerAdapters adapter = new ViewPagerAdapters(Subscription_Plan_Page.this.getSupportFragmentManager());
            adapter.clear();
            for (int i = 0; i < planListSerializes.size(); i++) {
                adapter.addFragment(new SubscriptionListFragment().newInstance(i, planListSerializes.get(i).getPlanName(), isuk,callFrom), planListSerializes.get(i).getPlanName());
            }
            viewPager.setOffscreenPageLimit(0);
            viewPager.setAdapter(adapter);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                break;
        }
    }
/*
        @Override
        public void onBackPressed() {
            planListSerializes.clear();
            finish();
            super.onBackPressed();
        }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
