package com.asiantimes.subscription

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.R
import com.asiantimes.subscriptionmodel.SubPlanUnit
import com.asiantimes.widget.TextViewRegularBold
import java.util.*

class SubscriptionPrintListDialog : AppCompatActivity(), View.OnClickListener {
    private var imageviewCloseSubscriptionDialog: ImageView? = null
    private var recyclerViewPlanList: RecyclerView? = null
    var getSubPlanUnit: ArrayList<SubPlanUnit>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.subscription_dialog)
        setFinishOnTouchOutside(false)
        getSubPlanUnit = intent.getParcelableArrayListExtra("subscriptionPlanData")
        initializationView()
    }

    fun initializationView() {
        imageviewCloseSubscriptionDialog =
            findViewById<View>(R.id.imageview_close_subscription_dialog) as ImageView
        recyclerViewPlanList =
            findViewById<View>(R.id.recycler_in_app_purchases_plan) as RecyclerView
        imageviewCloseSubscriptionDialog!!.setOnClickListener(this)
        recyclerViewPlanList!!.setHasFixedSize(true)
        // use a linear layout manager
        recyclerViewPlanList!!.layoutManager = LinearLayoutManager(this)
        recyclerViewPlanList!!.adapter = SubPrintUnitList(getSubPlanUnit)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imageview_close_subscription_dialog -> {
                val returnIntent = Intent()
                setResult(RESULT_CANCELED, returnIntent)
                finish()
            }
        }
    }

    inner class SubPrintUnitList(subPlanUnit: List<SubPlanUnit>?) :
        RecyclerView.Adapter<SubPrintUnitList.MyViewHolder>() {
        var subPlanUnit: List<SubPlanUnit>? = ArrayList()

        inner class MyViewHolder(var view: View) : RecyclerView.ViewHolder(
            view
        ) {
            val txtPlanPrice: TextViewRegularBold
            val btnPlanUnit: TextViewRegularBold

            init {
                txtPlanPrice = view.findViewById(R.id.txtPlanPrice)
                btnPlanUnit = view.findViewById(R.id.btnPlanUnit)
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_data_in_app_purchases_plan_list, parent, false) as View
            return MyViewHolder(v)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.txtPlanPrice.text = subPlanUnit!![position].subscriptionUnitPrice
            holder.btnPlanUnit.text = subPlanUnit!![position].subscriptionUnit
            holder.btnPlanUnit.setOnClickListener {
                redirectToSubscriptionList(
                    subPlanUnit!![position].subscriptionUnitPrice.replace("[^\\d]".toRegex(), "")
                )
            }
        }

        override fun getItemCount(): Int {
            return subPlanUnit!!.size
        }

        init {
            this.subPlanUnit = subPlanUnit
        }
    }

    private fun redirectToSubscriptionList(subscriptionPrice: String) {
        val returnIntent = Intent()
        returnIntent.putExtra("price", subscriptionPrice)
        setResult(RESULT_OK, returnIntent)
        finish()
    }
}