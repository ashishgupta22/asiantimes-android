package com.asiantimes.subscription

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.TransactionDetails
import com.asiantimes.R
import com.asiantimes.base.AppPreferences.getLoginStatus
import com.asiantimes.base.AppPreferences.getUserId
import com.asiantimes.base.AppPreferences.setPremiumArr
import com.asiantimes.base.Utils.htmlToSppanned
import com.asiantimes.base.Utils.showToast
import com.asiantimes.model.PlanListSerialize
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.viewmodel.SubscriptionViewModel
import com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.widget.TextViewRegular
import com.asiantimes.widget.TextViewRegularBold
import java.util.*

class SubscriptionInAppPurchasesList : AppCompatActivity(), View.OnClickListener {
    private var readyToPurchase = false
    var bp: BillingProcessor? = null
    private val yes_continue: TextViewRegular? = null
    private val visit_uk_site: TextViewRegular? = null
    private var action_bar_title: TextViewRegular? = null
    private var iv_menuBack: ImageView? = null
    private val planListSerializes: MutableList<PlanListSerialize> = ArrayList()
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private val subscriptionPlanData: MutableList<SubscriptionInAppPlanData> = ArrayList()
    private val getSubscriptionListingDetails = ArrayList<String>()
    var viewModel = SubscriptionViewModel()
    var progress_bar: ProgressBar? = null
    var title: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_in_app_purchaeses_list)
        title = intent.getStringExtra("package")
        getSubscriptionListingDetails.clear()
        if (title.equals("gold", ignoreCase = true)) {
            getSubscriptionListingDetails.add("android_sub_annually_gold")
            getSubscriptionListingDetails.add("android_sub_monthly_gold")
        } else {
            getSubscriptionListingDetails.add("android_sub_annually_platinum")
            getSubscriptionListingDetails.add("android_sub_monthly_platinum")
        }
        initializationView()
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Toast.makeText(
                this,
                "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16",
                Toast.LENGTH_LONG
            ).show()
        }
        bp = BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, object : IBillingHandler {
            override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                //purchaseToken;
                setReadyToPurchase(
                    details!!.purchaseInfo.purchaseData.purchaseToken,
                    details.purchaseInfo.purchaseData.productId,
                    "Android"
                )
            }

            override fun onBillingError(errorCode: Int, error: Throwable?) {
                Toast.makeText(
                    this@SubscriptionInAppPurchasesList,
                    "Something went wrong !",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onBillingInitialized() {
                readyToPurchase = true
                val subs = bp!!.getSubscriptionListingDetails(getSubscriptionListingDetails)
                for (i in subs.indices) {
                    subscriptionPlanData.add(
                        SubscriptionInAppPlanData(
                            subs[i].productId,
                            subs[i].title,
                            subs[i].priceText,
                            subs[i].description,
                            subs[i].subscriptionPeriod
                        )
                    )
                }
                mAdapter!!.notifyDataSetChanged()
                progress_bar!!.visibility = View.GONE
            }

            override fun onPurchaseHistoryRestored() {}
        })
    }

    fun initializationView() {
        action_bar_title = findViewById(R.id.action_bar_title)
        action_bar_title!!.setText("Subscription")
        iv_menuBack = findViewById(R.id.iv_menuBack)
        mRecyclerView = findViewById<View>(R.id.inapp_recycler_seealldata) as RecyclerView
        progress_bar = findViewById(R.id.progress_bar)
        progress_bar!!.setVisibility(View.VISIBLE)

        //    setSubscriptionList(title, isuk);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView!!.setHasFixedSize(true)

        // use a linear layout manager
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = mLayoutManager

        // specify an adapter (see also next example)
        mAdapter = MyAdapter(subscriptionPlanData, this)
        mRecyclerView!!.adapter = mAdapter
        iv_menuBack!!.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.iv_menuBack -> onBackPressed()
        }
    }

    inner class MyAdapter     // Provide a suitable constructor (depends on the kind of dataset)
        (
        private val subscriptionPlanDataList: List<SubscriptionInAppPlanData>,
        private val context: Context
    ) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {
        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        inner class MyViewHolder(  // each data item is just a string in this case
            //	public TextView mTextView;
            var view: View
        ) : RecyclerView.ViewHolder(view) {
            val getsubTitle: TextViewRegularBold
            private val getForgroundSp: ImageView
            val contentOfPlan: TextViewRegular
            private val subSubscriptionRecyclerView: RecyclerView? = null
            val seealloption: TextViewRegularBold
            val plan_price: TextViewRegularBold
            private val seeall: RelativeLayout
            private val showtext: TextViewRegular

            init {
                getsubTitle = view.findViewById<View>(R.id.getsub_title) as TextViewRegularBold
                getForgroundSp = view.findViewById<View>(R.id.get_forground_sp) as ImageView
                getForgroundSp.background = null
                contentOfPlan = view.findViewById<View>(R.id.content_of_plan) as TextViewRegular
                seealloption = view.findViewById<View>(R.id.seealloption) as TextViewRegularBold
                seeall = view.findViewById<View>(R.id.seeall) as RelativeLayout
                seeall.visibility = View.GONE
                plan_price = view.findViewById<View>(R.id.plan_price) as TextViewRegularBold

                // in content do not change the layout size of the RecyclerView
                showtext = view.findViewById<View>(R.id.showtext) as TextViewRegular
                // specify an adapter (see also next example)
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            // create a new view
            val v = LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.in_app_subscription_plan_details_recycler_data,
                    parent,
                    false
                ) as View
            return MyViewHolder(v)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.getsubTitle.text = htmlToSppanned(
                subscriptionPlanDataList[position].subscriptionTitle.replace("(Eastern Eye)", "")
            )
            holder.contentOfPlan.text = htmlToSppanned(
                subscriptionPlanDataList[position].subscriptionDescription
            )
            //  holder.contentOfPlan.setText(subscriptionPlanDataList.get(position).getSubscriptionDescription());
            holder.plan_price.text = "" + subscriptionPlanDataList[position].subscriptionPrice
            //  ApplicationConfig.loadImage(holder.getForgroundSp, subscriptionPlanDataList.get(position).getSubscriptionImage(), R.drawable.dummy);
            holder.seealloption.text = "Pay"
            holder.seealloption.setOnClickListener {
                if (getLoginStatus()) {
                    bp!!.subscribe(
                        this@SubscriptionInAppPurchasesList,
                        subscriptionPlanDataList[position].subscriptionId
                    )
                } else {
                    startActivity(Intent(context, LogInActivity::class.java))
                    /*    if (title.equalsIgnoreCase("free")) {
                                    startActivity(new Intent(context, DialogLoginRegister.class).putExtra("SubscriptionCall", "yes").putExtra("SubscriptionFree", "yes"));
                                }
                                else
                                {
                                    startActivity(new Intent(context, DialogLoginRegister.class).putExtra("SubscriptionCall", "yes").putExtra("SubscriptionFree", "no"));
                                }*/
                }
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount(): Int {
            return subscriptionPlanDataList.size
        }
    }

    public override fun onDestroy() {
        if (bp != null) bp!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!bp!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

    fun setReadyToPurchase(transactionId: String?, productId: String?, deviceType: String?) {
        progress_bar!!.visibility = View.VISIBLE
        val requestBean = RequestBean()
        requestBean.setOrderPlaced(transactionId, productId, deviceType, getUserId())
        viewModel.addTransaction(requestBean).observe(this, { responseResult ->
            progress_bar!!.visibility = View.GONE
            planListSerializes.clear()
            if (responseResult.getErrorCode() === 0) {
                showToast("" + responseResult.getMessage())
                premiumPostArray
            } else showToast(getString(R.string.internetConnectionFailed))
        })
    }

    //save user membership
    private val premiumPostArray: Unit
        private get() {
            try {
                val requestBean = RequestBean()
                requestBean.getTopStory(getUserId())
                viewModel.getPremiumPostRequestNew(requestBean).observe(this, { responseResult ->
                    if (responseResult.getErrorCode() === 0) {
                        //save user membership
                        setPremiumArr(
                            responseResult.goldMembership,
                            responseResult.digitalPowerList,
                            responseResult.digitalRichList,
                            responseResult.digitalFreeContent,
                            responseResult.premiumPowerList,
                            responseResult.premiumRichList
                        )
                        redirectToMainActivity()
                    }
                })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    private fun redirectToMainActivity() {
        val intent = Intent(this@SubscriptionInAppPurchasesList, DashboardActivity::class.java)
        intent.putExtra("select", "0")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    companion object {
        private const val LICENSE_KEY =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB" // PUT YOUR MERCHANT KEY HERE;

        // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
        // if filled library will provide protection against Freedom alike Play Market simulators
        private const val MERCHANT_ID = "15638088474552299429"
    }
}