package com.asiantimes.subscription.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.model.RequestBean
import com.asiantimes.model.ResponseResult

class SubscriptionViewModel : ViewModel() {

    fun getSubScriptionPlanDetails(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getSubScriptionPlanDetails(requestBean);
    }

    fun getSpecialSubscription(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getSpecialSubscription(requestBean);
    }

    fun addTransaction(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.addTransaction(requestBean);
    }

    fun getPremiumPostRequestNew(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getPremiumPostRequestNew(requestBean);
    }
}