package com.asiantimes.subscription.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.model.RequestBean
import com.asiantimes.model.ResponseResult

class SubscriptionHomeViewModel : ViewModel() {

    fun getSubScriptionPlan(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getSubScriptionPlan(requestBean);
    }

    fun setSubmitPayment(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.setSubmitPayment(requestBean);
    }

    fun getPremiumPostRequestNew(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getPremiumPostRequestNew(requestBean);
    }
    fun getShippingPrice(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        return ApiRepositoryDigital.getShippingPrice(requestBean);
    }
}