package com.asiantimes.subscription

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.TransactionDetails
import com.asiantimes.R
import com.asiantimes.base.AppPreferences.getLoginStatus
import com.asiantimes.base.AppPreferences.getUserId
import com.asiantimes.base.AppPreferences.setPremiumArr
import com.asiantimes.base.Utils.showToast
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.viewmodel.SubscriptionViewModel
import com.asiantimes.subscriptionmodel.SubPlanUnit
import com.asiantimes.subscriptionmodel.SubscriptionInAppPlanData
import com.asiantimes.ui.dashboard.DashboardActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.widget.TextViewRegularBold
import java.util.*

class SubscriptionInAppPurchasesListDialog : AppCompatActivity(), View.OnClickListener {
    private var readyToPurchase = false
    var bp: BillingProcessor? = null
    private val subscriptionPlanData: MutableList<SubscriptionInAppPlanData> = ArrayList()
    private var getSubPlanUnit: List<SubPlanUnit>? = ArrayList()
    var progress_bar: ProgressBar? = null
    var title: String? = null
    private var imageviewCloseSubscriptionDialog: ImageView? = null
    private var recyclerViewPlanList: RecyclerView? = null
    var viewModel = SubscriptionViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.subscription_dialog)
        setFinishOnTouchOutside(false)
        title = intent.getStringExtra("package")
        getSubPlanUnit = intent.getParcelableArrayListExtra("subscriptionList")
        initializationView()
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Toast.makeText(
                this,
                "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16",
                Toast.LENGTH_LONG
            ).show()
        }
        bp = BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, object : IBillingHandler {
            override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                //purchaseToken;
                setReadyToPurchase(
                    details!!.purchaseInfo.purchaseData.purchaseToken,
                    details.purchaseInfo.purchaseData.productId,
                    "Android"
                )
            }

            override fun onBillingError(errorCode: Int, error: Throwable?) {
                Toast.makeText(
                    this@SubscriptionInAppPurchasesListDialog,
                    "Something went wrong !",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onBillingInitialized() {
                readyToPurchase = true
                for (planUnit in getSubPlanUnit!!) {
                    val listingDetails =
                        bp!!.getSubscriptionListingDetails(planUnit.subscriptionUnitKey)
                    if (listingDetails != null && !TextUtils.isEmpty(listingDetails.priceText)) {
                        subscriptionPlanData.add(
                            SubscriptionInAppPlanData(
                                listingDetails.productId,
                                listingDetails.title,
                                listingDetails.priceText,
                                listingDetails.description,
                                planUnit.subscriptionUnit
                            )
                        )
                    }
                }
                recyclerViewPlanList!!.adapter = SubPlanUnitAdapter(subscriptionPlanData)
                progress_bar!!.visibility = View.GONE
            }

            override fun onPurchaseHistoryRestored() {}
        })
    }

    fun initializationView() {
        progress_bar = findViewById(R.id.progress_bar)
        progress_bar!!.setVisibility(View.VISIBLE)
        imageviewCloseSubscriptionDialog =
            findViewById<View>(R.id.imageview_close_subscription_dialog) as ImageView
        recyclerViewPlanList =
            findViewById<View>(R.id.recycler_in_app_purchases_plan) as RecyclerView
        imageviewCloseSubscriptionDialog!!.setOnClickListener(this)
        recyclerViewPlanList!!.setHasFixedSize(true)
        // use a linear layout manager
        recyclerViewPlanList!!.layoutManager = LinearLayoutManager(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imageview_close_subscription_dialog -> finish()
        }
    }

    public override fun onDestroy() {
        if (bp != null) bp!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!bp!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

    fun setReadyToPurchase(transactionId: String?, productId: String?, deviceType: String?) {
        progress_bar!!.visibility = View.VISIBLE
        val requestBean = RequestBean()
        requestBean.setOrderPlaced(transactionId, productId, deviceType, getUserId())
        viewModel.addTransaction(requestBean).observe(this, { responseResult ->
            progress_bar!!.visibility = View.GONE
            if (responseResult.getErrorCode() === 0) {
                showToast("" + responseResult.getMessage())
                premiumPostArray
            } else showToast(getString(R.string.internetConnectionFailed))
        })
    }

    inner class SubPlanUnitAdapter(subPlanUnit: List<SubscriptionInAppPlanData>) :
        RecyclerView.Adapter<SubPlanUnitAdapter.MyViewHolder>() {
        var subPlanUnit: List<SubscriptionInAppPlanData> = ArrayList()

        inner class MyViewHolder(var view: View) : RecyclerView.ViewHolder(
            view
        ) {
            val txtPlanPrice: TextViewRegularBold
            val btnPlanUnit: TextViewRegularBold

            init {
                txtPlanPrice = view.findViewById(R.id.txtPlanPrice)
                btnPlanUnit = view.findViewById(R.id.btnPlanUnit)
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_data_in_app_purchases_plan_list, parent, false) as View
            return MyViewHolder(v)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.txtPlanPrice.text = subPlanUnit[position].subscriptionPrice
            holder.btnPlanUnit.text = subPlanUnit[position].subscriptionPeriod
            holder.btnPlanUnit.setOnClickListener {
                getSubscription(
                    this@SubscriptionInAppPurchasesListDialog,
                    position
                )
            }
        }

        override fun getItemCount(): Int {
            return subPlanUnit.size
        }

        init {
            this.subPlanUnit = subPlanUnit
        }
    }

    //save user membership
    private val premiumPostArray: Unit
        private get() {
            try {
                val requestBean = RequestBean()
                requestBean.getTopStory(getUserId())
                viewModel.getPremiumPostRequestNew(requestBean).observe(this, { responseResult ->
                    if (responseResult.getErrorCode() === 0) {
                        //save user membership
                        setPremiumArr(
                            responseResult.goldMembership,
                            responseResult.digitalPowerList,
                            responseResult.digitalRichList,
                            responseResult.digitalFreeContent,
                            responseResult.premiumPowerList,
                            responseResult.premiumRichList
                        )
                        redirectToMainActivity()
                    }
                })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    private fun redirectToMainActivity() {
        val intent =
            Intent(this@SubscriptionInAppPurchasesListDialog, DashboardActivity::class.java)
        intent.putExtra("select", "0")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun getSubscription(activity: Activity, position: Int) {
        if (getLoginStatus()) {
            bp!!.subscribe(
                this@SubscriptionInAppPurchasesListDialog,
                subscriptionPlanData[position].subscriptionId
            )
        } else {
            startActivity(Intent(this, LogInActivity::class.java))
        }
    }

    companion object {
        private const val LICENSE_KEY =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB" // PUT YOUR MERCHANT KEY HERE;

        // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
        // if filled library will provide protection against Freedom alike Play Market simulators
        private const val MERCHANT_ID = "15638088474552299429"
    }
}