package com.asiantimes.digital_addition.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.digital_addition.model.EditionCategoryModel

@Dao
interface EditionCategoryDao {
    @Insert
    fun insert(categoryListEntity: List<EditionCategoryModel.DigitalEditionCategoryList>?)

    @Query("SELECT * from edition_category_list_table")
    fun getEditionCategoryList(): List<EditionCategoryModel.DigitalEditionCategoryList>

    @Query("delete from edition_category_list_table")
    fun delete()
/*
    @Query("select * from edition_category_list_table where categoryId in (:categoryId)")
    fun getWatchItemById(categoryId: Int): List<EditionCategoryModel.DigitalEditionCategoryList>

    @Query("delete from edition_category_list_table where categoryId = :categoryId")
    fun deleteItemByID(categoryId: Int)

   */
}