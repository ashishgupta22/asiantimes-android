package com.asiantimes.digital_addition.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.asiantimes.digital_addition.database.converter.DataConverter
import com.asiantimes.digital_addition.database.converter.HomeCategoriesConverter
import com.asiantimes.digital_addition.database.dao.EditionCategoryDao
import com.asiantimes.digital_addition.database.dao.EditionListDao
import com.asiantimes.digital_addition.database.dao.EditionPostListDao
import com.asiantimes.digital_addition.model.EditionCategoryModel
import com.asiantimes.digital_addition.model.EditionListModel
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.model.GetAllCategoriesModel

@Database(entities = [GetAllCategoriesModel.Post::class, EditionCategoryModel.DigitalEditionCategoryList::class,
    EditionListModel.DigitalEditionList::class, EditionPostListModel::class], version = 2, exportSchema = false)
@TypeConverters(DataConverter::class, HomeCategoriesConverter::class)   // add here
abstract class AppDatabase : RoomDatabase() {
    abstract fun editionCategoryDao(): EditionCategoryDao
    abstract fun editionListDao(): EditionListDao
    abstract fun editionPostListDao(): EditionPostListDao
//    abstract fun homeCategoriesDao(): HomeCategoriesDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext, AppDatabase::class.java, "EasternEye_New"
                ).fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}