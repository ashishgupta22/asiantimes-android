package com.asiantimes.digital_addition.database.converter

import androidx.room.TypeConverter
import com.asiantimes.model.GetAllCategoriesModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type

class HomeCategoriesConverter  : Serializable {
    @TypeConverter // note this annotation
    fun fromOptionValuesList(optionValues: List<GetAllCategoriesModel.Post.SubCatArr>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<GetAllCategoriesModel.Post.SubCatArr>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList(optionValuesString: String?): List<GetAllCategoriesModel.Post.SubCatArr>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<GetAllCategoriesModel.Post.SubCatArr?>?>() {}.type
        return gson.fromJson<List<GetAllCategoriesModel.Post.SubCatArr>>(optionValuesString, type)
    }
}