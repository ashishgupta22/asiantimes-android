package com.asiantimes.digital_addition.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.digital_addition.model.EditionPostListModel

@Dao
interface EditionPostListDao {
    @Insert
    fun insert(editionPostListEntity: EditionPostListModel?)

/*    @Query("SELECT * from edition_post_list")
    fun getEditionPostList(): List<EditionPostListModel.PostList>*/

    @Query("select * from edition_post_list where editionId in (:editionId)")
    fun getEditionPostById(editionId: Int): EditionPostListModel

/*    @Query("update edition_post_list SET qty = (:quantity) WHERE productId = (:productId)")
    fun updateCartItem(productId: Int, quantity: Int)*/

    @Query("delete from edition_post_list")
    fun delete()
}