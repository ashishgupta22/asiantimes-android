package com.asiantimes.digital_addition.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.asiantimes.digital_addition.model.EditionListModel

@Dao
interface EditionListDao {
    @Insert
    fun insert(editionListEntity: List<EditionListModel.DigitalEditionList>?)

    @Query("SELECT * from edition_list_table")
    fun getEditionList(): List<EditionListModel.DigitalEditionList>

    @Query("select * from edition_list_table where productId in (:productId)")
    fun getEditionById(productId: Int): EditionListModel.DigitalEditionList

    @Query("delete from edition_list_table")
    fun delete()
}