package com.asiantimes.digital_addition.database.converter

import androidx.room.TypeConverter
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type


class DataConverter : Serializable {
    @TypeConverter // note this annotation
    fun fromOptionValuesList(optionValues: List<EditionPostListModel.PostList>?): String? {
        if (optionValues == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<EditionPostListModel.PostList>?>() {}.type
        return gson.toJson(optionValues, type)
    }

    @TypeConverter // note this annotation
    fun toOptionValuesList(optionValuesString: String?): List<EditionPostListModel.PostList>? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<EditionPostListModel.PostList?>?>() {}.type
        return gson.fromJson<List<EditionPostListModel.PostList>>(optionValuesString, type)
    }
}