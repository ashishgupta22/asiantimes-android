package com.asiantimes.digital_addition.popup

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.interfaces.OnClickListener
import kotlinx.android.synthetic.main.dialog_text_line_spacing.*


class TextLineSpacingDialog(mContext: Context, private val onClickListener: OnClickListener) : Dialog(mContext), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCanceledOnTouchOutside(false)
        setContentView(R.layout.dialog_text_line_spacing)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        when (AppPreferences.getDigitalTextLineSpacing()) {
            "2" -> {
                imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal_active))
                imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing))
                imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large))
            }
            "10" -> {
                imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal))
                imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_active))
                imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large))
            }
            "20" -> {
                imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal))
                imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing))
                imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large_active))
            }
        }

        imageViewSpacingSmall.setOnClickListener(this)
        imageViewSpacingMedium.setOnClickListener(this)
        imageViewSpacingLarge.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (view == imageViewSpacingSmall) {
            imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal_active))
            imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing))
            imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large))
            AppPreferences.setDigitalTextLineSpacing("2")
            onClickListener.onClickListener(null)
            dismiss()
        } else if (view == imageViewSpacingMedium) {
            imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal))
            imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_active))
            imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large))
            AppPreferences.setDigitalTextLineSpacing("10")
            onClickListener.onClickListener(null)
            dismiss()
        } else if (view == imageViewSpacingLarge) {
            imageViewSpacingSmall.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_normal))
            imageViewSpacingMedium.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing))
            imageViewSpacingLarge.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.spacing_large_active))
            AppPreferences.setDigitalTextLineSpacing("20")
            onClickListener.onClickListener(null)
            dismiss()
        }
    }
}