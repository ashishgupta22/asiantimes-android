package com.asiantimes.digital_addition.popup

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.interfaces.OnClickListener
import com.warkiz.widget.IndicatorSeekBar
import kotlinx.android.synthetic.main.dialog_text_resize.*

class TextResizeDialog(
    private val mContext: Context,
    private val onClickListener: OnClickListener
) : Dialog(mContext) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCanceledOnTouchOutside(false)
        setContentView(R.layout.dialog_text_resize)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )

        when (AppPreferences.getDigitalTextSize()) {
            "12" -> indicatorSeekBarTextSize.setProgress(0f)
            "16" -> indicatorSeekBarTextSize.setProgress(33f)
            "20" -> indicatorSeekBarTextSize.setProgress(67f)
            "24" -> indicatorSeekBarTextSize.setProgress(100f)
        }
        indicatorSeekBarTextSize.setOnSeekChangeListener(object :
            IndicatorSeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: IndicatorSeekBar,
                progress: Int,
                progressFloat: Float,
                fromUserTouch: Boolean
            ) {
                when (progress) {
                    0 -> {
                        AppPreferences.setDigitalTextSize("12")
                    }
                    33 -> {
                        AppPreferences.setDigitalTextSize("16")
                    }
                    67 -> {
                        AppPreferences.setDigitalTextSize("20")
                    }
                    100 -> {
                        AppPreferences.setDigitalTextSize("24")
                    }
                }
                onClickListener.onClickListener(progress)
            }

            override fun onSectionChanged(
                seekBar: IndicatorSeekBar,
                thumbPosOnTick: Int,
                textBelowTick: String,
                fromUserTouch: Boolean
            ) {
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar, thumbPosOnTick: Int) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {
                dismiss()
            }
        })

    }
}