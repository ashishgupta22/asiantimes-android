package com.asiantimes.digital_addition.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.model.SearchModel
import com.asiantimes.digital_addition.ui.detail.DetailActivity
import com.asiantimes.digital_addition.ui.download.DownloadViewModel
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.model.RequestBean
import kotlinx.android.synthetic.main.activity_digital_book_marks.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.digital_action_bar.*

class SearchActivity : AppCompatActivity() {
    private var searchAdapter: SearchAdapter? = null
    private var editionId: String? = null
    private var searchResponse: SearchModel? = null
    val viewModel : DownloadViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        editionId = intent.getStringExtra("EDITION_ID")
        setAdapter()
        imageViewBack.setOnClickListener {
            finish()
        }
        text_view_search.setOnClickListener {
            val searchText = search_view.query.toString().trim()
            if (searchText.length < 3) {
                progress_bar.visibility = View.GONE
                textViewNoData.visibility = View.VISIBLE
                textViewNoData.text = getString(R.string.search_error)
            } else {
                Utils.hideSoftKeyBoard(this@SearchActivity, text_view_search)
                recycle_view_search.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
                textViewNoData.visibility = View.GONE
                if (Utils.isOnline(this))
                    searchAPIAPI(searchText)
                else
                    Utils.showToast(getString(R.string.internetConnectionFailedTop))
            }
        }

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String?): Boolean {
                val searchText = search_view.query.toString().trim()
                if (searchText.length < 3) {
                    progress_bar.visibility = View.GONE
                    textViewNoData.visibility = View.VISIBLE
                    textViewNoData.text = getString(R.string.search_error)
                } else {
                    Utils.hideSoftKeyBoard(this@SearchActivity, text_view_search)
                    recycle_view_search.visibility = View.GONE
                    progress_bar.visibility = View.VISIBLE
                    textViewNoData.visibility = View.GONE
                    if (Utils.isOnline(this@SearchActivity))
                        searchAPIAPI(searchText)
                    else
                        Utils.showToast(getString(R.string.internetConnectionFailedTop))
                }
                return true
            }

            override fun onQueryTextChange(s: String?): Boolean {
                return false
            }
        })
    }

    private fun setAdapter() {
        searchAdapter = SearchAdapter(object : OnClickPositionListener {
            override fun onClickPositionListener(data: Any?, position: Int) {
                val filteredList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> = ArrayList()

                for (i in searchResponse!!.postList!!.indices) {
                    val model = EditionPostListModel.PostList.PostCatNewsList()
                    model.isBookmark = searchResponse!!.postList!![i].isBookmark
                    model.isPremium = searchResponse!!.postList!![i].isPremium
                    model.postId = searchResponse!!.postList!![i].postId
                    model.postDate = searchResponse!!.postList!![i].postDate
                    model.postTitle = searchResponse!!.postList!![i].postTitle
                    model.postImage = searchResponse!!.postList!![i].postImage
                    model.productList = searchResponse!!.postList!![i].productList
                    filteredList.add(model)
                }

                val intent = Intent(this@SearchActivity, DetailActivity::class.java)
                intent.putParcelableArrayListExtra("EDITION_POST_LIST", filteredList)
                intent.putExtra("CLICK_POSITION", position)
                startActivity(intent)
            }
        })
        recycle_view_search.adapter = searchAdapter
    }

    private fun searchAPIAPI(searchText: String) {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionSearchResult(searchText, AppPreferences.getUserId(), editionId)
            viewModel.getSearchResult(requestBean).observe(this,{
                val response = it
                try {
                    progress_bar.visibility = View.GONE
                    if (response.errorCode == 0) {
                        if (response.postList.isNullOrEmpty()) {
                            recycle_view_search.visibility = View.GONE
                            textViewNoData.visibility = View.VISIBLE
                            textViewNoData.text = response.message
                        } else {
                            textViewNoData.visibility = View.GONE
                            recycle_view_search.visibility = View.VISIBLE
                            searchResponse = response
                            searchAdapter!!.setData(response.postList)
                        }
                    } else {
                        recycle_view_search.visibility = View.GONE
                        textViewNoData.visibility = View.VISIBLE
                        textViewNoData.text = response.message
                    }
                } catch (e: Exception) {
                    Utils.showToast(resources.getString(R.string.no_network))
                    recycle_view_search.visibility = View.GONE
                    progress_bar.visibility = View.GONE
                    textViewNoData.visibility = View.VISIBLE
                    textViewNoData.text = response.message
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}