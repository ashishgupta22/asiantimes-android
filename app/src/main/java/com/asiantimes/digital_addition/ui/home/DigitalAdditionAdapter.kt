package com.asiantimes.digital_addition.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.databinding.AdapterDigitalAdditionBinding
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.model.EditionListModel
import com.asiantimes.interfaces.OnClickPositionListener


class DigitalAdditionAdapter(private val clickListener: OnClickPositionListener, private val dataList: ArrayList<EditionListModel.DigitalEditionList>) : RecyclerView.Adapter<DigitalAdditionAdapter.ViewHolder>() {
    private var localDB: AppDatabase? = null
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_digital_addition, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        localDB = AppDatabase.getDatabase(context!!)
        val editionPostList = localDB!!.editionPostListDao().getEditionPostById(dataList[position].productId!!.toInt())
        try {
            if (!editionPostList.postList.isNullOrEmpty()) {
                holder.imageViewDownload.visibility = View.GONE
                holder.textViewRead.visibility = View.VISIBLE
            } else {
                holder.textViewRead.visibility = View.GONE
                holder.imageViewDownload.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
        }
        holder.bind(clickListener, dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(private val bindingView: AdapterDigitalAdditionBinding) : RecyclerView.ViewHolder(bindingView.root) {
        val imageViewDownload: AppCompatImageView = bindingView.imageViewDownload
        val textViewRead: AppCompatTextView = bindingView.textViewRead
        private val textViewSubscribe: AppCompatTextView = bindingView.textViewSubscribe
        fun bind(clickListener: Any, digitalEditionList: EditionListModel.DigitalEditionList) {
            if (AppPreferences.getUserDigitalSubscription() == "t") {
                textViewSubscribe.visibility = View.GONE
            } else {
                textViewSubscribe.visibility = View.VISIBLE
                if (digitalEditionList.subscribed!! == "t") {
                    textViewSubscribe.visibility = View.GONE
                } else {
                    textViewSubscribe.visibility = View.VISIBLE
                }
            }
            bindingView.setVariable(BR.clickListener, clickListener)
            bindingView.setVariable(BR.digitalEditionList, digitalEditionList)
        }
    }
}