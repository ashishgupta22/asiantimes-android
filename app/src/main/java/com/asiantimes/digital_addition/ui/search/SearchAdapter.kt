package com.asiantimes.digital_addition.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterSearchBinding
import com.asiantimes.digital_addition.model.SearchModel
import com.asiantimes.interfaces.OnClickPositionListener

class SearchAdapter(private val clickListener: OnClickPositionListener) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private var dataList: List<SearchModel.PostList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_search, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clickListener, dataList!![position], position)
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    class ViewHolder(private val bindingView: AdapterSearchBinding) : RecyclerView.ViewHolder(bindingView.root) {
        fun bind(clickListener: Any, digitalEditionList: SearchModel.PostList, position: Int) {
            bindingView.setVariable(BR.clickListener, clickListener)
            bindingView.setVariable(BR.position, position)
            bindingView.setVariable(BR.searchData, digitalEditionList)
        }
    }

    fun setData(postList: List<SearchModel.PostList>?) {
        dataList = postList
        notifyDataSetChanged()
    }
}