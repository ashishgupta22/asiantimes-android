package com.asiantimes.digital_addition.ui.home

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.asiantimes.R
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.ui.bookmark.BookMarksActivity
import com.asiantimes.digital_addition.ui.setting.SettingActivity
import com.asiantimes.model.ResponseResult
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.databinding.ActivityDigitalHomeBinding
import com.asiantimes.databinding.DigitalDrawerBinding
import com.asiantimes.model.CommonModel
import com.asiantimes.model.RequestBean
import com.asiantimes.services.BackgroundApiCallingServices
import com.asiantimes.services.BackgroundApiResultReceiver
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.profile.ProfileActivity
import com.asiantimes.ui.sign_up.SignUpActivityActivity
import java.util.*

class DigitalHomeActivity : AppCompatActivity(), View.OnClickListener,
    BackgroundApiResultReceiver.Receiver {
    private var bindingView: ActivityDigitalHomeBinding? = null
    private var bindingViewDrawer: DigitalDrawerBinding? = null
    private var localDB: AppDatabase? = null
    private var digitalHomePagerAdapter: DigitalHomePagerAdapter? = null
    var mReceiver: BackgroundApiResultReceiver? = null
    var userData = CommonModel.UserDetail()
    private val viewModel: DigitalHomeViewModel by viewModels()

    private var appDatabase: com.asiantimes.database.AppDatabase? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appDatabase = com.asiantimes.database.AppDatabase.getDatabase(this)
        userData = AppPreferences.getUserDetails()!!
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_digital_home)
        bindingViewDrawer = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.digital_drawer,
            bindingView!!.navigationView,
            true
        )
        localDB = AppDatabase.getDatabase(this)
        mReceiver = BackgroundApiResultReceiver(Handler())
        mReceiver!!.setReceiver(this)
        bindingView?.imageViewDrawer?.setOnClickListener(this)
        bindingViewDrawer!!.layoutBookmark.setOnClickListener(this)
        bindingViewDrawer!!.layoutNewsApp.setOnClickListener(this)
        bindingViewDrawer!!.layoutSetting.setOnClickListener(this)
        bindingViewDrawer!!.buttonLogin.setOnClickListener(this)
        bindingViewDrawer!!.buttonSignUp.setOnClickListener(this)
        bindingViewDrawer!!.layoutLogout.setOnClickListener(this)
        bindingViewDrawer!!.imageViewFacebook.setOnClickListener(this)
        bindingViewDrawer!!.imageViewGoogle.setOnClickListener(this)
        bindingViewDrawer!!.imageViewLinkedIn.setOnClickListener(this)
        bindingViewDrawer!!.imageViewTwitter.setOnClickListener(this)
        bindingViewDrawer!!.imageViewCloseDrawer.setOnClickListener(this)
        bindingViewDrawer!!.layoutImage.setOnClickListener(this)

        /* if (Utils.isOnline(this)) {
             checkSubscriptionPlanAPI()
         } else
             viewPagerSetup()*/
    }


    override fun onClick(view: View?) {
        when (view) {
            bindingView?.imageViewDrawer -> {
                bindingView!!.layoutDrawer.openDrawer(GravityCompat.START)
            }
            bindingViewDrawer!!.imageViewCloseDrawer -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
            }
            bindingViewDrawer!!.layoutImage -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                if (Utils.isOnline(this))
                    startActivity(Intent(this, ProfileActivity::class.java))
                else Utils.showToast(getString(R.string.internetConnectionFailedTop))
            }
            bindingViewDrawer!!.imageViewFacebook -> {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(resources.getString(R.string.facebookURL))
                    )
                )
            }
            bindingViewDrawer!!.imageViewGoogle -> {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(resources.getString(R.string.googleURL))
                    )
                )
            }
            bindingViewDrawer!!.imageViewTwitter -> {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(resources.getString(R.string.twitterURL))
                    )
                )
            }
            bindingViewDrawer!!.imageViewLinkedIn -> {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(resources.getString(R.string.linkedInURL))
                    )
                )
            }
            bindingViewDrawer!!.layoutBookmark -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                if (!AppPreferences.getLoginStatus()) {
                    startActivity(Intent(this, LogInActivity::class.java))
                } else {
                    startActivity(Intent(this, BookMarksActivity::class.java))
                }
            }
            bindingViewDrawer!!.layoutSetting -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                val intent = Intent(this, SettingActivity::class.java)
                startActivity(intent)
            }

            bindingViewDrawer!!.layoutNewsApp -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                finish()
            }
            bindingViewDrawer!!.buttonLogin -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                startActivity(Intent(this, LogInActivity::class.java))
            }
            bindingViewDrawer!!.buttonSignUp -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)
                startActivity(Intent(this, SignUpActivityActivity::class.java))
            }
            bindingViewDrawer!!.layoutLogout -> {
                bindingView!!.layoutDrawer.closeDrawer(GravityCompat.START)

                AlertDialog.Builder(this)
                    .setIcon(ContextCompat.getDrawable(this, R.drawable.app_logo))
                    .setTitle(resources.getString(R.string.app_name))
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Yes") { _, _ ->
                        run {
                            clearPreference()
                            setProfile()
                        }
                    }
                    .setNegativeButton("No", null)
                    .show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setProfile()
        if (Utils.isOnline(this)) {
            checkSubscriptionPlanAPI()
        } else
            viewPagerSetup()
    }

    /*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOGIN_REGISTER && resultCode == RESULT_OK) {
            if (Utils.isOnline(this)) {
                checkSubscriptionPlanAPI()
            }else{
                Utils.showToast(getString(R.string.no_network),this)
            }
        }
    }*/

    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
        when (resultCode) {
            200 -> {
                val responseResult: ResponseResult? = resultData.getParcelable("datalist")
                if (responseResult!!.errorCode == 0) {
                    AppPreferences.setPremiumArr(
                        responseResult.goldMembership,
                        responseResult.digitalPowerList,
                        responseResult.digitalRichList,
                        responseResult.digitalFreeContent,
                        responseResult.premiumPowerList,
                        responseResult.premiumRichList
                    )
                }
            }
        }
    }

    private fun viewPagerSetup() {
        val dataList = localDB!!.editionCategoryDao().getEditionCategoryList()
        if (!dataList.isNullOrEmpty()) {
            bindingView!!.textViewNoData.visibility = View.GONE
            bindingView!!.viewPagerHome.visibility = View.VISIBLE
            for (i in dataList.indices) {
                bindingView!!.tabLayoutHome.addTab(
                    bindingView!!.tabLayoutHome.newTab().setText(dataList[i].categoryName)
                )
            }
            bindingView!!.tabLayoutHome!!.tabGravity = TabLayout.GRAVITY_FILL
            digitalHomePagerAdapter = DigitalHomePagerAdapter(this, dataList)
            bindingView!!.viewPagerHome.adapter = digitalHomePagerAdapter
            TabLayoutMediator(
                bindingView!!.tabLayoutHome,
                bindingView!!.viewPagerHome
            ) { tab, position -> tab.text = dataList[position].categoryName }.attach()
        } else {
            bindingView!!.textViewNoData.visibility = View.VISIBLE
            bindingView!!.viewPagerHome.visibility = View.GONE
        }
    }

    /*
    * show drawer profile
    * */
    private fun setProfile() {
        if (userData == null){
            appDatabase = com.asiantimes.database.AppDatabase.getDatabase(this)
            userData = AppPreferences.getUserDetails()!!
        }
        if (!AppPreferences.getLoginStatus()) {
            bindingViewDrawer!!.layoutProfile.visibility = View.GONE
            bindingViewDrawer!!.layoutLogout.visibility = View.GONE
            bindingViewDrawer!!.layoutLogin.visibility = View.VISIBLE
        } else {
            if (userData.image.isNullOrEmpty()) {
                bindingViewDrawer!!.textViewProfileName.visibility = View.VISIBLE
                bindingViewDrawer!!.imageViewProfile.visibility = View.GONE
                bindingViewDrawer!!.textViewProfileName.text =
                    userData.userName!!.substring(0, 1)
            } else {
                AppConfig.loadImage(
                    bindingViewDrawer?.imageViewProfile,
                   userData.image,
                    R.drawable.dummy
                )
                bindingViewDrawer!!.textViewProfileName.visibility = View.GONE
                bindingViewDrawer!!.imageViewProfile.visibility = View.VISIBLE
            }
            if (userData.image.isNullOrEmpty()) {
                bindingViewDrawer?.imageViewProfile!!.setImageBitmap(
                    Utils.drawTextToBitmap(
                        this,
                        R.drawable.profile_background,
                        userData.userName!!.substring(0, 1)
                            .toUpperCase(Locale.ROOT)
                    )
                )
            } else {
                AppConfig.loadImage(
                    bindingViewDrawer?.imageViewProfile,
                    userData.image,
                    R.drawable.dummy
                )
            }
            bindingViewDrawer?.email = userData.emailAddress
            bindingViewDrawer?.name = userData.userName
            bindingViewDrawer!!.layoutLogin.visibility = View.GONE
            bindingViewDrawer!!.layoutProfile.visibility = View.VISIBLE
            bindingViewDrawer!!.layoutLogout.visibility = View.VISIBLE
        }
    }

    /*
    * Preferences
    * */
    private fun clearPreference() {
        if (Utils.isOnline(this)) {
            AppPreferences.clearData()
            AppPreferences.setLoginStatus(false)
            getPremiumPostArray()
            checkSubscriptionPlanAPI()
        } else Utils.showToast(getString(R.string.internet_error))
    }

    /*
    * API
    * */

    private fun getEditionCategoryAPI() {
        try {
            /*  AppPopup.setProgressbar(this)
              AppPopup.showProgressbar(true)*/
            viewModel.getDigitalEditionCategory().observe(this,{
                val response = it
                try {
                    if (response.errorCode == 0) {
                        bindingView!!.textViewNoData.visibility = View.GONE
                        localDB!!.editionCategoryDao().delete()
                        localDB!!.editionCategoryDao()
                            .insert(response.digitalEditionCategoryList)
                        getEditionListAPI()
                    } else {
                        AppConfig.hideProgress()
                        bindingView!!.textViewNoData.visibility = View.VISIBLE
                        bindingView!!.viewPagerHome.visibility = View.GONE
                        bindingView!!.textViewNoData.text = response.message
                    }
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getEditionListAPI() {
        try {
            /*
            AppPopup.setProgressbar(this)
            AppPopup.showProgressbar(true)*/
            val requestBean = RequestBean()
            requestBean.setDigitalEditionList(AppPreferences.getUserId(), "", "ANDROID")
            viewModel.getATDigitalEditionList(requestBean).observe(this, androidx.lifecycle.Observer {
                val responseData = it
                AppConfig.hideProgress()
                try {
                    if (responseData.errorCode == 0) {
                        localDB!!.editionListDao().delete()
                        localDB!!.editionListDao().insert(responseData.digitalEditionList)
                        viewPagerSetup()
                    } else {
                        bindingView!!.textViewNoData.visibility = View.VISIBLE
                        bindingView!!.viewPagerHome.visibility = View.GONE
                        bindingView!!.textViewNoData.text = responseData.message
                    }
                    AppConfig.hideProgress()
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getPremiumPostArray() {
        val intent = Intent(this, BackgroundApiCallingServices::class.java)
        intent.putExtra("receiver", mReceiver)
        intent.putExtra("commandfrom", "MainActivity")
        startService(intent)
    }

    private fun checkSubscriptionPlanAPI() {
        try {
            AppConfig.showProgress(this)
            val requestBean = RequestBean()
            requestBean.setDigitalEditionBookmarks(AppPreferences.getUserId())
            viewModel.checkSubscriptionPlan(requestBean).observe(this, androidx.lifecycle.Observer {
                val responseResult = it
                if (responseResult.errorCode == 0) {
                    userData.isExpired = responseResult.userDetail!!.isExpired
                    userData.isSubscribe = responseResult!!.userDetail!!.isSubscribed
                    getEditionCategoryAPI()
                } else {
                    bindingView!!.textViewNoData.visibility = View.VISIBLE
                    bindingView!!.viewPagerHome.visibility = View.GONE
                    userData.isExpired = true
                    userData.isSubscribe = false
                    getEditionCategoryAPI()
                }
            })
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

}