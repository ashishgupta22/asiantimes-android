package com.asiantimes.digital_addition.ui.detail

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.databinding.FragmentDetailBinding
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.popup.TextLineSpacingDialog
import com.asiantimes.digital_addition.popup.TextResizeDialog
import com.asiantimes.interfaces.OnClickListener
import com.asiantimes.model.RequestBean
import com.asiantimes.ui.login.LogInActivity
import kotlinx.android.synthetic.main.digital_action_bar.*
import kotlinx.android.synthetic.main.digital_action_bar.view.*

class DetailFragment(
    private val postCatNewsList: EditionPostListModel.PostList.PostCatNewsList,
    private val editionId: String?
) : Fragment(), View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    private var bindingView: FragmentDetailBinding? = null
    private var detailAdapter: DetailAdapter? = null
    private var mImageViewBack: ImageView? = null
    private var mImageViewMenu: ImageView? = null
    private var mImageViewShare: ImageView? = null
    private var popup: PopupMenu? = null
    private var localDB: AppDatabase? = null
    private val viewModel: DetailViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        mImageViewMenu = bindingView!!.root.imageViewMenu
        mImageViewBack = bindingView!!.root.imageViewBack
        mImageViewShare = bindingView!!.root.imageViewShare
        mImageViewMenu!!.visibility = View.VISIBLE
        mImageViewShare!!.visibility = View.VISIBLE
        mImageViewBack!!.setOnClickListener(this)
        mImageViewMenu!!.setOnClickListener(this)
        mImageViewShare!!.setOnClickListener(this)
        bindingView!!.imageViewAdsBanner.setOnClickListener(this)
        localDB = AppDatabase.getDatabase(requireContext())
        setData()

        if (AppPreferences.getDigitalTextLineSpacing() == "0")
            AppPreferences.setDigitalTextLineSpacing("16")
        return bindingView!!.root
    }

    override fun onClick(view: View?) {
        when (view) {
            imageViewBack -> {
                (context as Activity).finish()
            }
            imageViewMenu -> {
                if (view != null) {
                    showCustomPopUpMenu(view)
                }
            }
            mImageViewShare -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, postCatNewsList.shareUrl)
                startActivity(Intent.createChooser(shareIntent, "Share news Url"))
            }
            bindingView!!.imageViewAdsBanner -> {
                if (!postCatNewsList.url.isNullOrEmpty()) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(postCatNewsList.url))
                    startActivity(browserIntent)
                }
            }
        }
    }

    private fun showCustomPopUpMenu(view: View) {
        popup = PopupMenu(requireContext(), view)
        popup!!.setOnMenuItemClickListener(this)
        popup!!.inflate(R.menu.digital_detail_menu)
        if (postCatNewsList.isBookmark.isNullOrEmpty() || postCatNewsList.isBookmark == "f") {
            popup!!.menu.findItem(R.id.action_bookmark).isVisible = true
            popup!!.menu.findItem(R.id.action_remove_bookmark).isVisible = false
        } else {
            popup!!.menu.findItem(R.id.action_bookmark).isVisible = false
            popup!!.menu.findItem(R.id.action_remove_bookmark).isVisible = true
        }
        popup!!.show()
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_bookmark -> {
                if (!AppPreferences.getLoginStatus())
                    startActivity(Intent(context, LogInActivity::class.java))
                else
                    addBookmarkAPI("f")
            }
            R.id.action_remove_bookmark -> {
                if (!AppPreferences.getLoginStatus())
                    startActivity(Intent(context, LogInActivity::class.java))
                else
                    addBookmarkAPI("t")
            }
            R.id.action_text_size -> {
                TextResizeDialog(requireContext(), object : OnClickListener {
                    override fun onClickListener(data: Any?) {
                        detailAdapter!!.setData(postCatNewsList.productList)
                    }
                }).show()
            }
            R.id.action_spacing -> {
                TextLineSpacingDialog(requireContext(), object : OnClickListener {
                    override fun onClickListener(data: Any?) {
                        detailAdapter!!.setData(postCatNewsList.productList)
                    }
                }).show()
            }
        }
        return true
    }

    private fun setData() {
        if (postCatNewsList.type == "ads") {
            bindingView!!.recycleViewEditionDetail.visibility = View.GONE
            bindingView!!.actionBar.visibility = View.GONE
            bindingView!!.imageViewAdsBanner.visibility = View.VISIBLE
            bindingView!!.adsBanner = postCatNewsList.bannerImage

        } else {
            bindingView!!.imageViewAdsBanner.visibility = View.GONE
            bindingView!!.actionBar.visibility = View.VISIBLE
            bindingView!!.recycleViewEditionDetail.visibility = View.VISIBLE
            detailAdapter = DetailAdapter()
            bindingView!!.recycleViewEditionDetail.adapter = detailAdapter
            if (!postCatNewsList.productList.isNullOrEmpty())
                detailAdapter!!.setData(postCatNewsList.productList)
        }
    }


    private fun addBookmarkAPI(isRemoved: String) {
        AppConfig.showProgress(requireContext())
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionAddBookmark(
                postCatNewsList.postId,
                AppPreferences.getUserId(),
                isRemoved,
                editionId
            )
            viewModel.addBookmark(requestBean).observe(this, androidx.lifecycle.Observer {
                val response = it
                AppConfig.hideProgress()
                try {
                    Utils.showToast(response.msg)
                    if (response.errorCode == 0) {
                        if (postCatNewsList.isBookmark.equals("t", true)) {
                            postCatNewsList.isBookmark = "f"
                        } else {
                            postCatNewsList.isBookmark = "t"
                        }
                        getATDigitalEditionPostListAPI()
                    }

                } catch (e: Exception) {

                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getATDigitalEditionPostListAPI() {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionPostList(AppPreferences.getUserId(), editionId)

            viewModel.getATDigitalEditionPostList(requestBean).observe(this,{
                val response = it
                try {
                    if (response.errorCode == 0) {
                        val model = EditionPostListModel()
                        model.postList = response.postList
                        model.editionId = editionId!!.toInt()
                        localDB!!.editionPostListDao().delete()
                        localDB!!.editionPostListDao().insert(model)
                    }
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}