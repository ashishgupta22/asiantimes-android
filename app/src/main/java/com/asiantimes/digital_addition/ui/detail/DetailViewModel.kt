package com.asiantimes.digital_addition.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.model.CommonModel
import com.asiantimes.model.RequestBean

class DetailViewModel : ViewModel() {

    fun addBookmark(requestBean: RequestBean): MutableLiveData<CommonModel> {
        return ApiRepositoryDigital.addBookmark(requestBean);
    }

    fun getATDigitalEditionPostList(requestBean: RequestBean): MutableLiveData<EditionPostListModel> {
        return ApiRepositoryDigital.getATDigitalEditionPostList(requestBean);
    }
}