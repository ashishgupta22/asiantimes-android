package com.asiantimes.digital_addition.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.databinding.AdapterEditionDetailBinding
import com.asiantimes.digital_addition.model.EditionPostListModel

class DetailAdapter : RecyclerView.Adapter<DetailAdapter.ViewHolder>() {
    private var dataList: List<EditionPostListModel.PostList.PostCatNewsList.ProductList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_edition_detail, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val textSize = AppPreferences.getDigitalTextSize()
        val textLineSpacing = AppPreferences.getDigitalTextLineSpacing()
        holder.textViewDetail.textSize = textSize!!.toFloat()
        holder.textViewTitle.textSize = textSize!!.toFloat()
        holder.textViewTitle.setLineSpacing(textLineSpacing!!.toFloat(), 1f)
        holder.textViewDetail.setLineSpacing(textLineSpacing.toFloat(), 1f)
        if (dataList!![position].types.equals("header")) {
            holder.textViewDetail.visibility = View.GONE
            holder.imageViewDetail.visibility = View.GONE
            holder.layoutHeader.visibility = View.VISIBLE
            holder.bindHeader(dataList!![position])
        } else if (dataList!![position].types.equals("text")) {
            holder.layoutHeader.visibility = View.GONE
            holder.imageViewDetail.visibility = View.GONE
            holder.textViewDetail.visibility = View.VISIBLE
            holder.bindDetailText(dataList!![position])
        } else if (dataList!![position].types.equals("image")) {
            holder.layoutHeader.visibility = View.GONE
            holder.textViewDetail.visibility = View.GONE
            holder.imageViewDetail.visibility = View.VISIBLE
            holder.bindDetailText(dataList!![position])
        }
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    class ViewHolder(private val bindingView: AdapterEditionDetailBinding) : RecyclerView.ViewHolder(bindingView.root) {
        val textViewTitle: AppCompatTextView = bindingView.textViewTitle
        val layoutHeader: ConstraintLayout = bindingView.layoutHeader
        val textViewDetail: AppCompatTextView = bindingView.textViewDetail
        val imageViewDetail: ImageView = bindingView.imageViewDetail
        fun bindHeader(productList: EditionPostListModel.PostList.PostCatNewsList.ProductList) {
            bindingView.setVariable(BR.postHeader, productList)
        }

        fun bindDetailText(productList: EditionPostListModel.PostList.PostCatNewsList.ProductList) {
            bindingView.setVariable(BR.postDetailText, productList)
        }
    }

    fun setData(postList: List<EditionPostListModel.PostList.PostCatNewsList.ProductList>?) {
        this.dataList = postList
        notifyDataSetChanged()
    }
}
