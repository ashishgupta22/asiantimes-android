package com.asiantimes.digital_addition.ui.sub_category

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.ui.forgot_password.ForgotPasswordActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.profile.ProfileActivity
import com.asiantimes.ui.sign_up.SignUpActivityActivity
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.digital_action_bar.*

class AccountActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        imageViewBack.setOnClickListener(this)
        textViewLogin.setOnClickListener(this)
        textViewForgotPassword.setOnClickListener(this)
        textViewRegister.setOnClickListener(this)
        textViewProfile.setOnClickListener(this)
        textViewLogout.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        setData()
    }

    override fun onClick(view: View?) {
        when (view) {
            imageViewBack -> {
                finish()
            }
            textViewForgotPassword -> {
                startActivity(Intent(this, ForgotPasswordActivity::class.java))
            }
            textViewRegister -> {
                startActivity(Intent(this, SignUpActivityActivity::class.java))
            }
            textViewLogin -> {
                startActivity(Intent(this, LogInActivity::class.java))
            }
            textViewProfile -> {
                if (Utils.isOnline(this)) {
                    startActivity(Intent(this, ProfileActivity::class.java))
                } else {
                    Utils.showToast(getString(R.string.internetConnectionFailed))
                }
            }
            textViewLogout -> {
                clearPreference()
            }
        }
    }

    private fun setData() {
        if (AppPreferences.getLoginStatus()) {
            layoutWithoutLogin.visibility = View.GONE
            layoutWithLogin.visibility = View.VISIBLE
        } else {
            layoutWithLogin.visibility = View.GONE
            layoutWithoutLogin.visibility = View.VISIBLE
        }
    }

    /*
* Preferences
* */
    private fun clearPreference() {
        if (Utils.isOnline(this)) {
            AppPreferences.cleanGetPremiumArr()
            AppPreferences.setLoginStatus(false)
            AppPreferences.setUserId("0")
            setData()
        } else Utils.showToast(getString(R.string.internetConnectionFailed))
    }

}