package com.asiantimes.digital_addition.ui.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.subscription.SubscriptionHomePageActivity
import com.asiantimes.ui.login.LogInActivity
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val clickPosition = intent.getIntExtra("CLICK_POSITION", 0)
        val isSubscribed = intent.getStringExtra("IS_SUBSCRIBED")
        val callFrom = intent.getStringExtra("CALL_FROM")
        val editionId = intent.getStringExtra("EDITION_ID")
        val postList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> = intent.extras!!.getParcelableArrayList("EDITION_POST_LIST")!!
        val a = EditionPostListModel.PostList.PostCatNewsList()
        postList.add(a)

        Log.d("dsjvkljcvxc", "editionIda : " + editionId)

        val detailPagerAdapter = DetailPagerAdapter(supportFragmentManager, postList, editionId)
        viewPagerDetail.adapter = detailPagerAdapter
        viewPagerDetail.currentItem = clickPosition

        viewPagerDetail.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                if (position == viewPagerDetail.adapter!!.count - 1) {
                    if (callFrom == "edition_cat") {
                        if (isSubscribed != "t") {
                            if (postList[position].isPremium == null || postList[position].isPremium == "t") {
                                if (!AppPreferences.getLoginStatus()) {
                                    val intent = Intent(this@DetailActivity, SubscriptionHomePageActivity::class.java)
                                    intent.putExtra("CALL_FROM", "digital")
                                    startActivity(intent)
                                    finish()
                                } else {
                                    startActivity(Intent(this@DetailActivity, LogInActivity::class.java))
                                }
                            }
                        } else {
                            finish()
                        }
                    } else {
                        finish()
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

}