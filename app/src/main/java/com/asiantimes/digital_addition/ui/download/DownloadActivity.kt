package com.asiantimes.digital_addition.ui.download

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.TransactionDetails
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Constants
import com.asiantimes.base.Utils
import com.asiantimes.databinding.ActivityDownloadBinding
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.ui.sub_category.EditionCategoryActivity
import com.asiantimes.model.CommonModel
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.SubscriptionHomePageActivity
import com.asiantimes.ui.login.LogInActivity
import kotlinx.android.synthetic.main.digital_action_bar.*
import java.util.*

class DownloadActivity : AppCompatActivity(), View.OnClickListener {
    private var bindingView: ActivityDownloadBinding? = null
    private var localDB: AppDatabase? = null
    private var isSubscribed: String? = null
    private var editionId: String? = null
    private var subscriptionId: String? = null
    private val LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB" // PUT YOUR MERCHANT KEY HERE;
    private val MERCHANT_ID = "15638088474552299429"
    var bp: BillingProcessor? = null
    private var readyToPurchase = false
    var userData = CommonModel.UserDetail()
    private var appDatabase: com.asiantimes.database.AppDatabase? = null
    private val viewModel : DownloadViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingView = DataBindingUtil.setContentView(this, R.layout.activity_download)
        appDatabase = com.asiantimes.database.AppDatabase.getDatabase(this)
        userData = AppPreferences.getUserDetails()!!

        editionId = intent.getStringExtra("EDITION_ID")
        subscriptionId = intent.getStringExtra("subscriptionId")
        isSubscribed = intent.getStringExtra("IS_SUBSCRIBED")
        localDB = AppDatabase.getDatabase(this)
        bindingView!!.digitalEditionList = localDB!!.editionListDao().getEditionById(editionId!!.toInt())
        imageViewBack.setOnClickListener(this)
        bindingView?.textViewDownload?.setOnClickListener(this)
        bindingView?.rippleSubscribe?.setOnClickListener(this)
        bindingView?.rippleBuySingle?.setOnClickListener(this)
        billingProcess()


        if (AppPreferences.getIsPremium() == "t") {
            bindingView!!.rippleBuySingle.visibility = View.GONE
            bindingView!!.layoutSubscribe.visibility = View.GONE
        } else {
            bindingView!!.layoutSubscribe.visibility = View.VISIBLE
            if (isSubscribed == "t") {
                bindingView!!.rippleBuySingle.visibility = View.GONE
            } else {
                bindingView!!.rippleBuySingle.visibility = View.VISIBLE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!bp!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (bp != null) bp!!.release()
    }

    override fun onClick(view: View?) {
        when (view) {
            imageViewBack -> {
                finish()
            }
            bindingView?.textViewDownload -> {
                if (Utils.isOnline(this)) {
                    getATDigitalEditionPostListAPI()
                    bindingView?.textViewDownload?.visibility = View.GONE
                    bindingView?.gifDownloading?.visibility = View.VISIBLE
                } else {
                    bindingView?.textViewDownload?.visibility = View.VISIBLE
                    bindingView?.gifDownloading?.visibility = View.GONE
                    Utils.showToast(resources.getString(R.string.no_network))
                }
            }
            bindingView?.rippleSubscribe -> {
                if (AppPreferences.getLoginStatus()) {
                    val intent = Intent(this, SubscriptionHomePageActivity::class.java)
                    intent.putExtra("CALL_FROM", "digital")
                    startActivity(intent)
                } else {
                    startActivity(Intent(this, LogInActivity::class.java))
                }
            }
            bindingView?.rippleBuySingle -> {
                if (AppPreferences.getLoginStatus()) {
                    if (Utils.isOnline(this)) {
                        //purchase single product and redirect to digital payment process
                        if (TextUtils.isEmpty(subscriptionId)) {
                            Utils.showToast("Something went wrong !")
                        } else {
                            bp!!.subscribe(this, subscriptionId)
                        }
                    } else {
                        Utils.showToast(getString(R.string.no_network))
                    }
                } else {
                    startActivity(Intent(this, LogInActivity::class.java))
                }
            }
        }
    }

    private fun getATDigitalEditionPostListAPI() {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionPostList(AppPreferences.getUserId(), editionId)
            viewModel.getATDigitalEditionPostList(requestBean).observe(this,{
                val response = it
                try {
                    if (response.errorCode == 0) {
                        val model = EditionPostListModel()
                        model.postList = response.postList
                        model.editionId = editionId!!.toInt()
                        localDB!!.editionPostListDao().insert(model)
                        val intent = Intent(this@DownloadActivity, EditionCategoryActivity::class.java)
                        intent.putExtra("EDITION_ID", editionId)
                        intent.putExtra("IS_SUBSCRIBED", isSubscribed)
                        intent.putExtra("subscriptionId", subscriptionId)
                        startActivity(intent)
                        finish()
                    } else {
                        bindingView?.textViewDownload?.visibility = View.VISIBLE
                        bindingView?.gifDownloading?.visibility = View.GONE
                        Utils.showToast(response.message)
                    }
                } catch (e: Exception) {
                    bindingView?.textViewDownload?.visibility = View.VISIBLE
                    bindingView?.gifDownloading?.visibility = View.GONE
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun billingProcess() {
        if (!BillingProcessor.isIabServiceAvailable(Objects.requireNonNull(this))) {
            Toast.makeText(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16", Toast.LENGTH_LONG).show()
        }
        bp = BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, object : IBillingHandler {
            override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                //purchaseToken;
                if (details != null) {
                    AppPreferences.getUserId()?.let {
                        userData.emailAddress?.let { it1 ->
                            buySingleProductApi(
                                subscriptionId!!,
                                editionId!!,
                                Constants.DEVICE_TYPE, it, it1,
                            )
                        }
                    }
//                    addTransactionAPI(details.purchaseInfo.purchaseData.purchaseToken, details.purchaseInfo.purchaseData.productId, "Android")
                }
            }

            override fun onBillingError(errorCode: Int, error: Throwable?) {
                Toast.makeText(this@DownloadActivity, "Something went wrong !", Toast.LENGTH_SHORT).show()
            }

            override fun onBillingInitialized() {
                readyToPurchase = true
            }

            override fun onPurchaseHistoryRestored() {}
        })
    }

    private fun buySingleProductApi(
            subscriptionId: String,
            productId: String,
            deviceType: String,
            userId: String,
            email: String
    ) {

        AppConfig.showProgress(this)
        val requestBean = RequestBean()
        requestBean.setDigitalEditionBuySingleProduct(subscriptionId, productId, deviceType, userId, email)
        viewModel.buySingleProduct(requestBean).observe(this,{
            getEditionListAPI()
            finish()
        })
//        val resultObservable = ApplicationConfig.apiInterface.buySingleProduct(requestBean)
//
//        resultObservable.subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(object : Observer<BuySingleProductModel> {
//                    override fun onCompleted() {}
//
//                    override fun onError(e: Throwable) {
//                        e.printStackTrace()
//                        AppPopup.showProgressbar(false)
//                        finish()
//                    }
//
//                    override fun onNext(buySingleProductModel: BuySingleProductModel) {
//
//
//                        /*      if (buySingleProductModel.errorCode == 0) {
//                                  if (!TextUtils.isEmpty(buySingleProductModel.orderItem!!.orderID)) {
//                                      digitalEditionDBHelper.updateIsSubscribedColumnInNewsEdition("t", productId.toString())
//                                      Utils.showToast(buySingleProductModel.message!!, this@ProductPurchaseActivity)
//                                      intent.putExtra("productId", productId.toString())
//                                      intent.putExtra("productStatus", true)
//                                      setResult(RESULT_OK, intent)
//                                      finish()
//                                  } else {
//                                      Utils.showToast(buySingleProductModel.message!!, this@ProductPurchaseActivity)
//                                      intent.putExtra("productId", productId.toString())
//                                      intent.putExtra("productStatus", false)
//                                      setResult(RESULT_OK, intent)
//                                      finish()
//                                  }
//                              } else {
//                                  Utils.showToast(buySingleProductModel.message!!, this@ProductPurchaseActivity)
//                                  intent.putExtra("productId", productId.toString())
//                                  intent.putExtra("productStatus", false)
//                                  setResult(RESULT_OK, intent)
//                                  finish()
//                              }*/
//
//                    }
//                })

    }


    private fun getEditionListAPI() {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionList(AppPreferences.getUserId(), AppPreferences.getFcmToken(), "ANDROID")
            AppConfig.showProgress(this)
            viewModel.getATDigitalEditionList(requestBean).observe(this,{
                val  response = it
                try {
                    AppConfig.hideProgress()
                    localDB!!.editionListDao().delete()
                    localDB!!.editionListDao().insert(response.digitalEditionList)
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}