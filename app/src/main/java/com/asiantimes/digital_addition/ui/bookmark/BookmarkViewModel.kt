package com.asiantimes.digital_addition.ui.bookmark

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.digital_addition.model.BookmarksModel
import com.asiantimes.model.RequestBean

class BookmarkViewModel : ViewModel() {

    fun getATDigitalEditionBookmarks(requestBean: RequestBean): MutableLiveData<BookmarksModel> {
        return ApiRepositoryDigital.getATDigitalEditionBookmarks(requestBean);
    }
}