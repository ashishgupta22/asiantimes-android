package com.asiantimes.digital_addition.ui.bookmark

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterBookmarkChildBinding
import com.asiantimes.databinding.AdapterBookmarkParentBinding
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.utility.StickHeaderItemDecoration
import com.asiantimes.interfaces.OnClickListener
import kotlinx.android.synthetic.main.adapter_bookmark_parent.view.*

class DemoAdapter(private val clickListener: OnClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), StickHeaderItemDecoration.StickyHeaderInterface {
    private var dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            ParentViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_bookmark_parent, parent, false))
        } else {
            ChildViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_bookmark_child, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ParentViewHolder) {
            holder.bind(dataList!![position].date)
        } else if (holder is ChildViewHolder) {
            holder.bind(clickListener, dataList!![position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return dataList!![position].isHeader!!
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var itemPos = itemPosition
        var headerPosition = 0
        do {
            if (isHeader(itemPos)) {
                headerPosition = itemPos
                break
            }
            itemPos -= 1
        } while (itemPos >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int {
        return if (dataList!![headerPosition].isHeader == 0) R.layout.adapter_bookmark_parent else {
            R.layout.adapter_bookmark_child
        }
    }

    override fun bindHeaderData(header: View, headerPosition: Int) {
        header.text_view_header.text = dataList!![headerPosition].date
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return dataList!![itemPosition].isHeader == 0
    }

    class ParentViewHolder(private val bindingView: AdapterBookmarkParentBinding) : RecyclerView.ViewHolder(bindingView.root) {
        fun bind(header: String?) {
            bindingView.setVariable(BR.header, header)
        }
    }

    class ChildViewHolder(private val bindingView: AdapterBookmarkChildBinding) : RecyclerView.ViewHolder(bindingView.root) {
        fun bind(clickListener: Any, postCatNewsList: EditionPostListModel.PostList.PostCatNewsList) {
            bindingView.setVariable(BR.postCatNewsList, postCatNewsList)
            bindingView.setVariable(BR.clickListener, clickListener)
        }
    }

    fun setData(dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList>) {
        this.dataList = dataList
    }
}