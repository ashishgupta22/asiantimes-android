package com.asiantimes.digital_addition.ui.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.asiantimes.digital_addition.model.EditionCategoryModel

class DigitalHomePagerAdapter(fragment: FragmentActivity, private val dataList: List<EditionCategoryModel.DigitalEditionCategoryList>?) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = dataList!!.size

    override fun createFragment(position: Int): Fragment {
        return DigitalHomeFragment(dataList!![position].categoryType!!)
    }
}