package com.asiantimes.digital_addition.ui.setting

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.database.AppDatabase
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.digital_action_bar.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        textViewTitle.text = "Settings"

        imageViewBack.setOnClickListener(this)
        textViewFeedback.setOnClickListener(this)
        textViewTermsOfUse.setOnClickListener(this)
        textViewPrivacyPolicy.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            imageViewBack -> {
                finish()
            }
            textViewFeedback -> {
                startActivity(Intent(this, DigitalFeedbackActivity::class.java))
            }
            textViewTermsOfUse -> {
                val intent = Intent(this, PolicyAndTermsActivity::class.java)
                intent.putExtra("TITLE", "Terms of Use")
                intent.putExtra("URL", getUrl("Terms"))
                startActivity(intent)
            }
            textViewPrivacyPolicy -> {
                val intent = Intent(this, PolicyAndTermsActivity::class.java)
                intent.putExtra("TITLE", "Privacy policy")
                intent.putExtra("URL", getUrl("Privacy"))
                startActivity(intent)
            }
        }
    }

    private fun getUrl(typr: String): String {
        var url: String = ""
        val lis = AppDatabase.getDatabase(this).getAllCategoryDao().getAllCategories()
        for (i in lis) {
            if (i.cat!!.contains(typr)) {
                url = i.catUrl!!
                break
            }
        }
        return url
    }
}