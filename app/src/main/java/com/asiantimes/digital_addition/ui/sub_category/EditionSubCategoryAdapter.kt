package com.asiantimes.digital_addition.ui.sub_category

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.asiantimes.BR
import com.asiantimes.R
import com.asiantimes.databinding.AdapterSubCategoryChildBinding
import com.asiantimes.databinding.AdapterSubCategoryParentBinding
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.utility.StickHeaderItemDecoration
import com.asiantimes.interfaces.OnClickListener
import kotlinx.android.synthetic.main.adapter_sub_category_parent.view.*

class EditionSubCategoryAdapter(private val clickListener: OnClickListener) : RecyclerView.Adapter<ViewHolder>(), StickHeaderItemDecoration.StickyHeaderInterface {
    private var dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList>? = ArrayList()
    private var isSubscribed: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == 0) {
            ParentViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_sub_category_parent, parent, false))
        } else {
            ChildViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_sub_category_child, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is ParentViewHolder) {
            holder.bind(dataList!![position].postCatName)
        } else if (holder is ChildViewHolder) {
            if (position == 1) {
                holder.layoutItems.visibility = View.GONE
                holder.layoutTopItem.visibility = View.VISIBLE
            } else {
                if (dataList!![position].type == "ads") {
                    holder.layoutTopItem.visibility = View.GONE
                    holder.layoutItems.visibility = View.GONE
                } else {
                    holder.layoutTopItem.visibility = View.GONE
                    holder.layoutItems.visibility = View.VISIBLE
                }
            }

            if (isSubscribed == "t" || dataList!![position].isPremium == "f") {
                holder.imageViewLock.visibility = View.GONE
            } else {
                holder.imageViewLock.visibility = View.VISIBLE
            }

            holder.bind(clickListener, dataList!![position])
            holder.layoutItems.setOnClickListener {
                clickListener.onClickListener(dataList!![position])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return dataList!![position].isHeader!!
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var itemPos = itemPosition
        var headerPosition = 0
        do {
            if (isHeader(itemPos)) {
                headerPosition = itemPos
                break
            }
            itemPos -= 1
        } while (itemPos >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int {
        return if (dataList!![headerPosition].isHeader == 0) R.layout.adapter_sub_category_parent else {
            R.layout.adapter_sub_category_child
        }
    }

    override fun bindHeaderData(header: View, headerPosition: Int) {
        header.text_view_header.text = dataList!![headerPosition].postCatName
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return dataList!![itemPosition].isHeader == 0
    }

    class ParentViewHolder(private val bindingView: AdapterSubCategoryParentBinding) : ViewHolder(bindingView.root) {
        fun bind(header: String?) {
            bindingView.setVariable(BR.header, header)
        }
    }

    class ChildViewHolder(private val bindingView: AdapterSubCategoryChildBinding) : ViewHolder(bindingView.root) {
        val layoutItems: ConstraintLayout = bindingView.layoutItems
        val layoutTopItem: ConstraintLayout = bindingView.layoutTopItem
        val imageViewLock: ImageView = bindingView.imageViewLock
        fun bind(clickListener: Any, postCatNewsList: EditionPostListModel.PostList.PostCatNewsList) {
            bindingView.setVariable(BR.postCatNewsList, postCatNewsList)
            bindingView.setVariable(BR.clickListener, clickListener)
        }
    }

    fun setData(dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList>, isSubscribed: String?) {
        this.dataList = dataList
        this.isSubscribed = isSubscribed
    }
}