package com.asiantimes.digital_addition.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.digital_addition.model.CheckSubscriptionModel
import com.asiantimes.digital_addition.model.EditionCategoryModel
import com.asiantimes.digital_addition.model.EditionListModel
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.model.RequestBean

class DigitalHomeViewModel : ViewModel() {

    fun getATDigitalEditionList(requestBean: RequestBean): MutableLiveData<EditionListModel> {
        return ApiRepositoryDigital.getATDigitalEditionList(requestBean);
    }

    fun checkSubscriptionPlan(requestBean: RequestBean): MutableLiveData<CheckSubscriptionModel> {
        return ApiRepositoryDigital.checkSubscriptionPlan(requestBean);
    }

    fun getDigitalEditionCategory(): MutableLiveData<EditionCategoryModel> {
        return ApiRepositoryDigital.getDigitalEditionCategory();
    }

    fun getATDigitalEditionPostList(requestBean: RequestBean): MutableLiveData<EditionPostListModel> {
        return ApiRepositoryDigital.getATDigitalEditionPostList(requestBean);
    }

}