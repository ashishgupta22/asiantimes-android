package com.asiantimes.digital_addition.ui.setting

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.digital_addition.ui.download.DownloadViewModel
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.enums.EPickType
import kotlinx.android.synthetic.main.activity_digital_feedback.*
import kotlinx.android.synthetic.main.digital_action_bar.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


class DigitalFeedbackActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var imageUri: Uri
    private var imageString: String? = ""
    private var body: MultipartBody.Part? = null
    private lateinit var requestFile: RequestBody
    var userData: com.asiantimes.model.CommonModel.UserDetail? = null
    val viewModel: DownloadViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_digital_feedback)
        textViewTitle.text = getString(R.string.feedback)
        userData = AppPreferences.getUserDetails()!!
        editTextComment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                for (span in s.getSpans(0, s.length, UnderlineSpan::class.java)) {
                    s.removeSpan(span)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        imageViewBack.setOnClickListener(this)
        imageViewSelectImage.setOnClickListener(this)
        imageViewRemove.setOnClickListener(this)
        buttonSubmit.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when (view) {
            imageViewSelectImage -> {
                PickImageDialog.build(PickSetup())
                    .setOnPickResult { r ->
                        imageString = r.path
                        imageUri = r.uri
                        imageViewRemove.visibility = View.VISIBLE

                        if (r.pickType == EPickType.GALLERY) {
//                                    selectedImage = getRealPathFromURI_API19(UserProfileActivity.this, r.getUri());
                            val fileName = Calendar.getInstance().time.toString()
                            val file: File = persistImage(r.bitmap, fileName)
                            requestFile =
                                file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                            body =
                                MultipartBody.Part.createFormData("image", file.name, requestFile)
                        } else {
                            val file = File(imageString!!)
                            requestFile =
                                file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                            body =
                                MultipartBody.Part.createFormData("image", file.name, requestFile)
                        }
                        imageViewSelectImage.setImageBitmap(r.bitmap)
                    }
                    .setOnPickCancel {

                    }.show(this)
            }
            imageViewRemove -> {
                imageString = ""
                imageViewSelectImage.setBackgroundResource(R.drawable.feedback_image_background)
                imageViewSelectImage.setImageResource(0)
                imageViewRemove.visibility = View.GONE
            }
            imageViewBack -> {
                finish()
            }
            buttonSubmit -> {
                val comment = editTextComment.text.toString().trim()
                when {
                    comment.isEmpty() -> {
                        editTextComment.error = getString(R.string.feedback_error)
                        editTextComment.requestFocus()
                    }
                    Utils.isOnline(this) -> {
                        submitFeedback(comment)
                    }
                    else -> Utils.showToast(getString(R.string.internet_error))
                }
            }
        }
    }

    private fun submitFeedback(comment: String) {
        AppConfig.showProgress(this)
        if (body == null) {
            requestFile = "".toRequestBody("multipart/form-data".toMediaTypeOrNull())
            body = MultipartBody.Part.createFormData("image", "", requestFile)
        }

        val yourReview = comment.toRequestBody("text/plain".toMediaTypeOrNull())
        val yourEmail = userData!!.emailAddress!!.toRequestBody("text/plain".toMediaTypeOrNull())
        viewModel.submitFeedback(yourReview, yourEmail, body!!).observe(this, {
            val response = it
            Utils.showToast(response.msg)
            if (response.errorCode == 0) {
                editTextComment.setText("")
                imageString = ""
                imageViewSelectImage.setBackgroundResource(R.drawable.feedback_image_background)
                imageViewSelectImage.setImageResource(0)
                imageViewRemove.visibility = View.GONE
            }
        })
    }

    private fun persistImage(bitmap: Bitmap, name: String): File {
        val filesDir = filesDir
        val imageFile = File(filesDir, "$name.jpg")
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
            os.flush()
            os.close()
        } catch (e: java.lang.Exception) {
            Log.e("Exception", "Error writing bitmap", e)
        }
        return imageFile
    }
}