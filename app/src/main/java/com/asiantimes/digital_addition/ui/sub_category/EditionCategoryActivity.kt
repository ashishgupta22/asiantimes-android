package com.asiantimes.digital_addition.ui.sub_category

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Constants
import com.asiantimes.base.Utils
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.model.EditionListModel
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.ui.bookmark.BookMarksActivity
import com.asiantimes.digital_addition.ui.detail.DetailActivity
import com.asiantimes.digital_addition.ui.download.DownloadViewModel
import com.asiantimes.digital_addition.ui.search.SearchActivity
import com.asiantimes.digital_addition.ui.setting.SettingActivity
import com.asiantimes.digital_addition.utility.StickHeaderItemDecoration
import com.asiantimes.interfaces.OnClickListener
import com.asiantimes.model.CommonModel
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.SubscriptionHomePageActivity
import com.asiantimes.ui.login.LogInActivity
import kotlinx.android.synthetic.main.activity_edition_sub_category.*
import kotlinx.android.synthetic.main.digital_action_bar.*
import java.util.*
import kotlin.collections.ArrayList

class EditionCategoryActivity : AppCompatActivity(), View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    private var localDB: AppDatabase? = null
    private var edition_Id: String? = null
    private var isSubscribed: String? = null
    private var subscriptionId: String? = null
    private val LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB"

    // PUT YOUR MERCHANT KEY HERE;
    private val MERCHANT_ID = "15638088474552299429"
    var bp: BillingProcessor? = null
    private var readyToPurchase = false
    private var editionData: EditionListModel.DigitalEditionList? = null
    private var editionPostList: List<EditionPostListModel.PostList>? = ArrayList()
    private var userData : CommonModel.UserDetail? = null
    private val viewModel : DownloadViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edition_sub_category)
        userData = AppPreferences.getUserDetails()!!
        imageViewMenu.visibility = View.VISIBLE
        imageViewMenu.setOnClickListener(this)
        imageViewBack.setOnClickListener(this)
        imageViewSearch.setOnClickListener(this)
        textViewSubscribe.setOnClickListener(this)
        textViewBuySingle.setOnClickListener(this)
        billingProcess()
        if (AppPreferences.getUserDigitalSubscription() == "t") {
            textViewBuySingle.visibility = View.GONE
            textViewSubscribe.visibility = View.GONE
        } else {
            textViewSubscribe.visibility = View.VISIBLE
            if (isSubscribed == "t") {
                textViewBuySingle.visibility = View.GONE
            } else {
                textViewBuySingle.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            edition_Id = intent.getStringExtra("EDITION_ID")
            isSubscribed = intent.getStringExtra("IS_SUBSCRIBED")
            subscriptionId = intent.getStringExtra("subscriptionId")

            localDB = AppDatabase.getDatabase(this)
            editionPostList = localDB!!.editionPostListDao().getEditionPostById(edition_Id!!.toInt()).postList
            editionData = localDB!!.editionListDao().getEditionById(edition_Id!!.toInt())
            textViewBuySingle.text = "Buy Single £ " + editionData!!.subscriptionPrice
            setAdapter()
        }catch (e: Exception){
            Log.d("fdhsfjkhd", "Exception " + e)
        }
    }

    override fun onClick(view: View?) {
        if (view != null) {
            when (view.id) {
                R.id.imageViewMenu -> {
                    val popup = PopupMenu(this, view)
                    popup.inflate(R.menu.menu_main)
                    popup.show()
                    popup.setOnMenuItemClickListener(this)
                    val menuHelper = MenuPopupHelper(this, popup.menu as MenuBuilder, view)
                    menuHelper.setForceShowIcon(true)
                    menuHelper.show()
                }
                R.id.imageViewSearch -> {
                    val intent = Intent(this, SearchActivity::class.java)
                    intent.putExtra("EDITION_ID", edition_Id)
                    startActivity(intent)
                }
                R.id.imageViewBack -> {
                    finish()
                }
                R.id.textViewSubscribe -> {
                    if (AppPreferences.getLoginStatus()) {
                        val intent = Intent(this, SubscriptionHomePageActivity::class.java)
                        intent.putExtra("CALL_FROM", "digital")
                        startActivity(intent)
                    } else {
                        startActivity(Intent(this, LogInActivity::class.java))
                    }
                }
                R.id.textViewBuySingle -> {
                    if (AppPreferences.getLoginStatus()) {
                        if (Utils.isOnline(this)) {
                            //purchase single product and redirect to digital payment process
                            if (TextUtils.isEmpty(subscriptionId)) {
                                Utils.showToast("Something went wrong !")
                            } else {
                                bp!!.subscribe(this, subscriptionId)
                            }
                        } else {
                            Utils.showToast(getString(R.string.no_network))
                        }
                    } else {
                        startActivity(Intent(this, LogInActivity::class.java))
                    }
                }
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.imageViewBack ->
                finish()
            R.id.action_bookmark -> {
                if (!AppPreferences.getLoginStatus()) {
                    startActivity(Intent(this, LogInActivity::class.java))
                } else {
                    startActivity(Intent(this, BookMarksActivity::class.java))
                }
            }
            /*  R.id.action_account -> {
                  startActivity(Intent(this, AccountActivity::class.java))
              }*/
            R.id.action_settings -> {
                startActivity(Intent(this, SettingActivity::class.java))
            }
        }
        return true
    }


    private fun setAdapter() {
        val dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> = ArrayList()
        var a = 0
        var loopPos = 5
        for (i in editionPostList!!.indices) {
            val model1 = EditionPostListModel.PostList.PostCatNewsList()
            model1.isHeader = 0
            model1.postCatName = editionPostList!![i].postCatName
            dataList.add(model1)
            for (j in editionPostList!![i].postCatNewsList!!.indices) {
                val model = EditionPostListModel.PostList.PostCatNewsList()
                model.isHeader = 1
                model.postCatID = editionPostList!![i].postCatID
                model.isBookmark = editionPostList!![i].postCatNewsList!![j].isBookmark
                if (editionPostList!![i].postCatNewsList!![j].type == "ads") {
                    loopPos += 1
                }
                model.isPremium = if (a < loopPos) {
                    "f"
                } else {
                    "t"
                }
                a += 1
                model.postId = editionPostList!![i].postCatNewsList!![j].postId
                model.postDate = editionPostList!![i].postCatNewsList!![j].postDate
                model.postTitle = editionPostList!![i].postCatNewsList!![j].postTitle
                model.shareUrl = editionPostList!![i].postCatNewsList!![j].shareUrl
                model.postDescription = editionPostList!![i].postCatNewsList!![j].postDescription
                model.postImage = editionPostList!![i].postCatNewsList!![j].postImage
                model.isPosition = j
                model.productList = editionPostList!![i].postCatNewsList!![j].productList
                model.type = editionPostList!![i].postCatNewsList!![j].type
                model.bannerImage = editionPostList!![i].postCatNewsList!![j].bannerImage
                model.url = editionPostList!![i].postCatNewsList!![j].url
                dataList.add(model)
            }
        }

        val adapter = EditionSubCategoryAdapter(object : OnClickListener {
            override fun onClickListener(data: Any?) {
                val selectedData = data as EditionPostListModel.PostList.PostCatNewsList
                val filteredList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> = ArrayList()
                var adsCount = 0
                if (isSubscribed == "t") {
                    for (i in dataList.indices) {
                        if (dataList[i].isHeader == 1 && selectedData.postCatID == dataList[i].postCatID) {
                            filteredList.add(dataList[i])
                        }
                    }
                    val intent = Intent(this@EditionCategoryActivity, DetailActivity::class.java)
                    intent.putParcelableArrayListExtra("EDITION_POST_LIST", filteredList)
                    intent.putExtra("CALL_FROM", "edition_cat")
                    intent.putExtra("CLICK_POSITION", selectedData.isPosition)
                    intent.putExtra("IS_SUBSCRIBED", isSubscribed)
                    intent.putExtra("EDITION_ID", edition_Id)
                    startActivity(intent)
                } else if (selectedData.isPremium == "t") {
                    if (AppPreferences.getLoginStatus()) {
                        val intent = Intent(this@EditionCategoryActivity, SubscriptionHomePageActivity::class.java)
                        intent.putExtra("CALL_FROM", "digital")
                        startActivity(intent)
                    } else {
                        startActivity(Intent(this@EditionCategoryActivity, LogInActivity::class.java))
                    }
                } else {
                    for (i in dataList.indices) {
                        if (dataList[i].isHeader == 1) {
                            if (dataList[i].type == "ads") {
                                adsCount += 1
                            }
                            if (filteredList.size <= (4 + adsCount))
                                filteredList.add(dataList[i])
                        }
                    }
                    val intent = Intent(this@EditionCategoryActivity, DetailActivity::class.java)
                    intent.putParcelableArrayListExtra("EDITION_POST_LIST", filteredList)
                    intent.putExtra("CALL_FROM", "edition_cat")
                    intent.putExtra("CLICK_POSITION", selectedData.isPosition)
                    intent.putExtra("IS_SUBSCRIBED", isSubscribed)
                    intent.putExtra("EDITION_ID", edition_Id)
                    startActivity(intent)
                }
            }
        })

        recycleViewSubCategory.adapter = adapter
        adapter.setData(dataList, isSubscribed)
        recycleViewSubCategory.addItemDecoration(StickHeaderItemDecoration(adapter))
    }


    private fun billingProcess() {
        if (!BillingProcessor.isIabServiceAvailable(Objects.requireNonNull(this))) {
            Toast.makeText(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16", Toast.LENGTH_LONG).show()
        }
        bp = BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, object : BillingProcessor.IBillingHandler {
            override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                //purchaseToken;
                if (details != null) {
                    userData!!.emailAddress?.let {
                        AppPreferences.getUserId()?.let { it1 ->
                            buySingleProductApi(
                                subscriptionId!!,
                                edition_Id!!,
                                Constants.DEVICE_TYPE, it1, it
                            )
                        }
                    }
                }
            }

            override fun onBillingError(errorCode: Int, error: Throwable?) {
                Toast.makeText(this@EditionCategoryActivity, "Something went wrong !", Toast.LENGTH_SHORT).show()
            }

            override fun onBillingInitialized() {
                readyToPurchase = true
                subscriptionId = intent.getStringExtra("subscriptionId").toString()
                edition_Id = intent.getStringExtra("EDITION_ID")
            }

            override fun onPurchaseHistoryRestored() {}
        })
    }


    private fun buySingleProductApi(
            subscriptionId: String,
            productId: String,
            deviceType: String,
            userId: String,
            email: String
    ) {


        AppConfig.showProgress(this)
        val requestBean = RequestBean()
        requestBean.setDigitalEditionBuySingleProduct(subscriptionId, productId, deviceType, userId, email)
        viewModel.buySingleProduct(requestBean).observe(this,{
            getEditionListAPI()
            finish()
        })
    }

    private fun getEditionListAPI() {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionList(AppPreferences.getUserId(), AppPreferences.getFcmToken(), "ANDROID")

            viewModel.getATDigitalEditionList(requestBean).observe(this,{
                val  response = it
                try {
                    AppConfig.hideProgress()
                    localDB!!.editionListDao().delete()
                    localDB!!.editionListDao().insert(response.digitalEditionList)
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}