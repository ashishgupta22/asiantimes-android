package com.asiantimes.digital_addition.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.asiantimes.R
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.databinding.FragmentDigitalHomeBinding
import com.asiantimes.digital_addition.database.AppDatabase
import com.asiantimes.digital_addition.model.EditionListModel
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.ui.download.DownloadActivity
import com.asiantimes.digital_addition.ui.sub_category.EditionCategoryActivity
import com.asiantimes.interfaces.OnClickPositionListener
import com.asiantimes.model.RequestBean
import com.asiantimes.subscription.SubscriptionHomePageActivity
import com.asiantimes.ui.login.LogInActivity

class DigitalHomeFragment(private val categoryType: String) : Fragment() {
    private var bindingView: FragmentDigitalHomeBinding? = null
    private var localDB: AppDatabase? = null
    private var digitalAdditionAdapter: DigitalAdditionAdapter? = null
    val viewModel : DigitalHomeViewModel by viewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_digital_home, container, false)
        return bindingView!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindingView!!.copyRight = Utils.COPYRIGHT_TXT
        localDB = AppDatabase.getDatabase(requireContext())
        setAdapter()
    }

    private fun setAdapter() {
        val dataList = localDB!!.editionListDao().getEditionList()
        if (!dataList.isNullOrEmpty()) {
            val filteredList: ArrayList<EditionListModel.DigitalEditionList> = ArrayList()
            if (categoryType == "all-issues") {
                filteredList.addAll(dataList)
                try {
                    val editionPostList = localDB!!.editionPostListDao().getEditionPostById(dataList[0].productId!!.toInt())
                    if (editionPostList.postList.isNullOrEmpty()) {
                        getATDigitalEditionPostListAPI(dataList[0].productId)
                    }
                } catch (e: Exception) {
//                    getATDigitalEditionPostListAPI(dataList[0].productId)
                }

            } else {
                for (i in dataList.indices) {
                    if (dataList[i].categoryType == categoryType) {
                        filteredList.add(dataList[i])
                    }
                }
            }

            digitalAdditionAdapter = DigitalAdditionAdapter(object : OnClickPositionListener {
                override fun onClickPositionListener(data: Any?, position: Int) {
                    val editionData = data as EditionListModel.DigitalEditionList
                    when (position) {
                        0 -> {
                            try {
                                val editionPostList = localDB!!.editionPostListDao().getEditionPostById(editionData.productId!!.toInt())
                                if (!editionPostList.postList.isNullOrEmpty()) {
                                    val intent = Intent(context, EditionCategoryActivity::class.java)
                                    intent.putExtra("EDITION_ID", editionData.productId)
                                    intent.putExtra("IS_SUBSCRIBED", editionData.subscribed)
                                    intent.putExtra("subscriptionId", editionData.subscriptionId)
                                    startActivity(intent)
                                } else {
                                    val intent = Intent(context, DownloadActivity::class.java)
                                    intent.putExtra("EDITION_ID", editionData.productId)
                                    intent.putExtra("IS_SUBSCRIBED", editionData.subscribed)
                                    intent.putExtra("subscriptionId", editionData.subscriptionId)
                                    startActivity(intent)
                                }
                            } catch (e: Exception) {
                            }

                        }
                        1 -> {
                            //subscribe button click
                            if (AppPreferences.getLoginStatus()) {
                                val intent = Intent(context, SubscriptionHomePageActivity::class.java)
                                intent.putExtra("CALL_FROM", "digital")
                                startActivity(intent)
                            } else {
                                startActivity(Intent(context, LogInActivity::class.java))
                            }
                        }
                        2 -> {
                            val intent = Intent(context, EditionCategoryActivity::class.java)
                            intent.putExtra("EDITION_ID", editionData.productId)
                            intent.putExtra("IS_SUBSCRIBED", editionData.subscribed)
                            intent.putExtra("subscriptionId", editionData.subscriptionId)
                            startActivity(intent)
                        }
                        3 -> {
                            val intent = Intent(context, DownloadActivity::class.java)
                            intent.putExtra("EDITION_ID", editionData.productId)
                            intent.putExtra("IS_SUBSCRIBED", editionData.subscribed)
                            intent.putExtra("subscriptionId", editionData.subscriptionId)
                            startActivity(intent)
                        }
                    }
                }
            }, filteredList)
            bindingView!!.recycleViewEdition.adapter = digitalAdditionAdapter
        }
    }

    private fun getATDigitalEditionPostListAPI(editionId: String?) {
        try {
            val requestBean = RequestBean()
            requestBean.setDigitalEditionPostList(AppPreferences.getUserId(), editionId)

            viewModel.getATDigitalEditionPostList(requestBean).observe(viewLifecycleOwner,{
                val response = it
                try {
                    if (response.errorCode == 0) {
                        val model = EditionPostListModel()
                        model.postList = response.postList
                        model.editionId = editionId!!.toInt()
                        localDB!!.editionPostListDao().insert(model)
                        setAdapter()
                    }
                } catch (e: Exception) {
                }
            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}