package com.asiantimes.digital_addition.ui.bookmark

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Utils
import com.asiantimes.digital_addition.model.BookmarksModel
import com.asiantimes.digital_addition.model.EditionPostListModel
import com.asiantimes.digital_addition.ui.detail.DetailActivity
import com.asiantimes.digital_addition.utility.StickHeaderItemDecoration
import com.asiantimes.interfaces.OnClickListener
import com.asiantimes.model.RequestBean
import kotlinx.android.synthetic.main.activity_digital_book_marks.*
import kotlinx.android.synthetic.main.digital_action_bar.*

class BookMarksActivity : AppCompatActivity() {
    val viewModel: BookmarkViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_digital_book_marks)
        textViewTitle.text = getString(R.string.bookmarks)
        imageViewBack.setOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        if (Utils.isOnline(this))
            getATDigitalEditionBookmarksAPI()
        else {
            recycleViewBookMark.visibility = View.GONE
            textViewNoData.visibility = View.VISIBLE
            textViewNoData.text = getString(R.string.internetConnectionFailedTop)
        }
    }

    private fun setAdapter(response: BookmarksModel) {
        val dataList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> = ArrayList()
        try {
            for (i in response.postList!!.indices) {
                val model1 = EditionPostListModel.PostList.PostCatNewsList()
                model1.isHeader = 0
                model1.date = response.postList!![i].date
                dataList.add(model1)
                for (j in response.postList!![i].subPostList!!.indices) {
                    val model = EditionPostListModel.PostList.PostCatNewsList()
                    model.isHeader = 1
                    model.postCatID = response.postList!![i].subPostList!![j].postCatID
                    model.isBookmark = response.postList!![i].subPostList!![j].isBookmark
                    model.isPremium = response.postList!![i].subPostList!![j].isPremium
                    model.postId = response.postList!![i].subPostList!![j].postId
                    model.postDate = response.postList!![i].subPostList!![j].postDate
                    model.postTitle = response.postList!![i].subPostList!![j].postTitle
                    model.editionId = response.postList!![i].subPostList!![j].editionId
                    model.shareUrl = response.postList!![i].subPostList!![j].shareUrl
                    model.postDescription = response.postList!![i].subPostList!![j].postDescription
                    model.postImage = response.postList!![i].subPostList!![j].postImage
                    model.isPosition = j
                    model.productList = response.postList!![i].subPostList!![j].productList
                    dataList.add(model)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        val adapter = DemoAdapter(object : OnClickListener {
            override fun onClickListener(data: Any?) {
                val selectedData = data as EditionPostListModel.PostList.PostCatNewsList
                val filteredList: ArrayList<EditionPostListModel.PostList.PostCatNewsList> =
                    ArrayList()
                for (i in dataList.indices) {
                    if (dataList[i].postCatID == selectedData.postCatID && dataList[i].isHeader == 1) {
                        filteredList.add(dataList[i])
                    }
                }

                val intent = Intent(this@BookMarksActivity, DetailActivity::class.java)
                intent.putParcelableArrayListExtra("EDITION_POST_LIST", filteredList)
                intent.putExtra("CLICK_POSITION", selectedData.isPosition)
                intent.putExtra("CLICK_POSITION", selectedData.isPosition)
                intent.putExtra("EDITION_ID", selectedData.editionId)
                intent.putExtra("CALL_FROM", "bookmark")
                startActivity(intent)
            }
        })

        recycleViewBookMark.adapter = adapter
        adapter.setData(dataList)
        recycleViewBookMark.addItemDecoration(StickHeaderItemDecoration(adapter))
    }


    private fun getATDigitalEditionBookmarksAPI() {
        try {
            AppConfig.showProgress(this)
            val requestBean = RequestBean()
            requestBean.setDigitalEditionBookmarks(AppPreferences.getUserId())

            viewModel.getATDigitalEditionBookmarks(requestBean).observe(this, {
                val response = it
                AppConfig.hideProgress()
                try {
                    if (response.errorCode == 0) {
                        if (response.postList.isNullOrEmpty()) {
                            recycleViewBookMark.visibility = View.GONE
                            textViewNoData.visibility = View.VISIBLE
                            textViewNoData.text = response.message
                        } else {
                            textViewNoData.visibility = View.GONE
                            recycleViewBookMark.visibility = View.VISIBLE
                            setAdapter(response)
                        }
                    } else {
                        recycleViewBookMark.visibility = View.GONE
                        textViewNoData.visibility = View.VISIBLE
                        textViewNoData.text = response.message
                    }
                } catch (e: Exception) {
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}