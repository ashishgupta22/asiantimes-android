package com.asiantimes.digital_addition.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.digital_addition.ui.home.DigitalHomeActivity
import com.asiantimes.ui.login.LogInActivity
import com.asiantimes.ui.sign_up.SignUpActivityActivity
import kotlinx.android.synthetic.main.activity_digital_splash.*

class DigitalSplashActivity : AppCompatActivity(), View.OnClickListener {
    private val LOGIN_REGISTER = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_digital_splash)
        textViewLogIn.setOnClickListener(this)
        textViewSignUp.setOnClickListener(this)
        textViewContinue.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            textViewLogIn -> {

                startActivityForResult(Intent(this, LogInActivity::class.java), LOGIN_REGISTER)
            }
            textViewSignUp -> {
                startActivityForResult(Intent(this, SignUpActivityActivity::class.java), LOGIN_REGISTER)
            }
            textViewContinue -> {
                startActivity(Intent(this, DigitalHomeActivity::class.java))
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == LOGIN_REGISTER && resultCode == RESULT_OK) {
                startActivity(Intent(this, DigitalHomeActivity::class.java))
            finish()
        }
    }
}