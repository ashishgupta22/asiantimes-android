package com.asiantimes.digital_addition.ui.setting

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import kotlinx.android.synthetic.main.activity_policy_and_terms.*
import kotlinx.android.synthetic.main.digital_action_bar.*

class PolicyAndTermsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_policy_and_terms)

       AppConfig.showProgress(this)
        val title = intent.getStringExtra("TITLE")
        val url = intent.getStringExtra("URL")
        textViewTitle.text = title
        loadWebView(url!!)

        imageViewBack.setOnClickListener { finish() }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(loadUrl: String) {
        webView.settings.setRenderPriority(WebSettings.RenderPriority.HIGH)
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                AppConfig.hideProgress()
            }

        }
        // Javascript enabled on webview
        webView.settings.javaScriptEnabled = true
        //Load url in web view
        webView.loadUrl(loadUrl)
    }
}