package com.asiantimes.digital_addition.ui.detail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.asiantimes.digital_addition.model.EditionPostListModel
import java.util.*

class DetailPagerAdapter(manager: FragmentManager?, private val postList: ArrayList<EditionPostListModel.PostList.PostCatNewsList>, private val editionId: String?) : FragmentStatePagerAdapter(manager!!) {
    override fun getItem(position: Int): Fragment {
        return DetailFragment(postList[position],editionId)
    }

    override fun getCount(): Int {
        return postList.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

}
