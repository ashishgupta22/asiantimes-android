package com.asiantimes.digital_addition.ui.download

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asiantimes.digital_addition.api.ApiRepositoryDigital
import com.asiantimes.digital_addition.model.*
import com.asiantimes.model.CommonModel
import com.asiantimes.model.RequestBean
import okhttp3.MultipartBody
import okhttp3.RequestBody

class DownloadViewModel : ViewModel() {

    fun getATDigitalEditionPostList(requestBean: RequestBean): MutableLiveData<EditionPostListModel> {
        return ApiRepositoryDigital.getATDigitalEditionPostList(requestBean);
    }

    fun buySingleProduct(requestBean: RequestBean): MutableLiveData<BuySingleProductModel> {
        return ApiRepositoryDigital.buySingleProduct(requestBean);
    }

    fun getATDigitalEditionList(requestBean: RequestBean): MutableLiveData<EditionListModel> {
        return ApiRepositoryDigital.getATDigitalEditionList(requestBean);
    }

    fun getSearchResult(requestBean: RequestBean): MutableLiveData<SearchModel> {
        return ApiRepositoryDigital.getSearchResult(requestBean);
    }

    fun submitFeedback(yourReview: RequestBody, yourEmail: RequestBody, body: MultipartBody.Part): MutableLiveData<CommonModel> {
        return ApiRepositoryDigital.submitFeedback(yourReview,yourEmail,body);
    }
}