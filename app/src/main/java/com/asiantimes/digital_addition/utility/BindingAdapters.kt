@file:Suppress("DEPRECATION")

package com.asiantimes.digital_addition.utility

import android.os.Build
import android.text.Html
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.asiantimes.R
import com.asiantimes.base.AppConfig
import com.nostra13.universalimageloader.core.DisplayImageOptions

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun imageUrl(view: ImageView, imageURL: String?) {
        val options: DisplayImageOptions = DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.dummy)
                .showImageForEmptyUri(R.drawable.dummy)
                .showImageOnFail(R.drawable.dummy)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .build()
        AppConfig.getImageLoader().displayImage(imageURL, view, options)
    }

    @JvmStatic
    @BindingAdapter("htmlText")
    fun htmlText(view: TextView, html: String?) {
        if (!html.isNullOrEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                view.text = Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
//                Removing HTML
//                var htlmString = Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT).toString()
//                Log.d("htmltest ", htlmString)
//                htlmString.replace("\n","")
//                view.text = htlmString;
            } else {
                view.text = Html.fromHtml(html)
            }
        }
    }
}
