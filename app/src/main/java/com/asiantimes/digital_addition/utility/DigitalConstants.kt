package com.asiantimes.digital_addition.utility

object DigitalConstants {
    var PRIVACY_POLICY_URL = "https://www.easterneye.biz/privacy-policy-mobile/"
    var TERMS_OF_USE_URL = "https://www.easterneye.biz/terms-conditions-mobile/"
}