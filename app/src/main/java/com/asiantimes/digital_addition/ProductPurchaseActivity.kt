package com.asiantimes.digital_addition

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.asiantimes.base.AppConfig
import com.asiantimes.base.AppPreferences
import com.asiantimes.base.Constants
import com.asiantimes.digital_addition.ui.download.DownloadViewModel
import com.asiantimes.model.RequestBean


class ProductPurchaseActivity : AppCompatActivity() {


    private lateinit var billingProcessor: BillingProcessor
    private var readyToPurchase = false
    private var subscriptionId: String = ""
    private var productIdd: String? = ""
    private val LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfQAjk5o3QLB74H9M8Ig1i71phLjc1MBP9W/FwxX6c+vzYDK2ZnuYWqQaR+lonj2i7P4uwULc7SKXDHO8hS3x2eJ/VaNiY9pYj+puUtAbpQoh3RLoVCjXmiO6YV9WZs8KyhUMSrS3PudUw1C3WNM8yLG5wkSMis4jdQe0h7lUTEsNEqT1oTlSqPvm04dnzHE6jACVufOYGz+33XZ4c7cKeOEe+J08XaH4AfEAq268LuBWsVDLKt1FZUBMDcGDLVn6GO2PpssIwlOiHfTZIPVOWtz7/MGG13lo28icQ+349tb0CagAgyndI4OSFztwfSKaGPstQzQWNgCzMzYaaiMJQIDAQAB"
    private val MERCHANT_ID = "15638088474552299429"
    var userDataDao: com.asiantimes.model.CommonModel.UserDetail? = null
    private val viewModel : DownloadViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
        //  setContentView(R.layout.at_digital_activity_purchase_product)
        this.setFinishOnTouchOutside(true)
        userDataDao = AppPreferences.getUserDetails() as com.asiantimes.model.CommonModel.UserDetail
        subscriptionId = intent.getStringExtra("subscriptionId").toString()
        productIdd = intent.getStringExtra("productId")

        if (TextUtils.isEmpty(subscriptionId)) {
            Toast.makeText(this@ProductPurchaseActivity, "Something went wrong !", Toast.LENGTH_SHORT).show()
            finish()
        }

        //check if your device is having in-app billing service or not
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Toast.makeText(
                    this,
                    "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16",
                    Toast.LENGTH_LONG
            ).show()
        }

        //in-app billing processor initialization and server api calling when payment success or failed
        billingProcessor = BillingProcessor(
                this, LICENSE_KEY,MERCHANT_ID,
                object : BillingProcessor.IBillingHandler {
                    override fun onProductPurchased(productId: String, details: TransactionDetails?) {

                        AppPreferences.getUserId()?.let {
                            userDataDao!!.emailAddress?.let { it1 ->
                                buySingleProductApi(
                                    subscriptionId, productIdd!!,
                                    Constants.DEVICE_TYPE, it, it1
                                )
                            }
                        }
                    }

                    override fun onBillingError(errorCode: Int, error: Throwable?) {
                        Toast.makeText(this@ProductPurchaseActivity, "Something went wrong !", Toast.LENGTH_SHORT).show()
                        finish()
                    }

                    override fun onBillingInitialized() {
                        readyToPurchase = true
                   /*     subscriptionId = intent.getStringExtra("subscriptionId").toString()
                        productIdd = intent.getStringExtra("productId")*/
                    }

                    override fun onPurchaseHistoryRestored() {

                    }
                })
        Handler().postDelayed({
            billingProcessor.purchase(this,"android_sub_annually_gold")
        }, 1000)
//        progressbar.visibility = View.VISIBLE

    }

    //called when digital is successful and registering purchased subscription plan on server
    private fun buySingleProductApi(
            subscriptionId: String,
            productId: String,
            deviceType: String,
            userId: String,
            email: String
    ) {

        AppConfig.showProgress(this)
        val requestBean = RequestBean()
        requestBean.setDigitalEditionBuySingleProduct(subscriptionId, productId, deviceType, userId, email)
        viewModel.buySingleProduct(requestBean).observe(this,{
            AppConfig.hideProgress()
        })
    }


    //to show transparent bottom dialog type activity
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val view: View = window.decorView
        val lp: WindowManager.LayoutParams = view.layoutParams as WindowManager.LayoutParams
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        lp.gravity = Gravity.BOTTOM
        windowManager.updateViewLayout(view, lp)
    }

    public override fun onDestroy() {
        if (billingProcessor != null)
            billingProcessor.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)
    }

}
