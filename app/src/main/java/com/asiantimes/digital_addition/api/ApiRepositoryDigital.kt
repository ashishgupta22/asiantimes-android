package com.asiantimes.digital_addition.api

import androidx.lifecycle.MutableLiveData
import com.asiantimes.base.AppConfig
import com.asiantimes.digital_addition.model.*
import com.asiantimes.model.*
import com.asiantimes.model.CommonModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

object ApiRepositoryDigital {

    fun getATDigitalEditionList(requestBean: RequestBean): MutableLiveData<EditionListModel> {
        val mutableLiveData = MutableLiveData<EditionListModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getATDigitalEditionList(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = EditionListModel()
                    modal.errorCode = 100
                    modal.message = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = EditionListModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun checkSubscriptionPlan(requestBean: RequestBean): MutableLiveData<CheckSubscriptionModel> {
        val mutableLiveData = MutableLiveData<CheckSubscriptionModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.checkSubscriptionPlan(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CheckSubscriptionModel()
                    modal.errorCode = 100
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CheckSubscriptionModel()
                modal.errorCode = 100
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getDigitalEditionCategory(): MutableLiveData<EditionCategoryModel> {
        val mutableLiveData = MutableLiveData<EditionCategoryModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getDigitalEditionCategory()
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = EditionCategoryModel()
                    modal.errorCode = 100
                    modal.message = response.body()!!.message
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = EditionCategoryModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getATDigitalEditionPostList(requestBean: RequestBean): MutableLiveData<EditionPostListModel> {
        val mutableLiveData = MutableLiveData<EditionPostListModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getATDigitalEditionPostList(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = EditionPostListModel()
                    modal.errorCode = 100
                    modal.message = response.body()!!.message
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = EditionPostListModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getSubScriptionPlanDetails(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getSubScriptionPlanDetails(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getSpecialSubscription(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getSpecialSubscription(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun addTransaction(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.addTransaction(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }

    fun getPremiumPostRequestNew(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getPremiumPostRequestNew(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun getSearchResult(requestBean: RequestBean): MutableLiveData<SearchModel> {
        val mutableLiveData = MutableLiveData<SearchModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getSearchResult(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
//                    val modal = SearchModel(response.message(), 100)
                    val modal = SearchModel()
                    modal.errorCode = 100
                    modal.message = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = SearchModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun submitFeedback(yourReview: RequestBody, yourEmail: RequestBody, body: MultipartBody.Part): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.submitFeedback(
                    yourReview,yourEmail,body
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
//                    val modal = SearchModel(response.message(), 100)
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun getSubScriptionPlan(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getSubScriptionPlan(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
//                    val modal = ResponseResult()
//                    modal.errorCode = 100
//                    modal.msg = response.message()
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }

        return mutableLiveData
    }
    fun setSubmitPayment(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.setSubmitPayment(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }
    fun getShippingPrice(requestBean: RequestBean): MutableLiveData<ResponseResult> {
        val mutableLiveData = MutableLiveData<ResponseResult>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getShippingPrice(
                    requestBean
                )
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = ResponseResult(response.message(), 100)
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = ResponseResult(e.message.toString(), 100)
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }
    fun getATDigitalEditionBookmarks(requestBean: RequestBean): MutableLiveData<BookmarksModel> {
        val mutableLiveData = MutableLiveData<BookmarksModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.getATDigitalEditionBookmarks(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = BookmarksModel()
                    modal.errorCode = 100
                    modal.message = response.body()!!.message
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = BookmarksModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }

    fun addBookmark(requestBean: RequestBean): MutableLiveData<CommonModel> {
        val mutableLiveData = MutableLiveData<CommonModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.addBookmark(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = CommonModel()
                    modal.errorCode = 100
                    modal.msg = response.body()!!.msg
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = CommonModel()
                modal.errorCode = 100
                modal.msg = e.message
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }

    fun buySingleProduct(requestBean: RequestBean): MutableLiveData<BuySingleProductModel> {
        val mutableLiveData = MutableLiveData<BuySingleProductModel>()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = AppConfig.msApiInterface!!.buySingleProduct(requestBean)
                if (response!!.isSuccessful) {
                    mutableLiveData.postValue(response.body())
                } else {
                    val modal = BuySingleProductModel()
                    modal.errorCode = 100
                    modal.message = response.body()!!.message
                    mutableLiveData.postValue(modal)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                val modal = BuySingleProductModel()
                modal.errorCode = 100
                modal.message = e.message
                mutableLiveData.postValue(modal)
            }
        }
        return mutableLiveData
    }
}