package com.asiantimes.digital_addition.model
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EditionListModel {

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("digitalEditionList")
    @Expose
    var digitalEditionList: List<DigitalEditionList>? = null

    @SerializedName("specailvaltimeignore")
    @Expose
    var specailvaltimeignore: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @Entity(tableName = "edition_list_table")
    class DigitalEditionList {
        @PrimaryKey(autoGenerate = true) var id: Int = 0

        @SerializedName("productId")
        @Expose
        var productId: String? = null

        @SerializedName("isDownloaded")
        @Expose
        var isDownloaded: Boolean? = null

        @SerializedName("date")
        @Expose
        var date: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("isSubscribed")
        @Expose
        var subscribed: String? = null

        @SerializedName("subscriptionPrice")
        @Expose
        var subscriptionPrice: String? = null

        @SerializedName("editionDescription")
        @Expose
        var editionDescription: String? = null

        @SerializedName("subscriptionId")
        @Expose
        var subscriptionId: String? = null

        @SerializedName("categoryType")
        @Expose
        var categoryType: String? = null

        @SerializedName("editionName")
        @Expose
        var editionName: String? = null
    }

}