package com.asiantimes.digital_addition.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CommonModel {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("userDetail")
    @Expose
    val userDetail: UserDetail? = null

    class UserDetail {
        @SerializedName("isSubscribed")
        @Expose
        var isSubscribed: String? = null

        @SerializedName("isExpired")
        @Expose
        var isExpired: String? = null
    }
}