package com.asiantimes.digital_addition.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class BookmarksModel {

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("postList")
    @Expose
    var postList: List<PostList>? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    class PostList {
        @SerializedName("date")
        @Expose
        var date: String? = null

        @SerializedName("subPostList")
        @Expose
        var subPostList: List<EditionPostListModel.PostList.PostCatNewsList>? = null

/*
        class SubPostList {
            var isHeader: Int? = null

            @SerializedName("date")
            @Expose
            var date: String? = null

            @SerializedName("isPremium")
            @Expose
            var isPremium: String? = null

            @SerializedName("postCatColor")
            @Expose
            var postCatColor: String? = null

            @SerializedName("postId")
            @Expose
            var postId: Int? = null

            @SerializedName("postTitle")
            @Expose
            var postTitle: String? = null
            @SerializedName("postDescription")
            @Expose
            var postDescription: String? = null

            @SerializedName("postDate")
            @Expose
            var postDate: String? = null

            @SerializedName("postImage")
            @Expose
            var postImage: String? = null

            @SerializedName("isBookmark")
            @Expose
            var isBookmark: String? = null

            @SerializedName("productList")
            @Expose
            var productList: List<ProductList>? = null

            class ProductList {
                @SerializedName("postId")
                @Expose
                var postId: Int? = null

                @SerializedName("types")
                @Expose
                var types: String? = null

                @SerializedName("numberComments")
                @Expose
                var numberComments: String? = null

                @SerializedName("Title")
                @Expose
                var title: String? = null

                @SerializedName("shareUrl")
                @Expose
                var shareUrl: String? = null

                @SerializedName("postDate")
                @Expose
                var postDate: String? = null

                @SerializedName("postImage")
                @Expose
                var postImage: String? = null

                @SerializedName("postCatName")
                @Expose
                var postCatName: String? = null

                @SerializedName("postImageCaption")
                @Expose
                var postImageCaption: String? = null

                @SerializedName("key")
                @Expose
                var key: String? = null
            }
        }
*/
    }
}