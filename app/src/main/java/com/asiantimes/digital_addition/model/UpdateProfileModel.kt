package com.asiantimes.digital_addition.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UpdateProfileModel {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("userDetail")
    @Expose
    var userDetail: UserDetail? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null


    class UserDetail {
        @SerializedName("userId")
        @Expose
        var userId: String? = null

        @SerializedName("userName")
        @Expose
        var userName: String? = null

        @SerializedName("image")
        @Expose
        var image: String? = null

        @SerializedName("mobileNumber")
        @Expose
        var mobileNumber: String? = null
    }
}