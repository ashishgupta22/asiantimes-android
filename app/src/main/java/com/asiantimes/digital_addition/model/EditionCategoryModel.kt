package com.asiantimes.digital_addition.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class EditionCategoryModel {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("digitalEditionCategoryList")
    @Expose
    var digitalEditionCategoryList: List<DigitalEditionCategoryList>? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @Entity(tableName = "edition_category_list_table")
    class DigitalEditionCategoryList {
        @PrimaryKey(autoGenerate = true) var id: Int = 0

        @SerializedName("categoryId")
        @Expose
        var categoryId: String? = null

        @SerializedName("categoryName")
        @Expose
        var categoryName: String? = null

        @SerializedName("categoryType")
        @Expose
        var categoryType: String? = null
    }
}