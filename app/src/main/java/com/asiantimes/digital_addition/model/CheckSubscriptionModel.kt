package com.asiantimes.digital_addition.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class CheckSubscriptionModel {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("userDetail")
    @Expose
    var userDetail: UserDetail? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    class UserDetail {
        @SerializedName("isSubscribed")
        @Expose
        var isSubscribed: Boolean? = null

        @SerializedName("isExpired")
        @Expose
        var isExpired: Boolean? = null
    }
}