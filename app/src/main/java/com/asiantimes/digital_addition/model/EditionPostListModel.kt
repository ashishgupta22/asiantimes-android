package com.asiantimes.digital_addition.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.asiantimes.digital_addition.database.converter.DataConverter
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Entity(tableName = "edition_post_list")
class EditionPostListModel {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("postList")
    @Expose
    var postList: List<PostList>? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("editionId")
    @Expose
    var editionId: Int? = null

    @TypeConverters(DataConverter::class)   // add here
    class PostList {

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("errorCode")
        @Expose
        var errorCode: Int? = null

        @SerializedName("message")
        @Expose
        var message: String? = null

        @SerializedName("postCatID")
        @Expose
        var postCatID: Int? = null

        @SerializedName("postCatName")
        @Expose
        var postCatName: String? = null

        @SerializedName("postCatColor")
        @Expose
        var postCatColor: String? = null

        @SerializedName("postCatNewsList")
        @Expose
        var postCatNewsList: List<PostCatNewsList>? = null


        class PostCatNewsList() : Parcelable {
            @SerializedName("isPremium")
            @Expose
            var isPremium: String? = null
            @SerializedName("isHeader")
            @Expose
            var isHeader: Int? = null

            @SerializedName("isPosition")
            @Expose
            var isPosition: Int? = null


            @SerializedName("date")
            @Expose
            var date: String? = null


            @SerializedName("postCatID")
            @Expose
            var postCatID: Int? = null

            @SerializedName("postCatName")
            @Expose
            var postCatName: String? = null

            @TypeConverters()
            @SerializedName("postDate")
            @Expose
            var postDate: String? = null

            @SerializedName("postCatColor")
            @Expose
            var postCatColor: String? = null

            @SerializedName("postId")
            @Expose
            var postId: String? = null

            @SerializedName("shareUrl")
            @Expose
            var shareUrl: String? = null

            @SerializedName("videoUrl")
            @Expose
            var videoUrl: String? = null

            @SerializedName("postImage")
            @Expose
            var postImage: String? = null

            @SerializedName("editionId")
            @Expose
            var editionId: String? = null

            @SerializedName("postTitle")
            @Expose
            var postTitle: String? = null

            @SerializedName("isBookmark")
            @Expose
            var isBookmark: String? = null

            @SerializedName("postDescription")
            @Expose
            var postDescription: String? = null

            @SerializedName("productList")
            @Expose
            var productList: List<ProductList>? = null

            @SerializedName("type")
            @Expose
            var type: String? = null

            @SerializedName("banner_image")
            @Expose
            var bannerImage: String? = null

            @SerializedName("url")
            @Expose
            var url: String? = null

            constructor(parcel: Parcel) : this() {
                isPremium = parcel.readString()
                isHeader = parcel.readValue(Int::class.java.classLoader) as? Int
                postCatName = parcel.readString()
                postDate = parcel.readString()
                postCatColor = parcel.readString()
                postId = parcel.readString()
                shareUrl = parcel.readString()
                videoUrl = parcel.readString()
                postImage = parcel.readString()
                postTitle = parcel.readString()
                isBookmark = parcel.readString()
                postDescription = parcel.readString()
                productList = parcel.createTypedArrayList(ProductList)
                type = parcel.readString()
                bannerImage = parcel.readString()
                url = parcel.readString()
            }

            class ProductList() : Parcelable {

                @SerializedName("postId")
                @Expose
                var postId: String? = null

                @SerializedName("types")
                @Expose
                var types: String? = null

                @SerializedName("numberComments")
                @Expose
                var numberComments: String? = null

                @SerializedName("Title")
                @Expose
                var title: String? = null

                @SerializedName("shareUrl")
                @Expose
                var shareUrl: String? = null

                @TypeConverters
                @SerializedName("postDate")
                @Expose
                var postDate: String? = null

                @SerializedName("postCatName")
                @Expose
                var postCatName: String? = null

                @SerializedName("postImageCaption")
                @Expose
                var postImageCaption: String? = null

                @SerializedName("postImage")
                @Expose
                var postImage: String? = null

                @SerializedName("postDescription")
                @Expose
                var postDescription: String? = null

                @SerializedName("key")
                @Expose
                var key: String? = null

                constructor(parcel: Parcel) : this() {
                    postId = parcel.readString()
                    types = parcel.readString()
                    numberComments = parcel.readString()
                    title = parcel.readString()
                    shareUrl = parcel.readString()
                    postDate = parcel.readString()
                    postCatName = parcel.readString()
                    postImageCaption = parcel.readString()
                    postImage = parcel.readString()
                    postDescription = parcel.readString()
                    key = parcel.readString()
                }

                override fun writeToParcel(parcel: Parcel, flags: Int) {
                    parcel.writeString(postId)
                    parcel.writeString(types)
                    parcel.writeString(numberComments)
                    parcel.writeString(title)
                    parcel.writeString(shareUrl)
                    parcel.writeString(postDate)
                    parcel.writeString(postCatName)
                    parcel.writeString(postImageCaption)
                    parcel.writeString(postImage)
                    parcel.writeString(postDescription)
                    parcel.writeString(key)
                }

                override fun describeContents(): Int {
                    return 0
                }

                companion object CREATOR : Parcelable.Creator<ProductList> {
                    override fun createFromParcel(parcel: Parcel): ProductList {
                        return ProductList(parcel)
                    }

                    override fun newArray(size: Int): Array<ProductList?> {
                        return arrayOfNulls(size)
                    }
                }
            }

            override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(isPremium)
                parcel.writeValue(isHeader)
                parcel.writeString(postCatName)
                parcel.writeString(postDate)
                parcel.writeString(postCatColor)
                parcel.writeString(postId)
                parcel.writeString(shareUrl)
                parcel.writeString(videoUrl)
                parcel.writeString(postImage)
                parcel.writeString(postTitle)
                parcel.writeString(isBookmark)
                parcel.writeString(postDescription)
                parcel.writeTypedList(productList)
                parcel.writeString(type)
                parcel.writeString(bannerImage)
                parcel.writeString(url)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object CREATOR : Parcelable.Creator<PostCatNewsList> {
                override fun createFromParcel(parcel: Parcel): PostCatNewsList {
                    return PostCatNewsList(parcel)
                }

                override fun newArray(size: Int): Array<PostCatNewsList?> {
                    return arrayOfNulls(size)
                }
            }

        }

    }

}