package com.asiantimes.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.asiantimes.base.AppConfig;
import com.asiantimes.base.AppPreferences;
import com.asiantimes.model.RequestBean;
import com.asiantimes.model.ResponseResult;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class BackgroundApiCallingServices extends IntentService {

    RequestBean requestBean;
    ResultReceiver receiver;
    Observable<ResponseResult> resultObservable;

    public BackgroundApiCallingServices() {
        super("CALLED");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        receiver = intent.getParcelableExtra("receiver");
        String command = intent.getStringExtra("commandfrom");

        if (command != null) {
            switch (command) {
                /*
                 *  Splash Api Calling
                 * */

                case "SplashScreenActivity": {
                  /*  requestBean = new RequestBean();
                    requestBean.getDeviceId(AppPreferences.getInstance().getFcmToken());
                    resultObservable = ApplicationConfig.apiInterface.getCategoryAllList(requestBean);*/
                    break;
                }
                case "MainActivity": {
                    requestBean = new RequestBean();
                    requestBean.getTopStory(AppPreferences.INSTANCE.getUserId());
                    resultObservable = AppConfig.Companion.getMsApiInterface().getPremiumPostRequest(requestBean);
                    break;
                }
            }
        }

        // finally code
        try {
            resultObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<ResponseResult>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(ResponseResult responseResult) {
                            try{
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("datalist", responseResult);
                                receiver.send(200, bundle);
                            }catch (Exception e){}

                        }

                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
